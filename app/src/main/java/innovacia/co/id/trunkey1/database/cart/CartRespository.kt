package innovacia.co.id.trunkey1.database.cart

import android.app.Application
import android.arch.lifecycle.LiveData
import innovacia.co.id.trunkey1.database.cart.CartDao
import innovacia.co.id.trunkey1.database.cart.CartDatabase
import innovacia.co.id.trunkey1.database.cart.CartEntity

class CartRespository constructor(application: Application){

    lateinit var dao         : CartDao
    lateinit var data        : LiveData<List<CartEntity>>

    init {
        val database = CartDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.getAll()
        }
    }

    fun getAll() : LiveData<List<CartEntity>> {
        return data
    }

    fun getByIdProduct(id: Int) : List<CartEntity> {
        var carts :List<CartEntity> = dao.getByIdProduct(id)
        return carts
    }

    fun getByIdTrans(id: Int) : List<CartEntity> {
        var carts :List<CartEntity> = dao.getByIdTrans(id)
        return carts
    }

    fun getByIdMerchant(id: Int) : List<CartEntity> {
        var carts :List<CartEntity> = dao.getByIdMerchant(id)
        return carts
    }


//    //==================================================================== Transaksi
//    fun getId(id: Int) : List<CartEntity> {
//        var carts :List<CartEntity> = dao.getId(id)
//        return carts
//    }
//    fun getJumlah(id: Int) : List<CartEntity> {
//        var carts :List<CartEntity> = dao.getJumlah(id)
//        return carts
//    }

    fun add(entity: CartEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.insert(entity)
            }
        }
        thread.start()
    }

    fun update(entity: CartEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.update(entity)
            }
        }
        thread.start()
    }

    fun deleteEntry(entity: CartEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.delete(entity)
            }
        }
        thread.start()
    }

}
