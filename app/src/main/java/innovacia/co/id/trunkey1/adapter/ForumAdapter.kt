package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.model.ForumModel
import kotlinx.android.synthetic.main.list_forum_post.view.*
import org.json.JSONObject
import android.content.Context
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.R
import android.support.v4.content.ContextCompat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.helper.user_info


class ForumAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var forumList = mutableListOf<ForumModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return forumDataViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_forum_post, parent, false))
    }
    override fun getItemCount(): Int = forumList.size

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as forumDataViewHolder



        viewHolder.bindView(forumList[position])

        // Layout Click

        viewHolder.itemView.setOnClickListener { view ->
                val id = forumList[position].id
                val id_user = forumList[position].id_user

                val i = Intent(view.context, DetailForum::class.java)
                i.putExtra("id", id)
                i.putExtra("id_user",id_user)
                view.context.startActivity(i)

//            viewHolder.itemView.iconComment.setOnClickListener {
//                viewHolder.itemView.iconComment.startAnimation(user_info.animasiButton)
//
//                val id = forumList[position].id
//                val id_user = forumList[position].id_user
//
//                val i = Intent(view.context, DetailForum::class.java)
//                i.putExtra("id", id)
//                i.putExtra("id_user",id_user)
//                view.context.startActivity(i)
//            }
////
//            viewHolder.itemView.iconLike.setOnClickListener {
//                viewHolder.itemView.iconComment.startAnimation(user_info.animasiButton)
//
//                var iconlikeTouch = false
//
//                if (!user_info.loginStatus) {
//                    val i = Intent(view.context, login::class.java)
//                    i.putExtra("back", "profile")
//                    view.context.startActivity(i)
//                }else if(forumList[position].like_status == 1 || iconlikeTouch == true){
//                    val id = forumList[position].id
//
//                    like(id,viewHolder.itemView.context)
//                    val likenow = Integer.valueOf(forumList[position].like)
//                    viewHolder.itemView.like.text = ((likenow - 1).toString())
//                    viewHolder.itemView.iconLike.setTextColor(ContextCompat.getColor(viewHolder.itemView.iconLike.context, R.color.gray))
//
//                    iconlikeTouch = false
//                }
//                else if(forumList[position].like_status == 0 || iconlikeTouch == false){
//                    val id = forumList[position].id
//                    val id_user = forumList[position].id_user
//
//                    like(id,viewHolder.itemView.context)
//                    val likenow = Integer.valueOf(forumList[position].like)
//                    viewHolder.itemView.like.text = ((likenow + 1).toString())
//                    viewHolder.itemView.iconLike.setTextColor(ContextCompat.getColor(viewHolder.itemView.iconLike.context, R.color.blue))
//
//
//                    iconlikeTouch = true
//
//
////                    val i = Intent(view.context, Forum::class.java)
////                    i.putExtra("id", id)
////                    i.putExtra("id_user",id_user)
////                    view.context.startActivity(i)
////                    (view.context as Activity).finish()
//
//
//
//                }
//
//            }

        }


    }

    fun update(modelList:MutableList<ForumModel>){
        forumList = modelList
        val vendorAdapter = ForumAdapter()
        vendorAdapter.notifyDataSetChanged()
    }

    fun setList(listOfVendor: List<ForumModel>) {
        this.forumList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<ForumModel>) {
        this.forumList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class forumDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(model: ForumModel) {

//            when {
//                vendorModel.is_open == "Close" -> itemView.is_open.setBackgroundResource(R.drawable.text_shape_blue)
//                vendorModel.is_open == "Open" -> itemView.is_open.setBackgroundResource(R.drawable.text_shape_orange)
//                vendorModel.is_open == "Unknown" -> itemView.is_open.visibility = View.GONE
//            }
//
//            when {
//                vendorModel.rating == 1 -> itemView.rating.setBackgroundResource(R.drawable.bintang2)
//                vendorModel.rating == 2 -> itemView.rating.setBackgroundResource(R.drawable.bintang2)
//                vendorModel.rating == 3 -> itemView.rating.setBackgroundResource(R.drawable.bintang3)
//                vendorModel.rating == 4 -> itemView.rating.setBackgroundResource(R.drawable.bintang4)
//                vendorModel.rating == 5 -> itemView.rating.setBackgroundResource(R.drawable.bintang5)
//            }

            itemView.nama.text          = model.userName
            itemView.comment.text       = model.comment
            itemView.like.text          = model.like
            itemView.contentText.text   = model.contentDesc

            Log.e("aim","like status forum adapter"+model.like_status)

            if(model.like_status == 1){
                itemView.iconLike.setTextColor(ContextCompat.getColor(itemView.iconLike.context, R.color.blue))
            }else{
                itemView.iconLike.setTextColor(ContextCompat.getColor(itemView.iconLike.context, R.color.gray))
            }

            if (model.contentPhoto == "null") {
                itemView.contentImage.visibility = View.GONE
            } else {
                Glide.with(itemView.context).load(model.contentPhoto)
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(itemView.contentImage)
            }

            Glide.with(itemView.context).load(model.userPhoto)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(itemView.photo)

        }
    }

    fun like(id:String ,context: Context){

        val rq: RequestQueue = Volley.newRequestQueue(context)
        val sr = object : StringRequest(Request.Method.POST, user_info.forum_like+id, Response.Listener { response ->
            //========================================================================================= data from server

//            val i = Intent(mContext, Forum::class.java)
//            i.putExtra("id", id)
//            mContext?.startActivity(i)


        }, Response.ErrorListener { response ->
            //========================================================================================= error handling

            val networkResponse = response.networkResponse
            if (networkResponse == null){
//                Toast.makeText(this, "Tidak terhubung dengan server", Toast.LENGTH_LONG).show()
            } else {
                // error message
                val err = String(networkResponse.data)
                val jsonObj: JSONObject = JSONObject(err)
                val pesan = jsonObj["message"].toString()
                Log.d("aim","$pesan")
            }

        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
//                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        rq.add(sr)
    }
}