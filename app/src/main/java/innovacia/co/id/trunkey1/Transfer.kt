package innovacia.co.id.trunkey1

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.chaos.view.PinView
import innovacia.co.id.trunkey1.database.cart.CartRespository
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_transfer.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class Transfer : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val helper = user_info()
    var saldo = 0.0
    var cashBack = 0.0
    var isiWallet = 0.0

    var txPin = ""
    var txConfirmPin = ""

    private val method = arrayOf("Wallet","Cash Back","Split")
    var methodString = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer)
        header_name.text = "Transfer"


        transfer.text = user_info.activeChildName

        (pin as PinView).setAnimationEnable(true)
        spinner!!.onItemSelectedListener = this
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, method)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = aa

        getProfile()

//        Log.d("aim","pin status ${user_info.pinStatus}")
//        if (!user_info.pinStatus) {
//            createPinDialog()
//        }

        // Pay With Cash Back
//        if (user_info.walletStatus) {
//            IDR.text = helper.bank(saldo).replace("IDR ", "")
//        } else {
//            IDR.text = "Inactive"
//            IDR.setTextColor(Color.parseColor("#FF0000"))
//            spinner.visibility = View.GONE
//            splitWallet_lay.visibility = View.GONE
//            methodString = "cashback"
//        }

//        cashback.text = helper.bank(cashBack).replace("IDR ","")
//
//        // Saldo Tidak Cukup
//        if (isiWallet == 0.0) {
//            pay_lay.visibility = View.GONE
//
//            txnotif.visibility = View.VISIBLE
//            option_lay.visibility = View.VISIBLE
//            statusBayar = false
//        } else {
//            statusBayar = true
//        }

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

//        valueWallet.setCurrency("IDR")
        valueWallet.setDelimiter(false)
        valueWallet.setSpacing(true)
        valueWallet.setDecimals(false)
        valueWallet.setSeparator(".")

        //        valueWallet.setCurrency("IDR")
        valueCashBack.setDelimiter(false)
        valueCashBack.setSpacing(true)
        valueCashBack.setDecimals(false)
        valueCashBack.setSeparator(".")

        pay.setOnClickListener {
            pay.startAnimation(user_info.animasiButton)
            val transferValue = valueWallet.cleanDoubleValue + valueCashBack.cleanDoubleValue
            if (user_info.pinStatus) {
                Log.d("aim","wallet: $isiWallet, transfer : $transferValue")
                if (isiWallet >= transferValue) {
                    transfer()
                } else {
                    Toast.makeText(this,"Insufficient balance.",Toast.LENGTH_LONG).show()
                }
            } else {
                createPinDialog()
            }
        }

        topup.setOnClickListener {
            topup.startAnimation(user_info.animasiButton)
            val i = Intent(this, Profile::class.java)
            startActivity(i)
        }

    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        when {
            method[position] == "Wallet" -> {
                methodString = "wallet"
                txWallet.visibility = View.VISIBLE
                valueWallet.visibility = View.VISIBLE
                txCashBack.visibility = View.GONE
                valueCashBack.visibility = View.GONE
            }
            method[position] == "Cash Back" -> {
                methodString = "cashback"
                txCashBack.visibility = View.VISIBLE
                valueCashBack.visibility = View.VISIBLE
                txWallet.visibility = View.GONE
                valueWallet.visibility = View.GONE
            }
            method[position] == "Split" -> {
                methodString = "split"
                txCashBack.visibility = View.VISIBLE
                valueCashBack.visibility = View.VISIBLE
                txWallet.visibility = View.VISIBLE
                valueWallet.visibility = View.VISIBLE
            }
        }

    }
    override fun onNothingSelected(arg0: AdapterView<*>) {}

    fun getProfile(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.profile, Response.Listener { response ->
            try {
                Log.i("AIM", response.toString())
                dialog.dismiss()

                val jsonObject = JSONObject(response)
                val status = jsonObject.getInt("status")
                if (status == 1) {
                    val jo = jsonObject.getJSONObject("data")
                    // User
                    val user = jo.getJSONObject("user")
                    val needPin = user.getInt("need_set_pin")
                    val photo = user.getString("photo")
                    Log.d("aim","need pin $needPin")
                    if (needPin == 1) { user_info.pinStatus = true }
                    else { createPinDialog() }

                    user_info.gender = user.getString("gender")
                    user_info.userName = user.getString("username")
                    user_info.name = user.getString("nama")
                    user_info.alamat = user.getString("alamat")
                    user_info.email = user.getString("email")
                    user_info.phone = user.getString("no_telp")
                    user_info.cashBack = user.getDouble("points")
                    user_info.photo = photo
                    user_info.referal_code = user.getString("referal_code")

                    // Balance
                    val balance = jo.getJSONObject("balance")
                    if (balance.getInt("status") == 1) {
                        user_info.walletStatus = true
                        saldo = balance.getDouble("balance")
                        cashBack = user.getDouble("points")
                        isiWallet = saldo + cashBack
                        user_info.wallet = saldo
                        user_info.cashBack = cashBack
                        IDR.text = helper.bank(saldo).replace("IDR ", "")
                    } else {
                        user_info.walletStatus = false
                        IDR.text = "Inactive"
                        IDR.setTextColor(Color.parseColor("#FF0000"))
                        spinner.visibility = View.GONE
                        splitWallet_lay.visibility = View.GONE
                        methodString = "cashback"
                    }

                    cashback.text = helper.bank(cashBack).replace("IDR ","")

                }

            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun transfer(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.transfer+user_info.activeChildId, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim","respon transaksi : $response")

            val jsonObject = JSONObject(response)
            val status = jsonObject.getInt("status")
            Log.i("status_child" , status.toString())
            if(status == 1) {
                doneDialog()
                Log.i("AIM", response.toString())
            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            pin.setText("")
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val sendWallet = valueWallet.cleanDoubleValue
                val sendCashBack = valueCashBack.cleanDoubleValue

                val map = HashMap<String, String>()
                map["method"] = methodString
                if (sendWallet > 0.0) { map["wallet_nominal"] = valueWallet.cleanDoubleValue.toString() }
                if (sendCashBack > 0.0) { map["cashback_nominal"] = valueCashBack.cleanDoubleValue.toString() }
                map["pin"] = pin.text.toString()
                Log.d("aim",map.toString())
                Log.d("aim",pin.text.toString())
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun doneDialog() {
        // Dialog full screen
        val dialogs = Dialog(this,android.R.style.Theme_Light_NoTitleBar_Fullscreen)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_transaction_success)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        textNotif.text = resources.getText(R.string.transaction_done)

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val intent = Intent(this, HomeBaru::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun createPinDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_pin)

        val pin = dialogs.findViewById<EditText>(R.id.newPin)
        val confirmPin = dialogs.findViewById<EditText>(R.id.confirmPin)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        pin.setText(txPin)
        confirmPin.setText(txConfirmPin)

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            txPin = pin.text.toString()
            txConfirmPin = confirmPin.text.toString()

            if (txPin == txConfirmPin) {
                createPin(pin.text.toString(), confirmPin.text.toString())
                dialogs.dismiss()
            } else {
                pin.setText("")
                confirmPin.setText("")
                Toast.makeText(this,"Pin mismatch", Toast.LENGTH_LONG).show()
            }
        }

        dialogs.show()
    }

    fun createPin(newPin:String,confirmPin:String) {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.set_pin, Response.Listener { response ->
            try {
                Log.d("aim", response)
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    Toast.makeText(this, "Create pin Success", Toast.LENGTH_LONG).show()
                    user_info.walletStatus = true
                    user_info.pinStatus = true
                }else {
                    Toast.makeText(this, "Create pin fail", Toast.LENGTH_LONG).show()
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            // Resend pin
            createPinDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["pin"] = newPin
                map["pin_confirmation"] = confirmPin
                Log.d("aim","$map")
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }
}
