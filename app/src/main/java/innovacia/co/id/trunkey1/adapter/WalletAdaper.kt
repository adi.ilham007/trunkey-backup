package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.DetailHistory
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.WalletModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_payment.view.*

class WalletAdaper : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private var walletList= mutableListOf<WalletModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return reviewListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_payment, parent, false))
    }
    override fun getItemCount(): Int = walletList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as reviewListViewHolder
        viewHolder.bindView(walletList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val id = walletList[position].id
            val i = Intent(view.context,DetailHistory::class.java)
            i.putExtra("id",id)
            view.context.startActivity(i)
        }

    }
    fun setList(listOfWallet: List<WalletModel>) {
        this.walletList= listOfWallet.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfWallet: List<WalletModel>) {
        this.walletList.addAll(listOfWallet)
        notifyDataSetChanged()
    }

    class reviewListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(walletModel: WalletModel) {
            var separator = ""
            itemView.nama_transaksi.text = walletModel.nama
            itemView.type.text = walletModel.type
            itemView.tanggal_transaksi.text = walletModel.tanggal
            if(walletModel.debet != "0"){
                separator = "+"
            }else{
                separator = "-"
            }

            itemView.status_transaksi.text = separator +" "+walletModel.jumlah

            if(walletModel.cashback != "IDR 0" ){
                itemView.cashback.text = "CashBack "+ walletModel.cashback
//                Log.d("aim",""+walletModel.cashback)
            }else{
                itemView.cashback.text = ""
            }



//            Log.e("aim"," Transaksion ${PactivityModel.nama} ,${PactivityModel.jumlah},${PactivityModel.tanggal}")
        }

    }
}