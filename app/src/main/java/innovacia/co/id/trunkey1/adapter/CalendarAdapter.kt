package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.model.CalendarModel
import android.util.Log
import android.view.MenuItem
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_schedule_by_date.view.*


class CalendarAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mCalendarModel = mutableListOf<CalendarModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CalendarModelViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_schedule_by_date, parent, false))
    }
    override fun getItemCount(): Int = mCalendarModel.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as CalendarModelViewHolder

        viewHolder.bindView(mCalendarModel[position])
    }

    fun setList(listOfGallery: List<CalendarModel>) {
        this.mCalendarModel = listOfGallery.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfGallery: List<CalendarModel>) {
        this.mCalendarModel.addAll(listOfGallery)
        notifyDataSetChanged()
    }
    fun removeData(position: Int) {
        this.mCalendarModel.removeAt(position)
        notifyItemRemoved(position)
    }


    class CalendarModelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(model: CalendarModel) {
            itemView.tx_startTime.text = model.startTime
            itemView.tx_endTime.text = model.endTime
            itemView.tx_name.text = model.subject
            itemView.logo_huruf.text = model.subject[0].toString().capitalize()
        }
    }
}