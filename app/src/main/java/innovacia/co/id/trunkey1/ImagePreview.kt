package innovacia.co.id.trunkey1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import innovacia.co.id.trunkey1.adapter.PreviewGalleryAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.GalleryModel
import kotlinx.android.synthetic.main.activity_image_preview.*

class ImagePreview : AppCompatActivity() {

    private val listGallery = mutableListOf<GalleryModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_preview)

        val position = intent.getIntExtra("position",0)
        val galleryArray = user_info.gallery
        if (galleryArray!!.length() != 0) {
            for (i in 0 until galleryArray.length()) {
                val jo = galleryArray.getJSONObject(i)
                val id = jo.getString("id")
                val img = jo.getString("photo_path")

                val mItem = GalleryModel(id,img)
                listGallery.add(mItem)
            }

            recycler_gallery.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL,false)
            val galleryAdapter = PreviewGalleryAdapter()
            recycler_gallery.adapter = galleryAdapter
            galleryAdapter.setList(listGallery)
            ar_indicator.attachTo(recycler_gallery, true)
            recycler_gallery.scrollToPosition(position)
            ar_indicator.selectedPosition = position
        }
        Log.d("aim","preview : $listGallery")

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }
    }
}
