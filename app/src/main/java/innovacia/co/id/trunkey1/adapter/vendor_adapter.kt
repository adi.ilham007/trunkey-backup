package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.vendor_model
import kotlinx.android.synthetic.main.list_vendor.view.*
import kotlinx.android.synthetic.main.list_vendor.view.file_logo
import kotlinx.android.synthetic.main.list_vendor2.view.*
import kotlinx.android.synthetic.main.list_vendor2.view.tanggal_view

class vendor_adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var vendorList = mutableListOf<vendor_model>()

    companion object {
        var TYPE_OF_VIEW = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        val viewHolder: RecyclerView.ViewHolder = when (TYPE_OF_VIEW) {
            0 -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_vendor, parent, false))
            else -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_vendor2, parent, false))
        }
        return viewHolder
    }

    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as vendorListViewHolder

        viewHolder.bindView(vendorList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val idMix = vendorList[position].id_vendor
            val category = vendorList[position].category
            val tipe = vendorList[position].tipe

            when (tipe) {
                "vendor" -> {
                    val i = Intent(view.context, detail_vendor::class.java)
                    i.putExtra("id", idMix)
                    i.putExtra("category", category)
                    view.context.startActivity(i)
                }
                "course" -> {
                    val i = Intent(view.context, Course::class.java)
                    i.putExtra("slug", idMix)
                    view.context.startActivity(i)
                }
                "event" -> {
                    val i = Intent(view.context, Event::class.java)
                    i.putExtra("slug", idMix)
                    view.context.startActivity(i)
                }
                "product" -> {

                }
            }

        }

    }

    fun update(modelList:MutableList<vendor_model>){
        vendorList = modelList
        val vendorAdapter = vendor_adapter()
        vendorAdapter.notifyDataSetChanged()
    }

    fun setList(listOfVendor: List<vendor_model>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(listOfVendor: List<vendor_model>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: vendor_model) {

            if (TYPE_OF_VIEW == 0 ){
                itemView.vendor_name.text      = vendorModel.nama
                itemView.vendor_addrs.text      = vendorModel.category_name
                itemView.vendor_distance.text  = vendorModel.alamat

                itemView.harga_awal.text = "belum ada"
                itemView.discount.text = "belum ada"
                itemView.harga_akhir.text = "belum ada"
                Glide.with(itemView.context).load(vendorModel.file_logo).into(itemView.file_logo)
            }
            else{
                itemView.id_vendor2.text = vendorModel.id_vendor
                itemView.judul.text = vendorModel.nama
                Glide.with(itemView.context).load(vendorModel.file_logo).into(itemView.file_logo2)

                var tags = ""
                if(vendorModel.tags != ""){
                    tags = " - " +vendorModel.tags
                }
                itemView.tagJudul.text = vendorModel.category_name + tags
                val tanggalMulai = vendorModel.tanggal_mulai
                val tanggalAkhir = vendorModel.tanggal_akhir

                if(tanggalAkhir !="null"  && tanggalMulai != "null"){
                    val bulanAwal = tanggalMulai.subSequence(5,7)
                    val bulanAkhir = tanggalAkhir.subSequence(5,7)
                    var textBulanAwal = ""
                    var textBulanAkhir = ""

                    when (bulanAwal) {
                        "01" -> textBulanAwal= "Jan"
                        "02" -> textBulanAwal= "Feb"
                        "03" -> textBulanAwal= "March"
                        "04" -> textBulanAwal= "Apr"
                        "05" -> textBulanAwal= "May"
                        "06" -> textBulanAwal= "Jun"
                        "07" -> textBulanAwal= "Jul"
                        "08" -> textBulanAwal= "Aug"
                        "09" -> textBulanAwal= "Sep"
                        "10" -> textBulanAwal= "Oct"
                        "11" -> textBulanAwal= "Nov"
                        "12" -> textBulanAwal= "Des"
                    }
                    when (bulanAkhir) {
                        "01" -> textBulanAkhir= "Jan"
                        "02" -> textBulanAkhir= "Feb"
                        "03" -> textBulanAkhir= "March"
                        "04" -> textBulanAkhir= "Apr"
                        "05" -> textBulanAkhir= "May"
                        "06" -> textBulanAkhir= "Jun"
                        "07" -> textBulanAkhir= "Jul"
                        "08" -> textBulanAkhir= "Aug"
                        "09" -> textBulanAkhir= "Sep"
                        "10" -> textBulanAkhir= "Oct"
                        "11" -> textBulanAkhir= "Nov"
                        "12" -> textBulanAkhir= "Des"
                    }

                    val tahun = tanggalMulai.subSequence(0,4)
                    if(textBulanAwal != textBulanAkhir){
                        itemView.tanggal_view.text = tanggalMulai.subSequence(8,10).toString()+ " "+ textBulanAwal +" - " +tanggalAkhir.subSequence(8,10).toString() + " "+textBulanAkhir +" "+ tahun
                    }else{
                        itemView.tanggal_view.text = tanggalMulai.subSequence(8,10).toString()+ " - " +tanggalAkhir.subSequence(8,10).toString() + " "+textBulanAwal +" "+ tahun
                        itemView.jam.text =  tanggalMulai.subSequence(11,16).toString() +" - "+tanggalAkhir.subSequence(11,16).toString()
                    }

                }else{
                    itemView.icn_tanggal.visibility = View.GONE
                    itemView.icn_jam.visibility = View.GONE
                }

//                if (tanggalMulai != "null"){
//                    itemView.tanggal.text = tanggalMulai.subSequence(8,10)
//                }
                itemView.location_view.text = vendorModel.alamat
                val userInfo = user_info()
                val harga = userInfo.bank(vendorModel.harga_normal)
                itemView.price.text = harga
                itemView.regency.text = vendorModel.regency

//            itemView.seet.text = "seet"
            }







        }
    }
}