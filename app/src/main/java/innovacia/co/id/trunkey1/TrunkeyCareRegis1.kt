package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import innovacia.co.id.trunkey1.adapter.TrunkeyCareSubscribeAdapter
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.TrunkeyCareSubscribeModel
import kotlinx.android.synthetic.main.activity_trunkey_care_regis1.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_step_view.*
import org.json.JSONException

class TrunkeyCareRegis1 : AppCompatActivity() {

    val helper = user_info()
    val mVolleyHelper = VolleyHelper()

    private val subscribeData = mutableListOf<TrunkeyCareSubscribeModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trunkey_care_regis1)
        header_name.text = "Activation"
        text_satu.text = "Subscribe"
        text_dua.text = "Confirm"
        text_tiga.text = "Payment"

        back_link.setOnClickListener {
            onBackPressed()
        }

        tx_sub.setOnClickListener {
            val i = Intent(this,TrunkeyCareRegis2::class.java)
            startActivity(i)
        }

        getSubscribe()

    }

    private fun getSubscribe() {
        val errSection = "getSubscribe"
        swipe_container.isRefreshing = true

        val request = object : JsonObjectRequest(Request.Method.GET, user_info.care_subscribe,null, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val dataArray = response.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    val id = dataObj.getString("id")
                    val name = dataObj.getString("nama")
                    val price = dataObj.getDouble("besar")

                    val mItem = TrunkeyCareSubscribeModel(id,name,"",price)
                    subscribeData.add(mItem)
                }

                recycler_subscribe.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val historyAdapter = TrunkeyCareSubscribeAdapter()
                recycler_subscribe.adapter = historyAdapter
                historyAdapter.setList(subscribeData)

            } catch (e: JSONException) {
                Log.d("aim", "$errSection $e")
            }

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }
}
