package innovacia.co.id.trunkey1

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.widget.SimpleAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.TiketAdapter
import innovacia.co.id.trunkey1.database.model.CartModelDB
import innovacia.co.id.trunkey1.database.cart.CartEntity
import innovacia.co.id.trunkey1.database.cart.CartRespository
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.TiketModel
import kotlinx.android.synthetic.main.activity_detail_purchase.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_step_view.*
import org.json.JSONException
import org.json.JSONObject

class DetailPurchase : AppCompatActivity() {
    val space = "|>:M:<|"

    // Kids Data
    val kidsIdArray = ArrayList<String>()
    val kidsNameArray = ArrayList<String>()

    // Total Item dan Harga untuk list pembelian
    val productName = ArrayList<String>()
    val productPrice = ArrayList<Double>()
    val totalGlobal = ArrayList<Double>()

    lateinit var idArray: ArrayList<Int>
    lateinit var seatArray: ArrayList<Int>
    lateinit var hargaArray: ArrayList<*>
    var memberArray = ArrayList<String>()
    var typeArray = ArrayList<String>()

    val helper = user_info()
    lateinit var mCartModelDB: CartModelDB
    lateinit var cartRespository    : CartRespository
    var itemArrayDB = ArrayList<String>()   // itemNama, itemTanggal

//    private var whatBuy = ""
    private var merchantId = 0
    private var merchantSlug = ""
    private var merchantNama = ""
    private var merchantAlamat = ""
    private var merchantLogo = ""
    private var itemTanggal = ""
    private var itemNama = ""
    private var itemDesc = ""
    var totalHarga = 0.0
    var totalCashBack = 0.0

    private val tiketModel = mutableListOf<TiketModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_purchase)

        // Step Transaksi View
        satu_ke_dua.setBackgroundResource(R.color.colorPrimary)
        step_dua.setBackgroundResource(R.drawable.circle_orange)

        mCartModelDB = ViewModelProviders.of(this).get(CartModelDB::class.java)
        cartRespository = CartRespository(application)

        getChild()

        if (intent.getStringExtra("buyer") != null) {
            val whatBuy = intent.getStringExtra("whatBuy")
            merchantSlug = intent.getStringExtra("slug")
            idArray = intent.getIntegerArrayListExtra("idArray")
            seatArray = intent.getIntegerArrayListExtra("seatArray")
            hargaArray = intent.getSerializableExtra("hargaArray") as ArrayList<*>
            memberArray = intent.getStringArrayListExtra("memberArray")
//            totalHarga = intent.getDoubleExtra("totalHarga",0.0)
//            totalCashBack = intent.getDoubleExtra("totalCashBack",0.0)
//            harga.text = helper.bank(totalHarga)
//            cashback.text = helper.bank(totalCashBack)

            if (whatBuy == "ticket") {
                header_name.text = resources.getString(R.string.trans_header_ticket)
                Log.d("aim", "TIKET id: $idArray, seat: $seatArray, member: $memberArray, harga: $hargaArray")
                for (i in 0 until idArray.size) {
                    typeArray.add(whatBuy)
                }
                getTiket()
            } else if (whatBuy == "class") {
                header_name.text = resources.getString(R.string.trans_header_course)
                Log.d("aim", "CLASS id: $idArray, seat: $seatArray, member: $memberArray, harga: $hargaArray")
                for (i in 0 until idArray.size) {
                    typeArray.add(whatBuy)
                }
                getCourse()
            } else if (whatBuy == "product") {
                header_name.text = resources.getString(R.string.trans_header_Product)
            }

        }

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

//        cart.setOnClickListener {
//            cart.startAnimation(user_info.animasiButton)
//            addToCart()
//            Toast.makeText(this,"Item add to cart",Toast.LENGTH_LONG).show()
//        }

        btn_buy.setOnClickListener {
            btn_buy.startAnimation(user_info.animasiButton)

            val i = Intent(this, Select_Purchase::class.java)
            i.putExtra("bayar", "bayar")
            i.putExtra("whatBuy",typeArray)
            i.putExtra("idArray", idArray)
            i.putExtra("seatArray", seatArray)
            i.putExtra("hargaArray", hargaArray)
            i.putExtra("memberArray",memberArray)
            i.putExtra("totalHarga",totalGlobal.sum())
            i.putExtra("totalCashBack",totalCashBack)
            startActivity(i)
        }

    }

    // tambahan
    override fun onResume() {
        super.onResume()
        TiketAdapter.TYPE_OF_VIEW = 2
    }

    fun getTiket(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.event_api + merchantSlug, Response.Listener { response ->
            //========================================================================================= data from server
            dialog.dismiss()
            try {
                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data")
                merchantId = data.getInt("id")
                merchantSlug = data.getString("slug")
                merchantNama = data.getString("nama")
                merchantAlamat = data.getString("place_nama")
                merchantLogo = data.getString("thumbnail_logo")

                if (!data.isNull("tickets")) {
                    val tiketArray = data.getJSONArray("tickets")
                    if (tiketArray.length() != 0) {
                        for (i in 0 until tiketArray.length()) {
                            val jo = tiketArray.getJSONObject(i)
                            val id = jo.getInt("id")
                            itemNama = jo.getString("nama")
                            itemDesc = jo.getString("keterangan")
                            itemDesc = helper.textHtml(itemDesc)

                            var mCashBack = 0.0
                            val maxCashBack = jo.getDouble("max_cashback")
                            val cashBack = jo.getDouble("nominal_cashback")
                            Log.d("aim", "cash back : $cashBack, max : $maxCashBack")
                            if (cashBack < maxCashBack) {
                                mCashBack = cashBack
                            } else if (cashBack >= maxCashBack) {
                                mCashBack = maxCashBack
                            }


                            val kapasitas = jo.getInt("capacity")
                            val partisipan = jo.getInt("participant")
                            val seat = kapasitas - partisipan

                            val lokasi = data.getString("place_nama")
                            itemTanggal = data.getString("tanggal_mulai")
                            val dateTime = itemTanggal.split(" ")

                            // Baypass Database (itemNama,itemTanggal,itemCashBack)
                            if (idArray.contains(id)) {
                                itemArrayDB.add(id.toString())
                                itemArrayDB.add(itemNama)
                                itemArrayDB.add(itemTanggal)
                                itemArrayDB.add(mCashBack.toString())
                            }

                            if (idArray.contains(id)) {
                                val index = idArray.indexOf(id)
                                val seatBuy = seatArray[index]
                                val hargaDouble = hargaArray[index]

//                                val indexMember = memberArray.indexOf(id.toString())
                                var member = memberArray[index].trim()
                                if (kidsIdArray.contains(member)) {
                                    val kidsIndex = kidsIdArray.indexOf(member)
                                   member = kidsNameArray[kidsIndex]
                                }

                                if (seatArray[index] != 0) {
                                    val mItem = TiketModel(id, itemNama, itemDesc,0.0, hargaDouble as Double, 0.0,0.0, member, seatBuy, seat, dateTime[0], dateTime[1], lokasi, merchantNama)
                                    tiketModel.add(mItem)
                                }

                                // Total item dan harga untuk list pembelian
                                productName.add(jo.getString("nama"))
                                productPrice.add(jo.getDouble("harga_diskon"))

                            }
//                            else if (idArray.contains(id)) {
//                                val index = idArray.indexOf(id)
//                                val seatBuy = seatArray[index]
//                                val hargaDouble = hargaArray[index]
//
//                                if (seatArray[index] != 0) {
//                                    val mItem = TiketModel(id, itemNama, itemDesc, 0.0, hargaDouble as Double, 0.0,0.0, "", seatBuy, seat, dateTime[0], dateTime[1], lokasi, merchantNama)
//                                    tiketModel.add(mItem)
//                                }
//                            }
                        }
                        // =========================================== Layout Setting
                        TiketAdapter.TYPE_OF_VIEW = 2 // Ticket Detail

                        // ============ recycler_tiket ============
                        recycler_tiket.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                        val vendorAdapter = TiketAdapter()
                        recycler_tiket.adapter = vendorAdapter
                        vendorAdapter.setList(tiketModel)
                        tiketModel.clear()

                        totalPaymentList()

                    } else {
                        recycler_tiket.visibility = View.GONE
                        Toast.makeText(this, "No Ticket Available", Toast.LENGTH_LONG).show()
                    }
                }

            }catch (e: JSONException) {
                Log.e("aim", "Event err: $e")
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {}
        rq.add(sr)
    }

    fun getCourse(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.detail_course+ merchantSlug, Response.Listener { response ->
            //========================================================================================= data from server
            dialog.dismiss()

            try {
                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data")
                merchantId = data.getInt("id")
                merchantSlug = data.getString("slug")
                merchantNama = data.getString("nama")
                merchantAlamat = "Under Construction"
                merchantLogo = data.getString("thumbnail_logo")

                if (!data.isNull("classes")) {
                    val courseArray = data.getJSONArray("classes")
                    if (courseArray.length() != 0) {
                        for (i in 0 until courseArray.length()) {
                            val jo = courseArray.getJSONObject(i)
                            val id = jo.getInt("id")
                            itemNama = jo.getString("batch")
                            itemDesc = helper.textHtml(jo.getString("keterangan"))

                            val harga = jo.getDouble("harga_diskon")
                            val kapasitas = jo.getInt("capacity")
                            val partisipan = jo.getInt("participant")
                            val seat = kapasitas - partisipan

                            var mCashBack = 0.0
                            val maxCashBack = jo.getDouble("max_cashback")
                            val cashBack = jo.getDouble("nominal_cashback")
                            Log.d("aim","cash back : $cashBack, max : $maxCashBack")
                            if (cashBack < maxCashBack) {
                                mCashBack = cashBack
                            } else if (cashBack >= maxCashBack) {
                                mCashBack = maxCashBack
                            }

                            //                            var ket = jo.getString("keterangan")
                            //                            ket = helper.textHtml(ket)

//                            val lokasi = "Under Construction"
//                            val dateTime = data.getString("tanggal_mulai")

                            val lokasi = "Under Construction"
                            itemTanggal = data.getString("tanggal_mulai").plus(" Under Construction")
                            val dateTime = itemTanggal.split(" ")

                            // Baypass Database (itemNama,itemTanggal,itemCashBack)
                            if (idArray.contains(id)) {
                                itemArrayDB.add(id.toString())
                                itemArrayDB.add(itemNama)
                                itemArrayDB.add(itemTanggal)
                                itemArrayDB.add(mCashBack.toString())
                            }

                            if (idArray.contains(id) && memberArray.contains(id.toString())) {
                                val index = idArray.indexOf(id)
                                val seatBuy = seatArray[index]
                                val hargaDouble = hargaArray[index]

                                val indexMember = memberArray.indexOf(id.toString())
                                val member = memberArray[indexMember + 1]

                                if (seatArray[index] != 0) {
                                    val mItem = TiketModel(id, itemNama, itemDesc, 0.0, hargaDouble as Double, 0.0,0.0, member, seatBuy, seat, dateTime[0], dateTime[1], lokasi, merchantNama)
                                    tiketModel.add(mItem)
                                }
                            } else if (idArray.contains(id)) {
                                val index = idArray.indexOf(id)
                                val seatBuy = seatArray[index]
                                val hargaDouble = hargaArray[index]

                                if (seatArray[index] != 0) {
                                    val mItem = TiketModel(id, itemNama, itemDesc, 0.0, hargaDouble as Double, 0.0,0.0, "", seatBuy, seat, dateTime[0], dateTime[1], lokasi, merchantNama)
                                    tiketModel.add(mItem)
                                }
                            }
                        }
                        // =========================================== Layout Setting
                        TiketAdapter.TYPE_OF_VIEW = 2 // Ticket Detail

                        // ============ recycler_tiket ============
                        recycler_tiket.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                        val vendorAdapter = TiketAdapter()
                        recycler_tiket.adapter = vendorAdapter
                        vendorAdapter.setList(tiketModel)
                        tiketModel.clear()
                    } else {
                        recycler_tiket.visibility = View.GONE
                        Toast.makeText(this, "No Course Available", Toast.LENGTH_LONG).show()
                    }
                }
            }catch (e: JSONException) {
                Log.e("aim", "Event err: $e")
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {}
        rq.add(sr)
    }

    fun addToCart() {
        for (i in 0 until idArray.size) {
            val hargaPerItem = hargaArray[i] as Double
            val qtyPerItem = seatArray[i]
            val itemSubTotal = hargaPerItem * qtyPerItem

            val indexByPass = itemArrayDB.indexOf(idArray[i].toString())
            val indexMember = memberArray.indexOf(idArray[i].toString())

            val thread = object : Thread() {
                override fun run() {
                    val list = cartRespository.getByIdProduct(idArray[i])
                    if(list.isNotEmpty()){
                        list[0].itemId          = idArray[i]
                        list[0].itemNama        = itemArrayDB[indexByPass+1]
                        list[0].itemMember      = memberArray[indexMember+1]
                        list[0].itemDesc        = itemDesc
                        list[0].itemType        = typeArray[0]
                        list[0].itemPrice       = hargaArray[i] as Double
                        list[0].itemCashBack    = itemArrayDB[indexByPass+3].toDouble()
                        list[0].itemDiscount    = 0
                        list[0].itemQty         = seatArray[i]
                        list[0].itemSubTotal    = itemSubTotal
                        cartRespository.update(list[0])
                    } else {
                        val cart = CartEntity(
                                0,
                                merchantId,
                                merchantSlug,
                                merchantNama,
                                merchantAlamat,
                                merchantLogo,
                                itemArrayDB[indexByPass+2],
                                idArray[i],
                                itemArrayDB[indexByPass+1],
                                memberArray[indexMember+1],
                                itemDesc,
                                typeArray[0],
                                hargaArray[i] as Double,
                                itemArrayDB[indexByPass+3].toDouble(),
                                0,
                                seatArray[i],
                                itemSubTotal,
                                false
                        )
                        Log.d("aim", "add database = $cart")
                        mCartModelDB.addData(cart)
                    }
                }
            }
            thread.start()
        }
    }

    fun getChild() {
        if (user_info.restoreKids != null) {
            val dataArray = user_info.restoreKids
            if (dataArray != null) {

                kidsIdArray.clear()
                kidsNameArray.clear()
                memberArray.clear()

                for (i in 0 until dataArray.length()) {
                    val childObject = dataArray.getJSONObject(i)
                    kidsIdArray.add(childObject.getString("id"))
                    kidsNameArray.add(childObject.getString("nama"))
                }
            }
        }
    }

    fun totalPaymentList() {
        val itemDataList = ArrayList<Map<String,String>>()

        for (i in 0 until productName.size)
        {
            val memberString = memberArray[i]
            val jumlahmember = memberString.split(space).size

            totalGlobal.add(productPrice[i] * jumlahmember)

            val listItemMap = HashMap<String, String>()
            listItemMap["name"] = productName[i].capitalize()
            listItemMap["price"] = helper.bank(productPrice[i] * jumlahmember)

            listItemMap["test"] = "test"

            itemDataList.add(listItemMap)
        }
        val simpleAdapter = SimpleAdapter(this, itemDataList, R.layout.simple_two_textview,
                arrayOf("name", "price"), intArrayOf(R.id.simple_text_name, R.id.simple_text_price))

        payment_detail.adapter = simpleAdapter
        Log.d("aim","TOTAL : $totalGlobal")
        total_value.text = helper.bank(totalGlobal.sum())
    }
}
