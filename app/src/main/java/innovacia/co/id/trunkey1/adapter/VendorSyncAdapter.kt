package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.VendorSyncModel
import kotlinx.android.synthetic.main.list_vendor_sync.view.*

class VendorSyncAdapter  : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mVendorSyncModel = mutableListOf<VendorSyncModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorSyncModelViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_vendor_sync, parent, false))
    }
    override fun getItemCount(): Int = mVendorSyncModel.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as VendorSyncModelViewHolder
        viewHolder.bindView(mVendorSyncModel[position])

        viewHolder.itemView.vendorState.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                Log.d("aim","nyalakan")
                val intent = Intent("vendorSync")
                intent.putExtra("childId", mVendorSyncModel[position].childId)
                intent.putExtra("vendorId", mVendorSyncModel[position].vendorId)
                intent.putExtra("vendorState", 1)
                LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
            } else {
                Log.d("aim","matikan")
                val intent = Intent("vendorSync")
                intent.putExtra("childId", mVendorSyncModel[position].childId)
                intent.putExtra("vendorId", mVendorSyncModel[position].vendorId)
                intent.putExtra("vendorState", 0)
                LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
            }
        }
    }

    fun setList(listOfReview: List<VendorSyncModel>) {
        this.mVendorSyncModel = listOfReview.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfReview: List<VendorSyncModel>) {
        this.mVendorSyncModel.addAll(listOfReview)
        notifyDataSetChanged()
    }

    class VendorSyncModelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(mVendorSyncModel: VendorSyncModel) {
            itemView.childName.text = mVendorSyncModel.childName
            itemView.vendorName.text = mVendorSyncModel.vendorName
            itemView.vendorState.isChecked = mVendorSyncModel.vendorState == 1
        }
    }
}
