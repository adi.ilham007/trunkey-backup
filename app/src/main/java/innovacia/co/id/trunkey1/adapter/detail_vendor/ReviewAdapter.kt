package innovacia.co.id.trunkey1.adapter.detail_vendor

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.vendor_adapter
import innovacia.co.id.trunkey1.model.ReviewModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_review.view.*

class ReviewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var reviewList = mutableListOf<ReviewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_review, parent, false))
    }
    override fun getItemCount(): Int = reviewList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as vendorListViewHolder

        viewHolder.bindView(reviewList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

//            val slug = reviewList[position].slug
////                Click listener
//            val i = Intent(view.context, Course::class.java)
//            i.putExtra("slug", slug)
//            view.context.startActivity(i)

        }

    }

    fun update(modelList:MutableList<ReviewModel>){
        reviewList = modelList
        val vendorAdapter = vendor_adapter()
        vendorAdapter.notifyDataSetChanged()
    }

    fun setList(listOfVendor: List<ReviewModel>) {
        this.reviewList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<ReviewModel>) {
        this.reviewList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ReviewModel) {
//            itemView.id.text = vendorModel.id
            itemView.txNama.text            = vendorModel.nama
            itemView.txReview.text          = vendorModel.review
            itemView.txRating.text          = vendorModel.rating
            itemView.txTanggal.text         = vendorModel.tanggal
            if (vendorModel.rating != "") {
                itemView.rating_img2.progress = Math.ceil(vendorModel.rating.toDouble()).toInt()
            } else {
                itemView.txRating.visibility = View.GONE
                itemView.rating_img2.visibility = View.GONE
            }
            Glide.with(itemView.context).load(vendorModel.photo)
                    .apply(RequestOptions.skipMemoryCacheOf(true)) //
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(itemView.photo)
        }
    }
}