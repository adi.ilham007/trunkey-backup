package innovacia.co.id.trunkey1.model

data class recomended_model (
        var id_vendor : String,
        var nama_vendor : String,
        var alamat_vendor : String,
        var img  : String,
        var slug : String,
        var type : String
)