package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.Course
import innovacia.co.id.trunkey1.Event
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.detail_vendor
import innovacia.co.id.trunkey1.model.ListingCustomModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_vendor.view.*

class ListingCustomAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var vendorList = mutableListOf<ListingCustomModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_vendor, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder

        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener { view ->
            holder.itemView.startAnimation(user_info.animasiButton)

            val id_vendor = vendorList[position].id_vendor
            val category = vendorList[position].category

            when (category) {
                "vendor" -> {
                    val i = Intent(view.context, detail_vendor::class.java)
                    i.putExtra("id", id_vendor)
                    i.putExtra("category", category)
                    view.context.startActivity(i)
                }
                "course" -> {
                    val i = Intent(view.context, Course::class.java)
                    i.putExtra("slug", id_vendor)
                    view.context.startActivity(i)
                }
                "event" -> {
                    val i = Intent(view.context, Event::class.java)
                    i.putExtra("slug", id_vendor)
                    view.context.startActivity(i)
                }
            }

        }

    }

    fun update(modelList:MutableList<ListingCustomModel>){
        vendorList = modelList
        val vendorAdapter = ListingCustomAdapter()
        vendorAdapter.notifyDataSetChanged()
    }

    fun setList(listOfVendor: List<ListingCustomModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<ListingCustomModel>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ListingCustomModel) {

//            when {
//                vendorModel.is_open == "Close" -> itemView.is_open.setBackgroundResource(R.drawable.text_shape_blue)
//                vendorModel.is_open == "Open" -> itemView.is_open.setBackgroundResource(R.drawable.text_shape_orange)
//                vendorModel.is_open == "Unknown" -> itemView.is_open.visibility = View.GONE
//            }
//
//            when {
//                vendorModel.rating == 1 -> itemView.rating.setBackgroundResource(R.drawable.bintang2)
//                vendorModel.rating == 2 -> itemView.rating.setBackgroundResource(R.drawable.bintang2)
//                vendorModel.rating == 3 -> itemView.rating.setBackgroundResource(R.drawable.bintang3)
//                vendorModel.rating == 4 -> itemView.rating.setBackgroundResource(R.drawable.bintang4)
//                vendorModel.rating == 5 -> itemView.rating.setBackgroundResource(R.drawable.bintang5)
//            }

            itemView.vendor_name.text      = vendorModel.nama
            itemView.vendor_addrs.text      = vendorModel.category
            itemView.vendor_distance.text  = vendorModel.alamat

//            itemView.harga_awal.text = "belum ada"
//            itemView.discount.text = "belum ada"
//            itemView.harga_akhir.text = "belum ada"
            Glide.with(itemView.context).load(vendorModel.file_logo).into(itemView.file_logo)
        }
    }
}