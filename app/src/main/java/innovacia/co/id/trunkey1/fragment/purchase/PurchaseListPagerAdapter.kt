package innovacia.co.id.trunkey1.fragment.purchase

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class PurchaseListPagerAdapter (fm: FragmentManager): FragmentStatePagerAdapter(fm){

    private val pages = listOf(
            PurchaseListCourse(),
            PurchaseListEvent()
    )

    // menentukan fragment yang akan dibuka pada posisi tertentu
    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    // judul untuk tabs
    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Course"
            1 -> "Event"
            else -> "Product"
        }
    }
}