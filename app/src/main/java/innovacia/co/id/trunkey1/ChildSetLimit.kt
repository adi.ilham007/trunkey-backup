package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.widget.SeekBar
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.adapter.ChildTransHistoriAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildTransHistory_model
import kotlinx.android.synthetic.main.activity_child_set_limit.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject

class ChildSetLimit : AppCompatActivity() {

    private val mChildTransHistory_model = mutableListOf<ChildTransHistory_model>()
    val helper = user_info()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_child_set_limit)
        header_name?.text = resources.getString(R.string.set_limit_header)

        daily_value.setDelimiter(false)
        daily_value.setSpacing(true)
        daily_value.setDecimals(false)
        daily_value.setSeparator(".")
        weekly_value.setDelimiter(false)
        weekly_value.setSpacing(true)
        weekly_value.setDecimals(false)
        weekly_value.setSeparator(".")
        monthly_value.setDelimiter(false)
        monthly_value.setSpacing(true)
        monthly_value.setDecimals(false)
        monthly_value.setSeparator(".")

        generateView()

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        btn_done.setOnClickListener {
            btn_done.startAnimation(user_info.animasiButton)
            if (daily_value.text!!.isNotEmpty() && weekly_value.text!!.isNotEmpty() && monthly_value.text!!.isNotEmpty()) {
                setLimit()
            }
        }

    }

    private fun generateView() {
        if (intent.hasExtra("daily")) {
            val dailyValue = intent.getDoubleExtra("daily",0.0)
            val weeklyValue = intent.getDoubleExtra("weekly",0.0)
            val monthlyValue = intent.getDoubleExtra("monthly",0.0)

            val dailyClean = helper.bank(dailyValue)
                    .replace("IDR ","")
                    .replace(".","")

            val weeklyClean = helper.bank(weeklyValue)
                    .replace("IDR ","")
                    .replace(".","")

            val monthlyClean = helper.bank(monthlyValue)
                    .replace("IDR ","")
                    .replace(".","")

            daily_value.setText(dailyClean)
            weekly_value.setText(weeklyClean)
            monthly_value.setText(monthlyClean)

        }
    }

    private fun setLimit() {
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.setLimit+user_info.activeChildId, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                val jsonObject = JSONObject(response)
                val status = jsonObject.getInt("status")
                Log.i("status_child" , status.toString())
                if(status == 1) {
                    Toast.makeText(this,"Done",Toast.LENGTH_LONG).show()
                    finish()
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container.isRefreshing = false
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["day_limit"] = "${daily_value.cleanDoubleValue}"
                map["week_limit"] = "${weekly_value.cleanDoubleValue}"
                map["month_limit"] = "${monthly_value.cleanDoubleValue}"
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
