package innovacia.co.id.trunkey1.database.schedule

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "schedule_table")
class ScheduleEntity (
        @PrimaryKey(autoGenerate = true)
        var id                  : Int,
        var date                : String,
        var startTime           : String,
        var endTime             : String,
        var subject             : String,
        var location            : String,
        var userId              : Int,
        var childId             : Int,
        var keterangan          : String
)
