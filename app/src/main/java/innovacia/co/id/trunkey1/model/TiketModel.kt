package innovacia.co.id.trunkey1.model

data class TiketModel(
        var id      :Int = 0,
        var nama    :String = "",
        var ket     :String = "",
        var harga1  :Double = 0.0,
        var harga2  :Double = 0.0,
        var cashBack:Double = 0.0,
        var maxCashBack:Double = 0.0,
        var member  :String = "",
        var jumlah  :Int = 0,
        var seat    :Int = 0,
        var date    :String = "",
        var time    :String = "",
        var lokasi  :String = "",
        var vendor  :String = "",
        var tipe    :String = ""
)