package innovacia.co.id.trunkey1

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

class Editprofile : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val mVolleyHelper = VolleyHelper()

    private val gender = arrayOf("Mom", "Dad")
    var genderString = ""
    private val PICK_IMAGE_REQUEST = 1111
    var imageName = ""
    var imageData = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        header_name.text = resources.getString(R.string.edit_profile_header)

        name.setText(user_info.name)
        email.setText(user_info.email)
        phone.setText(user_info.phone)
        Glide.with(this).load(user_info.photo)
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .into(photo)

        back_link.setOnClickListener {
            onBackPressed()
        }

        editPhoto.setOnClickListener {
            editPhoto.startAnimation(user_info.animasiButton)
            val intent = Intent()
                    .setType("image/*")
                    .setAction(Intent.ACTION_GET_CONTENT)

            startActivityForResult(Intent.createChooser(intent, "Select a Photo"), PICK_IMAGE_REQUEST)
        }

        spinner!!.onItemSelectedListener = this
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, gender)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = aa
        if (user_info.gender != "") {
            if(user_info.gender == "female"){
                val spinnerPosition = aa.getPosition("Mom")
                spinner!!.setSelection(spinnerPosition)
            }else if(user_info.gender == "male"){
                val spinnerPosition = aa.getPosition("Dad")
                spinner!!.setSelection(spinnerPosition)
            }

        }

        // Profile Parent Layout Visibility
        save.setOnClickListener {
            save.startAnimation(user_info.animasiButton)
            if (profileCheck()) {
                name_lay.error = ""
                phone_lay.error = ""
                updateProfile()
            }
        }

    }

    override fun onNothingSelected(arg0: AdapterView<*>) {}
    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        when {
            gender[position] == "Mom" -> {
                genderString = "female"
            }
            gender[position] == "Dad" -> {
                genderString = "male"
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            val filePath = data.data
            imageName = getFileName(filePath)
            val fileType = imageName.split(".")
            val lastIndex = fileType.lastIndex
            val extension = fileType[lastIndex].toLowerCase()
            Log.d("aim","nama file : $imageName, extension : $extension")

            if (extension == "jpg" || extension == "jpeg") {
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                    //Bitmap lastBitmap = null;
                    //lastBitmap = bitmap;

                    //encoding image to string
                    imageData = getStringImage(bitmap)
                    sendImage(imageData)

                } catch (e:IOException) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(this,"Please choose picture with .jpg or .jpeg extension",Toast.LENGTH_LONG).show()
            }
        }
    }

    fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    fun getStringImage(bmp: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }

//    @RequiresApi(Build.VERSION_CODES.N)
//    fun DatePicker(view: View) {
//        val c = Calendar.getInstance()
//        val year = c.get(Calendar.YEAR)
//        val month = c.get(Calendar.MONTH)
//        val day = c.get(Calendar.DAY_OF_MONTH)
//
//        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//            profile_birthday_edittext.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")
//        }, year, month, day)
//        dpd.show()
//    }

    fun profileCheck(): Boolean{
        var cek = 0

        if (name.text!!.isEmpty()){
            name_lay.error = "Can't be empty"; cek++
        }
        if(phone.text!!.isEmpty()){
            phone_lay.error = "Can't be empty"; cek++
        }
        if(email.text!!.isEmpty()){
            email_lay.error = "Can't be empty"; cek++
        }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun sendImage(image:String) {
        val errSection = "upload image"
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val body = JSONObject()
        body.put("file","data:image/png;base64,$image")

        val rq = object : JsonObjectRequest(Request.Method.POST,user_info.update_foto,body,Response.Listener { response ->
            dialog.dismiss()
            try {
                val objectData = response.getJSONObject("data")
                val img = objectData.getString("photo")
                Glide.with(this).load(img)
                        .apply(RequestOptions.skipMemoryCacheOf(true)) //
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(photo)
            } catch (e:JSONException) {
                Log.d("aim",e.toString())
            }
        },Response.ErrorListener { response ->
            dialog.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }

    private fun updateProfile() {
        val errSection = "update profile"
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val body = JSONObject()
        body.put("username",user_info.userName)
        body.put("email",email.text.toString())
        body.put("nama",name.text.toString())
        body.put("phone",phone.text.toString().replace("+62","0"))
        body.put("gender",genderString)

        val rq = object : JsonObjectRequest(Request.Method.POST,user_info.update_profile,body,Response.Listener { response ->
            dialog.dismiss()
            try {
                val data = response.getJSONObject("data")
                user_info.userName = data.getString("username")
                user_info.name = data.getString("nama")
                user_info.alamat = data.getString("alamat")
                user_info.email = data.getString("email")
                user_info.phone = data.getString("no_telp")
                user_info.photo = data.getString("photo")
            }catch (e:JSONException){
                Log.d("aim","err json : $e")
            }

            finish()

        },Response.ErrorListener { response ->
            dialog.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }


}
