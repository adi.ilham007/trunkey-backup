package innovacia.co.id.trunkey1

import android.Manifest
import android.annotation.SuppressLint
import android.location.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.adapter.BlogAdapter
import innovacia.co.id.trunkey1.adapter.ListingCustomAdapter
import innovacia.co.id.trunkey1.helper.VolleySingleton
//import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ListingCustomModel
import innovacia.co.id.trunkey1.model.blog_model
import kotlinx.android.synthetic.main.activity_home_makanan.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class home_makanan : AppCompatActivity(),LocationListener {

    private var lokasiGps = Manifest.permission.ACCESS_FINE_LOCATION
    private var lokasiNet = Manifest.permission.ACCESS_COARSE_LOCATION
    val helper = user_info()

    var rating = ""
    var searchText = ""
    var searchType = ""
    var latitude = user_info.latitude
    var longitude = user_info.longitude

    private var vendorCategory = ""
    private var loadMore = false    // First load
    private var lastData = false    // Pagination
    private var skip = 0            // Pagination

    private var page = 0
    private var lastPage = 0


    override fun onLocationChanged(location: Location?) {
        this.getLocation()
    }
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        this.getLocation()
    }
    override fun onProviderEnabled(provider: String?) {
        this.getLocation()
    }
    override fun onProviderDisabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    // Recycler View
    private val vendorData = mutableListOf<ListingCustomModel>()
    private val blogData = mutableListOf<blog_model>()

    var category_out = ""
    var product_type_out = ""
    var grade_out = ""
    var price_min_out = ""
    var price_max_out = ""
    var distance_out = ""
    var rating_out = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_makanan)
        header_name?.text = resources.getString(R.string.search_result_header)

        generate_view()
        rvListener()

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        swipe_container.setOnRefreshListener {
            if(searchType == "blog"){
                page = 1
                lastPage = 0
                loadMore = false    // First load
                lastData = false    // Pagination
                blogData.clear()
                searchBlog()
            }else{
                skip = 0
                vendorData.clear()
                searchVendor()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() = runWithPermissions(lokasiGps, lokasiNet) {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (fusedLocationClient == null) {
            Toast.makeText(this,"Turn ON GPS",Toast.LENGTH_LONG).show()
            user_info.latitude = 0.0
            user_info.longitude = 0.0
        } else {
            fusedLocationClient.lastLocation?.addOnSuccessListener { location: Location? ->
                if (location != null) {
                    user_info.latitude= location.latitude
                    user_info.longitude = location.longitude
                }
            }
        }
    }

    fun generate_view(){
        if (intent.hasExtra("category")) {
            when(intent.getStringExtra("category")) {
                "nearme" -> {
                    search_value.visibility = View.GONE
                    getLocation()
                    searchVendor()
                }
                "favorite" -> {
                    search_value.visibility = View.GONE
                    rating = "4,5"
                    searchVendor()
                }
            }
        }
        else if (intent.hasExtra("search")) {
            when(intent.getStringExtra("search")) {
                "vendor" -> {
                    searchType = "vendor"
                    searchText = intent.getStringExtra("text")
                    search_value.visibility = View.VISIBLE
                    search_value.text = searchText.replace(" "," | ")

                    Log.d("aim","search vendor = text : $searchText")
                    searchVendor()
                }
                "vendor_category" -> {
                    searchType = "vendor"
                    searchText = intent.getStringExtra("text")
                    vendorCategory = intent.getStringExtra("category_id")
                    val categoryName = intent.getStringExtra("category_name")

                    val allText = ArrayList<String>()
                    allText.add(categoryName)
                    allText.addAll(searchText.split(" "))

                    search_value.visibility = View.VISIBLE
                    search_value.text = allText.toString().replace(","," | ")
                            .replace("[","")
                            .replace("]","")

                    Log.d("aim","search vendor category = text : $searchText, category : $vendorCategory")
                    searchVendor()
                }
                "blog" -> {
                    searchType = "blog"
                    searchText = intent.getStringExtra("text")
                    search_value.visibility = View.VISIBLE
                    search_value.text = searchText.replace(" "," | ")

                    Log.d("aim","search blog = text: $searchText")
                    searchBlog()
                }
            }
        }
    }

    private fun rvListener() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (isLastPosition && !lastData && searchType == "vendor") {
                    loadMore = true
                    skip += 10
                    searchVendor()
                }else if(isLastPosition && !lastData && page < lastPage && searchType == "blog") {
                    page ++
                    loadMore = true
                    searchBlog()
                }
            }
        })
    }

    private fun searchVendor() {
        val errSection = "searchVendor :"
        swipe_container?.isRefreshing = true

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.listing,Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val jsonArray = jsonObject.getJSONArray("list")

                if (jsonArray.length() == 0 && skip == 0){          // Data tidak tersedia
                    root_lay?.visibility = View.VISIBLE
                    root_lay?.setBackgroundResource(R.drawable.ic_oops)
                } else if (jsonArray.length() == 0 && skip != 0){     // Last data
                    root_lay?.setBackgroundResource(R.color.textWhite)
                    lastData = true
                } else {
                    root_lay?.setBackgroundResource(R.color.textWhite)
                    swipe_container?.setBackgroundResource(R.color.textWhite)
                    for (i in 0 until jsonArray.length()) {
                        val jo = jsonArray.getJSONObject(i)

                        var jarak: String
                        if (jo.isNull("distance")) {
                            jarak = "Unknown"
                        } else jarak = jo["distance"].toString() + " Km"

                        val category = jo.getString("type")
                        val id = if (category == "vendor") {
                            jo.getString("id_vendor")
                        } else {
                            jo.getString("slug")
                        }

                        val nama = jo.getString("nama")
                        val alamat = jo.getString("alamat_vendor")
                        //var rating = jo["rating"].toString().toDouble()
                        val img = jo.getString("thumbnail_logo")
                        val alamatFix = helper.textHtml(alamat)

                        val mItem = ListingCustomModel(id, nama, alamatFix, 0, "", img, category)
                        vendorData.add(mItem)
                    }

                    val vendorAdapter = ListingCustomAdapter()
                    if (!loadMore) {
                        recyclerView?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                        recyclerView?.adapter = vendorAdapter
                        vendorAdapter.setList(vendorData)
                    } else {
                        val recyclerViewState = recyclerView.layoutManager?.onSaveInstanceState()
                        recyclerView?.adapter = vendorAdapter
                        vendorAdapter.addData(vendorData)
                        recyclerView?.layoutManager!!.onRestoreInstanceState(recyclerViewState)
                    }

                }
            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()

                if(vendorCategory != "") {
                    map["category"] = "[$vendorCategory]"
                }

                map["text"] = searchText
                map["take"] = "10"
                map["skip"] = "$skip"
//                map["sort_by"] = ""
//                map["language"] = "[]"

                map["type"] = "all"

                map["nearme"] = "1"
//                map["distance"] = ""
//                map["location"] = ""

                // Location
                if (user_info.latitude != 0.0 && user_info.longitude != 0.0) {
                    map["latitude"] = user_info.latitude.toString()
                    map["longitude"] = user_info.longitude.toString()
                }

//                map["open_now"] = ""
//                map["rating"] = ""
//                map["rating_max"] = ""
//                map["rating_min"] = ""
                //map["ratings"] = "[$rating]"

//                if (user_info.tagSearch != "") {
//                    map["subject"] = user_info.tagSearch
//                }

                Log.d("aim", "Send : $map")
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    fun searchBlog() {
        val errSection = "searchBlog :"
        swipe_container?.isRefreshing = true

        val sr = object : StringRequest(Request.Method.POST, user_info.blog+"?page="+page, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val dataObject = jsonObject.getJSONObject("data")
                val dataArray = dataObject.getJSONArray("data")

                lastPage = dataObject.getInt("last_page")

                if (dataArray.length() == 0 && skip == 0){          // Data tidak tersedia
                    root_lay?.visibility = View.VISIBLE
                    root_lay?.setBackgroundResource(R.drawable.ic_oops)
                } else if (dataArray.length() == 0 && skip != 0){     // Last data
                    root_lay?.setBackgroundResource(R.color.textWhite)
                    lastData = true
                } else {
                    root_lay?.setBackgroundResource(R.color.textWhite)

                    for (i in 0 until dataArray.length()) {
                        val jo = dataArray.getJSONObject(i)
                        val slug = jo.getString("slug")
                        val nama = jo.getString("nama")
                        val img = jo.getString("image_path")

                        var category = ""
                        if (!jo.isNull("category")) {
                            val dataCategory = jo.getJSONObject("category")
                            category = dataCategory.getString("nama")
                        }

                        val aboutHtml = jo.getString("keterangan")
                        val aboutText = helper.textHtml(aboutHtml)

                        val mItem = blog_model("",slug, nama, aboutText, img, category)
                        blogData.add(mItem)
                    }
                }

                // ============ recycler_parent ============
                val adapter = BlogAdapter()

                if (!loadMore) {
                    recyclerView?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    recyclerView?.adapter = adapter
                    adapter.setList(blogData)
                }
                else {
                    val recyclerViewState = recyclerView.layoutManager?.onSaveInstanceState()
                    recyclerView?.adapter = adapter
                    adapter.addData(blogData)
                    recyclerView?.layoutManager!!.onRestoreInstanceState(recyclerViewState)
                }
            } catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }

        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["text"] = searchText
//                map["category"] = "$tag"
//                map["type"] = "VIDEOS"
                return map
            }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(sr)
    }
}
