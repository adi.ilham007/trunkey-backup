package innovacia.co.id.trunkey1.model

data class event_model(
    var slug        :String,
    var nama        :String,
    var keterangan  :String,
    var date        :String,
    var time        :String,
    var lokasi      :String,
    var kuota       :String,
    var img         :String
)