package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.Transfer
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.AttendanceModel
import kotlinx.android.synthetic.main.list_attendance.view.*

class AttendanceAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var attendanceData = mutableListOf<AttendanceModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return childListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_attendance, parent, false))
    }

    override fun getItemCount(): Int = attendanceData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as childListViewHolder
        holder.bindView(attendanceData[position])


        if (itemCount == 1) {
            holder.itemView.line_top.visibility = View.GONE
            holder.itemView.line_bottom.visibility = View.GONE
        } else if (position == 0) {
            holder.itemView.line_top.visibility = View.GONE
        } else if (position == itemCount -1) {
            holder.itemView.line_bottom.visibility = View.GONE
        }

//        val id = attendanceData[position].id.toString()
    }

    fun setList(listOfChild: List<AttendanceModel>) {
        this.attendanceData = listOfChild.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(listOfTransaction: List<AttendanceModel>) {
        this.attendanceData.addAll(listOfTransaction)
        notifyDataSetChanged()
    }


    class childListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()

        fun bindView(mAttendanceModel: AttendanceModel) {
            itemView.vendor_name.text = mAttendanceModel.vendorName

            val checkin = mAttendanceModel.isCheckIN
            val checkout = mAttendanceModel.isCheckOUT

            if (checkin == "1") {
                itemView.time_value.text = mAttendanceModel.timeIN
                itemView.attendance_method.text = "Type : ${mAttendanceModel.method}"
                itemView.status_img?.setBackgroundResource(R.drawable.ic_checkin_large)
            } else if (checkout == "1") {
                itemView.time_value.text = mAttendanceModel.timeOUT
                itemView.attendance_method.text = "Type : ${mAttendanceModel.method}"
                itemView.status_img?.setBackgroundResource(R.drawable.ic_checkout_large)
            }
        }
    }
}