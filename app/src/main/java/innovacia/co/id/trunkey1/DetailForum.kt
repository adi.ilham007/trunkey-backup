package innovacia.co.id.trunkey1

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.adapter.detail_vendor.ReviewAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ReviewModel
import kotlinx.android.synthetic.main.activity_detail_forum.*
import kotlinx.android.synthetic.main.activity_detail_forum.back_link
import org.json.JSONException
import org.json.JSONObject

class DetailForum : AppCompatActivity() {

    private val reviewForum = mutableListOf<ReviewModel>()
    var idForum = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_forum)

        generateView()

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            val i = Intent(this, Forum::class.java)
            startActivity(i)
            finish()
        }

        footerHandling()

        Reviewtext.onFocusChangeListener = View.OnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                if (!user_info.loginStatus) {
                    val i = Intent(this, login::class.java)
                    i.putExtra("back", "detail_forum")
                    startActivity(i)
                }
            }
        }

        review.setOnClickListener {
            review.startAnimation(user_info.animasiButton)
            if (!user_info.loginStatus) {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "detail_forum")
                startActivity(i)
            } else {
                postComment()
            }
        }

        iconLike.setOnClickListener {
            if (!user_info.loginStatus) {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "detail_forum")
                startActivity(i)
            } else {
                val idUser = intent.getStringExtra("id_user")
                val id = intent.getStringExtra("id")
                like(id)
            }
        }


    }

    fun generateView(){
        if (intent.hasExtra("data") && intent.getStringExtra("data") != null) {
            // From notification / FCM
            idForum = intent.getStringExtra("data")
            detailForum()
        } else if (intent.getStringExtra("id") != null) {
            idForum = intent.getStringExtra("id")
            Log.d("aim","forum id : $idForum")

            detailForum()
        }
    }

    fun footerHandling(){
        // ========================================================================================= Footer onClick
        try {
            forum_footer.setTextColor(resources.getColor(R.color.blue))
            forum_text.setTextColor(resources.getColor(R.color.blue))
        }catch (e: Exception) {
            Log.e("aim", "Err : $e")
        }

        // ========================================================================================= Footer
        home_footer.setOnClickListener {
            home_footer.startAnimation(user_info.animasiButton)
            val i = Intent(this, home::class.java)
            startActivity(i)
        }

        search_footer.setOnClickListener {
            search_footer.startAnimation(user_info.animasiButton)
            val i = Intent(this, Scan::class.java)
            startActivity(i)
        }

//        forum_footer.setOnClickListener{
//            user_info.statePosition = "forum"
//            forum_footer.startAnimation(user_info.animasiButton)
//            val i = Intent(this, Forum::class.java)
//            startActivity(i)
//        }

        profile_footer.setOnClickListener {
            profile_footer.startAnimation(user_info.animasiButton)

            if (!user_info.loginStatus) {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "profile")
                startActivity(i)
            } else {
                val i = Intent(this, Profile::class.java)
                startActivity(i)
            }
        }
    }

    fun detailForum(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = @SuppressLint("SetJavaScriptEnabled")
        object : StringRequest(Request.Method.POST, user_info.detail_forum+idForum, Response.Listener { response ->
            //========================================================================================= data from server
            dialog.dismiss()
            try {
                val respon = JSONObject(response)
                val data = respon.getJSONObject("data")

                val member = data.getJSONObject("member")
                nama.text = member.getString("nama")
                val memberPhoto = member.getString("photo")

                Glide.with(this).load(memberPhoto)
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(photo)

                contentText.text = data.getString("keterangan")

                val foto = data.getString("link_foto")
                Log.d("aim","foto forum : $foto")
                if (foto != "null") {
                    contentImage.visibility = View.VISIBLE
                    Glide.with(this).load(foto).into(contentImage) // Banner
                }

                like.text = data.getString("total_like")
                comment.text = data.getString("total_reply")
                tanggal_view.text = data.getString("created")
                Log.e("aim","like status"+ data.getInt("is_like"))
                if(data.getInt("is_like") == 1){
                    iconLike.setTextColor(ContextCompat.getColor(iconLike.context, R.color.blue))
                }else{
                    iconLike.setTextColor(ContextCompat.getColor(iconLike.context, R.color.gray))
                }

                val comment = data.getJSONObject("comments")
                val dataArray = comment.getJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    val jo = dataArray.getJSONObject(i)
                    val id = jo.getString("id")
                    val tanggal = jo.getString("created")
                    val review = jo.getString("keterangan")
                    val rating = jo.getString("total_like")

                    val memberData = jo.getJSONObject("member")
                    val nama = memberData.getString("nama")
                    val img = memberData.getString("photo")

                    val mItem = ReviewModel(id, nama, review, "", tanggal, img)
                    reviewForum.add(mItem)
                }
                // ============ recycler_comment ============
                recycler_comment.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val reviewAdapter = ReviewAdapter()
                recycler_comment.adapter = reviewAdapter
                reviewAdapter.setList(reviewForum)
                reviewForum.clear()

            } catch (e: JSONException) {
                Toast.makeText(this, "Data Tidak Tersedia", Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()

                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        rq.add(sr)
    }

    fun postComment(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.forum_comment+idForum, Response.Listener { response ->
            //========================================================================================= data from server
            dialog.dismiss()
            try {
                Log.d("aim","review Terkirim, $response")
                Reviewtext.setText("")
                detailForum()
            } catch (e: JSONException) {
                Log.e("aim", "post review e: $e")
//                Toast.makeText(this,"Data Tidak Tersedia",Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
//                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map["description"] = Reviewtext.text.toString()
                Log.d("aim","comment forum : $map")
                return map
            }
        }
        rq.add(sr)
    }

    fun like(id:String){

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.forum_like+id, Response.Listener { response ->
            //========================================================================================= data from server

            detailForum()
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
//                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        rq.add(sr)
    }
}
