package innovacia.co.id.trunkey1

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
//import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.list_vendor.*
import android.view.View
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.helper.user_info.Companion.category


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private val mVolleyHelper = VolleyHelper()

    private var mMap: GoogleMap? = null
    private lateinit var mLastLocation: Location
    private lateinit var mLocationResult: LocationRequest
    private lateinit var mLocationCallback: LocationCallback
    private var mCurrLocationMarker: Marker? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private lateinit var mLocationRequest: LocationRequest
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    // Permissions
    private var lokasi_gps = Manifest.permission.ACCESS_FINE_LOCATION
    private var lokasi_net = Manifest.permission.ACCESS_COARSE_LOCATION


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

//        obtieneLocalizacion()

        if (intent.hasExtra("id") && intent.hasExtra("name")) {
            getLocation(intent.getStringExtra("id"),intent.getStringExtra("name"))
        }

    }

    private fun getLocation(id:String,name:String) {
        val errSection = "getDriverLocation"
        val loading = ProgressDialog.show(this,"Loading",null,false)
        loading.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : JsonObjectRequest(Request.Method.GET,user_info.care_location+id,null, Response.Listener { response ->
            try {
                loading.dismiss()
                Log.d("aim","$errSection : $response")

                val dataArray = response?.getJSONArray("data")
                if (dataArray != null) {
                    val dataObj = dataArray.getJSONObject(0)
                    val latitude = dataObj.getDouble("latitude")
                    val longitude = dataObj.getDouble("longitude")

                    if (latitude != 0.0 && longitude != 0.0 ) {
                        val markerOptions = MarkerOptions()
                        val koordinat = LatLng(latitude,longitude)
                        markerOptions.position(koordinat)
                                .title(name)
//                            .snippet(category)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))

                        mMap!!.addMarker(markerOptions)
                    } else {
                        Toast.makeText(this,"Location not available",Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this,"Location not available",Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }

        }, Response.ErrorListener { response ->
            loading.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

//
//    @SuppressLint("MissingPermission")
//    private fun obtieneLocalizacion() = runWithPermissions(lokasi_gps, lokasi_net) {
//        if (mFusedLocationClient == null) {
//        } else {
//            mFusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
//                user_info.latitude = location?.latitude
//                user_info.longitude = location?.longitude
//
//                Log.d("aim","last location : ${user_info.latitude}, ${user_info.longitude}")
//            }
//        }
//    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProviderEnabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProviderDisabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.isMyLocationButtonEnabled = true
        mMap!!.uiSettings.isCompassEnabled = false

//        val locationButton= (mMap!!.findViewById<View>(Integer.parseInt("1")).parent as View).findViewById<View>(Integer.parseInt("2"))
//        val rlp=locationButton.layoutParams as (RelativeLayout.LayoutParams)
//        // position on right bottom
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP,0)
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE)
//        rlp.setMargins(0,0,30,30)

        mMap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style))
        val tengah = LatLng(-4.445177,119.813787)  // titik tengah indonesia, Umpungeng, Lalabata, Soppeng, Sulawesi Selatan

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient()
                mMap!!.isMyLocationEnabled = true
                mMap!!.moveCamera(CameraUpdateFactory.newLatLng(tengah))
                mMap!!.animateCamera(CameraUpdateFactory.zoomTo(3.35f))
            }
        } else {
            buildGoogleApiClient()
            mMap!!.isMyLocationEnabled = true
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(tengah))
            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(3.35f))
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()
        mGoogleApiClient!!.connect()
    }

    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//            mFusedLocationClient?.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper())

        }
    }

    override fun onLocationChanged(location: Location) {

        mLastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }
        //Place current location marker
        var latLng = LatLng(location.latitude, location.longitude)
        Log.d("aim", "test")
        Log.d("aim","LAT : ${location.latitude} ,LON : ${location.longitude}")


        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title("Current Position")
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
        mCurrLocationMarker = mMap!!.addMarker(markerOptions)

        //move map camera
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(0F))
        Log.d("aim", "$latLng")

        //stop location updates
        if (mGoogleApiClient != null) {
            mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Toast.makeText(applicationContext,"connection failed", Toast.LENGTH_SHORT).show()
    }

    override fun onConnectionSuspended(p0: Int) {
        Toast.makeText(applicationContext,"connection suspended", Toast.LENGTH_SHORT).show()
    }
}
