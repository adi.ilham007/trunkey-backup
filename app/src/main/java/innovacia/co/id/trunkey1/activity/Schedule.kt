package innovacia.co.id.trunkey1.activity

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.ChildAdapter2
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Handler
import android.view.View
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import innovacia.co.id.trunkey1.AddChildManual
import innovacia.co.id.trunkey1.CreateAgenda
import innovacia.co.id.trunkey1.adapter.CalendarAdapter
import innovacia.co.id.trunkey1.adapter.ScheduleAdapter
import innovacia.co.id.trunkey1.database.adapter.ScheduleByDateAdapter
import innovacia.co.id.trunkey1.database.cart.CartEntity
import innovacia.co.id.trunkey1.database.model.ScheduleModelDB
import innovacia.co.id.trunkey1.database.schedule.ScheduleDatabase
import innovacia.co.id.trunkey1.database.schedule.ScheduleEntity
import innovacia.co.id.trunkey1.database.schedule.ScheduleRespository
import innovacia.co.id.trunkey1.helper.CalendarCustomView
import innovacia.co.id.trunkey1.helper.SnapHelperOneByOne
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.login
import innovacia.co.id.trunkey1.model.*
import kotlinx.android.synthetic.main.activity_schedule.*
import kotlinx.android.synthetic.main.sub_header_rv.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class Schedule : AppCompatActivity() {

    private val helper = user_info()

//    var mScheduleDatabase: ScheduleDatabase? = null
//    lateinit var mScheduleModelDB: ScheduleModelDB
//    private lateinit var mScheduleRespository: ScheduleRespository

    val allEvent = ArrayList<String>()
    private val mChildModel = mutableListOf<ChildModel>()
    private val mCalendarModel = mutableListOf<CalendarModel>()
    private val mScheduleModel = mutableListOf<ScheduleModel>()

    @SuppressLint("SimpleDateFormat")
    val sdf = SimpleDateFormat("yyyy-MM-dd-u")
    val today = sdf.format(Date()).split("-")
    var globalYear = today[0].toInt()
    var globalMonth = today[1].toInt()
    var globalDate = today[2].toInt()
    var globalDay = today[3].toInt()

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
        header_name?.text = resources.getString(R.string.schedule_header)

//        mScheduleDatabase = ScheduleDatabase.getInstance(this)
//        mScheduleModelDB = ViewModelProviders.of(this).get(ScheduleModelDB(application)::class.java)
//        mScheduleRespository = ScheduleRespository(application)

        swipe_container?.setOnRefreshListener {
            mCalendarModel.clear()
            mScheduleModel.clear()

            // Reset event by date data
            eventByDate("","")
            date_value?.text = ""

            getToday()
            getAllSchedule()
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        datePicker?.setOnClickListener {
            datePicker?.startAnimation(user_info.animasiButton)
            SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .displayDaysOfMonth(false)
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayYears(true)
                    .displayMonth(true)
                    .listener(object : SingleDateAndTimePickerDialog.Listener{
                        override fun onDateSelected(datePick: Date?) {
                            val pick = sdf.format(datePick).split("-")
                            globalYear = pick[0].toInt()
                            globalMonth = pick[1].toInt()
                            globalDate = pick[2].toInt()

                            getAllSchedule()
                        }
                    }).display()
        }

        btn_add?.setOnClickListener {
            btn_add?.startAnimation(user_info.animasiButton)
            val i = Intent(this, CreateAgenda::class.java)
            startActivity(i)
        }

        btn_sync_setting?.setOnClickListener {
            btn_sync_setting?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                if (user_info.activeChildId == "0") {
                    Toast.makeText(this,"Please choose or register your child",Toast.LENGTH_LONG).show()
                } else {
                    val i = Intent(this, ScheduleSync::class.java)
                    startActivity(i)
                }
            }
        }

        cacheHandling()
        setEmptyCalendar()
        getToday()
        getAllSchedule()
    }

    private fun setEmptyCalendar() {
        val pickDate = "$globalYear-$globalMonth-$globalDate"
        custom_calendar?.setEmptyCalendar(pickDate,allEvent)
    }

    private fun setCalendar() {
        allEvent.clear()
        val dayString = resources.getStringArray(R.array.day_array)
        day_view?.text = "${dayString[globalDay-1]}, "

        val monthString = resources.getStringArray(R.array.month_array)
        date_view?.text = globalDate.toString()
        month_view?.text = monthString[globalMonth-1]
        year_view?.text = globalYear.toString()

        for(i in 0 until mCalendarModel.size) {
            val getDate = mCalendarModel[i].date
            allEvent.add(getDate)
        }

        Log.d("aim","all events schedule: $allEvent")
        val pickDate = "$globalYear-$globalMonth-$globalDate"
        custom_calendar?.setUpCalendarAdapter(pickDate,allEvent)

        custom_calendar?.setEventHandler(object : CalendarCustomView.CalendarClickHandler {
            override fun prevMonth() {
                custom_calendar?.setUpCalendarAdapter("prev",allEvent)
            }

            override fun nextMonth() {
                custom_calendar?.setUpCalendarAdapter("next",allEvent)
            }

            @SuppressLint("SimpleDateFormat", "SetTextI18n")
            override fun onDayPress(date: Date) {
                val uiFormat = SimpleDateFormat("EEEE, dd MMMM yyyy")
                val serverFormat = SimpleDateFormat("dd-MM-yyyy")
                eventByDate(serverFormat.format(date),uiFormat.format(date))
            }
        })
    }

    private fun cacheHandling() {
        if (user_info.restoreKids == null) getKids() else restoreKidsFun()
    }

    private fun getKids() {
        val errSection = "getKids :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                user_info.activeChildId = ""
                user_info.childCarouselState = 0

                mChildModel.clear()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restoreKids = dataArray
                if (dataArray.length() > 0) {
                    // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val gender = childObject.getString("gender")
                        val birthday = childObject.getString("birthday")
                        val foto = childObject.getString("photo")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val status = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                        mChildModel.add(mItem)

                        // Add child option
                        if (i+1 == dataArray.length()) {
                            val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                            mChildModel.add(addChild)
                        }
                    }
                } else {
                    for (i in 0 until 1) {
                        val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                        mChildModel.add(addChild)
                    }
                }

                val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                recycler_child?.layoutManager = mLinearLayoutManager

                // Triger carousel to center
                recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)

                val displayMetrics = this.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 320) / 2
                val pxWidth = helper.dpToPx(this, calculation.toInt())
                recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (recycler_child?.onFlingListener == null) {
                    val snapHelper = SnapHelperOneByOne()
                    snapHelper.attachToRecyclerView(recycler_child)
                }

                // Large Carousel
                ChildAdapter2.TYPE_OF_VIEW = 1
                val mChildAdapter = ChildAdapter2()
                recycler_child?.adapter = mChildAdapter
                mChildAdapter.setList(mChildModel)

                kidsCarouselListener(mLinearLayoutManager)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }
    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {
            // Default see all
//            if (dataArray.length() > 1) {
//                val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                mChildModel.add(seeAll)
//            }

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val gender = childObject.getString("gender")
                val birthday = childObject.getString("birthday")
                val foto = childObject.getString("photo")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val status = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                mChildModel.add(mItem)

                // Add child option
                if (i+1 == dataArray.length()) {
                    val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                    mChildModel.add(addChild)
                }
            }
        } else {
            for (i in 0 until 1) {
                val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                mChildModel.add(addChild)
            }
        }

        val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
        recycler_child?.layoutManager = mLinearLayoutManager

        // Handle carousel position (restore or not)
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }

        val displayMetrics = this.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 320) / 2
        val pxWidth = helper.dpToPx(this, calculation.toInt())
        recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (recycler_child?.onFlingListener == null) {
            val snapHelper = SnapHelperOneByOne()
            snapHelper.attachToRecyclerView(recycler_child)
        }

        // Large Carousel
        ChildAdapter2.TYPE_OF_VIEW = 1
        val mChildAdapter = ChildAdapter2()
        recycler_child?.adapter = mChildAdapter
        mChildAdapter.setList(mChildModel)

        kidsCarouselListener(mLinearLayoutManager)
    }
    private fun kidsCarouselListener(mLinearLayoutManager: LinearLayoutManager) {
        recycler_child?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val snapHelper = SnapHelperOneByOne()
                if(newState == RecyclerView.TOUCH_SLOP_PAGING && newState != RecyclerView.NO_POSITION) {
                    Handler().postDelayed({
                        val centerView = snapHelper.findSnapView(mLinearLayoutManager)
                        if (centerView != null) {
                            val pos = mLinearLayoutManager.getPosition(centerView)
                            val getId = mChildModel[pos % mChildModel.size].id
                            user_info.activeChildId = getId
                            user_info.childCarouselState = pos
                            Log.d("aim", "active child id : $getId")

                            mCalendarModel.clear()
                            mScheduleModel.clear()

                            // Reset event by date data
                            eventByDate("","")
                            date_value?.text = ""

                            getToday()
                            getAllSchedule()
                        }
                    }, 500)
                }
            }
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun getToday() {
        val errSection = "getToday :"
        swipe_container?.isRefreshing = true

        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val startDate = sdf.format(Date())

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.schedule+user_info.activeChildId+"/$startDate", Response.Listener { response ->
            try{
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")

                if (dataArray.length() > 0) {
                    tx_today?.visibility = View.VISIBLE
                    recycler_today?.visibility = View.VISIBLE

                    for (i in 0 until dataArray.length()) {
                        val dataAgenda = dataArray.getJSONObject(i)
                        val time = dataAgenda.getString("time")
                        val subject = dataAgenda.getString("subject")
                        var globalNotif = ""

                        // Notif dari sekolah / vendor
                        if (dataAgenda.getInt("is_editable") == 0) {
                            val notifArray = dataAgenda.getJSONArray("notifications")

                            if (notifArray.length() != 0) {
                                for (y in 0 until notifArray.length()) {
                                    val dataNotif = notifArray.getJSONObject(y)
                                    val name = dataNotif.getString("nama")
                                    val desc = dataNotif.getString("keterangan")
                                    globalNotif += "${y + 1}. $name, $desc \n"
                                }
                            }
                        } else {
                            globalNotif += "Note not availlable"
                        }

                        val mItem = ScheduleModel(time, subject, globalNotif)
                        mScheduleModel.add(mItem)

                    }

                    val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                    recycler_today?.layoutManager = mLinearLayoutManager

                    val displayMetrics = this.resources.displayMetrics
                    val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                    val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                    val calculation = (dpWidth - 320) / 2
                    val pxWidth = helper.dpToPx(this, calculation.toInt())
                    recycler_today?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                    if (recycler_today?.onFlingListener == null) {
                        val snapHelper = SnapHelperOneByOne()
                        snapHelper.attachToRecyclerView(recycler_today)
                    }

                    val mChildAdapter = ScheduleAdapter()
                    recycler_today?.adapter = mChildAdapter
                    mChildAdapter.setList(mScheduleModel)
                } else {
                    tx_today?.visibility = View.GONE
                    recycler_today?.visibility = View.GONE
                }

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun getAllSchedule(){
        val errSection = "getAllSchedule :"
        swipe_container?.isRefreshing = true

        val calendar = Calendar.getInstance()
        calendar.set(globalYear,globalMonth-1,globalDate)
        val startDate = calendar.getActualMinimum(Calendar.DAY_OF_MONTH)
        val endDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        val stringStart = "$globalYear-${String.format("%02d", globalMonth)}-${String.format("%02d", startDate)}"
        val stringEnd = "$globalYear-${String.format("%02d", globalMonth)}-${String.format("%02d", endDate)}"

        Log.d("aim","start date: $stringStart, end date: $stringEnd")
        Log.d("aim","active child id : ${user_info.activeChildId}")

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.allSchedule, Response.Listener { response ->
            try{
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")

                for(i in 0 until dataArray.length()) {
                    val dataObject = dataArray.getJSONObject(i)
                    val eventSplit = dataObject.getString("date").split("-")
                    val eventDate = "${eventSplit[2]}-${eventSplit[1]}-${eventSplit[0]}"

                    val scheduleArray = dataObject.getJSONArray("schedules")
                    if (scheduleArray.length() > 0) {
                        for(j in 0 until scheduleArray.length()) {
                            val scheduleObject = scheduleArray.getJSONObject(j)
                            val userId = scheduleObject.getString("editable_id")
                            val startTime = scheduleObject.getString("start_time")
                            val endTime = scheduleObject.getString("end_time")
                            val subject = scheduleObject.getString("subject")
                            val location = scheduleObject.getString("location")
                            val descDanUserId = "$subject<-*-> $location<-*->$userId"


                            val mItem = CalendarModel(
                                    "",
                                    eventDate,
                                    startTime,
                                    endTime,
                                    subject,
                                    location,
                                    userId,
                                    user_info.activeChildId,
                                    ""
                            )
                            mCalendarModel.add(mItem)

                        }
                    }
                }

                setCalendar()

            }catch (e: JSONException) {
                Log.e("aim", "$errSection : $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["start_date"] = stringStart
                map["end_date"] = stringEnd
                map["children"] = user_info.activeChildId
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    @SuppressLint("SimpleDateFormat")
    private fun eventByDate(serverDate: String, uiDate: String) {
        val filterResult = mCalendarModel.filter { it.date == serverDate }

        val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
        recycler_click?.layoutManager = mLinearLayoutManager

        val mCalendarAdapter = CalendarAdapter()
        recycler_click?.adapter = mCalendarAdapter
        mCalendarAdapter.setList(filterResult)

        if (filterResult.isNotEmpty()) {
            date_value?.text = uiDate
            scrollView.post { scrollView.fullScroll(View.FOCUS_DOWN) }
        } else {
            date_value?.text = ""
        }
    }
}
