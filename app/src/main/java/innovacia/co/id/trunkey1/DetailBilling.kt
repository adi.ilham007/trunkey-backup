package innovacia.co.id.trunkey1

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_detail_billing.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONObject

class DetailBilling : AppCompatActivity() {

    private val mVolleyHelper = VolleyHelper()
    private val helper = user_info()

    private var fcmStatus = false
    private var billingId = ""
    private var billingPrice = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_billing)
        header_name.text = resources.getString(R.string.billing_detail)

        generateView()

        back_link.setOnClickListener {
            if (fcmStatus) {
                val intent = Intent(this, HomeBaru::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            } else {
                onBackPressed()
            }
        }

        swipe_container.setOnRefreshListener {
            detailBilling(billingId)
        }

        pay.setOnClickListener {
            pay.startAnimation(user_info.animasiButton)
            val i = Intent(this, Select_Purchase::class.java)
            i.putExtra("bayarBilling", "bayarBilling")
            i.putExtra("idBilling",billingId)
            i.putExtra("hargaBilling",billingPrice)
            startActivity(i)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (fcmStatus) {
            val intent = Intent(this, HomeBaru::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }

    private fun generateView() {
        if (intent.hasExtra("data")) {
            // From notification / FCM
            fcmStatus = true

            val reader = this.getSharedPreferences("my_preferences", Context.MODE_PRIVATE)
            val token = reader.getString("access_token", "")
            val refresh = reader.getString("refresh_token","")

            if (!token.isNullOrEmpty() && !refresh.isNullOrEmpty()) {
                user_info.loginStatus = true
                user_info.token = token
                user_info.refresh = refresh

                val fcmData = JSONObject(intent.getStringExtra("data"))
                billingId = fcmData.getString("id")
                detailBilling(billingId)
            }
        } else if (intent.hasExtra("idBilling")) {
            billingId = intent.getStringExtra("idBilling")
            detailBilling(billingId)
        }
    }

    private fun detailBilling(id: String?) {
        val errSection = "detail billing"
        swipe_container.isRefreshing = true
        val request = object : JsonObjectRequest(Request.Method.POST,user_info.detailBilling+id,null, Response.Listener { response ->

            try {
                swipe_container.isRefreshing = false
                val jo = response.getJSONObject("data")

                billingPrice = jo.getDouble("besar")

                student_value.text = jo.getString("member_name")
                name_value.text = jo.getString("nama")
                amount_value.text = helper.bank(billingPrice)
                due_date_value.text = jo.getString("due_date")
                desc_value.text = helper.textHtml(jo.getString("keterangan"))

                val vendorObject = jo.getJSONObject("vendor")
                vendor_name.text = vendorObject.getString("nama")

            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }

}
