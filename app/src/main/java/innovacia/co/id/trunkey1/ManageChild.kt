package innovacia.co.id.trunkey1

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.Window
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.ChildAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.activity_manage_child.*
import org.json.JSONObject
import org.json.JSONException


class ManageChild : AppCompatActivity() {

    private val childData = mutableListOf<ChildModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_child)

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        swipe_container.setOnRefreshListener {
            getChild()
        }

        addChild.setOnClickListener {
            addChild.startAnimation(user_info.animasiButton)
            addChildDialog()
        }

        getChild()

    }

    private fun addChildDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        textNotif.text = resources.getText(R.string.add_child)
        ok.text = "Yes"
        cancel.text = "No"

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val i = Intent(this, Scan::class.java)
            startActivity(i)
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            val i = Intent(this, AddChildManual::class.java)
            startActivity(i)
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun getChild() {
        swipe_container.isRefreshing = true
        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                shimmerLayout.stopShimmerAnimation()
                val jsonObject = JSONObject(response)
                val status = jsonObject.getInt("status")
                Log.i("aim" , response)
                if(status == 1) {
                    val jo_array = jsonObject.getJSONArray("data")
                    for(i in 0 until jo_array.length()) {
                        val childObject = jo_array.getJSONObject(i)
                        Log.i("data" , childObject.toString())
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val foto = childObject.getString("photo")
                        val birthday = childObject.getString("birthday")
                        val gender = childObject.getString("gender")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val attendanceStatus = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,attendanceStatus)
                        childData.add(mItem)
                    }
                    // ============ recycler_transaction ============
                    recycler_child.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    val childAdapter = ChildAdapter()
                    recycler_child.adapter = childAdapter
                    childAdapter.setList(childData)
                    childData.clear()
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container.isRefreshing = false
            shimmerLayout.stopShimmerAnimation()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq1.add(sr1)
    }

}