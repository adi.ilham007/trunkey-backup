package innovacia.co.id.trunkey1.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alexvasilkov.gestures.Settings
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.GalleryModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_image_preview.view.*


class PreviewGalleryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var galleryList = mutableListOf<GalleryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PreviewListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_image_preview, parent, false))
    }
    override fun getItemCount(): Int = galleryList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as PreviewListViewHolder

        viewHolder.bindView(galleryList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)
        }

    }

    fun setList(listOfGallery: List<GalleryModel>) {
        this.galleryList = listOfGallery.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfGallery: List<GalleryModel>) {
        this.galleryList.addAll(listOfGallery)
        notifyDataSetChanged()
    }


    class PreviewListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(eventModel: GalleryModel) {
            itemView.preview.controller.settings
                    .setMaxZoom(2f)
                    .setDoubleTapZoom(-1f) // Falls back to max zoom level
                    .setPanEnabled(true)
                    .setZoomEnabled(true)
                    .setDoubleTapEnabled(true)
                    .setRotationEnabled(false)
                    .setRestrictRotation(false)
                    .setOverscrollDistance(0f, 0f)
                    .setOverzoomFactor(2f)
                    .setFillViewport(true)
                    .setFitMethod(Settings.Fit.INSIDE)
                    .gravity = Gravity.CENTER
            Glide.with(itemView.context).load(eventModel.img).into(itemView.preview)

        }
    }
}