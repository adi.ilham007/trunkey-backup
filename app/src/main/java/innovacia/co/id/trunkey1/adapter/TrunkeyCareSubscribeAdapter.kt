package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.TrunkeyCareRegis2
import innovacia.co.id.trunkey1.model.TrunkeyCareSubscribeModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_trunkey_care_subscribe.view.*

class TrunkeyCareSubscribeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var subscribeData = mutableListOf<TrunkeyCareSubscribeModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SubscribeDataViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_trunkey_care_subscribe, parent, false))
    }
    override fun getItemCount(): Int = subscribeData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as SubscribeDataViewHolder
        holder.bindView(subscribeData[position])

        val id = subscribeData[position].id
        val name = subscribeData[position].name
        val price = subscribeData[position].price

        holder.itemView.setOnClickListener {
            holder.itemView.startAnimation(user_info.animasiButton)
            val i = Intent(it.context,TrunkeyCareRegis2::class.java)
            i.putExtra("id",id)
            i.putExtra("name",name)
            i.putExtra("price",price)
            it.context.startActivity(i)
        }
    }
    fun setList(listOfVendor: List<TrunkeyCareSubscribeModel>) {
        this.subscribeData = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<TrunkeyCareSubscribeModel>) {
        this.subscribeData.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class SubscribeDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(careSubscribeData: TrunkeyCareSubscribeModel) {
            val helper = user_info()

            itemView.name.text = careSubscribeData.name
            itemView.price.text = helper.bank(careSubscribeData.price)
        }
    }

}