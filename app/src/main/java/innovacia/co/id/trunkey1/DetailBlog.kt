package innovacia.co.id.trunkey1

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_detail_blog.*
import org.json.JSONException
import org.json.JSONObject
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.webkit.WebSettings
import android.webkit.WebView
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.adapter.detail_vendor.ReviewAdapter
import innovacia.co.id.trunkey1.model.ReviewModel
import innovacia.co.id.trunkey1.helper.user_info.Companion.animasiButton
import innovacia.co.id.trunkey1.helper.user_info.Companion.comment_blog
import innovacia.co.id.trunkey1.helper.user_info.Companion.detail_blog
import innovacia.co.id.trunkey1.helper.user_info.Companion.listComment_blog
import innovacia.co.id.trunkey1.helper.user_info.Companion.token
import android.view.View.OnFocusChangeListener
import innovacia.co.id.trunkey1.helper.YoutubePlayer
import innovacia.co.id.trunkey1.helper.user_info


class  DetailBlog : AppCompatActivity() {

    private val reviewBlog = mutableListOf<ReviewModel>()
    var idVideo = ""
    var slug = ""
    var idBlog = ""
    var judulForShare = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_blog)

        generateView()

        back_link.setOnClickListener {
            back_link.startAnimation(animasiButton)
            onBackPressed()
        }

        share.setOnClickListener {
            share.startAnimation(animasiButton)
            val link = "http://trunkey.id/blog/detail/$slug"
            val message = "$judulForShare  $link"

            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT,message)
            intent.type = "text/plain"
            try {
                startActivity(Intent.createChooser(intent, "Share to :"))
            }catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "There are no share clients installed.", Toast.LENGTH_SHORT).show()
            }
        }

        Reviewtext.onFocusChangeListener = OnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                if (!user_info.loginStatus) {
                    val i = Intent(this, login::class.java)
                    i.putExtra("back", "detail_blog")
                    startActivity(i)
                }
            }
        }

        review.setOnClickListener {
            review.startAnimation(animasiButton)
            if (!user_info.loginStatus) {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "detail_blog")
                startActivity(i)
            } else {
                if(Reviewtext.length() < 10){
                    Reviewtext.error = "please enter 10 characters"
                }else{
                    postComment()
                }

            }
        }

        imageVideo.setOnClickListener {
            imageVideo.startAnimation(user_info.animasiButton)
            val i = Intent(this, YoutubePlayer::class.java)
            i.putExtra("idVideo", idVideo)
            startActivity(i)
            Log.d("aim","id video $idVideo")
        }
    }

    fun generateView() {
        if (intent.hasExtra("data") && intent.getStringExtra("data") != null) {
            // From notification / FCM
            slug = intent.getStringExtra("data")
            detailBlog()
        } else if (intent.hasExtra("slug") && intent.getStringExtra("slug") != null) {
            slug = intent.getStringExtra("slug")
            Log.d("aim","push notif : $slug")

            if (intent.hasExtra("idVideo") && intent.getStringExtra("idVideo") != null) {
                idVideo = intent.getStringExtra("idVideo")
                val category = intent.getStringExtra("category")

                Log.d("aim", "idVideo : $idVideo")
                Log.d("aim", "slug : $slug")
                Log.d("aim", "category : $category")
            }

            if (idVideo == ""){
                imageVideo_layout.visibility = View.GONE
            }
            if (idVideo != ""){
                webView.visibility = View.GONE
            }

            detailBlog()
        }
    }

    fun detailBlog(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = @SuppressLint("SetJavaScriptEnabled")
        object : StringRequest(Request.Method.POST, detail_blog+slug, Response.Listener { response ->
            //========================================================================================= data from server
            dialog.dismiss()
            try {
                val respon = JSONObject(response)
                val data = respon.getJSONObject("data")

                idBlog = data.getString("id")

                val name = data.getString("nama")
                val content: String
                val imgUrl: String

                if (idVideo == "") {
                    imgUrl = data.getString("image_path")
                    content = data.getString("keterangan")
                    val webHtml =
                        "<html><head> <style>p{line-height:20pt;} img, iframe{width:100%}</style> </head>" +
                        "<img src=$imgUrl>" +
                        "<H3 style='text-align:left'>$name</H3>" +
                        "<body>$content</body></html>"
                    webView.loadData(webHtml, "text/html", "UTF-8")
                } else {
                    val gambarVideo = "https://i.ytimg.com/vi/$idVideo/hqdefault.jpg"
                    Glide.with(this).load(gambarVideo).into(imageVideo)
                }

                webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
                webView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
                webView.fitsSystemWindows = true
                webView.settings.javaScriptEnabled = true

                share.visibility = View.VISIBLE
                judulForShare = name

                listComment()

            } catch (e: JSONException) {
                Toast.makeText(this, "Data Tidak Tersedia", Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {        }
        rq.add(sr)
    }

    fun listComment(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, listComment_blog+idBlog, Response.Listener { response ->
            //========================================================================================= data from server
            Handler().postDelayed({ dialog.dismiss() }, 0)
            try {
                Log.d("aim",response)
                val respon = JSONObject(response)
                val dataObject = respon.getJSONObject("data")
                val dataArray = dataObject.getJSONArray("data")

                if (dataArray.length() != 0) {

                    for (i in 0 until dataArray.length()) {
                        if (i <= 5) {
                            val jo = dataArray.getJSONObject(i)
                            val id = jo.getString("id")

                            val member = jo.getJSONObject("member")
                            val nama = member.getString("nama")
                            val img = member.getString("photo")

                            val review = jo.getString("keterangan")
                            val tanggal = jo.getString("created")

                            val mItem = ReviewModel(id, nama, review, "", tanggal, img)
                            reviewBlog.add(mItem)
                        }
                    }
                    // ============ recycler_review ============
                    recycler_comment.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    val reviewAdapter = ReviewAdapter()
                    recycler_comment.adapter = reviewAdapter
                    reviewAdapter.setList(reviewBlog)
                    reviewBlog.clear()
                }

            } catch (e: JSONException) {
                Toast.makeText(this, "Data Tidak Tersedia", Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            Handler().postDelayed({dialog.dismiss()},0)
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {        }
        rq.add(sr)
    }

    fun postComment(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, comment_blog+idBlog, Response.Listener { response ->
            //========================================================================================= data from server
            Handler().postDelayed({ dialog.dismiss() }, 0)
            try {
                Log.d("aim","review Terkirim, $response")
                Reviewtext.setText("")
                listComment()
            } catch (e: JSONException) {
                Log.e("aim", "post review e: $e" + " id blog "+ idBlog)
//                Toast.makeText(this,"Data Tidak Tersedia",Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            Handler().postDelayed({dialog.dismiss()},0)
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer $token"
                return headers
            }

            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map["keterangan"] = Reviewtext.text.toString()
                return map
            }
        }
        rq.add(sr)
    }
}
