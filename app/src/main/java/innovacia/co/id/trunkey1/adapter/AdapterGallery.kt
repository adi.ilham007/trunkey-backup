package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.model.GalleryModel
import kotlinx.android.synthetic.main.list_gallery.view.*
import android.util.Log
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info


class AdapterGallery : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var galleryList = mutableListOf<GalleryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return galleryListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_gallery, parent, false))
    }
    override fun getItemCount(): Int = galleryList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as galleryListViewHolder

        viewHolder.bindView(galleryList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val image = galleryList[position].img
            Log.d("aim","preview : $position")

//            val dialog = Dialog(view.context)
//            dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
//            dialog .setCancelable(true)
//            dialog .setContentView(R.layout.list_image_preview)
//
//            val img = dialog.findViewById<ImageView>(R.id.preview)
//            Glide.with(view.context).load(image).into(img)
//            dialog .show()

            val i = Intent(view.context, ImagePreview::class.java)
            i.putExtra("position",position)
            view.context.startActivity(i)

        }

    }

    fun setList(listOfGallery: List<GalleryModel>) {
        this.galleryList = listOfGallery.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfGallery: List<GalleryModel>) {
        this.galleryList.addAll(listOfGallery)
        notifyDataSetChanged()
    }


    class galleryListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(eventModel: GalleryModel) {

            Glide.with(itemView.context).load(eventModel.img)
                    .fallback(R.drawable.ic_logo) //7
                    .into(itemView.file_logo)

        }
    }
}