package innovacia.co.id.trunkey1.model

data class NotifModel(
        val id      :String = "",
        val title   :String = "",
        val desc    :String = ""
)