package innovacia.co.id.trunkey1

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_referal.*
import android.widget.Toast
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import innovacia.co.id.trunkey1.helper.user_info
import java.lang.Exception


class Referal : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_referal)

        if (intent.hasExtra("caretaker")) {
            tx_note.visibility = View.VISIBLE
        }

//        profile_point.text = user_info.level.plus(" ").plus(user_info.activity).plus(" stars")
        if(user_info.referal_code!="null"|| user_info.referal_code!=""){
            refferal_value.text = user_info.referal_code

            val text = user_info.referal_code
            val multiFormatWriter = MultiFormatWriter()
            try {
                val bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 1000, 1000)
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                qr_code.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        textView39.setOnClickListener {
            textView39.startAnimation(user_info.animasiButton)
            val clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val myClip = ClipData.newPlainText("label", refferal_value.text)
            clipboard.primaryClip = myClip
            Toast.makeText(this, "Virtual code copied", Toast.LENGTH_SHORT).show()
        }

        share_lay.setOnClickListener {
            val message = "Hii.., We have key for successful parenting https://play.google.com/store/apps/details?id=innovacia.co.id.com.innovacia.trunkey1"

            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT,message)
            intent.type = "text/plain"
            try {
                startActivity(Intent.createChooser(intent, "Share to :"))
            }catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "There are no share clients installed.", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
