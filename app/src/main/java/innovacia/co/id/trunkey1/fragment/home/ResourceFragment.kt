package innovacia.co.id.trunkey1.fragment.home


import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.getSystemService
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.FragmentListing
import innovacia.co.id.trunkey1.ListingPagerAdapter
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.home_makanan
import innovacia.co.id.trunkey1.model.vendor_model
import kotlinx.android.synthetic.main.fragment_resource.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject

class ResourceFragment : Fragment() {

    lateinit var appContext: Context
    lateinit var viewContext: Context

    val listAll: kotlin.collections.MutableList<String> = java.util.ArrayList()

    val helper = user_info()
    val mVolleyHelper = VolleyHelper()

    var tagData = ""

    private val lokasi = arrayOf("Your Location")
    private var lokasiString = ""
    var ads: IntArray = intArrayOf(R.drawable.ads_a, R.drawable.ads_b, R.drawable.ads_c, R.drawable.ads_d)
    private val vendorData = mutableListOf<vendor_model>()

    // Permissions
    private var lokasi_gps = Manifest.permission.ACCESS_FINE_LOCATION
    private var lokasi_net = Manifest.permission.ACCESS_COARSE_LOCATION

    var latitude = user_info.latitude
    var longitude = user_info.longitude
    var idCategory = ""
//    var tagParent = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        appContext = activity!!.applicationContext

//        val a = activity?.applicationContext
//        val b = this.context!!
        return inflater.inflate(R.layout.fragment_resource, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        header_name?.text = resources.getString(R.string.explore_header)

        viewContext = view.context

        blogCategory()

        search?.setOnEditorActionListener { v, actionId, event ->
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(search?.windowToken, 0)

            val i = Intent(viewContext, home_makanan::class.java)
            i.putExtra("search", "blog")
            i.putExtra("text", search?.text.toString())
            startActivity(i)
            true
        }
    }

    fun blogCategory() {
        val errSection = "blogCategory"
        val rq = object : StringRequest(Request.Method.POST,user_info.blog_category, Response.Listener { response ->
            try {
//                Log.d("aim", response.toString())
                val fromServer = JSONObject(response)
                val dataArray = fromServer.getJSONArray("data")

                // ALL
                val pageAdapter = ListingPagerAdapter(childFragmentManager)
                val mFragmentAll = FragmentListing.newInstance(0,0,0,22)
                if (!mFragmentAll.isAdded) pageAdapter.add(mFragmentAll, "All")

                if (dataArray != null) {
                    for (i in 0 until dataArray.length()) {
                        val jo = dataArray.getJSONObject(i)
                        val idTag = jo.getInt("id")
                        val nama = jo.getString("nama")

                        val mFragmentCategory = FragmentListing.newInstance(i + 1,0,idTag,22)
                        if (!mFragmentAll.isAdded) pageAdapter.add(mFragmentCategory, nama)
                    }

                    // ============ tab_tag ============
                    viewpager_main.adapter = pageAdapter
                    tabs_main.setupWithViewPager(viewpager_main)
                    parentPage()
                }

            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }
        },Response.ErrorListener {
            response -> mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }

    fun parentPage(){
        Log.d("aim","TAB")
        val tagParent = activity?.intent?.getStringExtra("parentTag")
        Log.d("aim","tag : $tagParent")
        if (tagParent != null) {

            when (tagParent) {
                "1" -> viewpager_main.setCurrentItem(1,true) // Must read
                "3" -> viewpager_main.setCurrentItem(2,true) // Tips
                "4" -> viewpager_main.setCurrentItem(3,true) // Stories
                "7" -> viewpager_main.setCurrentItem(4,true) // Must Watch
            }
        }
    }

}
