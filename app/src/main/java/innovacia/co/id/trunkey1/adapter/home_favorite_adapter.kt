package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.list_home1.view.*
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.detail_vendor
import innovacia.co.id.trunkey1.home_makanan
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.vendor_model



class home_favorite_adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<vendor_model>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_home4, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as vendorListViewHolder

        viewHolder.bindView(vendorList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            var id_vendor = vendorList[position].id_vendor
            var category = vendorList[position].category

            if (id_vendor == "more") {
                // list by favorite (multiple vendor)
                val i = Intent(view.context, home_makanan::class.java)
                i.putExtra("category", "favorite")
                view.context.startActivity(i)
            }
            else if (id_vendor != "more"){
                val i = Intent(view.context, detail_vendor::class.java)
                i.putExtra("id", id_vendor)
                i.putExtra("category", category)
                view.context.startActivity(i)
            }

        }

    }
    fun setList(listOfVendor: List<vendor_model>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<vendor_model>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: vendor_model) {
//            itemView.id_vendor.text = vendorModel.id_vendor
//            itemView.nama.text      = vendorModel.nama
//
//            if (vendorModel.rating > 4){
//                itemView.distance.setBackgroundResource(R.drawable.bintang5)
//            } else {
//                itemView.distance.setBackgroundResource(R.drawable.bintang4)
//            }

            //Glide.with(itemView.context).load(vendorModel.file_logo).into(itemView.file_logo)

            Glide
                .with(itemView.context)
                .load(vendorModel.file_logo)
                .into(itemView.file_logo)

            if (vendorModel.id_vendor == "more"){
                itemView.file_logo.setBackgroundResource(R.mipmap.load_more)
//                itemView.distance.visibility = View.GONE
            }
        }
    }
}