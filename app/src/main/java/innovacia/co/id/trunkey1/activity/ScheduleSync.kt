package innovacia.co.id.trunkey1.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.PlaceAdapter
import innovacia.co.id.trunkey1.adapter.VendorSyncAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.PlaceModel
import innovacia.co.id.trunkey1.model.VendorSyncModel
import kotlinx.android.synthetic.main.activity_schedule_sync.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class ScheduleSync : AppCompatActivity() {

    private val mVendorSyncModel = mutableListOf<VendorSyncModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_sync)
        header_name?.text = resources.getString(R.string.sync_header)

        swipe_container?.setOnRefreshListener {
            populateSyncState()
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        populateSyncState()
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadCastReceiver, IntentFilter("vendorSync"))
    }
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            when (intent?.action) {
                "vendorSync" -> {
                    val childId = intent.getStringExtra("childId")
                    val vendorId = intent.getStringExtra("vendorId")
                    val vendorState = intent.getIntExtra("vendorState",2)
                    changeSyncState(childId,vendorId,vendorState)
                }
            }
        }
    }
    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadCastReceiver)
    }

    private fun populateSyncState() {
        val errSection = "populateSyncState :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.POST, user_info.place, Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                mVendorSyncModel.clear()
                val jsonObject = JSONObject(response)
                val dataVendor = jsonObject.getJSONArray("data")

                for (x in 0 until dataVendor.length()) {
                    val vendorObject = dataVendor.getJSONObject(x)
                    val childId = vendorObject.getString("id_child")
                    val childName = vendorObject.getString("child_name")
                    val vendorId = vendorObject.getString("id")
                    val vendorName = vendorObject.getString("nama")
                    val vendorState = vendorObject.getInt("is_synchronized")

                    val mItem = VendorSyncModel(childId, childName, vendorId, vendorName, vendorState)
                    mVendorSyncModel.add(mItem)
                }
                recycler_sync?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val mVendorSyncAdapter = VendorSyncAdapter()
                recycler_sync?.adapter = mVendorSyncAdapter
                mVendorSyncAdapter.setList(mVendorSyncModel)
            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["child"] = user_info.activeChildId
                return map
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }

    private fun changeSyncState(childId: String, vendorId: String, vendorState: Int) {
        val errSection = "changeSyncState :"
        swipe_container?.isRefreshing = true

        Log.d("aim","data masuk, id: $vendorId, state: $vendorState")

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.vendorSync+childId, Listener { response ->
            try{
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")


                populateSyncState()

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["eca_vendors"] = vendorId
                map["synchronized"] = "$vendorState"
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
