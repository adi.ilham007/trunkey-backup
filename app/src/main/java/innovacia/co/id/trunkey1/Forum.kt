package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_forum.*
import innovacia.co.id.trunkey1.fragment.forum.ForumPagerAdapter
import innovacia.co.id.trunkey1.helper.user_info

class Forum : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forum)

        viewpager_main.adapter = ForumPagerAdapter(supportFragmentManager)
//        viewpager_main.setOnTouchListener(View.OnTouchListener { v, event -> true })

        tabs_main.setupWithViewPager(viewpager_main)

        val page = viewpager_main.currentItem
        Log.d("aim","page forum : $page")
        if (page == 1) {
            post.visibility = View.GONE
        }

        footerHandling()

        post.setOnClickListener {
            post.startAnimation(user_info.animasiButton)
            if (!user_info.loginStatus) {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "forum_post")
                startActivity(i)
            } else {
                val i = Intent(this, ForumPost::class.java)
                startActivity(i)
            }
        }

    }

    fun footerHandling(){
        // ========================================================================================= Footer onClick
        try {
            forum_footer.setTextColor(resources.getColor(R.color.blue))
            forum_text.setTextColor(resources.getColor(R.color.blue))
        }catch (e: Exception) {
            Log.e("aim", "Err : $e")
        }

        // ========================================================================================= Footer
        home_footer.setOnClickListener {
            home_footer.startAnimation(user_info.animasiButton)
            val i = Intent(this, home::class.java)
            startActivity(i)
        }

        search_footer.setOnClickListener {
            search_footer.startAnimation(user_info.animasiButton)
            val i = Intent(this, Scan::class.java)
            startActivity(i)
        }

//        forum_footer.setOnClickListener{
//            user_info.statePosition = "forum"
//            forum_footer.startAnimation(user_info.animasiButton)
//            val i = Intent(this, Forum::class.java)
//            startActivity(i)
//        }

        profile_footer.setOnClickListener {
            profile_footer.startAnimation(user_info.animasiButton)

            if (!user_info.loginStatus) {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "profile")
                startActivity(i)
            } else {
                val i = Intent(this, Profile::class.java)
                startActivity(i)
            }
        }
    }
}
