package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.TrunkeyCareAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.TrunkeyCareChildModel
import innovacia.co.id.trunkey1.model.TrunkeyCareModel
import kotlinx.android.synthetic.main.activity_trunkey_care.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject

class TrunkeyCare : AppCompatActivity() {

    private val caretakerData = mutableListOf<TrunkeyCareModel>()
    private val childData = mutableListOf<TrunkeyCareChildModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trunkey_care)
        header_name.text = resources.getString(R.string.tx_care)

        back_link.setOnClickListener {
            onBackPressed()
        }

        swipe_container.setOnRefreshListener {
            careList()
        }

        btn_add.setOnClickListener {
            btn_add.startAnimation(user_info.animasiButton)
            val i = Intent(this,Referal::class.java)
            i.putExtra("caretaker","caretaker")
            startActivity(i)
        }

        careList()

    }

    override fun onResume() {
        super.onResume()
        careList()
    }

    private fun careList(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.GET, user_info.care_list, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                dialog.dismiss()
                Log.i("AIM", response.toString())

                val jsonObject = JSONObject(response)
                val ja = jsonObject.getJSONArray("data")
                for (i in 0 until ja.length()) {
                    val driverObject = ja.getJSONObject(i)
                    val id = driverObject.getString("id")
                    val nama = driverObject.getString("nama")
                    val type = driverObject.getString("type")
                    val photo = driverObject.getString("thumbnail_logo")
                    val status = driverObject.getInt("status")


                    if (driverObject.has("children")) {

                        childData.clear()
                        val childArray = driverObject.getJSONArray("children")

                        for (x in 0 until childArray.length()) {
                            val childObject = childArray.getJSONObject(x)
                            val childId = childObject.getString("id_member")
                            val childName = childObject.getString("child_name")
                            val childPhoto = childObject.getString("child_photo_path")

                            val timeObject = childObject.getJSONObject("format_checkin")
                            val jam = timeObject.getString("hour")
                            val menit = timeObject.getString("minute")
                            val time = "$jam:$menit"

                            val childItem = TrunkeyCareChildModel(childId, childName, "", time, childPhoto)
                            childData.add(childItem)
                        }
                    }

                    val caretakerItem = TrunkeyCareModel(id,nama,type,photo,status,childData)
                    caretakerData.add(caretakerItem)
                }

                val filterCareTaker = caretakerData.sortedWith(compareBy { it.status })
                caretakerData.clear()

                recycler_caretaker.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val caretakerAdapter = TrunkeyCareAdapter()
                recycler_caretaker.adapter = caretakerAdapter
                caretakerAdapter.setList(filterCareTaker)

            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

//                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")

                    if (errCode == "422") {
                        val i = Intent(this,TrunkeyCareDescription::class.java)
                        startActivity(i)
                        finish()
                    }

                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
