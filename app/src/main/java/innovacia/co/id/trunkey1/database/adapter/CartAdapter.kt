package innovacia.co.id.trunkey1.database.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.database.cart.CartEntity
import innovacia.co.id.trunkey1.database.cart.CartRespository
import innovacia.co.id.trunkey1.helper.user_info
import android.util.SparseBooleanArray

//      https://medium.com/nusanet/cara-membuat-multiple-view-type-recyclerview-kotlin-android-a407fcb22b22
//      https://codinginfinite.com/recycler-view-scroll-issue/

private val itemStateArray = SparseBooleanArray()

class CartAdapter : RecyclerView.Adapter<CartAdapter.CartListViewHolder> {
    val helper = user_info()
    var mContext: Context
    var mRepository: CartRespository
    var data: List<CartEntity>? = null
    var isSelectedAll = false

    constructor(context: Context?, data: List<CartEntity>, repository: CartRespository){
        this.mContext       = context!!
        this.data           = data
        this.mRepository    = repository
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartListViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.list_cart, parent, false)
        return CartListViewHolder(v)
    }

    override fun getItemCount(): Int {
        if(data != null)
            return data!!.size
        else
            return 0
    }

    override fun onBindViewHolder(holder: CartListViewHolder, position: Int) {
        val mCartEntity = data!!.get(position)
        holder.checkBox.isChecked = itemStateArray.get(position)

//        val totalCashBack = mCartEntity.itemQty * mCartEntity.itemCashBack
        holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                holder.checkBox.isChecked = true
                itemStateArray.put(position, true)
                val intent = Intent("cart")
                intent.putExtra("method", "+")
                intent.putExtra("idDB", mCartEntity.id)
                intent.putExtra("id", mCartEntity.itemId)
                intent.putExtra("whatBuy", mCartEntity.itemType)
                intent.putExtra("seat", mCartEntity.itemQty)
                intent.putExtra("harga", mCartEntity.itemPrice)
                intent.putExtra("cashBack", mCartEntity.itemCashBack)
                intent.putExtra("member", mCartEntity.itemMember)
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
                Log.d("aim", "++  $position, ${mCartEntity.itemMember}")
            } else {
                holder.checkBox.isChecked = false
                itemStateArray.put(position, false)
                val intent = Intent("cart")
                intent.putExtra("method", "-")
                intent.putExtra("idDB", mCartEntity.id)
                intent.putExtra("id", mCartEntity.itemId)
                intent.putExtra("whatBuy", mCartEntity.itemType)
                intent.putExtra("seat", mCartEntity.itemQty)
                intent.putExtra("harga", mCartEntity.itemPrice)
                intent.putExtra("cashBack", mCartEntity.itemCashBack)
                intent.putExtra("member", mCartEntity.itemMember)
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
                Log.d("aim", "--  $position, ${mCartEntity.itemMember}")
            }
        }

        Glide.with(mContext).load(mCartEntity.merchantLogo).into(holder.vendorFoto)

        holder.vendorNama.text = mCartEntity.merchantNama
        holder.vendorAlamat.text = mCartEntity.merchantAlamat
        holder.itemNama.text = mCartEntity.itemNama
        holder.itemHarga.text = helper.bank(mCartEntity.itemPrice)
        val dateTime = mCartEntity.tanggal.split(" ")
        Log.d("aim",dateTime.toString())
        holder.itemTanggal.text = dateTime[0]
        holder.itemWaktu.text = dateTime[1]
        holder.itemQty.text = mCartEntity.itemQty.toString()

        holder.hapus.setOnClickListener {
            val builder = AlertDialog.Builder(mContext)
            builder.setMessage("Remove from cart ?")
                .setPositiveButton("Yes") {
                    dialog, which ->
                    mRepository.deleteEntry(mCartEntity)
                    notifyDataSetChanged()
                }
                .setNegativeButton("No") {
                    dialog, which ->

                }.show()
        }
    }

    fun setDatas(data: List<CartEntity>){
        this.data = data
    }

    fun selectAll() {
        itemStateArray.clear()
        for (i in 0 until itemCount) {
            itemStateArray.put(i, true)
        }
        val intent = Intent("cart")
        intent.putExtra("method", "enable")
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
        notifyDataSetChanged()
    }

    fun unSelectAll() {
        itemStateArray.clear()
        for (i in 0 until itemCount) {
            itemStateArray.put(i, false)
        }
        val intent = Intent("cart")
        intent.putExtra("method", "disable")
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent)
        notifyDataSetChanged()
    }

    fun reset() {
        itemStateArray.clear()
//        notifyDataSetChanged()
    }


    class CartListViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        val vendorFoto      : ImageView = itemView.findViewById(R.id.vendor_foto)
        val checkBox        : CheckBox = itemView.findViewById(R.id.checkBox)
        val hapus           : TextView = itemView.findViewById(R.id.hapus)

        var vendorNama      : TextView = itemView.findViewById(R.id.vendor_nama)
        var vendorAlamat    : TextView = itemView.findViewById(R.id.vendor_alamat)

        var itemNama        : TextView = itemView.findViewById(R.id.item_nama)
        var itemHarga       : TextView = itemView.findViewById(R.id.item_harga)
        var itemTanggal     : TextView = itemView.findViewById(R.id.item_tanggal)
        var itemWaktu       : TextView = itemView.findViewById(R.id.item_waktu)
        var itemQty         : TextView = itemView.findViewById(R.id.item_qty)
        var itemSeparator   : TextView = itemView.findViewById(R.id.item_separator)

    }
}