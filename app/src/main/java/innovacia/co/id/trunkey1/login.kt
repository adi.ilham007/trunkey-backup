package innovacia.co.id.trunkey1

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.Firebase
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_login.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import org.json.JSONException
import org.json.JSONObject

class login : AppCompatActivity() {

    var back = ""
    val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        generatedView()

        imageView?.setOnClickListener {
            val blue = R.color.blue
            val gray = R.color.gray
            val orange = R.color.colorPrimary

            Log.d("aim","blue : $blue, gray : $gray, orange : $orange")
        }

        btnLogin.setOnClickListener {
            btnLogin.startAnimation(user_info.animasiButton)
            if (check_filled()){
                getfirebaseToken()
                cekLogin()
            }
        }

        KeyboardVisibilityEvent.setEventListener(this,object: KeyboardVisibilityEvent(), KeyboardVisibilityEventListener {
            override fun onVisibilityChanged(isOpen: Boolean) {
                if(isOpen)
                {
                    bukaKeyboard()
                }
                else
                    tutupKeyboard()
                    Log.d("aim","tutup Keyboard")
            }

        })


        btnSignup.setOnClickListener {
            btnSignup.startAnimation(user_info.animasiButton)
            val i = Intent(this, Register::class.java)
            startActivity(i)
        }

        btnForgot.setOnClickListener {
            btnForgot.startAnimation(user_info.animasiButton)
            forgotDialog()
        }
    }

    private fun bukaKeyboard()
    {
        val helper = user_info()
        params.setMargins(0,helper.dpToPx(this,30),0,0)
        params.gravity = Gravity.CENTER
        btnLogin.layoutParams = params
        btnLogin.layoutParams.height = helper.dpToPx(this,40)
        btnLogin.layoutParams.width = helper.dpToPx(this,150)

        params.setMargins(0,helper.dpToPx(this,20),0,0)
        linear_spasi.layoutParams = params
        imageView.visibility = View.GONE
    }
    private fun tutupKeyboard()
    {
        val helper = user_info()
        params.setMargins(0,helper.dpToPx(this,100),0,0)
        params.gravity = Gravity.CENTER
        btnLogin.layoutParams = params
        btnLogin.layoutParams.height = helper.dpToPx(this,40)
        btnLogin.layoutParams.width = helper.dpToPx(this,150)

        params.setMargins(0,helper.dpToPx(this,100),0, 0)
        linear_spasi.layoutParams = params
        imageView.visibility = View.VISIBLE
    }

    private fun generatedView() {
        if (intent.getStringExtra("back") != null) {
            back = intent.getStringExtra("back")
        }
    }

//    override fun onBackPressed() {
//        super.onBackPressed()
//        finish()
//    }

    private fun check_filled(): Boolean{
        var cek = 0

        if(user.text.length < 5){
            user_lay.error = "At least five characters"; cek++
        } else { user_lay.error = "" }

        if(pass.text.length < 5){
            pass_lay.error = "At least five characters"; cek++
        } else { pass_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun forgotDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_forgot)

        val reset = dialogs.findViewById<TextView>(R.id.reset)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val resetVariable = reset.text.toString()
            forgotPassword(resetVariable)
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus?.windowToken, InputMethodManager.SHOW_FORCED)
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        dialogs.show()
    }
    private fun forgotPassword(resetVariable:String){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        Log.d("aim","reset = $resetVariable")

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.forgot, Response.Listener { response ->
            try {
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                val status = jsonObject.getInt("status")

                if (status == 1) {
                    Toast.makeText(this,R.string.forgot_password,Toast.LENGTH_LONG).show()
                } else {
                    val message = jsonObject.getString("status")
                    Toast.makeText(this,message,Toast.LENGTH_LONG).show()
                    forgotDialog()
                }

            }catch (e: JSONException) {
                Toast.makeText(this,e.toString(),Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
                forgotDialog()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }

        }) {
            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["username"] = resetVariable.replace("+62","0")
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun getfirebaseToken(){
        val firebase = Firebase()
        firebase.getFirebaseToken()
    }

    private fun cekLogin(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.login, Response.Listener { response ->
            try {
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                user_info.token = jsonObject.getString("access_token")
                user_info.refresh = jsonObject.getString("refresh_token")

                Log.d("aim","Token : ${jsonObject.getString("access_token")}")

                val data = jsonObject.getJSONObject("data")
                user_info.userName = data.getString("username")
                user_info.name = data.getString("nama")
                user_info.alamat = data.getString("alamat")
                user_info.email = data.getString("email")
                user_info.phone = data.getString("no_telp")
                user_info.photo = data.getString("photo")
                user_info.gender = data.getString("gender")

                val reader = this.getSharedPreferences("my_preferences", Context.MODE_PRIVATE)
                val editor = reader.edit()
                editor.putString("access_token",jsonObject.getString("access_token"))
                editor.putString("refresh_token",jsonObject.getString("refresh_token"))
                editor.apply()

                user_info.loginStatus = true
                when (back) {
                    "profile" -> {
                        finish()
                        val i = Intent(this, Profile::class.java)
                        startActivity(i)
                    }
                    "forum_post" -> {
                        finish()
                        val i = Intent(this, ForumPost::class.java)
                        startActivity(i)
                    }
                    "event" -> {
                        getProfile()
                        //onBackPressed()
                    }
                    else -> {
                        finish()
                    }
                }
            }catch (e: JSONException) {
                Toast.makeText(this,e.toString(),Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }

        }) {
            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["username"] = user.text.toString().replace("+62","0")
                map["password"] = pass.text.toString()
                map["fcm_token"] = user_info.FCM
                Log.d("aim","FCM : ${user_info.FCM}")
                return map
            }
        }
        rq.add(sr)
    }

    private fun getProfile(){
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.profile, Response.Listener { response ->
            try {
                Log.i("AIM", response.toString())
                val jsonObject = JSONObject(response)
                val status = jsonObject.getInt("status")
                if (status == 1) {
                    val jo = jsonObject.getJSONObject("data")

                    // User
                    val user = jo.getJSONObject("user")

                    // Balance
                    val balance = jo.getJSONObject("balance")
                    if (balance.getInt("status") == 1) {
                        user_info.walletStatus = true
                        user_info.wallet = balance.getDouble("balance")
                        user_info.cashBack = user.getDouble("points")
                    } else {
                        user_info.walletStatus = false
                        user_info.wallet = 0.0
                        user_info.cashBack = 0.0
                    }
                }

                onBackPressed()

            }catch (e: Exception) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                val code = networkResponse.statusCode
                if (code == 401) {
//                    refreshToken()
                } else {
                    try {
                        val err = String(networkResponse.data)
                        val jsonObj = JSONObject(err)
                        val errCode   = jsonObj.getString("code")
                        val errMessage   = jsonObj.getString("message")

                        Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                        Log.d("aim", "Err code : $errCode, $err")
                    } catch (e: JSONException) {
                        Log.e("aim", "Err : $e")
                    }
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()

                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }

}
