package innovacia.co.id.trunkey1

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.andrognito.patternlockview.PatternLockView
import com.andrognito.patternlockview.listener.PatternLockViewListener
import com.andrognito.patternlockview.utils.PatternLockUtils
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.Result
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.helper.CustomViewFinder
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_scan.*
import kotlinx.android.synthetic.main.sub_header.*
import me.dm7.barcodescanner.core.IViewFinder
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Text
import java.util.HashMap

class Scan : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private var cameraPermission = Manifest.permission.CAMERA
    private var mScannerView: ZXingScannerView? = null
    private var isCaptured = false
    private var QRcode = ""
    private var childPin = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)
        header_name?.text = resources.getString(R.string.scan_header)

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        initScannerView()
    }

    private fun initScannerView()  = runWithPermissions(cameraPermission) {
        mScannerView = object : ZXingScannerView(this) {
            override fun createViewFinderView(context: Context?): IViewFinder {
                return CustomViewFinder(context!!)
            }
        }

        if (mScannerView != null) {
            mScannerView!!.startCamera()
            mScannerView!!.setAutoFocus(true)
            mScannerView!!.setResultHandler(this)
            frame_layout_camera.addView(mScannerView)
        }
    }

    override fun onStart() {
        super.onStart()
        if (mScannerView != null) {
            mScannerView!!.startCamera()
            initScannerView()
        }
    }
//
//    override fun onPause() {
//        super.onPause()
//        if (mScannerView != null) {
//            mScannerView!!.stopCamera()
//        }
//    }

    override fun handleResult(p0: Result?) {
        Log.d("aim","Scanner: $p0")
        if(p0 != null) {
            QRcode = p0.toString()
            addChild()
//            childValidation()
        }

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }

    private fun childValidation(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.addChildScan, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim",response)
            val jsonObject = JSONObject(response)
            val data = jsonObject.getJSONObject("data")
            val nama = data.getString("nama")

            val dataSekolah = data.getJSONObject("school")
            val sekolah = dataSekolah.getString("nama")

            confirmDialog(nama,sekolah)

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["qr_code"] = QRcode
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun confirmDialog(nama:String,sekolah:String) {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.popup_add_child)

        val name = dialogs.findViewById<TextView>(R.id.childName)
        val school = dialogs.findViewById<TextView>(R.id.childSchool)

        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        val mPattern = dialogs.findViewById<PatternLockView>(R.id.patternLockView)
        mPattern.addPatternLockListener(object : PatternLockViewListener {
            override fun onStarted() {

            }

            override fun onProgress(progressPattern: MutableList<PatternLockView.Dot>?) {

            }

            override fun onComplete(pattern: MutableList<PatternLockView.Dot>?) {
                Log.d("aim", "Pattern complete: " +
                        PatternLockUtils.patternToString(mPattern, pattern))
                if (  PatternLockUtils.patternToString(mPattern, pattern).count()< 4) {
                    mPattern.setViewMode(PatternLockView.PatternViewMode.WRONG);
                    Toast.makeText(applicationContext,"Connect at least 4 points",Toast.LENGTH_LONG).show()
                }
                else {
                    childPin = PatternLockUtils.patternToString(mPattern, pattern)
                    Toast.makeText(applicationContext,"Pattern Saved",Toast.LENGTH_LONG).show()
                }
                Log.d("aim","pattern : $childPin")
            }

            override fun onCleared() {

            }
        })

//        val pattern = dialogs.findViewById<PatternLockView>(R.id.patternLockView)
//
//        pattern.setOnPatternListener(object : PatternLockView.OnPatternListener {
//
//            override fun onComplete(ids: ArrayList<Int>): Boolean {
//                if (ids.size < 4) {
//                    Toast.makeText(applicationContext,"Connect at least 4 points",Toast.LENGTH_LONG).show()
//                } else {
//                    childPin = ids.joinToString().replace(",","").replace(" ","")
//                }
//                Log.d("aim","pattern : $childPin")
//                return true
//
//            }
//        })

        name.text = nama
        school.text = sekolah

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            addChild()
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            finish()
            val i = Intent(this, Scan::class.java)
            startActivity(i)
        }

        dialogs.show()
    }

    private fun addChild(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.addChildScan2, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim",response)
            val jsonObject = JSONObject(response)
            val status = jsonObject.getInt("status")
            val data = jsonObject.getJSONObject("data")
            val id = data.getString("id")

            if (status == 1) {
                finish()
//                childSetPin(id)
            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
            finish()
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["qr_code"] = QRcode
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun childSetPin(id:String){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.setPin+id, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim",response)
            val jsonObject = JSONObject(response)
            val status = jsonObject.getInt("status")

            if (status == 1) {
                finish()
                Toast.makeText(this,"Done",Toast.LENGTH_LONG).show()
                val i = Intent(this, ManageChild::class.java)
                startActivity(i)

            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["pin"] = childPin
                map["pin_confirmation"] = childPin
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
