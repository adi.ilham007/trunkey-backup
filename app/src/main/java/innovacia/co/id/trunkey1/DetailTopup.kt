package innovacia.co.id.trunkey1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.ListBankAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ListBankModel
import kotlinx.android.synthetic.main.activity_detail_topup.*
import kotlinx.android.synthetic.main.sub_header.*


import org.json.JSONException
import org.json.JSONObject

class DetailTopup : AppCompatActivity() {

    private val namabankModel = mutableListOf<ListBankModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_topup)
        header_name?.text = resources.getString(R.string.detail_topup_header)


        val helper = user_info()
        tx_IDR.text = helper.bank(user_info.wallet)

        back_link.setOnClickListener{
            onBackPressed()
        }
//        panah_detail.setOnClickListener {
//            panah_detail.startAnimation(user_info.animasiButton)
//            val i = Intent(this, Scan::class.java)
//
//            startActivity(i)
//        }

//        fun copyText(view: View) {
//            myClip = ClipData.newPlainText("text", et_copy_text.text);
//            myClipboard?.setPrimaryClip(myClip);
//
//            Toast.makeText(this, "Text Copied", Toast.LENGTH_SHORT).show();
//        }

//        top_up.setOnClickListener {
//            top_up.startAnimation(user_info.animasiButton)
//            var i = Intent(this, ListingNew::class.java)
//
//            startActivity(i)
//        }
//
//        billing.setOnClickListener {
//            billing.startAnimation(user_info.animasiButton)
//            var i = Intent(this, ListingNew::class.java)
//
//            startActivity(i)
//        }


        shimmerWallet.startShimmerAnimation()
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.data_bank, Response.Listener { response ->
            try {
                shimmerWallet.stopShimmerAnimation()
                val jsonObject = JSONObject(response)

                val jsonArray = jsonObject.getJSONArray("data")
                for (i in 0 until jsonArray.length()) {  // ======================================================== custom take
                    val jo = jsonArray.getJSONObject(i)
                    val imgbank  = jo.getString("img_src")
                    val namabank = jo.getString("bank_name_full")
                    val kodeVA = jo.getString("bank_account_no")
                    val kodebank = jo.getString("bank_code")
                    val topUpInstruction = jo.getString("topup_tutorial")
                    val id_user = jo.getString("trunkey_wallet_account_name")
                    id_trunkey.text = id_user


                    val mItem = ListBankModel(imgbank,namabank, kodeVA, kodebank,topUpInstruction)
                    namabankModel.add(mItem)
                    Log.d("aim",mItem.toString())
                }

                // ============ recycler_nearme ============
                recycler_listbank.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val nama_bankAdapter = ListBankAdapter()
                recycler_listbank.adapter = nama_bankAdapter
                nama_bankAdapter.setList(namabankModel)
                namabankModel.clear()

            }
            catch (e: Exception) {
                Log.e("aim", "top up detail e: $e")
            }
        }, Response.ErrorListener { response ->
            shimmerWallet.stopShimmerAnimation()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }){
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()

                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)



    }
}


