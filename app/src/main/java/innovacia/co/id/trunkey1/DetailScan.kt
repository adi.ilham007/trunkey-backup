package innovacia.co.id.trunkey1

import android.content.Intent
import kotlinx.android.synthetic.main.activity_detail_scan.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import innovacia.co.id.trunkey1.helper.user_info

class DetailScan : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_scan)

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        if (intent.getStringExtra("qr_code") != null) {
            name.text = intent.getStringExtra("qr_code")
        }

        pay.setOnClickListener {
            pay.startAnimation(user_info.animasiButton)
        }

        checkin.setOnClickListener {
            checkin.startAnimation(user_info.animasiButton)
        }

        promo.setOnClickListener {
            promo.startAnimation(user_info.animasiButton)
        }

        footerHandling()

    }

    private fun footerHandling(){
        // ========================================================================================= Footer onClick
        try {
            search_footer.setTextColor(resources.getColor(R.color.blue))
            search_text.setTextColor(resources.getColor(R.color.blue))
        }catch (e: Exception) {
            Log.e("aim", "Err : $e")
        }

        // ========================================================================================= Footer
        home_footer.setOnClickListener {
            home_footer.startAnimation(user_info.animasiButton)
            val i = Intent(this, home::class.java)
            startActivity(i)
        }

//        search_footer.setOnClickListener {
//            search_footer.startAnimation(user_info.animasiButton)
//            user_info.statePosition = "scan"
//            val i = Intent(this, Scan::class.java)
//            startActivity(i)
//        }

        forum_footer.setOnClickListener{
            forum_footer.startAnimation(user_info.animasiButton)
            val i = Intent(this, Forum::class.java)
            startActivity(i)
        }

        profile_footer.setOnClickListener {
            profile_footer.startAnimation(user_info.animasiButton)

            if (!user_info.loginStatus) {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "profile")
                startActivity(i)
            } else {
                val i = Intent(this, Profile::class.java)
                startActivity(i)
            }
        }
    }
}
