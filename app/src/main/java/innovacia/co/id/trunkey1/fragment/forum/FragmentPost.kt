package innovacia.co.id.trunkey1.fragment.forum

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.ForumAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ForumModel
import kotlinx.android.synthetic.main.fragment_fragment_post.*
import org.json.JSONException
import org.json.JSONObject

class FragmentPost : Fragment() {

    private val forumData = mutableListOf<ForumModel>()

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser)
            Handler().postDelayed({
                if (activity != null) {
                    loadForum()
                }
            }, 200) // 200
        super.setUserVisibleHint(isVisibleToUser)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_post, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe_container.setOnRefreshListener {
            loadForum()
        }
    }

    fun loadForum() {
        try {
            Log.d("aim","fragment")
            swipe_container.isRefreshing = true
            val rq: RequestQueue = Volley.newRequestQueue(activity)
            val sr = object : StringRequest(Request.Method.POST, user_info.forum, Response.Listener { response ->
                Log.d("aim", "forum = $response")
                val jsonObject = JSONObject(response)
                val dataObject = jsonObject.getJSONObject("data")
                val dataArray = dataObject.getJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    val jo = dataArray.getJSONObject(i)

                    val id = jo.getString("id")
                    val contentDesc = jo.getString("keterangan")
                    val contentPhoto = jo.getString("link_foto")
                    Log.d("aim","content photo : $contentPhoto")
                    val comment = jo.getString("total_reply")
                    val like = jo.getString("total_like")

                    val like_status = jo.getInt("is_like")

                    val member = jo.getJSONObject("member")
                    val id_member = member.getString("id")
                    val userName = member.getString("nama")
                    val userPhoto = member.getString("photo")

                    val mItem = ForumModel(id,id_member,userName,userPhoto,contentDesc,contentPhoto,comment,like,like_status)
                    forumData.add(mItem)
                }

                // ============ recycler_vendor ============
                recyclerView.layoutManager = LinearLayoutManager(activity, OrientationHelper.VERTICAL, false)
                val forumAdapter = ForumAdapter()
                recyclerView.adapter = forumAdapter
                forumAdapter.setList(forumData)
                forumData.clear()
                swipe_container.isRefreshing = false

            }, Response.ErrorListener { response ->
                swipe_container.isRefreshing = false
                val networkResponse = response.networkResponse
                if (networkResponse == null){
                    Toast.makeText(activity, R.string.connFail,Toast.LENGTH_LONG).show()
                }
                else {
                    try {
                        val err = String(networkResponse.data)
                        val jsonObj = JSONObject(err)
                        val errCode   = jsonObj.getString("code")
                        val errMessage   = jsonObj.getString("message")

                        Toast.makeText(activity, errMessage, Toast.LENGTH_LONG).show()
                        Log.d("aim", "Err code : $errCode, $err")
                    } catch (e: JSONException) {
                        Log.e("aim", "Err : $e")
                    }
                }
            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = java.util.HashMap<String, String>()
                    headers["Accept"] = "application/json"
//                    headers["Content-Type"] = "application/json"
                    headers["Authorization"] = "Bearer ${user_info.token}"
                    return headers
                }

                override fun getParams(): MutableMap<String,String> {
                    val map = HashMap<String, String>()
                    map["take"] = "10"
                    return map
                }
            }
            sr.retryPolicy = DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )
            rq.add(sr)
        }catch (e: Exception) {
            Log.e("aim", "Err academic : $e")
        }
    }
}
