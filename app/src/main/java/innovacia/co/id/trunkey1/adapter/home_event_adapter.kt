package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.model.vendor_model
import kotlinx.android.synthetic.main.list_home3.view.*
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info

class home_event_adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<vendor_model>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_home3, parent, false))
    }

    override fun getItemCount(): Int {
        return  Int.MAX_VALUE
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as vendorListViewHolder

        viewHolder.bindView(vendorList[position % vendorList.size])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val idMix = vendorList[position % vendorList.size].id_vendor
            val category = vendorList[position % vendorList.size].category
            val tipe = vendorList[position % vendorList.size].tipe

            when (tipe) {
                "vendor" -> {
                    val i = Intent(view.context, detail_vendor::class.java)
                    i.putExtra("id", idMix)
                    i.putExtra("category", category)
                    view.context.startActivity(i)
                }
                "course" -> {
                    val i = Intent(view.context, Course::class.java)
                    i.putExtra("slug", idMix)
                    view.context.startActivity(i)
                }
                "event" -> {
                    val i = Intent(view.context, Event::class.java)
                    i.putExtra("slug", idMix)
                    view.context.startActivity(i)
                }
                "product" -> {

                }
                else -> {
                    Log.d("aim","unknown : $tipe")
                }
            }

        }
    }
    fun setList(listOfVendor: List<vendor_model>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<vendor_model>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: vendor_model) {


            itemView.card_nama.text = vendorModel.nama
            itemView.id_vendor.text = vendorModel.id_vendor

//            itemView.card_tempat.text = vendorModel.alamat
//            itemView.card_bulan.text = vendorModel.
//            itemView.card_tanggal.text = vendorModel.tanggal_mulai
            val tanggalMulai = vendorModel.tanggal_mulai
            val tanggalAkhir = vendorModel.tanggal_akhir

            val bulanAwal = tanggalMulai.subSequence(5,7)
            val bulanAkhir = tanggalAkhir.subSequence(5,7)
            if(tanggalMulai.subSequence(8,10).toString() == tanggalAkhir.subSequence(8,10).toString()){
                itemView.tanggal_view.text = tanggalMulai.subSequence(8,10).toString()
            }else{
                itemView.tanggal_view.text = tanggalMulai.subSequence(8,10).toString()+ " - " +tanggalAkhir.subSequence(8,10).toString()
            }


            var bulanAwalTxt = ""
            var bulanAkhirTxt = ""

            when (bulanAwal) {
                "01" -> bulanAwalTxt = "JAN"
                "02" -> bulanAwalTxt = "FEB"
                "03" -> bulanAwalTxt = "MARCH"
                "04" -> bulanAwalTxt = "APR"
                "05" -> bulanAwalTxt = "MAY"
                "06" -> bulanAwalTxt = "JUN"
                "07" -> bulanAwalTxt = "JUL"
                "08" -> bulanAwalTxt = "AUG"
                "09" -> bulanAwalTxt = "SEP"
                "10" -> bulanAwalTxt = "OCT"
                "11" -> bulanAwalTxt = "NOV"
                "12" -> bulanAwalTxt = "DES"
            }
            when (bulanAkhir) {
                "01" -> bulanAkhirTxt = "JAN"
                "02" -> bulanAkhirTxt = "FEB"
                "03" -> bulanAkhirTxt = "MARCH"
                "04" -> bulanAkhirTxt = "APR"
                "05" -> bulanAkhirTxt = "MAY"
                "06" -> bulanAkhirTxt = "JUN"
                "07" -> bulanAkhirTxt = "JUL"
                "08" -> bulanAkhirTxt = "AUG"
                "09" -> bulanAkhirTxt = "SEP"
                "10" -> bulanAkhirTxt = "OCT"
                "11" -> bulanAkhirTxt = "NOV"
                "12" -> bulanAkhirTxt = "DES"
            }

            if(bulanAwalTxt != bulanAkhirTxt){
                itemView.bulan.text = bulanAwalTxt +" - "+bulanAkhirTxt
            }else{
                itemView.bulan.text = bulanAwalTxt
            }

            var tags = ""
            if(vendorModel.tags != ""){
                tags = " - " +vendorModel.tags
            }
            itemView.desc.text = vendorModel.category_name + tags

            Glide.with(itemView.context).load(vendorModel.file_logo)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(itemView.file_logo)

        }
    }
}