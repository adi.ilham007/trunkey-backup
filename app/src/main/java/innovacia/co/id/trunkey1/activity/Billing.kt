package innovacia.co.id.trunkey1.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.BillingAdapter
import innovacia.co.id.trunkey1.adapter.ChildAdapter2
import innovacia.co.id.trunkey1.helper.SnapHelperOneByOne
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.BillingModel
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.activity_billing.*
import kotlinx.android.synthetic.main.sub_header_rv.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class Billing : AppCompatActivity() {

    private val helper = user_info()
    private val mChildModel = mutableListOf<ChildModel>()
    private val mBillingModel = mutableListOf<BillingModel>()

    private var page = 1
    private var maxPage = 1
    private var loadMore = false
    private var redeemStatus = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_billing)
        header_name?.text = resources.getString(R.string.billing_header)

        swipe_container?.setOnRefreshListener {
            mBillingModel.clear()
            page = 1
            redeemStatus = ""
            getBilling()
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        cacheHandling()
        spinnerFilter()
        rvListener()
    }

    private fun cacheHandling() { if (user_info.restoreKids == null) getKids() else restoreKidsFun() }

    private fun getKids() {
        val errSection = "getKids :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                user_info.activeChildId = ""
                user_info.childCarouselState = 0

                mChildModel.clear()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restoreKids = dataArray
                if (dataArray.length() > 0) {
                    // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val gender = childObject.getString("gender")
                        val birthday = childObject.getString("birthday")
                        val foto = childObject.getString("photo")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val status = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                        mChildModel.add(mItem)

                        // Add child option
                        if (i+1 == dataArray.length()) {
                            val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                            mChildModel.add(addChild)
                        }
                    }
                } else {
                    for (i in 0 until 1) {
                        val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                        mChildModel.add(addChild)
                    }
                }

                val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                recycler_child?.layoutManager = mLinearLayoutManager

                // Triger carousel to center
                recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)

                val displayMetrics = this.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 320) / 2
                val pxWidth = helper.dpToPx(this, calculation.toInt())
                recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (recycler_child?.onFlingListener == null) {
                    val snapHelper = SnapHelperOneByOne()
                    snapHelper.attachToRecyclerView(recycler_child)
                }

                // Large Carousel
                ChildAdapter2.TYPE_OF_VIEW = 1
                val mChildAdapter = ChildAdapter2()
                recycler_child?.adapter = mChildAdapter
                mChildAdapter.setList(mChildModel)

                kidsCarouselListener(mLinearLayoutManager)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }
    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {
            // Default see all
//            if (dataArray.length() > 1) {
//                val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                mChildModel.add(seeAll)
//            }

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val gender = childObject.getString("gender")
                val birthday = childObject.getString("birthday")
                val foto = childObject.getString("photo")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val status = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                mChildModel.add(mItem)

                // Add child option
                if (i+1 == dataArray.length()) {
                    val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                    mChildModel.add(addChild)
                }
            }
        } else {
            for (i in 0 until 1) {
                val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                mChildModel.add(addChild)
            }
        }

        val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
        recycler_child?.layoutManager = mLinearLayoutManager

        // Handle carousel position (restore or not)
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }

        val displayMetrics = this.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 320) / 2
        val pxWidth = helper.dpToPx(this, calculation.toInt())
        recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (recycler_child?.onFlingListener == null) {
            val snapHelper = SnapHelperOneByOne()
            snapHelper.attachToRecyclerView(recycler_child)
        }

        // Large Carousel
        ChildAdapter2.TYPE_OF_VIEW = 1
        val mChildAdapter = ChildAdapter2()
        recycler_child?.adapter = mChildAdapter
        mChildAdapter.setList(mChildModel)

        kidsCarouselListener(mLinearLayoutManager)
    }
    private fun kidsCarouselListener(mLinearLayoutManager: LinearLayoutManager) {
        recycler_child?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val snapHelper = SnapHelperOneByOne()
                if(newState == RecyclerView.TOUCH_SLOP_PAGING && newState != RecyclerView.NO_POSITION) {
                    Handler().postDelayed({
                        val centerView = snapHelper.findSnapView(mLinearLayoutManager)
                        if (centerView != null) {
                            val pos = mLinearLayoutManager.getPosition(centerView)
                            val getId = mChildModel[pos % mChildModel.size].id
                            page = 1

                            user_info.activeChildId = getId
                            user_info.childCarouselState = pos
                            Log.d("aim", "active child id : $getId")

                            mBillingModel.clear()

                            getBilling()
                        }
                    }, 500)
                }
            }
        })
    }

    private fun spinnerFilter(){

//        `0` (belum terbayar atau overdue)
//        `1` (sukses / terbayar)
//        `2` (parsial)
//        `4` (cancel)

        val spinnerData = arrayOf("All","Active","Paid","Routine","Cancel")
        val arrayAdapter = ArrayAdapter(this, R.layout.spinner_txt_white, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner?.adapter = arrayAdapter

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                page = 1
                val mBillingAdapter = BillingAdapter()

                when (spinnerData[position]){
                    "All" -> {
                        redeemStatus = ""
                        mBillingModel.clear()
                        mBillingAdapter.notifyDataSetChanged()
                        getBilling()
                    }
                    "Active" -> {
                        redeemStatus = "0"
                        mBillingModel.clear()
                        mBillingAdapter.notifyDataSetChanged()
                        getBilling()
                    }
                    "Paid" -> {
                        redeemStatus = "1"
                        mBillingModel.clear()
                        mBillingAdapter.notifyDataSetChanged()
                        getBilling()
                    }
                    "Routine" -> {
                        redeemStatus = "2"
                        mBillingModel.clear()
                        mBillingAdapter.notifyDataSetChanged()
                        getBilling()
                    }
                    "Cancel" -> {
                        redeemStatus = "4"
                        mBillingModel.clear()
                        mBillingAdapter.notifyDataSetChanged()
                        getBilling()
                    }
                }
                Log.d("aim","spinner clicked")
            }
        }
    }

    private fun rvListener() {
        recycler_billing?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if(isLastPosition && page < maxPage){
                    page++
//                    progress_bar_wallet.visibility = View.VISIBLE
                    loadMore = true

                    getBilling()

                }
            }
        })
    }

    private fun getBilling() {
        val errSection = "getBilling :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.POST,user_info.billing+"?page=$page", Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data")
                maxPage = data.getInt("last_page")

                val dataArray = data.getJSONArray("data")
                for(i in 0 until dataArray.length()) {
                    val billing = dataArray.getJSONObject(i)
                    val id = billing.getString("id")
                    val nama = billing.getString("nama")
                    val orderDate = billing.getString("tanggal")
                    val expiredDate = billing.getString("due_date")
                    val harga = billing.getDouble("besar")
                    val desc = helper.textHtml(billing.getString("keterangan"))
                    val origin = billing.getString("origin_status")

                    val student = billing.getString("member_name")
                    val vendorObject = billing.getJSONObject("vendor")
                    val vendorName = vendorObject.getString("nama")

                    val mItem = BillingModel(id,nama,orderDate,expiredDate,harga,origin,desc,student,vendorName)
                    mBillingModel.add(mItem)
                }

                if (loadMore) {
                    val recyclerViewState = recycler_billing?.layoutManager?.onSaveInstanceState()
                    val mBillingAdapter = BillingAdapter()
                    recycler_billing?.adapter = mBillingAdapter
                    mBillingAdapter.setList(mBillingModel)
                    recycler_billing?.layoutManager?.onRestoreInstanceState(recyclerViewState)
                } else {
                    recycler_billing?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    val mBillingAdapter = BillingAdapter()
                    recycler_billing?.adapter = mBillingAdapter
                    mBillingAdapter.setList(mBillingModel)
                }

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["children"] = user_info.activeChildId
                map["redeem_status"] = redeemStatus
                return map
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }

    fun filterByStatus(status: String){
        val mBillingAdapter = BillingAdapter()
        val filterResult = mBillingModel.filter { it.status == status }
        filterResult.sortedBy { it.status.toInt() }
        mBillingAdapter.setList(filterResult)
        recycler_billing.adapter = mBillingAdapter
    }

    fun filterDisable(){
        val mBillingAdapter = BillingAdapter()
        mBillingAdapter.setList(mBillingModel)
        recycler_billing.adapter = mBillingAdapter
    }
}
