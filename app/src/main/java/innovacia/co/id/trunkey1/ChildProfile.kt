package innovacia.co.id.trunkey1

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.andrognito.patternlockview.PatternLockView
import com.andrognito.patternlockview.listener.PatternLockViewListener
import com.andrognito.patternlockview.utils.PatternLockUtils
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import innovacia.co.id.trunkey1.adapter.VendorSyncAdapter
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.VendorSyncModel
import kotlinx.android.synthetic.main.activity_child_profile.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class ChildProfile : AppCompatActivity() {

    private val mVolleyHelper = VolleyHelper()

    private var orange = ""
    private var blue = ""
    private var gray = ""

    private var childPin = ""

    private var id = ""
    private var name = ""
    private var gender = ""
    private var birth = ""
    private var photo = ""
    private var theme = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_child_profile)
        header_name?.text = resources.getString(R.string.child_header)


        orange = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.colorPrimary, null)).replace("ff","").capitalize()
        blue = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.blue, null)).replace("ff","").capitalize()
        gray = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.gray, null)).replace("ff","").capitalize()

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        swipe_container?.setOnRefreshListener {
            generateView()
        }

        childPin_btn?.setOnClickListener {
            childPin_btn?.startAnimation(user_info.animasiButton)
            setPinDialog()
        }

        editProfile?.setOnClickListener {
            editProfile?.startAnimation(user_info.animasiButton)
            val i = Intent(this, EditProfileChild::class.java)
            i.putExtra("name",name)
            i.putExtra("gender",gender)
            i.putExtra("birth",birth)
            i.putExtra("theme",theme)
            i.putExtra("photo",photo)
            startActivity(i)
        }

        btn_remove.setOnClickListener {
            btn_remove.startAnimation(user_info.animasiButton)
            removeChildDialog()
        }

        generateView()
    }

    override fun onResume() {
        super.onResume()
        Log.d("aim","ON RESUME")
        getKids(id)
    }

    private fun generateView() {
        if (intent.hasExtra("id")) {
            id = intent.getStringExtra("id")
            getKids(id)
        }
    }

    private fun getKids(id:String) {
        val errSection = "getKids :"
        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.detailChild+id, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data")

                name = data.getString("nama")
                gender = data.getString("gender")
                birth = data.getString("birthday")
                theme = data.getString("color_theme").capitalize()
                photo = data.getString("photo")

                val codeQR = data.getString("qr_code")
                val multiFormatWriter = MultiFormatWriter()
                try {
                    val bitMatrix = multiFormatWriter.encode(codeQR, BarcodeFormat.QR_CODE, 512, 512)
                    val barcodeEncoder = BarcodeEncoder()
                    val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                    qr_code.setImageBitmap(bitmap)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }

                val card = data.getString("image_card")
                if (card != "0"){
                    try { Glide.with(this).load(card)
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .into(kartu)
                    } catch (e:Exception){ Log.d("aim","glide err : $e") }

                    qr_code.visibility = View.GONE
                    kartu.visibility = View.VISIBLE
                } else {
                    qr_code.visibility = View.VISIBLE
                    kartu.visibility = View.GONE
                }

                name_value.text = name.capitalize()
                nama_sementara.text = name
                birthday_value.text = birth
                gender_value.text = gender.capitalize()

                Log.d("aim","blue : $blue, gray : $gray")
                Log.d("aim","theme : $theme")

                when (theme) {
                    blue -> { banner_lay?.setBackgroundResource(R.color.blue) }
                    gray -> { banner_lay?.setBackgroundResource(R.color.gray) }
                    else -> {banner_lay?.setBackgroundResource(R.color.colorPrimary)}
                }

                try { Glide.with(this).load(photo)
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(child_photo)
                } catch (e:Exception){ Log.d("aim","glide err : $e") }

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }

    private fun setPinDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.popup_add_child)

        val name = dialogs.findViewById<TextView>(R.id.childName)
        val school = dialogs.findViewById<TextView>(R.id.childSchool)

        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

//        val mPattern : PatternLockView = findViewById(R.id.patternLockView)
        val mPattern = dialogs.findViewById<PatternLockView>(R.id.patternLockView)
        mPattern.addPatternLockListener(object : PatternLockViewListener {
            override fun onStarted() {

            }

            override fun onProgress(progressPattern: MutableList<PatternLockView.Dot>?) {

            }

            override fun onComplete(pattern: MutableList<PatternLockView.Dot>?) {
                Log.d("aim", "Pattern complete: " +
                        PatternLockUtils.patternToString(mPattern, pattern))
                if (  PatternLockUtils.patternToString(mPattern, pattern).count()< 4) {
                    mPattern.setViewMode(PatternLockView.PatternViewMode.WRONG);
                    Toast.makeText(applicationContext,"Connect at least 4 points",Toast.LENGTH_LONG).show()
                }
                else {
                    childPin = PatternLockUtils.patternToString(mPattern, pattern)
                    Toast.makeText(applicationContext,"Pattern Saved",Toast.LENGTH_LONG).show()
                }
                Log.d("aim","pattern : $childPin")
            }

            override fun onCleared() {

            }
        })

//        val pattern = dialogs.findViewById<PatternLockView>(R.id.patternLockView)
//        pattern.setOnPatternListener(object : PatternLockView.OnPatternListener {
//            override fun onComplete(ids: ArrayList<Int>): Boolean {
//                if (ids.size < 4) {
//                    Toast.makeText(applicationContext,"Connect at least 4 points",Toast.LENGTH_LONG).show()
//                } else {
//                    childPin = ids.joinToString().replace(",","").replace(" ","")
//                    Toast.makeText(applicationContext,"Pattern Saved",Toast.LENGTH_LONG).show()
//                }
//                Log.d("aim","pattern : $childPin")
//                return true
//
//            }
//        })



        name.text = user_info.activeChildName
        school.visibility = View.GONE

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            childSetPin(user_info.activeChildId)
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun childSetPin(id:String){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.setPin+id, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim",response)

            Toast.makeText(this,"Done",Toast.LENGTH_LONG).show()
            onBackPressed()

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["pin"] = childPin
                map["pin_confirmation"] = childPin
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun removeChildDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        val desc = "Are you sure to delete $name data ?"
        textNotif.text = desc

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            removeChild()
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun removeChild() {
        val errSection = "removeChild"

        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq = object : JsonObjectRequest(Request.Method.POST,user_info.removeChild+id,null, Response.Listener { response ->
            try {
                dialog.dismiss()
                Log.d("aim","$errSection: $response")

                if (response.getInt("status") == 1) {
                    user_info.restoreKids = null

                    val intent = Intent(this, HomeBaru::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }

            } catch (e: JSONException) {
                Log.d("aim","$errSection: $e")
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }
}
