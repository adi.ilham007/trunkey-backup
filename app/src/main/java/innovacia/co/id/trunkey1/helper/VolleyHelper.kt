package innovacia.co.id.trunkey1.helper

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.VolleyError
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap



class VolleyHelper {

    fun header(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"

        if (user_info.loginStatus || user_info.token != "") {
            headers["Authorization"] = "Bearer ${user_info.token}"
        }

        return headers
    }

    fun errHandling(context:Context, response:VolleyError, tag:String) {
        val networkResponse = response.networkResponse
        if (networkResponse == null) {
            Toast.makeText(context, innovacia.co.id.trunkey1.R.string.connFail, Toast.LENGTH_LONG).show()
        }
        else {
            try {
                val err = String(networkResponse.data)
                val jsonObj = JSONObject(err)
                val errCode = jsonObj.getString("code")
                val errMessage = jsonObj.getString("message")

                if (errCode == "401") {
                    user_info.loginStatus = false
                    Log.d("aim", "$tag : $errCode, $err")
                } else {
                    Toast.makeText(context, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$tag : $errCode, $err")
                }

            } catch (e: JSONException) {
                Log.d("aim", "$tag : $e")
            }
        }

    }
}