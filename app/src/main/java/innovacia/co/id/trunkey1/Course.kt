package innovacia.co.id.trunkey1

import android.Manifest
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.text.Html
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.adapter.AdapterGallery
import innovacia.co.id.trunkey1.adapter.TiketAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.GalleryModel
import innovacia.co.id.trunkey1.model.TiketModel
import kotlinx.android.synthetic.main.activity_course.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.activity_course.direction
//import kotlinx.android.synthetic.main.activity_course.harga
//import kotlinx.android.synthetic.main.activity_course.logo_lay
//import kotlinx.android.synthetic.main.activity_course.nama_vendor
import kotlinx.android.synthetic.main.activity_course.share
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class Course : AppCompatActivity() {

    private var phone_permission = Manifest.permission.CALL_PHONE

    private val listGallery = mutableListOf<GalleryModel>()
    private val tiketModel = mutableListOf<TiketModel>()

    val helper = user_info()

    var idArray = ArrayList<Int>()
    var seatArray = ArrayList<Int>()
    var hargaArray = ArrayList<Double>()
    val cashBackArray = ArrayList<Double>()

    private var phone = ""
    private var file = ""
    private var idVendor = 0
    private var mNamaVendor = ""
    private var mPlace = ""
    private var latitude = 0.0
    private var longitude = 0.0
    private var slug = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course)
        header_name.text = "Course Page"

        generateView()

        swipe_container.setOnRefreshListener {
            detailCourse()
        }

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

//        logo_lay.setOnClickListener {
//            logo_lay.startAnimation(user_info.animasiButton)
//            val i = Intent(this, detail_vendor::class.java)
//            i.putExtra("id", idVendor.toString())
//            startActivity(i)
//        }

        direction.setOnClickListener {
            direction.startAnimation(user_info.animasiButton)
            if (latitude != 0.0 && longitude != 0.0) {
                val i = Intent(this, MapsActivity::class.java)
                i.putExtra("nama", "test")
                i.putExtra("lat", latitude)
                i.putExtra("lon", longitude)
                startActivity(i)
            } else {
                Toast.makeText(this, "Location not available", Toast.LENGTH_LONG).show()
            }
        }

        share.setOnClickListener {
            val message = "Hii.., We have key for successful parenting https://play.google.com/store/apps/details?id=innovacia.co.id.com.innovacia.trunkey1"

            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT,message)
            intent.type = "text/plain"
            try {
                startActivity(Intent.createChooser(intent, "Share to :"))
            }catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "There are no share clients installed.", Toast.LENGTH_LONG).show()
            }
        }

        // =========================================================================================
//        val i = Intent(this,DetailBilling::class.java)
//        i.putExtra("idBilling",id)
//        i.putExtra("student",student)
//        startActivity(i)

        btn_buy.setOnClickListener {
            btn_buy.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                if (seatArray.sum() != 0) {
                    val i = Intent(this, list_tiket_detail::class.java)
                    i.putExtra("class", "class")
                    i.putExtra("slug", slug)
                    i.putExtra("idArray", idArray)
                    i.putExtra("seatArray", seatArray)
                    i.putExtra("hargaArray", hargaArray)
                    i.putExtra("totalHarga", hargaArray.sum())
                    i.putExtra("totalCashBack", cashBackArray.sum())
                    Log.d("aim", "kirim harga: $hargaArray")
                    Log.d("aim", "kirim id: $idArray")
                    startActivity(i)
                } else {
                    Toast.makeText(this, "Choose at least 1 course", Toast.LENGTH_LONG).show()
                }
            } else {
                if (!user_info.loginStatus) {
                    val i = Intent(this, login::class.java)
                    i.putExtra("back", "event")
                    startActivity(i)
                } else if (!user_info.walletStatus) {
                    activatedWalletDialog()
                }
            }
        }
    }

//    override fun onStart() {
//        super.onStart()
//        LocalBroadcastManager.getInstance(this)
//                .registerReceiver(broadCastReceiver, IntentFilter("qty"))
//    }
//
//    private val broadCastReceiver = object : BroadcastReceiver() {
//        override fun onReceive(contxt: Context?, intent: Intent?) {
//
//            when (intent?.action) {
//                "qty" -> {
//                    val idTiket = intent.getIntExtra("id",0)
//                    val seatTiket = intent.getIntExtra("seat",0)
//                    val hargaTiket = intent.getDoubleExtra("harga",0.0)
//                    val cashBack = intent.getDoubleExtra("cashBack",0.0)
//
////                    recycler_tiket.findViewHolderForAdapterPosition(index)!!.itemView.Amount.text.toString()
//
//                    if (idArray.contains(idTiket)){
//                        val a = idArray.indexOf(idTiket)
//                        if (seatTiket != 0) {
//                            idArray[a] = idTiket
//                            seatArray[a] = seatTiket
//                            hargaArray[a] = hargaTiket
//                            cashBackArray[a] = cashBack
//                        } else {
//                            idArray.removeAt(a)
//                            seatArray.removeAt(a)
//                            hargaArray.removeAt(a)
//                            cashBackArray.removeAt(a)
//                        }
//                    } else {
//                        idArray.add(idTiket)
//                        seatArray.add(seatTiket)
//                        hargaArray.add(hargaTiket)
//                        cashBackArray.add(cashBack)
//                    }
//
//                    Log.d("aim","all id: $idArray")
//                    Log.d("aim","all seat: $seatArray")
//                    Log.d("aim","all harga: $hargaArray")
//                    Log.d("aim","all cashBack: $cashBackArray")
//
//                    harga.text = helper.bank(hargaArray.sum())
//                    cashback_value.text = helper.bank(cashBackArray.sum())
//
//
////                    Log.d("aim","index: $index, id: $id, harga: $harga, tiket: $indexSum")
//
//                }
//            }
//        }
//    }
//
//    override fun onStop() {
//        super.onStop()
//        LocalBroadcastManager.getInstance(this)
//                .unregisterReceiver(broadCastReceiver)
//    }

    fun phone_call() = runWithPermissions(phone_permission) {
        Log.d("aim","ini course")

        try {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:+$phone")
            startActivity(intent)
        }catch (e: Exception) {
//            Log.e("aim ", "phone e: $e")
            Toast.makeText(this,"Data tidak tersedia",Toast.LENGTH_LONG).show()
        }

        //call lama
//        try {
//            val intent = Intent(Intent.ACTION_CALL)
//            intent.data = Uri.parse("tel:$phone")
//            startActivity(intent)
//        }catch (e: Exception) {
//            Toast.makeText(this,"Data tidak tersedia",Toast.LENGTH_LONG).show()
//        }
    }

    private fun generateView() {
        if (intent.hasExtra("data")) {
            // From notification / FCM
            slug = intent.getStringExtra("data")
            detailCourse()
        } else if (intent.hasExtra("slug")) {
            // From home
            slug = intent.getStringExtra("slug")
            detailCourse()
        }
        Log.d("aim",slug)
    }

    fun detailCourse(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : JsonObjectRequest(Request.Method.GET, user_info.detail_course+slug, null, Response.Listener { response ->
            //========================================================================================= data from server
            Log.d("aim",response.toString())
            swipe_container.isRefreshing = false
            dialog.dismiss()
            try {
                val data = response.getJSONObject("data")

                // Data Event
                val id = data.getInt("id")
                val banner = data.getString("wide_logo")

                Glide.with(this).load(banner)
                        .apply(RequestOptions.skipMemoryCacheOf(true)) //
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .into(image)

                course_name.text = data.getString("nama")
                tx_rating.text = data.getString("rating")

                val startDate = data.getString("tanggal_mulai")
                val endDate = data.getString("tanggal_akhir")

                course_date.text = "$startDate - $endDate"




                about.text = helper.textHtml(data.getString("keterangan"))

//                if (!data.isNull("photos")) {
//                    val galleryArray = data.getJSONArray("photos")
//                    user_info.gallery = galleryArray
//                    if (galleryArray.length() != 0) {
//                        for (i in 0 until galleryArray.length()) {
//                            val jo = galleryArray.getJSONObject(i)
//                            val id = jo.getString("id")
//                            val img = jo.getString("photo_path")
//
//                            val mItem = GalleryModel(id,img)
//                            listGallery.add(mItem)
//                        }
//                        // ============ recycler_course ============
//                        recycler_gallery.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL,false)
//                        val galleryAdapter = AdapterGallery()
//                        recycler_gallery.adapter = galleryAdapter
//                        galleryAdapter.setList(listGallery)
//                    } else {
////                        txGallery.visibility = View.GONE
//                        recycler_gallery.visibility = View.GONE
//                    }
//
//                }

//                harga.text = "IDR 0"

                //                rating.text = data.getString("")

                // Wallet Status
//                val walletObject = data.optJSONObject("is_wallet")
//                val walletStatus = walletObject.getInt("status")
//                if (walletStatus != 0) {
//                    user_info.walletStatus = true
//                    user_info.wallet = walletObject.getDouble("balance")
//                    Log.d("aim","wallet ${user_info.walletStatus}, ${user_info.wallet}")
//                } else {
//                    user_info.walletStatus = false
//                    user_info.wallet = 0.0
//                    Log.d("aim","wallet ${user_info.walletStatus}, ${user_info.wallet}")
//                }

                // Data Vendor
//                val vendor = data.getJSONObject("vendor")
//                val vendorPhoto = vendor.getString("photo_path")
//
//                Glide.with(this).load(vendorPhoto)
//                        .apply(RequestOptions.skipMemoryCacheOf(true)) //
//                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
//                        .into(vendor_photo)
//
//                mPlace = vendor.getString("alamat")
//                latitude = vendor.getDouble("latitude")
//                longitude = vendor.getDouble("longitude")
//
//                idVendor = vendor.getInt("id")
//                mNamaVendor = vendor.getString("nama")
//                vendor_name.text = mNamaVendor
//                vendor_addrs.text = helper.textHtml(mPlace)

            } catch (e: Exception) {
                Log.e("aim", "Event err: $e")
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container.isRefreshing = false
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {        }
        rq.add(sr)
    }



    fun getReview(idVendor:Int){
//        val builder = AlertDialog.Builder(this)
//        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
//        builder.setView(dialogView)
//        builder.setCancelable(false)
//        val dialog = builder.create()
//        dialog.show()
//
//        val rq: RequestQueue = Volley.newRequestQueue(this)
//        val sr = object : StringRequest(Request.Method.POST, user_info.listReview_course+idVendor, Response.Listener { response ->
//            //========================================================================================= data from server
//            Handler().postDelayed({ dialog.dismiss() }, 0)
//            swipe_container.isRefreshing = false
//            try {
//                val respon = JSONObject(response)
//                val data = respon.getJSONObject("data")
//                val detailReview = data.getJSONObject("detail_review")
//
//                // Rating Angka
//                val limaObject = detailReview.getJSONObject("5")
//                textView114.text = limaObject.getInt("jumlah").toString()
//                val empatObject = detailReview.getJSONObject("4")
//                textView30.text = empatObject.getInt("jumlah").toString()
//                val tigaObject = detailReview.getJSONObject("3")
//                textView32.text = tigaObject.getInt("jumlah").toString()
//                val duaObject = detailReview.getJSONObject("2")
//                textView44.text = duaObject.getInt("jumlah").toString()
//                val satuObject = detailReview.getJSONObject("1")
//                textView46.text = satuObject.getInt("jumlah").toString()
//
//                val review = data.getJSONObject("reviews")
//                val reviewArray = review.getJSONArray("data")
//
//                if (reviewArray.length() != 0) {
//    //                review_state++
//
//                    val totalReview = reviewArray.length()
//                    textView23.text = totalReview.toString().plus(" Reviews")
//
//                    progressBar.max = totalReview
//                    progressBar.progress = limaObject.getInt("jumlah")
//                    progressBar2.max = totalReview
//                    progressBar2.progress = empatObject.getInt("jumlah")
//                    progressBar3.max = totalReview
//                    progressBar3.progress = tigaObject.getInt("jumlah")
//                    progressBar4.max = totalReview
//                    progressBar4.progress = duaObject.getInt("jumlah")
//                    progressBar5.max = totalReview
//                    progressBar5.progress = satuObject.getInt("jumlah")
//
//                    for (i in 0 until reviewArray.length()) {
//                        if (i <= 5) {
//                            val jo = reviewArray.getJSONObject(i)
//                            val id = jo.getString("id")
//
//                            val member = jo.getJSONObject("member")
//                            val nama = member.getString("nama")
//                            val img = member.getString("photo")
//
//                            var review = ""
//                            if(jo.getString("keterangan") != null){
//                                review = jo.getString("keterangan")
//                            }
//
//                            val rating = jo.getString("rating")
//                            val tanggal = jo.getString("created")
//
//                            val mItem = ReviewModel(id, nama, review, rating, tanggal, img)
//                            reviewVendor.add(mItem)
//                        }
//                    }
//                    review_lay.visibility = View.VISIBLE
//                    detail_review.visibility = View.VISIBLE
//                    separator_review.visibility = View.VISIBLE
//                    // ============ recycler_review ============
//                    recycler_review.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
//                    val reviewAdapter = ReviewAdapter()
//                    recycler_review.adapter = reviewAdapter
//                    reviewAdapter.setList(reviewVendor)
//                    reviewVendor.clear()
//                }
//
//            } catch (e: JSONException) {
//                Toast.makeText(this, "Data Tidak Tersedia", Toast.LENGTH_LONG).show()
//            }
//
//        }, Response.ErrorListener { response ->
//            //========================================================================================= error handling
//            Handler().postDelayed({dialog.dismiss()},0)
//            swipe_container.isRefreshing = false
//            val networkResponse = response.networkResponse
//            if (networkResponse == null){
//                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
//            }
//            else {
//                try {
//                    val err = String(networkResponse.data)
//                    val jsonObj = JSONObject(err)
//                    val errCode   = jsonObj.getString("code")
//                    val errMessage   = jsonObj.getString("message")
//
//                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
//                    Log.d("aim", "Err code : $errCode, $err")
//                } catch (e: JSONException) {
//                    Log.e("aim", "Err : $e")
//                }
//            }
//        }) {
//            override fun getHeaders(): MutableMap<String, String> {
//                val headers = java.util.HashMap<String, String>()
//                headers["Accept"] = "application/json"
////                    headers["Content-Type"] = "application/json"
//                headers["Authorization"] = "Bearer ${user_info.token}"
//                return headers
//            }
//        }
//        rq.add(sr)
    }

    private fun activatedWalletDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        textNotif.text = resources.getText(R.string.activated_wallet)

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val i = Intent(this, Profile::class.java)
            i.putExtra("from", "event")
            i.putExtra("lastActivity", slug)
            startActivity(i)
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        dialogs.show()
    }
}
