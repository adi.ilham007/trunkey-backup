package innovacia.co.id.trunkey1.model

data class FeatureModel(
    var id: String,
    var nama: String,
    var desc: String,
    var link: String,
    val photo: String
)