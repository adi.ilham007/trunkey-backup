package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import innovacia.co.id.trunkey1.DetailBilling
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.RaportModel
import kotlinx.android.synthetic.main.list_billing_active.view.*
import kotlinx.android.synthetic.main.list_billing_history.view.*
import kotlinx.android.synthetic.main.list_score.view.*



class RaportAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var raportData = mutableListOf<RaportModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BillingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_score,parent,false))
    }

    override fun getItemCount(): Int = raportData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        val bindHolder = viewHolder as BillingViewHolder
        bindHolder.bindView(raportData[position])

//        val id = raportData[position].id
//        val student = raportData[position].student
        bindHolder.itemView.setOnClickListener { view ->
//            val i = Intent(view.context,DetailBilling::class.java)
//            i.putExtra("idBilling",id)
//            i.putExtra("student",student)
//            view.context.startActivity(i)
        }
    }

    fun setList(listOfChild: List<RaportModel>) {
        this.raportData = listOfChild.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfTransaction: List<RaportModel>) {
        this.raportData.addAll(listOfTransaction)
        notifyDataSetChanged()
    }
    fun reset() {
        raportData.removeAll(raportData)
        notifyDataSetChanged()
    }

    class BillingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()
        @SuppressLint("SetTextI18n")
        fun bindView(mRaportModel: RaportModel) {
            itemView.logo.text = mRaportModel.vendor_name[0].toString().capitalize()
            itemView.vendor_name.text = mRaportModel.vendor_name.capitalize()

            val childName = mRaportModel.child_name.split(" ")
            itemView.class_name.text = "${childName[0].capitalize()} - ${mRaportModel.class_name.capitalize()}"

            val tableLayout by lazy { TableLayout(itemView.context) }
            val helper = user_info()
            val lp = TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            tableLayout.apply {
                layoutParams = lp
                isShrinkAllColumns = true
            }
            for (i in 0 until mRaportModel.raport.size) {
                val row = TableRow(itemView.context)
                row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                if (i % 2 == 0) {
                    row.setBackgroundResource(R.color.light_gray)
                } else {
                    row.setBackgroundResource(R.color.textWhite)
                }


                val globalRaport = mRaportModel.raport[i].split(",")
                for (j in 0 until globalRaport.size) {
                    val text = TextView(itemView.context)
//                    text.textSize = helper.dpToPx(itemView.context, 9).toFloat()
//                    text.setBackgroundResource(R.drawable.table_border)

//                    if (j == 0) {
//                        text.text = globalRaport[0]
//                        val p = TableRow.LayoutParams()
//                        p.width = helper.dpToPx(itemView.context, 50)
//                        p.height = helper.dpToPx(itemView.context, 50)
//                        p.weight = 1F
//                        p.rightMargin = helper.dpToPx(itemView.context, 0) // right-margin = 10dp
//                        text.layoutParams = p
//                    }
                    if (j == 0) {
                        text.gravity = Gravity.CENTER_VERTICAL or Gravity.START
                        text.text = globalRaport[0].capitalize()
                        text.setSingleLine()
                        val p = TableRow.LayoutParams()
                        p.height = helper.dpToPx(itemView.context, 50)
                        p.weight = 9F
                        p.setMargins(helper.dpToPx(itemView.context, 5),0,0,0)

                        text.layoutParams = p
                    }
                    if (j == 1) {
                        text.gravity = Gravity.CENTER_VERTICAL or Gravity.END
                        text.text = globalRaport[1]
                        text.setSingleLine()
                        val p = TableRow.LayoutParams()
                        p.height = helper.dpToPx(itemView.context, 50)
                        p.weight =  1F
                        p.setMargins(0,0,helper.dpToPx(itemView.context, 5),0)
                        text.layoutParams = p
                    }
                    row.addView(text)
                }
                tableLayout.addView(row)
            }
            itemView.table_lay.addView(tableLayout)
        }
    }
}