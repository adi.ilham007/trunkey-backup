package innovacia.co.id.trunkey1.model


data class transaction_model(
        var payment_category    :String,
        var payment_value       :String,
        var bank                :String,
        var payment_response    :String,
        var payment_date        :String
)