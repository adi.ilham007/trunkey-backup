package innovacia.co.id.trunkey1.adapter

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.MapsActivity
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.TrunkeyCareHistory
import innovacia.co.id.trunkey1.TrunkeyCareInfo
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.model.TrunkeyCareModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_trunkey_care.view.*
import kotlinx.android.synthetic.main.list_trunkey_care_request.view.*

class   TrunkeyCareAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var careTakerData = mutableListOf<TrunkeyCareModel>()

    companion object {
        private const val REQUEST = 0
        private const val LISTING = 1
    }

    override fun getItemViewType(position: Int): Int {
        val comparable = careTakerData[position].status
        return when (comparable) {
            0 -> REQUEST
            else -> LISTING
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            REQUEST -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_trunkey_care_request, parent, false))
            else -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_trunkey_care, parent, false))
        }
    }
    override fun getItemCount(): Int = careTakerData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder
        holder.bindView(careTakerData[position])

        val idDriver = careTakerData[position].id
        val nameDriver = careTakerData[position].nama
        val photoDriver = careTakerData[position].photo

        when (holder.itemViewType) {
            REQUEST -> {
                holder.itemView.setOnClickListener {
                    val i = Intent(it.context,TrunkeyCareInfo::class.java)
                    i.putExtra("request_id",idDriver)
                    it.context.startActivity(i)
                }
            }
            LISTING -> {
                holder.itemView.btn_Info.setOnClickListener {
                    holder.itemView.startAnimation(user_info.animasiButton)
                    val i = Intent(it.context, TrunkeyCareInfo::class.java)
                    i.putExtra("id", idDriver)
                    it.context.startActivity(i)
                }

                holder.itemView.btn_maps.setOnClickListener {
                    holder.itemView.btn_maps.startAnimation(user_info.animasiButton)
                    val i = Intent(it.context, MapsActivity::class.java)
                    i.putExtra("id", idDriver)
                    i.putExtra("name", nameDriver)
                    it.context.startActivity(i)
                }

                holder.itemView.btn_history.setOnClickListener {
                    holder.itemView.btn_history.startAnimation(user_info.animasiButton)
                    val i = Intent(it.context, TrunkeyCareHistory::class.java)
                    i.putExtra("id", idDriver)
                    i.putExtra("name", nameDriver)
                    i.putExtra("photo", photoDriver)
                    it.context.startActivity(i)
                }
            }
        }

    }
    fun setList(listOfVendor: List<TrunkeyCareModel>) {
        this.careTakerData = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<TrunkeyCareModel>) {
        this.careTakerData.addAll(listOfVendor)
        notifyDataSetChanged()
    }

    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(model: TrunkeyCareModel) {
            when (itemViewType) {
                REQUEST -> {
                    itemView.request_name.text = model.nama
                    itemView.request_type.text = model.type
                    Glide.with(itemView.context).load(model.photo).into(itemView.request_photo)
                }
                LISTING -> {
                    itemView.caretaker_name.text = model.nama
                    itemView.caretaker_type.text = model.type
                    Glide.with(itemView.context).load(model.photo).into(itemView.photo)

                    itemView.recycler_child.layoutManager = LinearLayoutManager(itemView.context, OrientationHelper.VERTICAL, false)
                    val childAdapter = TrunkeyCareChildAdapter()
                    itemView.recycler_child.adapter = childAdapter
                    childAdapter.setList(model.child)
                }
            }
        }
    }

}