package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.activity.NewsDetail
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.NewsModel
import innovacia.co.id.trunkey1.model.activity_review_model
import kotlinx.android.synthetic.main.list_news.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat

class NewsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    val helper = user_info()
    private var newsData = mutableListOf<NewsModel>()

    companion object {
        var TYPE_OF_VIEW = 0
    }

    override fun getItemViewType(position: Int): Int {
//        TYPE_OF_VIEW = if (newsData[position].id.toInt() %2 == 0) { 0 } else { 1 }
        return 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_news, parent, false))
    }

    override fun getItemCount(): Int = newsData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        val bindHolder = viewHolder as NewsViewHolder
        bindHolder.bindView(newsData[position])

        bindHolder.itemView.setOnClickListener { view ->
            val i = Intent(view.context,NewsDetail::class.java)
            i.putExtra("newsId",newsData[position].id)
            i.putExtra("childId",newsData[position].childId)
            view.context.startActivity(i)
        }
    }

    fun setList(listOfChild: List<NewsModel>) {
        this.newsData = listOfChild.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfTransaction: List<NewsModel>) {
        this.newsData.addAll(listOfTransaction)
        notifyDataSetChanged()
    }
    fun reset() {
        newsData.removeAll(newsData)
        notifyDataSetChanged()
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()
        @SuppressLint("SetTextI18n")
        fun bindView(mNewsModel: NewsModel) {

//            val attachment = mNewsModel.attachment
//            var attachmentString = ""
//            for (i in 0 until attachment.size) {
//                val link = attachment[i].split("/")
//                val fileName = link[link.lastIndex]
//                attachmentString = "$fileName\n"
//            }
//
//            itemView.download?.visibility = View.VISIBLE
//            itemView.file_name?.text = attachmentString

            val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val outputFormat:DateFormat = SimpleDateFormat("dd MMM yyyy")
            val inputDateStr = mNewsModel.newsDate
            val date = inputFormat.parse(inputDateStr)
            val outputDateStr = outputFormat.format(date)
//            itemView.tanggal?.text = mNewsModel.newsDate
            itemView.tanggal?.text = outputDateStr
            itemView.vendor_name?.text = mNewsModel.vendorName
            itemView.news_title?.text = mNewsModel.newsTitle
            itemView.news_desc?.text = mNewsModel.newsDesc
            itemView.due_date?.text = mNewsModel.dueDate



            if (mNewsModel.attachment.isEmpty()) {
                itemView.ic_file.visibility = View.GONE
                itemView.foto_news.visibility = View.GONE
            }

            else {
                for (i in 0 until mNewsModel.attachment.size) {
                    if (i == 0) {
                        if (mNewsModel.attachment[0].contains(".jpg") || mNewsModel.attachment[0].contains(".jpeg") || mNewsModel.attachment[0].contains(".png")) {

                            itemView.foto_news.visibility = View.VISIBLE
                            Glide
                                    .with(itemView.context)
                                    .load(mNewsModel.attachment[0])
                                    .into(itemView.foto_news)
                        } else {
                            itemView.foto_news.visibility = View.GONE
                        }
                    }
                }
                itemView.ic_file.visibility = View.VISIBLE
            }
            itemView.logo_vendor?.text = mNewsModel.vendorName[0].toString().capitalize()
        }
    }
}