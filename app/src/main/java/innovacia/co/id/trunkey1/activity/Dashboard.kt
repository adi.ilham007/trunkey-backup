package innovacia.co.id.trunkey1.activity

import android.app.ActionBar
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.ChildAdapter2
import innovacia.co.id.trunkey1.adapter.RaportAdapter
import innovacia.co.id.trunkey1.helper.SnapHelperOneByOne
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import innovacia.co.id.trunkey1.model.NewsModel
import innovacia.co.id.trunkey1.model.RaportModel
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_manage_child.view.*
import kotlinx.android.synthetic.main.coba.*
import kotlinx.android.synthetic.main.sub_header_rv.*
import org.json.JSONException
import org.json.JSONObject
import java.security.AccessController.getContext

class Dashboard : AppCompatActivity() {

    private val helper = user_info()

    private val mChildModel = mutableListOf<ChildModel>()
    private val mRaportModel = mutableListOf<RaportModel>()

    private lateinit var mBar: BarChart
    private lateinit var seekBarY: BarChart
    private lateinit var seekBarX: BarChart

    private lateinit var mPie: PieChart

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        header_name?.text = resources.getString(R.string.dashboard_header)
        mPie = findViewById(R.id.piechart)

        swipe_container?.setOnRefreshListener {
            mPie.notifyDataSetChanged()
            mRaportModel.clear()

            getPieChartData()
            getRaport()
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        cacheHandling()
        getPieChartData()
        getRaport()

        val legend: Legend? = mPie.legend
        legend?.orientation = Legend.LegendOrientation.HORIZONTAL
        legend?.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        legend?.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        legend?.isWordWrapEnabled = true
        legend?.formToTextSpace = 5f
        legend?.form = Legend.LegendForm.CIRCLE
        legend?.textSize = 12f
        legend?.setDrawInside(false)
        legend?.mNeededWidth

        mPie.description.isEnabled = false
        mPie.setUsePercentValues(true)
        mPie.isRotationEnabled = false
        mPie.legend.isWordWrapEnabled = true
        mPie.setTouchEnabled(false)
        mPie.setDrawEntryLabels(false)

    }

    private fun cacheHandling() { if (user_info.restoreKids == null) getKids() else restoreKidsFun() }

    private fun getKids() {
        val errSection = "getKids :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                user_info.activeChildId = ""
                user_info.childCarouselState = 0

                mChildModel.clear()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restoreKids = dataArray
                if (dataArray.length() > 0) {
                    // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val gender = childObject.getString("gender")
                        val birthday = childObject.getString("birthday")
                        val foto = childObject.getString("photo")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val status = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                        mChildModel.add(mItem)

                        // Add child option
                        if (i+1 == dataArray.length()) {
                            val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                            mChildModel.add(addChild)
                        }
                    }
                } else {
                    for (i in 0 until 1) {
                        val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                        mChildModel.add(addChild)
                    }
                }

                val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                recycler_child?.layoutManager = mLinearLayoutManager

                // Triger carousel to center
                recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)

                val displayMetrics = this.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 320) / 2
                val pxWidth = helper.dpToPx(this, calculation.toInt())
                recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (recycler_child?.onFlingListener == null) {
                    val snapHelper = SnapHelperOneByOne()
                    snapHelper.attachToRecyclerView(recycler_child)
                }

                // Large Carousel
                ChildAdapter2.TYPE_OF_VIEW = 1
                val mChildAdapter = ChildAdapter2()
                recycler_child?.adapter = mChildAdapter
                mChildAdapter.setList(mChildModel)

                kidsCarouselListener(mLinearLayoutManager)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }
    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {
            // Default see all
//            if (dataArray.length() > 1) {
//                val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                mChildModel.add(seeAll)
//            }

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val gender = childObject.getString("gender")
                val birthday = childObject.getString("birthday")
                val foto = childObject.getString("photo")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val status = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                mChildModel.add(mItem)

                // Add child option
                if (i+1 == dataArray.length()) {
                    val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                    mChildModel.add(addChild)
                }
            }
        } else {
            for (i in 0 until 1) {
                val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                mChildModel.add(addChild)
            }
        }

        val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
        recycler_child?.layoutManager = mLinearLayoutManager

        // Handle carousel position (restore or not)
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }

        val displayMetrics = this.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 320) / 2
        val pxWidth = helper.dpToPx(this, calculation.toInt())
        recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (recycler_child?.onFlingListener == null) {
            val snapHelper = SnapHelperOneByOne()
            snapHelper.attachToRecyclerView(recycler_child)
        }

        // Large Carousel
        ChildAdapter2.TYPE_OF_VIEW = 1
        val mChildAdapter = ChildAdapter2()
        recycler_child?.adapter = mChildAdapter
        mChildAdapter.setList(mChildModel)

        kidsCarouselListener(mLinearLayoutManager)
    }
    private fun kidsCarouselListener(mLinearLayoutManager: LinearLayoutManager) {
        recycler_child?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val snapHelper = SnapHelperOneByOne()
                if(newState == RecyclerView.TOUCH_SLOP_PAGING && newState != RecyclerView.NO_POSITION) {
                    Handler().postDelayed({
                        val centerView = snapHelper.findSnapView(mLinearLayoutManager)
                        if (centerView != null) {
                            val pos = mLinearLayoutManager.getPosition(centerView)
                            val getId = mChildModel[pos % mChildModel.size].id
                            user_info.activeChildId = getId
                            user_info.childCarouselState = pos

                            mPie.notifyDataSetChanged()
                            mRaportModel.clear()
                            Log.d("aim", "active child id : $getId")

                            getPieChartData()
                            getRaport()
                        }
                    }, 500)
                }
            }
        })
    }

    private fun getPieChartData() {
        val errSection = "getPieChart :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.POST, user_info.childStatistic+ user_info.activeChildId, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val chartLabel = ArrayList<String>()
                val chartValue = ArrayList<Double>()

                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                if (dataArray.length() > 0) {
                    for (i in 0 until dataArray.length()) {
                        val dataObject = dataArray.getJSONObject(i)
                        val label = dataObject.getString("name")
                        val value = dataObject.getDouble("statistic_percentage")

                        chartLabel.add(label)
                        chartValue.add(value)
                    }

                    setupPieChartView(chartLabel, chartValue)
                }
            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }
    fun setupPieChartView(label:ArrayList<String>,value:ArrayList<Double>) {

        val entry = ArrayList<PieEntry>()
        for(i in 0 until value.size) {
            entry.add( PieEntry(value[i].toFloat(), label[i]) )
        }

        val dataSet = PieDataSet(entry, "")
        dataSet.colors = ColorTemplate.COLORFUL_COLORS.toList()
        dataSet.setDrawValues(true)

        val pieData = PieData(dataSet)
//        pieData.setValueFormatter(PercentFormatter())
        pieData.setValueTextSize(12f)
        pieData.setValueTextColor(Color.WHITE)
        mPie.data = pieData
        mPie.invalidate()

        Log.d("aim","label : $label, value : $value")
        Log.d("aim", "data set : $dataSet")


    }

    private fun getRaport(){
        val errSection = "getRaport :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.POST, user_info.raport , Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    val dataObject = dataArray.getJSONObject(i)
                    val childId = dataObject.getString("id")
                    val childName = dataObject.getString("nama")

                    val dataVendor = dataObject.getJSONArray("eca_vendors_test")
                    val raport: MutableList<String> = ArrayList()

                    if (dataVendor.length() > 0 ) {
                        for (j in 0 until dataVendor.length()) {
                            val vendorObject = dataVendor.getJSONObject(j)
                            val classId = vendorObject.getString("id_classes")
                            val className = vendorObject.getString("class_name")
                            val vendorId = vendorObject.getString("id_eca_vendors")
                            val vendorName = vendorObject.getString("school_name")

                            val scoreArray = vendorObject.getJSONArray("scores")
                            for (k in 0 until scoreArray.length()) {
                                val scoresObject = scoreArray.getJSONObject(k)
//                                val scoreNumber = "${k+1}."
                                val scoreName = scoresObject.getString("nama")
                                val scoreNilai = scoresObject.getString("nilai")

                                val scoreGlobal = "$scoreName,$scoreNilai"
                                raport.add(scoreGlobal)
                            }

                            val mItem = RaportModel(vendorId, classId, childId, vendorName,
                                    className, childName, raport)
                            mRaportModel.add(mItem)
                        }
                    }
                    else {
                        val classId = "Dummy data"
                        val className = "Trunkey Class"
                        val vendorId = "Dummy data"
                        val vendorName = "Trunkey School"

                        for (k in 0 until 3) {
                            val scoreNumber = "${k+1}"
                            val scoreName = "Tes - $scoreNumber"
                            val scoreNilai = "95"

                            val scoreGlobal = "$scoreName,$scoreNilai"
                            raport.add(scoreGlobal)
                        }

                        val mItem = RaportModel(vendorId, classId, childId, vendorName,
                                className, childName, raport)
                        mRaportModel.add(mItem)
                    }
                }

                recycler_raport.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val mRaportAdapter = RaportAdapter()
                recycler_raport.adapter = mRaportAdapter
                mRaportAdapter.setList(mRaportModel)

            }
            catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams():MutableMap<String,String>{
                val body = java.util.HashMap<String,String>()
                if (user_info.activeChildId == "See all" || user_info.activeChildId == "Add child") {
                    body["children"] = ""
                } else {
                    body["children"] = user_info.activeChildId
                }

                body["take"] = "10"
                body["skip"] = "0"
                return body
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)

    }

}
