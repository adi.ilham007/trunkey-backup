package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_select__purchase.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_step_view.*
import org.json.JSONException
import org.json.JSONObject

class Select_Purchase : AppCompatActivity() {

    val helper = user_info()
    private var paymentReady = false
    private var payFor = ""

    // Transaction
    lateinit var idArray: ArrayList<Int>
    lateinit var seatArray: ArrayList<Int>
    lateinit var hargaArray: ArrayList<*>
    var memberArray = ArrayList<String>()
    var typeArray = ArrayList<String>()

    // Billing
    var idBilling = ""

    var totalHarga = 0.0
    var totalCashBack = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select__purchase)
        header_name.text = "Payment"

        generatedView()

        getProfile()

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        trunkey_money.setOnClickListener {
            trunkey_money.startAnimation(user_info.animasiButton)
            if (paymentReady) {
                if (payFor == "transaction") {
                    val i = Intent(this, Pay_Trunkey::class.java)
                    i.putExtra("bayar", "bayar")
                    i.putExtra("whatBuy", typeArray)
                    i.putExtra("idArray", idArray)
                    i.putExtra("seatArray", seatArray)
                    i.putExtra("hargaArray", hargaArray)
                    i.putExtra("memberArray", memberArray)
                    i.putExtra("totalHarga", totalHarga)
                    i.putExtra("totalCashBack", totalCashBack)
                    startActivity(i)
                } else if (payFor == "billing") {
                    val i = Intent(this, Pay_Trunkey::class.java)
                    i.putExtra("bayarBilling", "bayarBilling")
                    i.putExtra("idBilling", idBilling)
                    i.putExtra("hargaBilling",totalHarga)
                    startActivity(i)
                } else if (payFor == "caretaker") {
                    val i = Intent(this, Pay_Trunkey::class.java)
                    i.putExtra("caretaker", "caretaker")
                    i.putExtra("id", idBilling)
                    i.putExtra("price",totalHarga)
                    startActivity(i)
                }
            } else {
                getProfile()
            }
        }

    }

    private fun generatedView() {
        when {
            intent.hasExtra("bayar") -> {
                step_view.visibility = View.VISIBLE
                satu_ke_dua.setBackgroundResource(R.color.colorPrimary)
                step_dua.setBackgroundResource(R.drawable.circle_orange)
                dua_ke_tiga.setBackgroundResource(R.color.colorPrimary)
                step_tiga.setBackgroundResource(R.drawable.circle_orange)

                payFor = "transaction"
                typeArray = intent.getStringArrayListExtra("whatBuy")
                idArray = intent.getIntegerArrayListExtra("idArray")
                seatArray = intent.getIntegerArrayListExtra("seatArray")
                hargaArray = intent.getSerializableExtra("hargaArray") as ArrayList<*>
                memberArray = intent.getStringArrayListExtra("memberArray")
                totalHarga = intent.getDoubleExtra("totalHarga",0.0)
                totalCashBack = intent.getDoubleExtra("totalCashBack",0.0)
//            harga.text = helper.bank(totalHarga)
//            cashback.text = helper.bank(totalCashBack)

                Log.d("aim",typeArray.toString())
                Log.d("aim","intent id: $idArray, seat: $seatArray, member: $memberArray, harga: $hargaArray")

            }
            intent.hasExtra("bayarBilling") -> {
                step_view.visibility = View.GONE

                payFor = "billing"
                idBilling = intent.getStringExtra("idBilling")
                totalHarga = intent.getDoubleExtra("hargaBilling",0.0)
//            harga.text = helper.bank(totalHarga)
            }
            intent.hasExtra("caretaker") -> {
                step_view.visibility = View.VISIBLE
                text_satu.text = "Subscribe"
                text_dua.text = "Confirm"
                text_tiga.text = "Payment"
                satu_ke_dua.setBackgroundResource(R.color.colorPrimary)
                step_dua.setBackgroundResource(R.drawable.circle_orange)
                dua_ke_tiga.setBackgroundResource(R.color.colorPrimary)
                step_tiga.setBackgroundResource(R.drawable.circle_orange)

                payFor = "caretaker"
                idBilling = intent.getStringExtra("id")
                totalHarga = intent.getDoubleExtra("price",0.0)
            }
        }
    }

    fun getProfile(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.profile, Response.Listener { response ->
            try {
                dialog.dismiss()
                Log.i("AIM", response.toString())
                val jsonObject = JSONObject(response)
                val status = jsonObject.getInt("status")
                if (status == 1) {
                    paymentReady = true
                    val jo = jsonObject.getJSONObject("data")

                    // User
                    val user = jo.getJSONObject("user")
                    val needPin = user.getInt("need_set_pin")
                    if (needPin == 1) { user_info.pinStatus = true }
                    user_info.gender = user.getString("gender")
                    user_info.userName = user.getString("username")
                    user_info.phone = user.getString("no_telp")
                    user_info.cashBack = user.getDouble("points")
                    user_info.name = user.getString("nama")
                    user_info.photo = user.getString("photo")
                    user_info.referal_code = user.getString("referal_code")

                    // Balance
                    val balance = jo.getJSONObject("balance")
                    if (balance.getInt("status") == 1) {
                        user_info.walletStatus = true
                        user_info.wallet = balance.getDouble("balance")
                        user_info.cashBack = user.getDouble("points")
                    } else {
                        user_info.walletStatus = false
                    }
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }
}


