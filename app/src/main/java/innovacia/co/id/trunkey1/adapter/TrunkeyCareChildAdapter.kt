package innovacia.co.id.trunkey1.adapter

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.TrunkeyCareChildModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_trunkey_care_child.view.*

class TrunkeyCareChildAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var childData = mutableListOf<TrunkeyCareChildModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_trunkey_care_child, parent, false))
    }
    override fun getItemCount(): Int = childData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder

        holder.bindView(childData[position])

        holder.itemView.setOnClickListener { view ->
            holder.itemView.startAnimation(user_info.animasiButton)

//            val idVideo = childData[position].idVideo
        }
    }
    fun setList(listOfVendor: List<TrunkeyCareChildModel>) {
        this.childData = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<TrunkeyCareChildModel>) {
        this.childData.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(model: TrunkeyCareChildModel) {
            itemView.time_value.text = model.time
            itemView.name_value.text = model.nama

            Glide.with(itemView.context).load(model.photo)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(itemView.photo)
        }
    }

}