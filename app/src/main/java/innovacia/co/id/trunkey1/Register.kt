package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONException
import android.view.View
import android.widget.*
import innovacia.co.id.trunkey1.helper.user_info
import org.json.JSONObject


class Register : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val gender = arrayOf("Female", "Male")
    var genderString = ""
    private val PICK_IMAGE_REQUEST = 1
    var imageName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btnRegister.setOnClickListener {
            btnRegister.startAnimation(user_info.animasiButton)
            if (check_filled()) {
                regis()
            }
        }

        spinner!!.onItemSelectedListener = this
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, gender)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = aa
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        when {
            gender[position] == "Female" -> {
                genderString = "female"
            }
            gender[position] == "Male" -> {
                genderString = "male"
            }
        }

    }
    override fun onNothingSelected(arg0: AdapterView<*>) {

    }

    fun check_filled(): Boolean{
        var cek = 0
        var password = pass.text.toString()
        var confirm = confirm_pass.text.toString()

        if(user.length() < 5) {
            user_lay.error = "At least five characters"; cek++
        } else { user_lay.error = "" }

        if(pass.length() < 5) {
            pass_lay.error = "At least five characters"; cek++
        } else { pass_lay.error = "" }

        if (confirm != password) {
            Log.d("aim","confirm : $confirm")
            Log.d("aim","password : $password")
            confirm_pass_lay.error = "Password mismatch"; cek++
        } else { confirm_pass_lay.error = "" }

        if (email.text!!.isEmpty()) {
            email_lay.error = "Can't be empty"; cek++
        } else { email_lay.error = "" }

        if (name.text!!.isEmpty()) {
            name_lay.error = "Can't be empty"; cek++
        } else { name_lay.error = "" }

        if(phone.text!!.isEmpty()) {
            phone_lay.error = "Can't be empty"; cek++
        } else { phone_lay.error = "" }

        if (cek == 0){
            return true
        }
        return false
    }

    fun regis(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.register, Response.Listener { response ->
            //========================================================================================= data from server
            Log.d("aim","regis : $response")
            Toast.makeText(this, "Sign up success", Toast.LENGTH_LONG).show()
//            val i = Intent(this, login::class.java)
//            startActivity(i)
            finish()

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {

            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["username"] = user.text.toString()
                map["password"] = pass.text.toString()
                map["password_confirmation"] = confirm_pass.text.toString()
                map["nama"] = name.text.toString()
                map["phone"] = phone.text.toString().replace("+62","0")
//                map["referal_code"] = code.text.toString()
//                map["alamat"] = alamat.text.toString()
                map["email"] = email.text.toString()
                map["gender"] = genderString
                map["referal_code"] = referal.text.toString()

                Log.d("aim",map.toString())

                return map
            }
        }
        rq.add(sr)
    }
}
