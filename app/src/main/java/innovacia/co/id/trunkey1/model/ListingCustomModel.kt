package innovacia.co.id.trunkey1.model

data class ListingCustomModel (
        var id_vendor   :String,
        var nama        :String,
        var alamat    :String,
        var rating      :Int,
        var is_open     :String,
        var file_logo   :String,
        var category    :String
)