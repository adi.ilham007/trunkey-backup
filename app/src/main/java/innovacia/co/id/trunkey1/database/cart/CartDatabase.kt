package innovacia.co.id.trunkey1.database.cart

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(CartEntity::class), version = 3,exportSchema = false)
abstract class CartDatabase : RoomDatabase(){

    abstract fun dao(): CartDao

    companion object {
        var instance: CartDatabase? = null
        fun getInstance(context: Context): CartDatabase? {
            if (instance == null) {
                synchronized(CartDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                            CartDatabase::class.java, "cart_database")
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return instance
        }
    }
}
