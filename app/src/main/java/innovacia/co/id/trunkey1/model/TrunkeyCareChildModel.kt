package innovacia.co.id.trunkey1.model

data class TrunkeyCareChildModel(
        val id:String,
        val nama:String,
        val theme:String,
        val time:String,
        val photo:String
)