package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_desc_parenting_tools.*
import kotlinx.android.synthetic.main.activity_trunkey_care_regis2.*

class DescParentingTools : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desc_parenting_tools)

        if (intent.hasExtra("title")) {
            val title = intent.getStringExtra("title")

            when (title) {
                "dashboard" -> {
                    btn_login.visibility = View.VISIBLE
                    title_value.text = resources.getText(R.string.tx_dashboard)
                    desc.text = resources.getText(R.string.desc_attendance)
                    picture_value.setImageResource(R.drawable.desc_dashboard)
                }
                "attendance" -> {
                    btn_login.visibility = View.VISIBLE
                    title_value.text = resources.getText(R.string.tx_attendance)
                    desc.text = resources.getText(R.string.desc_attendance)
                    picture_value.setImageResource(R.drawable.desc_attendance)
                }
                "schedule" -> {
                    btn_login.visibility = View.VISIBLE
                    title_value.text = resources.getText(R.string.tx_schedule)
                    desc.text = resources.getText(R.string.desc_schedule)
                    picture_value.setImageResource(R.drawable.desc_schedule)
                }
                "news" -> {
                    btn_login.visibility = View.VISIBLE
                    title_value.text = resources.getText(R.string.tx_information)
                    desc.text = resources.getText(R.string.desc_news)
                    picture_value.setImageResource(R.drawable.desc_news)
                }
                "billing" -> {
                    btn_login.visibility = View.VISIBLE
                    title_value.text = resources.getText(R.string.tx_billing)
                    desc.text = resources.getText(R.string.desc_billing)
                    picture_value.setImageResource(R.drawable.desc_billing)
                }
                "wallet" -> {
                    btn_login.visibility = View.VISIBLE
                    title_value.text = resources.getText(R.string.tx_wallet_sementra)
                    desc.text =  resources.getText(R.string.desc_wallet)
                    picture_value.setImageResource(R.drawable.desc_wallet)
                }
                "wallet_sementara" -> {
                    btn_login.visibility = View.INVISIBLE
                    title_value.text = resources.getText(R.string.tx_wallet_sementra)
                    desc.text = resources.getText(R.string.desc_wallet_sementara)
                    picture_value.setImageResource(R.drawable.desc_wallet)
                }
                "care" -> {
                    btn_login.visibility = View.VISIBLE
                    title_value.text = resources.getText(R.string.tx_care)
                    desc.text = resources.getText(R.string.desc_care)
                    picture_value.setImageResource(R.drawable.desc_care)
                }
            }
        }

        btn_login.setOnClickListener {
            val i = Intent(this, login::class.java)
            i.putExtra("back", "")
            startActivity(i)
            finish()
        }

    }
}
