package innovacia.co.id.trunkey1.model

data class ReviewModel(
        var id          :String,
        var nama        :String,
        var review      :String,
        var rating      :String,
        var tanggal     :String,
        val photo       :String
)