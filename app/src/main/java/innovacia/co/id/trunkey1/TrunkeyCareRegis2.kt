package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_trunkey_care_regis2.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_step_view.*

class TrunkeyCareRegis2 : AppCompatActivity() {

    val helper = user_info()

    var id = ""
    var name = ""
    var price = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trunkey_care_regis2)
        header_name.text = "Activation"
        text_satu.text = "Subscribe"
        text_dua.text = "Confirm"
        text_tiga.text = "Payment"

        satu_ke_dua.setBackgroundResource(R.color.colorPrimary)
        step_dua.setBackgroundResource(R.drawable.circle_orange)

        if (intent.hasExtra("id")) {
            id = intent.getStringExtra("id")
            name = intent.getStringExtra("name")
            price = intent.getDoubleExtra("price",0.0)
            val priceIDR = helper.bank(price)

            parent_value.text = user_info.name
            time_value.text = name
            price_value.text = priceIDR
            total_value.text = priceIDR

            btn_pay.setOnClickListener {
                btn_pay.startAnimation(user_info.animasiButton)
                val i = Intent(this,Select_Purchase::class.java)
                i.putExtra("caretaker","caretaker")
                i.putExtra("id",id)
                i.putExtra("price",price)
                startActivity(i)
            }
        }

        back_link.setOnClickListener {
            onBackPressed()
        }


    }
}
