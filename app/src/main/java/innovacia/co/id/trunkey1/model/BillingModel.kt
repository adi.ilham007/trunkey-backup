package innovacia.co.id.trunkey1.model

data class BillingModel(
    val id          :String,
    val name        :String,
    val orderDate   :String,
    val expiredDate :String,
    val price       :Double,
    val status      :String,
    val keterangan  :String,
    val student     :String,
    val vendor_name :String
)