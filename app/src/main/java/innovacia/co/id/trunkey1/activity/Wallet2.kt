package innovacia.co.id.trunkey1.activity

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.youtube.player.internal.v
import innovacia.co.id.trunkey1.ChildSetLimit
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.Transfer
import innovacia.co.id.trunkey1.adapter.ChildAdapter2
import innovacia.co.id.trunkey1.adapter.ChildTransHistoriAdapter
import innovacia.co.id.trunkey1.helper.SnapHelperOneByOne
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import innovacia.co.id.trunkey1.model.ChildTransHistory_model
import kotlinx.android.synthetic.main.activity_wallet2.*
import kotlinx.android.synthetic.main.sub_header_rv.*
import org.json.JSONException
import org.json.JSONObject

class Wallet2 : AppCompatActivity() {

    private val mVolleyHelper = VolleyHelper()
    private val helper = user_info()

    private val mChildModel = mutableListOf<ChildModel>()
    private val mChildTransHistory_model = mutableListOf<ChildTransHistory_model>()

    private var dailyLimit = 0.0
    private var weeklyLimit = 0.0
    private var monthlyLimit = 0.0

    private var page = 1
    private var maxPage = 1
    private var loadMore = false
    private var dateState = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet2)
        header_name?.text = resources.getString(R.string.wallet_header)

        swipe_container?.setOnRefreshListener {
            mChildTransHistory_model.clear()
            page = 1
            dateState = ""
            getLimit()
            historyTrans()
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        btn_topup?.setOnClickListener {
            btn_topup?.startAnimation(user_info.animasiButton)
            val i = Intent(this, Transfer::class.java)
            startActivity(i)
        }

        btn_edit?.setOnClickListener {
            btn_edit?.startAnimation(user_info.animasiButton)
            val i = Intent(this,ChildSetLimit::class.java)
            i.putExtra("daily",dailyLimit)
            i.putExtra("weekly",weeklyLimit)
            i.putExtra("monthly",monthlyLimit)
            startActivity(i)
        }

        cacheHandling()
        spinnerFilter()
        scrollListener()

    }

    private fun cacheHandling() { if (user_info.restoreKids == null) getKids() else restoreKidsFun() }

    private fun getKids() {
        val errSection = "getKids :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                user_info.activeChildId = ""
                user_info.childCarouselState = 0

                mChildModel.clear()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restoreKids = dataArray
                if (dataArray.length() > 0) {
                    // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val gender = childObject.getString("gender")
                        val birthday = childObject.getString("birthday")
                        val foto = childObject.getString("photo")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val status = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                        mChildModel.add(mItem)

                        // Add child option
                        if (i+1 == dataArray.length()) {
                            val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                            mChildModel.add(addChild)
                        }
                    }
                } else {
                    for (i in 0 until 1) {
                        val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                        mChildModel.add(addChild)
                    }
                }

                val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                recycler_child?.layoutManager = mLinearLayoutManager

                // Triger carousel to center
                recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)

                val displayMetrics = this.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 320) / 2
                val pxWidth = helper.dpToPx(this, calculation.toInt())
                recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (recycler_child?.onFlingListener == null) {
                    val snapHelper = SnapHelperOneByOne()
                    snapHelper.attachToRecyclerView(recycler_child)
                }

                // Large Carousel
                ChildAdapter2.TYPE_OF_VIEW = 1
                val mChildAdapter = ChildAdapter2()
                recycler_child?.adapter = mChildAdapter
                mChildAdapter.setList(mChildModel)

                kidsCarouselListener(mLinearLayoutManager)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }
    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {
            // Default see all
//            if (dataArray.length() > 1) {
//                val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                mChildModel.add(seeAll)
//            }

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val gender = childObject.getString("gender")
                val birthday = childObject.getString("birthday")
                val foto = childObject.getString("photo")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val status = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                mChildModel.add(mItem)

                // Add child option
                if (i+1 == dataArray.length()) {
                    val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                    mChildModel.add(addChild)
                }
            }
        } else {
            for (i in 0 until 1) {
                val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                mChildModel.add(addChild)
            }
        }

        val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
        recycler_child?.layoutManager = mLinearLayoutManager

        // Handle carousel position (restore or not)
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }

        val displayMetrics = this.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 320) / 2
        val pxWidth = helper.dpToPx(this, calculation.toInt())
        recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (recycler_child?.onFlingListener == null) {
            val snapHelper = SnapHelperOneByOne()
            snapHelper.attachToRecyclerView(recycler_child)
        }

        // Large Carousel
        ChildAdapter2.TYPE_OF_VIEW = 1
        val mChildAdapter = ChildAdapter2()
        recycler_child?.adapter = mChildAdapter
        mChildAdapter.setList(mChildModel)

        kidsCarouselListener(mLinearLayoutManager)
    }
    private fun kidsCarouselListener(mLinearLayoutManager: LinearLayoutManager) {
        recycler_child?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val snapHelper = SnapHelperOneByOne()
                if(newState == RecyclerView.TOUCH_SLOP_PAGING && newState != RecyclerView.NO_POSITION) {
                    Handler().postDelayed({
                        val centerView = snapHelper.findSnapView(mLinearLayoutManager)
                        if (centerView != null) {
                            val pos = mLinearLayoutManager.getPosition(centerView)
                            val getId = mChildModel[pos % mChildModel.size].id
                            page = 1

                            user_info.activeChildId = getId
                            user_info.childCarouselState = pos
                            Log.d("aim", "active child id : $getId")

                            mChildTransHistory_model.clear()

                            getLimit()
                            historyTrans()
                        }
                    }, 500)
                }
            }
        })
    }

    private fun spinnerFilter(){

//        `all`,`today`,`thisweek`,`thismonth`

        val spinnerData = arrayOf("All","Today","This Week","This Month")
        val arrayAdapter = ArrayAdapter(this, R.layout.spinner_txt_white, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner?.adapter = arrayAdapter

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                page = 1
                val mTransHistoryAdapter = ChildTransHistoriAdapter()

                when (spinnerData[position]){
                    "All" -> {
                        dateState = "all"
                        mChildTransHistory_model.clear()
                        mTransHistoryAdapter.notifyDataSetChanged()
                        getLimit()
                        historyTrans()
                    }
                    "Today" -> {
                        dateState = "today"
                        mChildTransHistory_model.clear()
                        mTransHistoryAdapter.notifyDataSetChanged()
                        getLimit()
                        historyTrans()
                    }
                    "This Week" -> {
                        dateState = "thisweek"
                        mChildTransHistory_model.clear()
                        mTransHistoryAdapter.notifyDataSetChanged()
                        historyTrans()
                        getLimit()
                    }
                    "This Month" -> {
                        dateState = "thismonth"
                        mChildTransHistory_model.clear()
                        mTransHistoryAdapter.notifyDataSetChanged()
                        getLimit()
                        historyTrans()
                    }
                }
                Log.d("aim","spinner clicked")
            }
        }
    }

    private fun scrollListener() {
        scrollView?.setOnScrollChangeListener(object: NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(v: NestedScrollView, scrollX:Int, scrollY:Int, oldScrollX:Int, oldScrollY:Int) {

            if (scrollY > oldScrollY) {  }
            if (scrollY < oldScrollY) {  }
            if (scrollY == 0) {  }

            val itemHeight = v.getChildAt(0).measuredHeight
            val layHeight = v.measuredHeight
            val pos = itemHeight - layHeight

            if (scrollY == pos && page < maxPage){
                page++
//                    progress_bar_wallet.visibility = View.VISIBLE
                loadMore = true

                historyTrans()
            }
       }
    })

//        recycler_history?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
//                val countItem = linearLayoutManager.itemCount
//                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
//                val isLastPosition = countItem.minus(1) == lastVisiblePosition
//
//                if(isLastPosition && page <= maxPage){
//                    page++
////                    progress_bar_wallet.visibility = View.VISIBLE
//                    loadMore = true
//
//                    historyTrans()
//
//                }
//            }
//        })
    }

    private fun getLimit() {
        val errSection = "getLimit :"
        swipe_container?.isRefreshing = true

        val rq = object : JsonObjectRequest(Request.Method.GET,user_info.detailChild+user_info.activeChildId,null,Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val dataObject = response.getJSONObject("data")
                val nama = dataObject.optString("nama","")
                val saldo = dataObject.optDouble("available_saldo",0.0)

                user_logo?.text = nama[0].toString().capitalize()
                balance?.text = helper.bank(saldo)

                dailyLimit = dataObject.optDouble("daily_limit",0.0)
                weeklyLimit = dataObject.optDouble("weekly_limit",0.0)
                monthlyLimit = dataObject.optDouble("monthly_limit",0.0)

                daily_value?.text = helper.bank(dailyLimit)
                weekly_value?.text = helper.bank(weeklyLimit)
                monthly_value?.text = helper.bank(monthlyLimit)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        },Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }

    private fun historyTrans() {
        val errSection = "historyTrans :"
        swipe_container?.isRefreshing = true

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.historyTrans2+"?page=$page", Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val dataObject = jsonObject.getJSONObject("data")
                val dataArray = dataObject.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val jo = dataArray.getJSONObject(i)
                    val faktur = jo.getString("no_faktur")
                    val orderDate = jo.getString("tanggal")
                    val total = jo.getDouble("subtotal")
                    val merchantObject = jo.getJSONObject("merchant")
                    val merchant = merchantObject.getString("nama")

                    val mItem = ChildTransHistory_model(faktur,merchant,orderDate,"","",helper.bank(total))
                    mChildTransHistory_model.add(mItem)
                }

                recycler_history?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val mChildTransHistoriAdapter = ChildTransHistoriAdapter()
                recycler_history?.adapter = mChildTransHistoriAdapter
                mChildTransHistoriAdapter.setList(mChildTransHistory_model)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["date"] = dateState
                map["child"] = user_info.activeChildId
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
