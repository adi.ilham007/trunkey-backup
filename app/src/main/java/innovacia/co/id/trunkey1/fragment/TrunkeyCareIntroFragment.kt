package innovacia.co.id.trunkey1.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import kotlinx.android.synthetic.main.fragment_trunkey_care_intro.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class TrunkeyCareIntroFragmnet : Fragment() {

    companion object {
        private const val PAGE_NUM = "PAGE_NUM"
        fun newInstance(page: Int): TrunkeyCareIntroFragmnet {
            val fragment = TrunkeyCareIntroFragmnet()
            val args = Bundle()
            args.putInt(PAGE_NUM, page)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trunkey_care_intro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (activity != null) {
            val page = arguments!!.getInt(PAGE_NUM)

            when (page) {
                1 -> {
                    intro_title?.text = resources.getString(R.string.onboarding_title1)
                    intro_desc?.text = resources.getString(R.string.onboarding1)
                    intro_image?.setImageResource(R.drawable.trunkey_intro1)
                }
                2-> {
                    intro_title?.text = resources.getString(R.string.onboarding_title2)
                    intro_desc?.text = resources.getString(R.string.onboarding2)
                    intro_image.setImageResource(R.drawable.trunkey_intro2)
                }
                3 -> {
                    intro_title?.text = resources.getString(R.string.onboarding_title3)
                    intro_desc?.text = resources.getString(R.string.onboarding3)
                    intro_image.setImageResource(R.drawable.trunkey_intro3)
                }
                4 -> {
                    intro_title?.text = resources.getString(R.string.onboarding_title4)
                    intro_desc?.text = resources.getString(R.string.onboarding4)
                    intro_image.setImageResource(R.drawable.trunkey_intro4)
                }
                5 -> {
                    intro_title?.text = resources.getString(R.string.onboarding_title5)
                    intro_image.setImageResource(R.drawable.trunkey_intro5)
                }
            }

        }

    }


}
