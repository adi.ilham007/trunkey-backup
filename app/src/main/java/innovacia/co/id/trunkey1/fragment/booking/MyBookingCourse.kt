package innovacia.co.id.trunkey1.fragment.booking

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.MybookingAdapter
import innovacia.co.id.trunkey1.model.MyBookingModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.fragment_my_booking_course.*
import org.json.JSONException
import org.json.JSONObject

class MyBookingCourse : Fragment() {

    private val bookingData = mutableListOf<MyBookingModel>()

    private var loadMore = false    // First load
    private var lastData = false    // Pagination
    private var page = 1 //page now
    private var max_page = 1

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser)
            Handler().postDelayed({
                if (activity != null) {
                    getBooking(page)
                }
            }, 200) // 200
        super.setUserVisibleHint(isVisibleToUser)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_booking_course, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe_container.setOnRefreshListener {
            page = 1
            bookingData.clear()
            getBooking(page)
        }
        rvListener()
    }

    private fun rvListener() {
        recycler_booking.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if(isLastPosition && page <= max_page){
                    page++
//                    progress_bar_wallet.visibility = View.VISIBLE
                    loadMore = true


                    getBooking(page)
//                    Log.d("aim","take: 10, skip: ")

                }
            }
        })
    }

    fun getBooking(page:Int) {
        try {
            Log.d("aim", "fragment")
            swipe_container.isRefreshing = true

            if (page == 1) {
                swipe_container.isRefreshing = true
            }
            val rq: RequestQueue = Volley.newRequestQueue(activity)
            val sr = object : StringRequest(Request.Method.POST, user_info.bookingCourse + "?page=" + page, Response.Listener { response ->
                Log.d("aim", "respon: $response")
                try {
                    if (swipe_container != null) {
                        swipe_container.isRefreshing = false
                    }
                    val jsonObject = JSONObject(response)
                    val dataobject = jsonObject.getJSONObject("data")
                    max_page = dataobject.getInt("last_page")
                    val jsonArray = dataobject.getJSONArray("data")
                    Log.d("aim", jsonArray.length().toString())

                    if (page == 1 && jsonArray.length() == 0) {
                        list_lay.setBackgroundResource(R.drawable.ic_oops)
                    }

                    for (i in 0 until jsonArray.length()) {
                        val jo = jsonArray.getJSONObject(i)
                        val id = jo.getString("id")
                        val namaTiket = jo.getString("batch")
                        var biayaIDR = "IDR 0"
                        val helper = user_info()
                        if (jo.getString("biaya") != "null") {
                            val biaya = jo.getDouble("biaya")
                            biayaIDR = helper.bank(biaya)
                        }
                        val keterangan = jo.getString("keterangan")

                        val kethtml = helper.textHtml(keterangan)
                        val objEvent = jo.getJSONObject("course")
                        val vendor_name = objEvent.getString("nama")
                        val tempat = "Under Construction"
                        val tanggalMulai = objEvent.getString("tanggal_mulai")
                        val tanggalAkhir = objEvent.getString("tanggal_akhir")

                        val objPivot = jo.getJSONObject("pivot")
                        val codeBooking = objPivot.getString("booking_code")
                        val namaParticipant = objPivot.getString("participant_name")
                        val tanggalBooking = objPivot.getString("tanggal_register")

                        val mItem = MyBookingModel(id, vendor_name, namaTiket, kethtml, biayaIDR, tanggalBooking, tanggalMulai, tanggalAkhir, namaParticipant, codeBooking, tempat,"course")
                        bookingData.add(mItem)

                    }

                    if (recycler_booking != null && !loadMore) {
                        try {
                            recycler_booking.layoutManager = LinearLayoutManager(activity, OrientationHelper.VERTICAL, false)
                            val bookingAdapter = MybookingAdapter()
                            recycler_booking.adapter = bookingAdapter
                            bookingAdapter.setList(bookingData)
                        }catch (e: Exception) {
                            Log.e("aim", "Err Swipe Pager if : $e")
                        }
                    } else if (recycler_booking != null && loadMore) {
                        try {
                            val recyclerViewState = recycler_booking.layoutManager!!.onSaveInstanceState()
                            val bookingAdapter = MybookingAdapter()
                            recycler_booking.adapter = bookingAdapter
                            bookingAdapter.setList(bookingData)
                            recycler_booking.layoutManager!!.onRestoreInstanceState(recyclerViewState)
//                        progress_bar_wallet.visibility = View.GONE
                        } catch (e: Exception) {
                            Log.e("aim", "Err Swipe Pager else : $e")
                        }

                    }
                } catch (e: JSONException) {
                    Log.e("aim", "Wallet e: $e")
                }

            }, Response.ErrorListener { response ->
                if (swipe_container != null) {
                    swipe_container.isRefreshing = false
                }
                val networkResponse = response.networkResponse
                if (networkResponse == null) {
                    Toast.makeText(activity, R.string.connFail, Toast.LENGTH_LONG).show()
                } else {
                    try {
                        val err = String(networkResponse.data)
                        val jsonObj = JSONObject(err)
                        val errCode = jsonObj.getString("code")
                        val errMessage = jsonObj.getString("message")

                        Toast.makeText(activity, errMessage, Toast.LENGTH_LONG).show()
                        Log.d("aim", "Err code : $errCode, $err")
                    } catch (e: JSONException) {
                        Log.e("aim", "Err : $e")
                    }
                }
            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()

                    headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                    headers["Authorization"] = "Bearer ${user_info.token}"
                    return headers
                }
                override fun getParams(): MutableMap<String,String> {
                    val map = HashMap<String, String>()
                    map["redeem_status"] = "0"
                    return map
                }
            }
            sr.retryPolicy = DefaultRetryPolicy(
                    0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )
            rq.add(sr)
        }catch (e: Exception) {
            Log.e("aim", "Err myBooking : $e")
        }
    }


}
