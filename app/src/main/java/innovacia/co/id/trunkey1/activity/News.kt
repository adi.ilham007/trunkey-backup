package innovacia.co.id.trunkey1.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.BillingAdapter
import innovacia.co.id.trunkey1.adapter.ChildAdapter2
import innovacia.co.id.trunkey1.adapter.NewsAdapter
import innovacia.co.id.trunkey1.helper.InputStreamVolleyRequest
import innovacia.co.id.trunkey1.helper.SnapHelperOneByOne
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import innovacia.co.id.trunkey1.model.NewsModel
import kotlinx.android.synthetic.main.activity_news.*
import kotlinx.android.synthetic.main.sub_header_rv.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class News : AppCompatActivity() {

    private val helper = user_info()
    private val mVolleyHelper = VolleyHelper()
    private var writeStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE

    lateinit var mContext: Context

    private val mChildModel = mutableListOf<ChildModel>()
    private val mNewsModel = mutableListOf<NewsModel>()

    private var isRead = ""
    private var page = 1
    private var maxPage = 1
    private var loadMore = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        header_name?.text = resources.getString(R.string.news_header)
        mContext = this

        swipe_container?.setOnRefreshListener {
            mNewsModel.clear()
            page = 1
            getNews()
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        cacheHandling()
        spinnerFilter()
        rvListener()

    }

    private fun cacheHandling() { if (user_info.restoreKids == null) getKids() else restoreKidsFun() }

    private fun getKids() {
        val errSection = "getKids :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                user_info.activeChildId = ""
                user_info.childCarouselState = 0

                mChildModel.clear()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restoreKids = dataArray
                if (dataArray.length() > 0) {
                    // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val gender = childObject.getString("gender")
                        val birthday = childObject.getString("birthday")
                        val foto = childObject.getString("photo")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val status = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                        mChildModel.add(mItem)

                        // Add child option
                        if (i+1 == dataArray.length()) {
                            val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                            mChildModel.add(addChild)
                        }
                    }
                } else {
                    for (i in 0 until 1) {
                        val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                        mChildModel.add(addChild)
                    }
                }

                val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                recycler_child?.layoutManager = mLinearLayoutManager

                // Triger carousel to center
                recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)

                val displayMetrics = this.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 320) / 2
                val pxWidth = helper.dpToPx(this, calculation.toInt())
                recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (recycler_child?.onFlingListener == null) {
                    val snapHelper = SnapHelperOneByOne()
                    snapHelper.attachToRecyclerView(recycler_child)
                }

                // Large Carousel
                ChildAdapter2.TYPE_OF_VIEW = 1
                val mChildAdapter = ChildAdapter2()
                recycler_child?.adapter = mChildAdapter
                mChildAdapter.setList(mChildModel)

                kidsCarouselListener(mLinearLayoutManager)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }
    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {
            // Default see all
//            if (dataArray.length() > 1) {
//                val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                mChildModel.add(seeAll)
//            }

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val gender = childObject.getString("gender")
                val birthday = childObject.getString("birthday")
                val foto = childObject.getString("photo")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val status = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                mChildModel.add(mItem)

                // Add child option
                if (i+1 == dataArray.length()) {
                    val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                    mChildModel.add(addChild)
                }
            }
        } else {
            for (i in 0 until 1) {
                val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                mChildModel.add(addChild)
            }
        }

        val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
        recycler_child?.layoutManager = mLinearLayoutManager

        // Handle carousel position (restore or not)
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }

        val displayMetrics = this.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 320) / 2
        val pxWidth = helper.dpToPx(this, calculation.toInt())
        recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (recycler_child?.onFlingListener == null) {
            val snapHelper = SnapHelperOneByOne()
            snapHelper.attachToRecyclerView(recycler_child)
        }

        // Large Carousel
        ChildAdapter2.TYPE_OF_VIEW = 1
        val mChildAdapter = ChildAdapter2()
        recycler_child?.adapter = mChildAdapter
        mChildAdapter.setList(mChildModel)

        kidsCarouselListener(mLinearLayoutManager)
    }
    private fun kidsCarouselListener(mLinearLayoutManager: LinearLayoutManager) {
        recycler_child?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val snapHelper = SnapHelperOneByOne()
                if(newState == RecyclerView.TOUCH_SLOP_PAGING && newState != RecyclerView.NO_POSITION) {
                    Handler().postDelayed({
                        val centerView = snapHelper.findSnapView(mLinearLayoutManager)
                        if (centerView != null) {
                            val pos = mLinearLayoutManager.getPosition(centerView)
                            val getId = mChildModel[pos % mChildModel.size].id
                            user_info.activeChildId = getId
                            user_info.childCarouselState = pos
                            page = 1

                            mNewsModel.clear()
                            val mNewsAdapter = NewsAdapter()
                            mNewsAdapter.reset()
                            recycler_news?.adapter = mNewsAdapter

                            getNews()
                        }
                    }, 500)
                }
            }
        })
    }

    private fun spinnerFilter(){
        val spinnerData = arrayOf("All","Unread","Read")
        val arrayAdapter = ArrayAdapter(this, R.layout.spinner_txt_white, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner?.adapter = arrayAdapter

        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                page = 1
                val mNewsAdapter = NewsAdapter()

                when (spinnerData[position]){
                    "All" -> {
                        isRead = ""
                        mNewsModel.clear()
                        mNewsAdapter.reset()
                        recycler_news?.adapter = mNewsAdapter
                        getNews()
                    }
                    "Unread" -> {
                        isRead = "0"
                        mNewsModel.clear()
                        mNewsAdapter.reset()
                        recycler_news?.adapter = mNewsAdapter
                        getNews()
                    }
                    "Read" -> {
                        isRead = "1"
                        mNewsModel.clear()
                        mNewsAdapter.reset()
                        recycler_news?.adapter = mNewsAdapter
                        getNews()
                    }
                }
            }
        }
    }

    private fun rvListener() {
        recycler_news?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if(isLastPosition && page < maxPage){
                    page++
//                    progress_bar_wallet.visibility = View.VISIBLE
                    loadMore = true

                    getNews()
                }

                if (dy > 0) {
                    // Scrolling up
//                    slideUp(header)
                } else {
                    // Scrolling down
//                    slideDown(header)
                }

            }
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun getNews() {
        val errSection = "getNews :"
        swipe_container?.isRefreshing = true

        // get Today
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val today = sdf.format(Date()).split("-")

        // get previous month
        val calendar = Calendar.getInstance()
        calendar.set(today[0].toInt(),today[1].toInt()-3,today[2].toInt())

        val body = JSONObject()
//        body.put("start_date",sdf.format(calendar.time))
//        body.put("end_date", sdf.format(Date()))
        body.put("is_read",isRead)
        body.put("child",
                if (user_info.activeChildId == "See all" || user_info.activeChildId == "Add child") {
                    ""
                } else {
                    user_info.activeChildId
                }
        )

        Log.d("aim","body : $body")


        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : JsonObjectRequest(Request.Method.POST,user_info.news+"?page=$page",body, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val data = response.getJSONObject("data")
                val notifObject = data.getJSONObject("notifications")
                maxPage = notifObject.getInt("last_page")

                val notifArray = notifObject.getJSONArray("data")
                if (notifArray.length() > 0) {
                    for (i in 0 until notifArray.length()) {
                        val newsObject = notifArray.getJSONObject(i)
                        val newsId = newsObject.getString("id")

                        val newsDate = newsObject.getString("tanggal")
                        val dueDate = newsObject.getString("due_date")
                        val newsTitle = newsObject.getString("nama")
                        val newsDesc = newsObject.getString("keterangan")

                        val attachment: MutableList<String> = ArrayList()
                        val fileArray = newsObject.optJSONArray("files")
                        if (fileArray != null) {
                            for (x in 0 until fileArray.length()) {
                                attachment.add(fileArray[x].toString())
                            }
                        }

                        val vendorObject = newsObject.getJSONObject("eca_vendors")
                        val vendorName = vendorObject.getString("nama")
                        val vendorAddrs = vendorObject.getString("alamat")

                        val childId = if (newsObject.has("detail_penerima")) {
                            val penerimaObject = newsObject.getJSONObject("detail_penerima")
                            penerimaObject.getString("id")
                        } else {
                            ""
                        }

                        val mItem = NewsModel(newsId, childId, newsDate, dueDate, vendorName, vendorAddrs, newsTitle, newsDesc, attachment)
                        mNewsModel.add(mItem)

                    }
                } else if (page == 1 && notifArray.length() > 0) {
                    Toast.makeText(this,"No data Available",Toast.LENGTH_LONG).show()
                }


                if (loadMore) {
                    val recyclerViewState = recycler_news?.layoutManager?.onSaveInstanceState()
                    val mNewsAdapter = NewsAdapter()
                    recycler_news?.adapter = mNewsAdapter
                    mNewsAdapter.setList(mNewsModel)
                    recycler_news?.layoutManager?.onRestoreInstanceState(recyclerViewState)
                } else {
                    recycler_news?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    val mNewsAdapter = NewsAdapter()
                    recycler_news?.adapter = mNewsAdapter
                    mNewsAdapter.setList(mNewsModel)
                }

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }

    // slide the view from below itself to the current position
    private fun slideUp(view:View){
        view.visibility = View.VISIBLE
        val animate = TranslateAnimation(
                0f,                 // fromXDelta
                0f,                 // toXDelta
                view.height.toFloat(),  // fromYDelta
                0f)                // toYDelta
        animate.duration = 500
        animate.fillAfter = true
        view.startAnimation(animate)
    }

    // slide the view from its current position to below itself
    private fun slideDown(view:View){
        val animate = TranslateAnimation(
                0f,                 // fromXDelta
                0f,                 // toXDelta
                0f,                 // fromYDelta
                view.height.toFloat()) // toYDelta
        animate.duration = 500
        animate.fillAfter = true
        view.startAnimation(animate)
    }
}
