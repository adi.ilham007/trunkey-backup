package innovacia.co.id.trunkey1.helper

import android.content.Context
import android.view.animation.AlphaAnimation
import org.json.JSONArray
import java.text.NumberFormat
import java.util.*
import android.util.DisplayMetrics
import android.util.Log
import android.widget.Toast
import com.android.volley.NetworkResponse
import innovacia.co.id.trunkey1.R
import org.json.JSONException
import org.json.JSONObject


class  user_info {
    companion object {
        // Button animation
        val animasiButton = AlphaAnimation(0.1F, 2.0F)

        // Category
        const val enrichment      = "1"
        const val entertainment   = "2"
        const val academic        = "3"
        const val healthcare      = "4"
        const val kidsmeal        = "5"
        const val event           = "6"
        const val shop            = "7"
        const val parentcorner    = "8"

        // Credensial
        var loginStatus         = false
        var walletStatus        = false
        var pinStatus           = false
        var FCM:String          = ""
        var token:String        = ""
        var refresh:String      = ""

        // Profile
        var userName:String     = ""
        var name:String         = ""
        var alamat:String       = ""
        var email:String        = ""
        var phone:String        = ""
        var photo:String        = ""
        var level:String        = ""
        var activity:String     = ""
        var referal_code:String = ""
        var wallet:Double       = 0.0
        var cashBack:Double     = 0.0
        var gallery:JSONArray?  = null
        var kalender:String     = ""
        var gender:String       = ""


        //Bank
        var topup:String        = ""

        //Manage Child
        var activeChildId        = ""
        var childCarouselState   = 0
        var restoreKids:JSONArray?  = null

        var activeChildName      = ""
        var activeChildBirthday  = ""
        var activeChildPhoto     = ""
        var activeChildBalance   = ""


        var childVendorId = java.util.ArrayList<String>()
        var childVendorName = java.util.ArrayList<String>()
        var childVendorState = java.util.ArrayList<Int>()

        // Listing Parameter
        var latitude: Double = 0.0
        var longitude: Double = 0.0
        var category = ""
        var tagSearch = ""

        // Filter
        var whatCategory = ""

        var filter_status = false
        var filter_type = ""
        var filter_grade = ""
        var filter_price_min = ""
        var filter_price_max = ""
        var filter_distance = ""
        var filter_rating = ""

        // Cache
        var cache_nearme: JSONArray? = null
        var cache_read:JSONArray? = null
        var cache_recomended:JSONArray? = null
        var cache_customA:JSONArray? = null
        var cache_stories:JSONArray? = null
        var cache_tips:JSONArray? = null
        var cache_watch:JSONArray? = null

        var restorePromotion:JSONArray? = null
        var restoreNearMe:JSONArray? = null
        var restoreEvent:JSONArray? = null

        // Domain
        // http://api.trunkey.id/
        // https://apitrunkey.innovacia.co.id/
        const val domain = "http://api.trunkey.id/"

        // API
        const val login = "${domain}v2/member/login"
        const val getCategory = "${domain}v2/main/getcategories"
        const val forgot = "${domain}member/profile/forgot-password"
        const val refresh_token = "${domain}member/login/refresh"
        const val version = "${domain}api/device/version/android"
        const val availableData = "${domain}api/list/available-product"
        const val nearest = "${domain}v2/api/preview?latitude=<lat>&longitude=<lon>"

        // New Menu
        const val place = "${domain}member/children/places"
        const val news = "${domain}member/children/notifications"
        const val newsDetail = "${domain}member/children/<child>/notifications/<news>/detail"
        const val raport = "${domain}v2/member/children/statistic/score"

        // Wish List
        const val wishList = "${domain}member/profile/wishlist"
        const val syncWishList = "${domain}member/profile/wishlist/sync"

        const val promo = "${domain}v2/api/promo"
        const val feature = "${domain}v2/api/eca-content-blog/priority"
        const val onGoingEvent = "${domain}api/list/ongoing"

        const val get_tag  = "${domain}api/vendor-types-tags/"
        const val listing = "${domain}api/list"
        const val listingAuth = "${domain}member/list"
        const val recomended = "${domain}api/list/recommended"
        const val data_bank = "${domain}member/transfer-info"
        const val profile = "${domain}member/xfers-account"

        // Profile
        const val activity_profile = "${domain}member/profile/activity"
        const val register = "${domain}v2/member/parent/register"
        const val register_upload = "${domain}member/register/upload-photo"
        const val logout = "${domain}member/logout"
        const val faq = "${domain}api/content-faq"
        const val profile_transaction = "${domain}member/profile/transactions"
        const val xfers_account = "${domain}member/xfers-account"

        // My Booking
        const val bookingEvent ="${domain}member/profile/my-booking"
        const val bookingCourse ="${domain}member/profile/my-booking/course"

        // API - Edit Profile
        const val update_foto = "${domain}member/update-photo"
        const val update_profile = "${domain}member/update-profile"
        const val reset_password = "${domain}member/reset-password"
        const val update_pin ="${domain}member/update-pin"

        // API - Otp
        const val send_otp = "${domain}member/xfers-otp"
        const val proses_otp = "${domain}member/xfers-signup"
        const val set_pin = "${domain}member/set-pin"

        // API - Vendor
        const val detail_vendor = "${domain}api/vendor/detail/"
        const val listReview_vendor = "${domain}vendor/reviews/"
        const val reviewVendor = "${domain}vendor/review/"

        // API - Course
        const val detail_course = "${domain}v2/api/eca-vendors-courses/detail/"
        const val listReview_course = "${domain}api/course/review/"

        // API - Blog
        const val blog = "${domain}api/v2/content-blog"
        const val blog_category = "${domain}api/content-blog-category"
        const val detail_blog = "${domain}api/detail-blog/"
        const val listComment_blog = "${domain}api/detail-blog/comments/"
        const val comment_blog = "${domain}api/blog/review/"

        // API - Lokasi
        const val getLokasi = "${domain}api/search-regency"

        // API - Payment
        const val transaksi = "${domain}v2/member/transaction"
        const val billing = "${domain}member/profile/trans-billing"
        const val detailBilling = "${domain}member/profile/trans-billing/detail/"
        const val payBilling = "${domain}member/profile/pay-billing/"

        // API - Event
        const val event_api = "${domain}api/event/detail/"

        // API - Forum
        const val forum = "${domain}member/posts/index"
        const val forum_post = "${domain}v2/member/posts/create"
        const val detail_forum = "${domain}member/posts/detail/"
        const val forum_comment = "${domain}member/posts/comments/create/"
        const val forum_like = "${domain}member/posts/like/"

        // API - Child
        const val childStatus = "${domain}v2/member/children/last-attendance/"
        const val historyAttendace = "${domain}member/student/history-attendance"
        const val getChild = "${domain}member/student-accounts"
        const val setLimit = "${domain}member/children/limit-transaction/"
        const val transfer = "${domain}member/children/transfer-saldo/"
        const val addChildScan = "${domain}member/children/check-qr-code" // check available child
        const val addChildScan2 = "${domain}member/children/add" // Add child scan
        const val addChild = "${domain}member/add-children" // Add child manual
        const val updateChild = "${domain}member/children/update/"
        const val setPin = "${domain}member/children/set-pin/"
        const val historyTrans = "${domain}member/children/orders/"
        const val historyTrans2 = "${domain}member/v2/children/orders"
        const val vendorSync = "${domain}member/children/synchronized-vendors/"
        const val childStatistic = "${domain}member/children/performance/"
        const val removeChild = "${domain}v2/member/children/remove/"

        // API - Calendar
        const val schedule = "${domain}member/calendars/"
        const val allSchedule = "${domain}member/v2/calendars"
        const val scheduleByUser = "${domain}member/member-calendar/list"
        const val scheduleSubject = "${domain}member/subjects"
        const val detailChild = "${domain}member/student/detail/"
        const val createAgenda = "${domain}member/member-calendar/create"
        const val updateAgenda = "${domain}member/member-calendar/update/"
        const val deleteAgenda = "${domain}member/member-calendar/delete/"

        // Trunkey Careker
        const val care_list = "${domain}v2/member/caretaker/list"
        const val care_info = "${domain}v2/member/caretaker/detail/"
        const val care_delete = "${domain}v2/member/caretaker/disable/"
        const val care_add = "${domain}v2/member/caretaker/enable/"
        const val care_cancel_req = "${domain}v2/member/caretaker/cancel/"
        const val care_location = "${domain}v2/member/caretaker/latest-location/"
        const val care_history = "${domain}v2/member/caretaker/history/"
        const val care_subscribe = "${domain}member/subscription-type"
        const val care_pay = "${domain}member/subscription-type/create"
    }

    fun bank(value:Double): String {
        // Format Rupiah
        val localeID = Locale("in", "ID")
        val rupiah = NumberFormat.getCurrencyInstance(localeID)
        return rupiah.format(value).replace("Rp", "IDR ")
    }

    fun textHtml(value:String): String {
        var html = value
        html = html.replace("<(.*?)\\>".toRegex(), " ")
        html = html.replace("<(.*?)\\\n".toRegex(), " ")
        html = html.replaceFirst("(.*?)\\>".toRegex(), " ")
        html = html.replace("&nbsp;".toRegex(), " ")
        html = html.replace("&amp;".toRegex(), " ").trim()
        return  html
    }

    fun textHtml_desc(value:String): String {
        var html = value
        html = html.replace("<(.*?)\\>".toRegex(), " ")
        html = html.replace("<(.*?)\\\n".toRegex(), "\n")
        html = html.replaceFirst("(.*?)\\>".toRegex(), " ")
        html = html.replace("&nbsp;".toRegex(), " ")
        html = html.replace("&amp;".toRegex(), " ").trim()
        return  html
    }

    fun dpToPx(context: Context, dp: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun pxToDp(context: Context, px: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    // ============================================================================================= R E S E T
    fun reset(){

        // Listing Parameter
        latitude = 0.0
        longitude = 0.0
        category = ""
        tagSearch = ""

        // Filter
        whatCategory = ""
        filter_status = false
        filter_type = ""
        filter_grade = ""
        filter_price_min = ""
        filter_price_max = ""
        filter_distance = ""
        filter_rating = ""

        // CREDENSIAL
        loginStatus = false
        walletStatus = false
        pinStatus = false
        FCM = ""
        token = ""
        refresh = ""

        // PROFILE
        name = ""
        photo = ""
        level = ""
        activity = ""
        referal_code = ""

        restoreKids = null
        restorePromotion = null
        restoreNearMe = null
        restoreEvent = null

    }

    fun filterReset() {
        // Filter AIM
        whatCategory = ""

        filter_status = false
        filter_type = ""
        filter_grade = ""
        filter_price_min = ""
        filter_price_max = ""
        filter_distance = ""
        filter_rating = ""
    }

}