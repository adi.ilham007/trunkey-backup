package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.support.constraint.ConstraintLayout
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.model.TiketModel
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.Window
import android.widget.*
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_ticket.view.*
import kotlinx.android.synthetic.main.list_ticket2.view.*
import kotlinx.android.synthetic.main.list_detail_tiket_pay.view.*
import android.widget.ArrayAdapter
import android.widget.TextView

/*

- Kids Data merupakan data anak yang tersimpan didalam server
==================================================================================================
- memberArray digunakan saat data diedit, mengolah data anak satu persatu ID-child dan NAMA-manual
  data yang tersimpan digunakan untuk satu tiket saja, untuk kemudian digabung di restoreMember
- memberArray direset saat user mengakses tombol DONE, kemudian bisa dilanjutkan dengan proses
  edit data melalui restoreProductId dan restoreMember.

memberArray = [ idAnak , idAnak , idAnak , namaAnak , namaAnak } - MAX 5
==================================================================================================
- Restore digunakan untuk view data setelah diedit, restore berupa gabungan semua tiket yang dibeli
  berserta membernya (ID-child dan NAMA-manual)
- Space digunakan sebagai pemisah antara ID / NAMA member, karena digabung menjadi satu index array
- Restore direset saat tiket CHECKED / UNCHECKED

restoreProductId = [        idTiket          ,        idTiket          ,        idTiket          ]
restoreMember =    [ idAnak |>:M:<| namaAnak , idAnak |>:M:<| namaAnak , idAnak |>:M:<| namaAnak ]
==================================================================================================
- Mekanisme intent broadcast, data tiket dikirim terlebih dahulu saat CHECKED / UNCHECKED, lalu
  diikuti dengan data member per-tiket yang dikirim saat user mengakses tombol DONE. Diikuti dengan
  perintah (+) dan (-), masing-masing untuk menambahkan dan menghapus data di kelas tujuan

 */

class TiketAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var tiketList = mutableListOf<TiketModel>()

    // Restore Data
    val space = "|>:M:<|"
    val restorProductId = ArrayList<Int>()
    val restoreMember = ArrayList<String>()

    // Member per ticket
    var memberArray = ArrayList<String>()

    // Kids Data
    val kidsIdArray = ArrayList<String>()
    val kidsNameArray = ArrayList<String>()

    companion object {
        var TYPE_OF_VIEW = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (TYPE_OF_VIEW) {
            0 -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_ticket, parent, false))
            1 -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_ticket2, parent, false))
            else -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_detail_tiket_pay, parent, false))
        }
    }

    override fun getItemCount(): Int = tiketList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder

        holder.bindView(tiketList[position])
        holder.setIsRecyclable(false)

        when (TYPE_OF_VIEW) {
            0 -> {
                holder.itemView.constraintLayout2.setOnClickListener {view ->
                    holder.itemView.constraintLayout2.startAnimation(user_info.animasiButton)

                    val dialogs = Dialog(view.context)
                    dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialogs.setCancelable(true)
                    dialogs.setContentView(R.layout.popup_detail_tiket)

                    val namaTiket = dialogs.findViewById<TextView>(R.id.nama)
                    val hargaTiket = dialogs.findViewById<TextView>(R.id.harga)
                    val popupCashback = dialogs.findViewById<TextView>(R.id.cash_point)
                    val popuptxtCashback = dialogs.findViewById<TextView>(R.id.txcash_point)
                    val desc = dialogs.findViewById<TextView>(R.id.textView52)

                    namaTiket.text = holder.itemView.tiket_name.text
                    hargaTiket.text = holder.itemView.tiket_harga2.text
                    Log.d("aim","tiket adapter "+holder.itemView.tiket_harga2.text)
                    if(holder.itemView.tiket_harga2.text != "IDR 0"){
                        popupCashback.text = holder.itemView.tiket_harga1.text
                    }else{
                        popupCashback.visibility = View.GONE
                        popuptxtCashback.visibility = View.GONE
                    }

                    desc.text = holder.itemView.tiket_desc.text
                    dialogs.show()
                }

            }
            1 -> {
                holder.itemView.check_box?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        val intent = Intent("data")
                        intent.putExtra("state","+")
                        intent.putExtra("productId",tiketList[position].id)
                        intent.putExtra("productPrice",tiketList[position].harga2)
                        intent.putExtra("productCashBack",tiketList[position].cashBack)
                        intent.putExtra("productMaxCashBack",tiketList[position].maxCashBack)
                        LocalBroadcastManager.getInstance(holder.itemView.context).sendBroadcast(intent)

                        restorProductId.add(tiketList[position].id)
                        showDialog(holder.itemView,tiketList[position].id,false)
                    } else {
                        val intent = Intent("data")
                        intent.putExtra("state","-")
                        intent.putExtra("productId",tiketList[position].id)
                        intent.putExtra("productPrice",tiketList[position].harga2)
                        intent.putExtra("productCashBack",tiketList[position].cashBack)
                        intent.putExtra("productMaxCashBack",tiketList[position].maxCashBack)

                        val memberString = memberArray.toString()
                                .replace("[","")
                                .replace("_"," ")
                                .replace("]","")
                                .replace(",",space)

                        holder.itemView.member_lay?.visibility = View.GONE
                        holder.itemView.name_satu?.setText("")
                        holder.itemView.name_dua?.setText("")
                        holder.itemView.name_tiga?.setText("")
                        holder.itemView.name_empat?.setText("")
                        holder.itemView.name_lima?.setText("")

                        restorProductId.remove(tiketList[position].id)
                        restoreMember.remove(memberString)
                        memberArray.clear()
                    }
                }

                holder.itemView.btn_edit?.setOnClickListener {
                    // Reset data produk agar index final pembayaran sesuai
                    memberArray.clear()
                    showDialog(holder.itemView,tiketList[position].id,true)
                }
            }
            2 -> {

            }
        }

    }

    fun getChild() {
        if (user_info.restoreKids != null) {
            val dataArray = user_info.restoreKids
            if (dataArray != null) {

                kidsIdArray.clear()
                kidsNameArray.clear()
                memberArray.clear()

                for (i in 0 until dataArray.length()) {
                    val childObject = dataArray.getJSONObject(i)
                    kidsIdArray.add(childObject.getString("id"))
                    kidsNameArray.add(childObject.getString("nama"))
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDialog(mView: View, restoreId:Int, isEdit:Boolean) {

        getChild()

        val dialogs = Dialog(mView.context)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.participant_doalog)

        val txTitle = dialogs.findViewById<TextView>(R.id.tx_title)
        val txJumlah = dialogs.findViewById<TextView>(R.id.tx_jumlah)
        val listView = dialogs.findViewById<ListView>(R.id.list_view)

        val textSatu = dialogs.findViewById<EditText>(R.id.nama_satu)
        val textDua = dialogs.findViewById<EditText>(R.id.nama_dua)
        val textTiga = dialogs.findViewById<EditText>(R.id.nama_tiga)
        val textEmpat = dialogs.findViewById<EditText>(R.id.nama_empat)
        val textLima = dialogs.findViewById<EditText>(R.id.nama_lima)

        val btnSatu = dialogs.findViewById<View>(R.id.btn_satu)
        val btnDua = dialogs.findViewById<View>(R.id.btn_dua)
        val btnTiga = dialogs.findViewById<View>(R.id.btn_tiga)
        val btnEmpat = dialogs.findViewById<View>(R.id.btn_empat)
        val btnLima = dialogs.findViewById<View>(R.id.btn_lima)

        val laySatu = dialogs.findViewById<LinearLayout>(R.id.lay_satu)
        val layDua = dialogs.findViewById<LinearLayout>(R.id.lay_dua)
        val layTiga = dialogs.findViewById<LinearLayout>(R.id.lay_tiga)
        val layEmpat = dialogs.findViewById<LinearLayout>(R.id.lay_empat)
        val layLima = dialogs.findViewById<LinearLayout>(R.id.lay_lima)

        val childLay = dialogs.findViewById<LinearLayout>(R.id.child_lay)
        val otherLay = dialogs.findViewById<LinearLayout>(R.id.other_lay)

        val btnCancel = dialogs.findViewById<TextView>(R.id.btn_cancel)
        val btnDone = dialogs.findViewById<TextView>(R.id.btn_done)
        val btnOther = dialogs.findViewById<TextView>(R.id.btn_other)
        var stateOther = 0

        listView?.adapter = ArrayAdapter<String>(mView.context,R.layout.simple_checked_textview,kidsNameArray)
        listView?.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        Log.d("aim","edit : $isEdit, member : $memberArray")
        if (isEdit) {
            btnCancel.visibility = View.GONE
        }

        listView?.setOnItemClickListener { parent, view, position, id ->
            if (memberArray.contains(kidsIdArray[position])){
                memberArray.remove(kidsIdArray[position])
            } else {
                if (memberArray.size <= 4) {
                    memberArray.add(kidsIdArray[position])
                } else {
                    listView.setItemChecked(position, false)
                }
            }
            txJumlah.text = "${memberArray.size} / 5"
            Log.d("aim","$memberArray")
        }

        textSatu?.addTextChangedListener(object : TextWatcher {
            var textBefore = ""
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){
                textBefore = p0.toString().replace(" ","_")
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){ }
            override fun afterTextChanged(p0: Editable?){
                if (textBefore != p0.toString()) {
                    if (memberArray.contains(textBefore)) { memberArray.remove(textBefore) }
                    memberArray.add(p0.toString().replace(" ","_"))
                }
                txJumlah.text = "${memberArray.size} / 5"
            }
        })
        textDua?.addTextChangedListener(object : TextWatcher {
            var textBefore = ""
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){
                textBefore = p0.toString().replace(" ","_")
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){ }
            override fun afterTextChanged(p0: Editable?){
                if (textBefore != p0.toString()) {
                    if (memberArray.contains(textBefore)) { memberArray.remove(textBefore) }
                    memberArray.add(p0.toString().replace(" ","_"))
                }
                txJumlah.text = "${memberArray.size} / 5"
            }
        })
        textTiga?.addTextChangedListener(object : TextWatcher {
            var textBefore = ""
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){
                textBefore = p0.toString().replace(" ","_")
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){ }
            override fun afterTextChanged(p0: Editable?){
                if (textBefore != p0.toString()) {
                    if (memberArray.contains(textBefore)) { memberArray.remove(textBefore) }
                    memberArray.add(p0.toString().replace(" ","_"))
                }
                txJumlah.text = "${memberArray.size} / 5"
            }
        })
        textEmpat?.addTextChangedListener(object : TextWatcher {
            var textBefore = ""
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){
                textBefore = p0.toString().replace(" ","_")
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){ }
            override fun afterTextChanged(p0: Editable?){
                if (textBefore != p0.toString()) {
                    if (memberArray.contains(textBefore)) { memberArray.remove(textBefore) }
                    memberArray.add(p0.toString().replace(" ","_"))
                }
                txJumlah.text = "${memberArray.size} / 5"
            }
        })
        textLima?.addTextChangedListener(object : TextWatcher {
            var textBefore = ""
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){
                textBefore = p0.toString().replace(" ","_")
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){ }
            override fun afterTextChanged(p0: Editable?){
                if (textBefore != p0.toString()) {
                    if (memberArray.contains(textBefore)) { memberArray.remove(textBefore) }
                    memberArray.add(p0.toString().replace(" ","_"))
                }
                txJumlah.text = "${memberArray.size} / 5"
            }
        })

        btnSatu?.setOnClickListener {
            if (memberArray.size <= 4 && textSatu.text.isNotEmpty() && !isEdit) {
                layDua.visibility = View.VISIBLE
                textDua.requestFocus()
            } else if (isEdit){
                val content = textSatu?.text.toString().replace(" ","_")
                memberArray.remove(content)

                laySatu?.visibility = View.GONE
                txJumlah.text = "${memberArray.size} / 5"
            }
        }
        btnDua?.setOnClickListener {
            if (memberArray.size <= 4 && textDua.text.isNotEmpty() && !isEdit) {
                layTiga.visibility = View.VISIBLE
                textTiga.requestFocus()
            } else if (isEdit){
                val content = textDua?.text.toString().replace(" ","_")
                memberArray.remove(content)

                layDua?.visibility = View.GONE
                txJumlah.text = "${memberArray.size} / 5"
            }
        }
        btnTiga?.setOnClickListener {
            if (memberArray.size <= 4 && textTiga.text.isNotEmpty() && !isEdit) {
                layEmpat.visibility = View.VISIBLE
                textEmpat.requestFocus()
            } else if (isEdit){
                val content = textTiga?.text.toString().replace(" ","_")
                memberArray.remove(content)

                layTiga?.visibility = View.GONE
                txJumlah.text = "${memberArray.size} / 5"
            }
        }
        btnEmpat?.setOnClickListener {
            if (memberArray.size <= 4 && textEmpat.text.isNotEmpty() && !isEdit) {
                layLima.visibility = View.VISIBLE
                textLima.requestFocus()
            } else if (isEdit){
                val content = textEmpat?.text.toString().replace(" ","_")
                memberArray.remove(content)

                layEmpat?.visibility = View.GONE
                txJumlah.text = "${memberArray.size} / 5"
            }
        }
        btnLima?.setOnClickListener {
            if (memberArray.size <= 4 && textLima.text.isNotEmpty() && !isEdit) {
                // Nothing
            } else if (isEdit){
                val content = textLima?.text.toString().replace(" ","_")
                memberArray.remove(content)

                layLima?.visibility = View.GONE
                txJumlah.text = "${memberArray.size} / 5"
            }
        }

        // ========================================================================================= Cancel
        btnCancel?.setOnClickListener {
            // Simpan data peserta jika edit, hapus jika input
            if (!isEdit) {
                val memberString = memberArray.toString()
                        .replace("[", "")
                        .replace("_", " ")
                        .replace("]", "")
                        .replace(",", space)

                val intent = Intent("data")
                intent.putExtra("state", "-member")
                intent.putExtra("memberArray", memberString)
                LocalBroadcastManager.getInstance(mView.context).sendBroadcast(intent)

                mView.check_box.isChecked = false
                mView.member_lay?.visibility = View.GONE

                mView.name_satu?.setText("")
                mView.name_dua?.setText("")
                mView.name_tiga?.setText("")
                mView.name_empat?.setText("")
                mView.name_lima?.setText("")

                mView.name_satu?.visibility = View.GONE
                mView.name_dua?.visibility = View.GONE
                mView.name_tiga?.visibility = View.GONE
                mView.name_empat?.visibility = View.GONE
                mView.name_lima?.visibility = View.GONE

                memberArray.clear()
            }
            dialogs.dismiss()
        }

        // ========================================================================================= Done
        btnDone?.setOnClickListener {
            mView.member_lay?.visibility = View.VISIBLE
            txJumlah.text = "${memberArray.size} / 5"

            // ===================================================================================== Add
            if (!isEdit) {
                // Tampilkan data hasil inputan add kids / other
                for (i in 0 until memberArray.size) {
                    // Nama peserta dari id yang dipilih (Add child)
                    if (kidsIdArray.contains(memberArray[i])) {
                        when (i + 1) {
                            1 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_satu?.setText(kidsNameArray[index])
                            }
                            2 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_dua?.setText(kidsNameArray[index])
                            }
                            3 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_tiga?.setText(kidsNameArray[index])
                            }
                            4 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_empat?.setText(kidsNameArray[index])
                            }
                            5 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_lima?.setText(kidsNameArray[index])
                            }
                        }
                    }
                    // Nama peserta dari input manual (other kids)
                    else {
                        when (i + 1) {
                            1 -> {
                                mView.name_satu?.setText(memberArray[i].replace("_", " "))
                            }
                            2 -> {
                                mView.name_dua?.setText(memberArray[i].replace("_", " "))
                            }
                            3 -> {
                                mView.name_tiga?.setText(memberArray[i].replace("_", " "))
                            }
                            4 -> {
                                mView.name_empat?.setText(memberArray[i].replace("_", " "))
                            }
                            5 -> {
                                mView.name_lima?.setText(memberArray[i].replace("_", " "))
                            }
                        }
                    }
                }

                // Hide layout edit text yang tidak terpakai
                when (memberArray.size) {
                    1 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.GONE
                        mView.name_tiga?.visibility = View.GONE
                        mView.name_empat?.visibility = View.GONE
                        mView.name_lima?.visibility = View.GONE
                    }
                    2 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.GONE
                        mView.name_empat?.visibility = View.GONE
                        mView.name_lima?.visibility = View.GONE
                    }
                    3 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.VISIBLE
                        mView.name_empat?.visibility = View.GONE
                        mView.name_lima?.visibility = View.GONE
                    }
                    4 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.VISIBLE
                        mView.name_empat?.visibility = View.VISIBLE
                        mView.name_lima?.visibility = View.GONE
                    }
                    5 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.VISIBLE
                        mView.name_empat?.visibility = View.VISIBLE
                        mView.name_lima?.visibility = View.VISIBLE
                    }
                }

                val memberString = memberArray.toString()
                        .replace("[", "")
                        .replace("_", " ")
                        .replace("]", "")
                        .replace(",", space)

                restoreMember.add(memberString)

                val intent = Intent("data")
                intent.putExtra("state", "+member")
                intent.putExtra("memberArray", memberString)
                LocalBroadcastManager.getInstance(mView.context).sendBroadcast(intent)
            }

            // ===================================================================================== Edit
            else if (isEdit) {
                // Tampilkan data hasil inputan add kids / other
                for (i in 0 until memberArray.size) {
                    Log.d("aim", "EDIT MEMBER : ${memberArray[i]}")


                    // Nama peserta dari id yang dipilih (Add child)
                    if (kidsIdArray.contains(memberArray[i])) {
                        when (i + 1) {
                            1 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_satu?.setText(kidsNameArray[index])
                            }
                            2 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_dua?.setText(kidsNameArray[index])
                            }
                            3 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_tiga?.setText(kidsNameArray[index])
                            }
                            4 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_empat?.setText(kidsNameArray[index])
                            }
                            5 -> {
                                val index = kidsIdArray.indexOf(memberArray[i])
                                mView.name_lima?.setText(kidsNameArray[index])
                            }
                        }
                    }
                    // Nama peserta dari input manual (other kids)
                    else {
                        when (i + 1) {
                            1 -> {
                                mView.name_satu?.setText(memberArray[i].replace("_", " "))
                            }
                            2 -> {
                                mView.name_dua?.setText(memberArray[i].replace("_", " "))
                            }
                            3 -> {
                                mView.name_tiga?.setText(memberArray[i].replace("_", " "))
                            }
                            4 -> {
                                mView.name_empat?.setText(memberArray[i].replace("_", " "))
                            }
                            5 -> {
                                mView.name_lima?.setText(memberArray[i].replace("_", " "))
                            }
                        }
                    }
                }

                // Hide layout edit text yang tidak terpakai
                when (memberArray.size) {
                    1 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.GONE
                        mView.name_tiga?.visibility = View.GONE
                        mView.name_empat?.visibility = View.GONE
                        mView.name_lima?.visibility = View.GONE
                    }
                    2 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.GONE
                        mView.name_empat?.visibility = View.GONE
                        mView.name_lima?.visibility = View.GONE
                    }
                    3 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.VISIBLE
                        mView.name_empat?.visibility = View.GONE
                        mView.name_lima?.visibility = View.GONE
                    }
                    4 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.VISIBLE
                        mView.name_empat?.visibility = View.VISIBLE
                        mView.name_lima?.visibility = View.GONE
                    }
                    5 -> {
                        mView.name_satu?.visibility = View.VISIBLE
                        mView.name_dua?.visibility = View.VISIBLE
                        mView.name_tiga?.visibility = View.VISIBLE
                        mView.name_empat?.visibility = View.VISIBLE
                        mView.name_lima?.visibility = View.VISIBLE
                    }
                }

                val memberString = memberArray.toString()
                        .replace("[", "")
                        .replace("_", " ")
                        .replace("]", "")
                        .replace(",", space)

                restoreMember.add(memberString)

                val intent = Intent("data")
                intent.putExtra("state", "edit")
                intent.putExtra("productId",restoreId)
                intent.putExtra("memberArray", memberString)
                LocalBroadcastManager.getInstance(mView.context).sendBroadcast(intent)
            }

            // Proses edit data telah selesai
            memberArray.clear()
            dialogs.dismiss()
        }

        // ========================================================================================= Other
        btnOther?.setOnClickListener {
            stateOther++
            if (stateOther % 2 == 0) {
                txTitle?.text = "Select your kid's"
                btnOther.text = "Other"
                otherLay?.visibility = View.GONE
                childLay?.visibility = View.VISIBLE
            } else {
                txTitle?.text = "Add other kid's"
                btnOther.text = "Kids"
                otherLay?.visibility = View.VISIBLE
                childLay?.visibility = View.GONE
            }
        }

        // ========================================================================================= Tampilkan data Restore
        if (restorProductId.contains(restoreId) && isEdit) {
            val restoreIndex = restorProductId.indexOf(restoreId)
            val stringMember = restoreMember.getOrNull(restoreIndex)
            Log.d("aim","After Restore : $restoreMember into -> $memberArray")

            if (stringMember != null) {
                // Hapus data restore untuk diedit
                restoreMember.remove(stringMember)

                val generateMember = stringMember.split(space)
                val allKidsName = ArrayList<String>()

                for (i in 0 until generateMember.size) {
                    if (kidsIdArray.contains(generateMember[i].trim())) {
                        val kidsIndex = kidsIdArray.indexOf(generateMember[i].trim())
                        listView.setItemChecked(kidsIndex, true)
                        memberArray.add(generateMember[i].trim())
                        Log.d("aim", "ID : ${generateMember[i]}")
                    } else {
                        allKidsName.add(generateMember[i].trim().capitalize())
                        Log.d("aim", "NAME : ${generateMember[i]}")
                    }

                }

                txJumlah.text = "${memberArray.size} / 5"
                when (allKidsName.size) {
                    1 -> {
                        btnSatu.setBackgroundResource(R.drawable.ic_remove)
                        textSatu?.setText(allKidsName[0].trim())
                        textSatu?.visibility = View.VISIBLE
                    }
                    2 -> {
                        btnSatu.setBackgroundResource(R.drawable.ic_remove)
                        btnDua.setBackgroundResource(R.drawable.ic_remove)
                        textSatu?.setText(allKidsName[0].trim())
                        textDua?.setText(allKidsName[1].trim())
                        laySatu?.visibility = View.VISIBLE
                        layDua?.visibility = View.VISIBLE
                    }
                    3 -> {
                        btnSatu.setBackgroundResource(R.drawable.ic_remove)
                        btnDua.setBackgroundResource(R.drawable.ic_remove)
                        btnTiga.setBackgroundResource(R.drawable.ic_remove)
                        textSatu?.setText(allKidsName[0].trim())
                        textDua?.setText(allKidsName[1].trim())
                        textTiga?.setText(allKidsName[2].trim())
                        laySatu?.visibility = View.VISIBLE
                        layDua?.visibility = View.VISIBLE
                        layTiga?.visibility = View.VISIBLE
                    }
                    4 -> {
                        btnSatu.setBackgroundResource(R.drawable.ic_remove)
                        btnDua.setBackgroundResource(R.drawable.ic_remove)
                        btnTiga.setBackgroundResource(R.drawable.ic_remove)
                        btnEmpat.setBackgroundResource(R.drawable.ic_remove)
                        textSatu?.setText(allKidsName[0].trim())
                        textDua?.setText(allKidsName[1].trim())
                        textTiga?.setText(allKidsName[2].trim())
                        textEmpat?.setText(allKidsName[3].trim())
                        laySatu?.visibility = View.VISIBLE
                        layDua?.visibility = View.VISIBLE
                        layTiga?.visibility = View.VISIBLE
                        layEmpat?.visibility = View.VISIBLE
                    }
                    5 -> {
                        btnSatu.setBackgroundResource(R.drawable.ic_remove)
                        btnDua.setBackgroundResource(R.drawable.ic_remove)
                        btnTiga.setBackgroundResource(R.drawable.ic_remove)
                        btnEmpat.setBackgroundResource(R.drawable.ic_remove)
                        btnLima.setBackgroundResource(R.drawable.ic_remove)
                        textSatu?.setText(allKidsName[0].trim())
                        textDua?.setText(allKidsName[1].trim())
                        textTiga?.setText(allKidsName[2].trim())
                        textEmpat?.setText(allKidsName[3].trim())
                        textLima?.setText(allKidsName[4].trim())
                        laySatu?.visibility = View.VISIBLE
                        layDua?.visibility = View.VISIBLE
                        layTiga?.visibility = View.VISIBLE
                        layEmpat?.visibility = View.VISIBLE
                        layLima?.visibility = View.VISIBLE
                    }
                }

            }
            Log.d("aim","COK : $memberArray")
        }

        dialogs.show()
    }

    fun setList(listOfVendor: List<TiketModel>) {
        this.tiketList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<TiketModel>) {
        this.tiketList.addAll(listOfVendor)
        notifyDataSetChanged()
    }

    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()

        @SuppressLint("SetTextI18n")
        fun bindView(model: TiketModel) {
            when (TYPE_OF_VIEW) {
                0 -> {

                    // Handle Background
//                    if (model.tipe == "ticket") {
//                        itemView.constraintLayout2.setBackgroundResource(R.drawable.ic_bg_ticket)
//                    } else if (model.tipe == "class") {
//                        itemView.constraintLayout2.setBackgroundResource(R.drawable.ic_bg_courses)
//                    }
                    itemView.tiket_name.text = model.nama
                    itemView.tiket_desc.text = helper.textHtml(model.ket)

                    if (model.harga1 > model.harga2) {
                        itemView.tiket_harga1.visibility = View.VISIBLE
                        itemView.tiket_harga1.text = helper.bank(model.harga1)
                        itemView.tiket_harga1.paintFlags = itemView.tiket_harga1.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    } else {
                        itemView.tiket_harga1.visibility = View.GONE
                    }

                    itemView.tiket_harga2.text = helper.bank(model.harga2)

                    if(model.cashBack != 0.0){
                        itemView.tx_cashback.visibility = View.VISIBLE
                        itemView.tiket_cashback.visibility = View.VISIBLE

                        itemView.tx_max_cashback.visibility = View.VISIBLE
                        itemView.tiket_max_cashback.visibility = View.VISIBLE

                        itemView.tiket_cashback.text = helper.bank(model.cashBack)
                        itemView.tiket_max_cashback.text = helper.bank(model.maxCashBack)
                        Log.d("aim","tiket ${model.cashBack}")
                        Log.d("aim","tiket idr ${helper.bank(model.cashBack)}")

                    }else{
                        itemView.tx_cashback.visibility = View.GONE
                        itemView.tiket_cashback.visibility = View.GONE

                        itemView.tx_max_cashback.visibility = View.GONE
                        itemView.tiket_max_cashback.visibility = View.GONE
                    }
                }
                1 -> {
                    // Ticket Detail
                    itemView.ticket_name.text        = model.nama
                    itemView.ticket_desc.text        = helper.textHtml(model.ket)


//                    if (model.harga1 > model.harga2) {
//                        itemView.harga_satu.text        = helper.bank(model.harga1)
//                        itemView.harga_satu.paintFlags = itemView.harga_satu.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
//                    } else {
//                        itemView.harga_satu.visibility  = View.GONE
//                    }
                    itemView.ticket_price.text         = helper.bank(model.harga2)
//                    itemView.tx_nama_date.text      = model.date
                    //                itemView.time.text              = model.time
//                    itemView.txLocation.text		= model.lokasi

//                    when(model.jumlah){
//                        1 -> {
//                            itemView.dua.visibility = View.GONE
//                            itemView.pencil2.visibility = View.GONE
//                            itemView.tiga.visibility = View.GONE
//                            itemView.pencil3.visibility = View.GONE
//                            itemView.empat.visibility = View.GONE
//                            itemView.pencil4.visibility = View.GONE
//                            itemView.lima.visibility = View.GONE
//                            itemView.pencil5.visibility = View.GONE
//                        } 2 -> {
//                            itemView.tiga.visibility = View.GONE
//                            itemView.pencil3.visibility = View.GONE
//                            itemView.empat.visibility = View.GONE
//                            itemView.pencil4.visibility = View.GONE
//                            itemView.lima.visibility = View.GONE
//                            itemView.pencil5.visibility = View.GONE
//                        } 3 -> {
//                            itemView.empat.visibility = View.GONE
//                            itemView.pencil4.visibility = View.GONE
//                            itemView.lima.visibility = View.GONE
//                            itemView.pencil5.visibility = View.GONE
//                        } 4 -> {
//                            itemView.lima.visibility = View.GONE
//                            itemView.pencil5.visibility = View.GONE
//                        }
//                    }
                }
                2 -> {
                    // Ticket Detail
                    itemView.id2.text               = "T#00${model.id}"
                    itemView.nama2.text             = model.nama
                    itemView.keterangan2.text       = helper.textHtml(model.ket)
                    if (model.harga1 > model.harga2) {
                        itemView.harga11.text        = helper.bank(model.harga1)
                        itemView.harga11.paintFlags  = itemView.harga11.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    } else {
                        itemView.harga11.visibility  = View.GONE
                    }
                    itemView.harga22.text         = helper.bank(model.harga2)

                    val listMember = model.member.split("|-<o>< ><o>-|")
                    val totalMember = listMember.size

                    val emptyCheck = model.member.replace("|-<o>< ><o>-|",", ")

                    if (emptyCheck != "") {
                        when (totalMember) {
                            1 -> {
                                itemView.tvSatu.text = "Participant 1 : ${listMember[0]}"
                                itemView.tvDua.visibility = View.GONE
                                itemView.tvTiga.visibility = View.GONE
                                itemView.tvEmpat.visibility = View.GONE
                                itemView.tvLima.visibility = View.GONE
                            }
                            2 -> {
                                itemView.tvSatu.text = "Participant 1 : ${listMember[0]}"
                                itemView.tvDua.text = "Participant 2 : ${listMember[1]}"
                                itemView.tvTiga.visibility = View.GONE
                                itemView.tvEmpat.visibility = View.GONE
                                itemView.tvLima.visibility = View.GONE
                            }
                            3 -> {
                                itemView.tvSatu.text = "Participant 1 : ${listMember[0]}"
                                itemView.tvDua.text = "Participant 2 : ${listMember[1]}"
                                itemView.tvTiga.text = "Participant 3 : ${listMember[2]}"
                                itemView.tvEmpat.visibility = View.GONE
                                itemView.tvLima.visibility = View.GONE
                            }
                            4 -> {
                                itemView.tvSatu.text = "Participant 1 : ${listMember[0]}"
                                itemView.tvDua.text = "Participant 2 : ${listMember[1]}"
                                itemView.tvTiga.text = "Participant 3 : ${listMember[2]}"
                                itemView.tvEmpat.text = "Participant 4 : ${listMember[3]}"
                                itemView.tvLima.visibility = View.GONE
                            }
                            5 -> {
                                itemView.tvSatu.text = "Participant 1 : ${listMember[0]}"
                                itemView.tvDua.text = "Participant 2 : ${listMember[1]}"
                                itemView.tvTiga.text = "Participant 3 : ${listMember[2]}"
                                itemView.tvEmpat.text = "Participant 4 : ${listMember[3]}"
                                itemView.tvLima.text = "Participant 5 : ${listMember[4]}"
                            }

                        }
                    } else {
                        itemView.tvSatu.text = "Participant : N/A"
                        itemView.tvDua.visibility = View.GONE
                        itemView.tvTiga.visibility = View.GONE
                        itemView.tvEmpat.visibility = View.GONE
                        itemView.tvLima.visibility = View.GONE
                    }
                }
            }
        }
    }
}