package innovacia.co.id.trunkey1

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_download_preview.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import android.support.v7.widget.PopupMenu
import android.view.MenuItem
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_download_preview.back_link
import kotlinx.android.synthetic.main.sub_header.*


class DownloadPreview : AppCompatActivity() {

    private var writeStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE

    private var imageName = ""
    private var imageUrl = ""
    private var imageBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_download_preview)

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }
        
        if (intent.hasExtra("title")) {
            imageName = intent.getStringExtra("title")
            imageUrl = intent.getStringExtra("imageUrl")

            Glide.with(this)
                    .asBitmap()
                    .load(imageUrl)
                    .into(object : SimpleTarget<Bitmap>(){
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            image_preview.setImageBitmap(resource)
                            imageBitmap = resource
                        }
                    })

            opsi.setOnClickListener {
                popupMenu(it)
            }

        }


    }

    private fun popupMenu(view: View) {
        val popup: PopupMenu?
        popup = PopupMenu(this, view)
        popup.inflate(R.menu.menu_download)

        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

            when (item!!.itemId) {
                R.id.download -> {
                    if (imageBitmap != null) {
                        saveImage()
                    }
                }
                R.id.share -> {
                    share()
                }
            }

            true
        })

        popup.show()
    }

    private fun share() {
        val share = Intent(Intent.ACTION_SEND)
        share.type = "image/jpeg"
        val bytes = ByteArrayOutputStream()
        imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val f = File((Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)), "/Trunkey/$imageName.jpeg")
        try {
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            fo.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        share.putExtra(Intent.EXTRA_STREAM, Uri.parse(f.path))
        startActivity(Intent.createChooser(share, "Share Attachment"))


    }

    private fun saveImage() = runWithPermissions(writeStorage) {
        val imageFileName = "$imageName.jpeg"
        val storageDir = File((Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)), "/Trunkey")
        var success = true
        if (!storageDir.exists())
        {
            success = storageDir.mkdirs()
        }
        if (success)
        {
            val imageFile = File(storageDir, imageFileName)
            val savedImagePath = imageFile.absolutePath
            try
            {
                val fOut = FileOutputStream(imageFile)
                imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            }
            catch (e:Exception) {
                e.printStackTrace()
            }
            // Add the image to the system gallery
            galleryAddPic(savedImagePath)
            Toast.makeText(this, "File saved", Toast.LENGTH_LONG).show()
        }
    }
    private fun galleryAddPic(imagePath:String) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(imagePath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        sendBroadcast(mediaScanIntent)
    }
}
