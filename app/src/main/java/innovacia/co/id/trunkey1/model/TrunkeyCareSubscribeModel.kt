package innovacia.co.id.trunkey1.model

data class TrunkeyCareSubscribeModel(
    var id:String,
    var name:String,
    var desc:String,
    var price:Double
)