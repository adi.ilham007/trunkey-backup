package innovacia.co.id.trunkey1

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_sort.*
import kotlinx.android.synthetic.main.sub_header.*

class Sort : AppCompatActivity() {

    private var price = ""
    private var priceUpState = 0
    private var priceDownState = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sort)
        header_name.text = resources.getString(R.string.tx_sort)

        buttonListener()
        generateView()
    }

    override fun onBackPressed() {
        val i = Intent()
        i.putExtra("resetSort","resetSort")
        setResult(Activity.RESULT_CANCELED,i)
        finish()
    }

    private fun generateView() {
        if (intent.getStringExtra("price") == "desc") {

            btn_price_up.callOnClick()

        } else if (intent.getStringExtra("price") == "asc") {

            btn_price_down.callOnClick()

        }

        Log.d("aim","intent masuk : ${intent.getStringExtra("price")}")
    }

    private fun buttonListener() {
        back_link.setOnClickListener {
            val i = Intent()
            i.putExtra("resetSort","resetSort")
            setResult(Activity.RESULT_CANCELED,i)
            finish()
        }

        btn_price_up.setOnClickListener {
            if (price != "desc") {
                price = "desc"
                ic_price_up.visibility = View.VISIBLE
                btn_price_up.setBackgroundResource(R.drawable.textview_border_yes)

                ic_price_down.visibility = View.INVISIBLE
                btn_price_down.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                price = ""
                ic_price_up.visibility = View.INVISIBLE
                btn_price_up.setBackgroundResource(R.drawable.textview_border_no)
            }
            Log.d("aim","state : $price")
        }

        btn_price_down.setOnClickListener {
            if (price != "asc") {
                price = "asc"
                ic_price_down.visibility = View.VISIBLE
                btn_price_down.setBackgroundResource(R.drawable.textview_border_yes)

                ic_price_up.visibility = View.INVISIBLE
                btn_price_up.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                price = ""
                ic_price_down.visibility = View.INVISIBLE
                btn_price_down.setBackgroundResource(R.drawable.textview_border_no)
            }
            Log.d("aim","state : $price")
        }

        btn_apply.setOnClickListener {
            val intent = Intent()
            intent.putExtra("sort","sort")
            intent.putExtra("price",price)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}
