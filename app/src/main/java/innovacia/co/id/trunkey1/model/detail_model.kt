package innovacia.co.id.trunkey1.model

data class detail_model(
    var slug        :String,
    var nama        :String,
    var harga       :Double,
    var file_photo  :String
)