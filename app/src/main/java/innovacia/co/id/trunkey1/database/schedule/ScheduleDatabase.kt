package innovacia.co.id.trunkey1.database.schedule

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(ScheduleEntity::class), version = 1,exportSchema = false)
abstract class ScheduleDatabase : RoomDatabase(){

    abstract fun dao(): ScheduleDao

    companion object {
        var instance: ScheduleDatabase? = null
        fun getInstance(context: Context): ScheduleDatabase? {
            if (instance == null) {
                synchronized(ScheduleDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext,
                            ScheduleDatabase::class.java, "schedule_database")
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return instance
        }
    }
}
