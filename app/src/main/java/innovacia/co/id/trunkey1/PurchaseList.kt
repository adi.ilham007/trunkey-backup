package innovacia.co.id.trunkey1


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import innovacia.co.id.trunkey1.fragment.purchase.PurchaseListPagerAdapter
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_purchase_list.*
import kotlinx.android.synthetic.main.sub_header.*

class PurchaseList : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase_list)
        header_name.text = resources.getString(R.string.tx_purchase)

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        viewpager_main.adapter = PurchaseListPagerAdapter(supportFragmentManager)
        tabs_main.setupWithViewPager(viewpager_main)
    }
}
