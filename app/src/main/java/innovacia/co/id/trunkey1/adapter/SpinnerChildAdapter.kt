package innovacia.co.id.trunkey1.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.list_child.view.*

class SpinnerChildAdapter (ctx: Context,moods: List<ChildModel>) : ArrayAdapter<ChildModel>(ctx, 0, moods) {

    private var profile_child_list = mutableListOf<ChildModel>()

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {

        return this.createView(position, recycledView, parent)

    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {

        return this.createView(position, recycledView, parent)

    }

    fun addData(listOfTransaction: List<ChildModel>) {
        this.profile_child_list.addAll(listOfTransaction)
        notifyDataSetChanged()
    }

    fun setList(listOfChild: List<ChildModel>) {
        this.profile_child_list = listOfChild.toMutableList()
        notifyDataSetChanged()
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val mChildModel = getItem(position)!!
        val view = recycledView ?: LayoutInflater.from(context).inflate(R.layout.list_child_spinner,parent,false)

        view.nama.text = mChildModel.name

        return view

    }

}