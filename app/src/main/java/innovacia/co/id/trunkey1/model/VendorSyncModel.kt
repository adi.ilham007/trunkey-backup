package innovacia.co.id.trunkey1.model

data class VendorSyncModel(
    val childId     :String,
    val childName   :String,
    val vendorId    :String,
    val vendorName  :String,
    val vendorState :Int
)