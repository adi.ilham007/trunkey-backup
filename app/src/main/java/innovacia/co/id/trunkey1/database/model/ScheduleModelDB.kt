package innovacia.co.id.trunkey1.database.model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import innovacia.co.id.trunkey1.database.schedule.ScheduleEntity
import innovacia.co.id.trunkey1.database.schedule.ScheduleRespository

class ScheduleModelDB constructor(application: Application) : AndroidViewModel(application) {
    private val repository: ScheduleRespository = ScheduleRespository(application)
    private val data: LiveData<List<ScheduleEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<ScheduleEntity>> {
        return data
    }

    fun addData(data: ScheduleEntity) {
        repository.add(data)
    }

    fun updateData(data: ScheduleEntity){
        repository.update(data)
    }

    fun deleteEntry(uom: ScheduleEntity){
        repository.deleteEntry(uom)
    }
}