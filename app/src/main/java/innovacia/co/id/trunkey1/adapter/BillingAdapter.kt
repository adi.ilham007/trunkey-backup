package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.DetailBilling
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.BillingModel
import kotlinx.android.synthetic.main.list_billing_active.view.*
import kotlinx.android.synthetic.main.list_billing_history.view.*

class BillingAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var billingData = mutableListOf<BillingModel>()

    companion object {
        var TYPE_OF_VIEW = 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (billingData[position].status == "1") {
            // paid
            TYPE_OF_VIEW = 1
            1
        } else {
            // active
            TYPE_OF_VIEW = 0
            0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (TYPE_OF_VIEW) {
            0 -> BillingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_billing_active, parent, false))
            else -> BillingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_billing_history, parent, false))
        }
    }

    override fun getItemCount(): Int = billingData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        val bindHolder = viewHolder as BillingViewHolder
        bindHolder.bindView(billingData[position])

        val id = billingData[position].id
        val student = billingData[position].student

        when (TYPE_OF_VIEW) {
            0 -> {
                // Active
                bindHolder.itemView.setOnClickListener { view ->
                    val i = Intent(view.context,DetailBilling::class.java)
                    i.putExtra("idBilling",id)
                    i.putExtra("student",student)
                    view.context.startActivity(i)
                }
            }
        }
    }

    fun setList(listOfChild: List<BillingModel>) {
        this.billingData = listOfChild.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfTransaction: List<BillingModel>) {
        this.billingData.addAll(listOfTransaction)
        notifyDataSetChanged()
    }
    fun reset() {
        billingData.removeAll(billingData)
        notifyDataSetChanged()
    }

    class BillingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()
        @SuppressLint("SetTextI18n")
        fun bindView(billingModel: BillingModel) {
            when (TYPE_OF_VIEW) {
                0 -> {
                    // Active
                    itemView.tx_active_vendor_name?.text = billingModel.vendor_name
                    itemView.tx_active_name?.text = billingModel.name
                    itemView.tx_active_keterangan?.text = billingModel.keterangan
                    itemView.tx_active_tanggal?.text = billingModel.expiredDate
                    itemView.tx_active_harga?.text = helper.bank(billingModel.price)
                    itemView.logo_active_huruf?.text = billingModel.vendor_name[0].toString().capitalize()
                }
                1 -> {
                    // Paid
                    itemView.tx_vendor_name?.text = billingModel.vendor_name
                    itemView.tx_name?.text = billingModel.name
                    itemView.tx_tanggal?.text = billingModel.expiredDate
                    itemView.logo_huruf?.text = billingModel.vendor_name[0].toString().capitalize()
                    itemView.tx_status?.text = if (billingModel.status == "1") {
                        "Paid"
                    } else {
                        "Cancel"
                    }
                }
            }

        }
    }
}