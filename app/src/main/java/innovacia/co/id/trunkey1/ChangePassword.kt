package innovacia.co.id.trunkey1

import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class ChangePassword : AppCompatActivity() {

    val mVolleyHelper = VolleyHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        header_name.text = resources.getString(R.string.change_pass_header)
        //coba
        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_pass.setOnClickListener {
            btn_pass.startAnimation(user_info.animasiButton)

            if (passwordCheck()) {
                updatePassword()
            }
        }
    }

    private fun passwordCheck(): Boolean{
        var cek = 0

        if(old.length() < 5){
            old_lay.error = "At least five characters"; cek++
        }
        if(pass.length() < 5){
            pass_lay.error = "At least five characters"; cek++
        }
        if (pass.text.toString() != confirm.text.toString()){
            confirm_lay.error = "Password mismatch"; cek++
        }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun updatePassword() {
        val errSection = "update password"
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val body = JSONObject()
        body.put("old_password",old.text.toString())
        body.put("new_password",pass.text.toString())
        body.put("new_password_confirmation",confirm.text.toString())

        val rq = object : JsonObjectRequest(Request.Method.POST,user_info.reset_password,body,Response.Listener { response ->
            dialog.dismiss()

            finish()

        },Response.ErrorListener { response ->
            dialog.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }
}
