package innovacia.co.id.trunkey1.model

data class PlaceModel (
        val id:String,
        val nama : String,
        val alamat : String,
        val telepon : String,
        val lattitude : Double,
        val longitude : Double,
        val share : String,
        val theme : String
)