package innovacia.co.id.trunkey1.database.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.database.cart.CartEntity
import innovacia.co.id.trunkey1.database.cart.CartRespository
import innovacia.co.id.trunkey1.helper.user_info
import android.util.SparseBooleanArray
import innovacia.co.id.trunkey1.adapter.ScheduleAdapter
import innovacia.co.id.trunkey1.database.schedule.ScheduleEntity
import innovacia.co.id.trunkey1.database.schedule.ScheduleRespository

class ScheduleByDateAdapter : RecyclerView.Adapter<ScheduleByDateAdapter.ScheduleViewHolder> {
    val helper = user_info()
    var mContext: Context
    var mRepository: ScheduleRespository
    var data: List<ScheduleEntity>? = null

    constructor(context: Context?, data: List<ScheduleEntity>, repository: ScheduleRespository){
        this.mContext       = context!!
        this.data           = data
        this.mRepository    = repository
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.list_schedule_by_date, parent, false)
        return ScheduleViewHolder(v)
    }

    override fun getItemCount(): Int {
        if(data != null)
            return data!!.size
        else
            return 0
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
        val mScheduleEntity = data!!.get(position)

        holder.txStartTime.text = mScheduleEntity.startTime
        holder.txEndTime.text = mScheduleEntity.endTime
        holder.txName.text = mScheduleEntity.subject
        holder.txLogo.text = mScheduleEntity.subject[0].toString().capitalize()
    }

    fun setDatas(data: List<ScheduleEntity>){
//        notifyDataSetChanged()
        this.data = data
    }

//    fun reset() {
//        itemStateArray.clear()
//    }


    class ScheduleViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txStartTime = itemView.findViewById<TextView>(R.id.tx_startTime)
        val txEndTime = itemView.findViewById<TextView>(R.id.tx_endTime)
        val txName = itemView.findViewById<TextView>(R.id.tx_name)
        val txLogo = itemView.findViewById<TextView>(R.id.logo_huruf)

    }
}