package innovacia.co.id.trunkey1.database.model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import innovacia.co.id.trunkey1.database.cart.CartRespository
import innovacia.co.id.trunkey1.database.cart.CartEntity

class CartModelDB constructor(application: Application) : AndroidViewModel(application) {
    private val repository: CartRespository = CartRespository(application)
    private val data: LiveData<List<CartEntity>>

    init {
        data = repository.getAll()
    }

    fun getAll(): LiveData<List<CartEntity>> {
        return data
    }

    fun addData(data: CartEntity) {
        repository.add(data)
    }

    fun updateData(data: CartEntity){
        repository.update(data)
    }

    fun deleteEntry(uom: CartEntity){
        repository.deleteEntry(uom)
    }
}