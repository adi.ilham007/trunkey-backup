package innovacia.co.id.trunkey1

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import innovacia.co.id.trunkey1.adapter.SpinnerChildAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.activity_create_agenda.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateAgenda : AppCompatActivity() {

    private val mChildModel = mutableListOf<ChildModel>()

    private var scheduleId = ""
    private var kidsId = ""

    private var categoryNameArray = ArrayList<String>()
    private var categoryIdArray = ArrayList<String>()
    private var categoryId = ""

    private var start = ""
    private var end = ""

    private val weeklyArray = ArrayList<Int>()
    private var repeatType = ""
    private var customType = ""
    private var repeatEnd = ""

    @SuppressLint("SimpleDateFormat")
    val sdf = SimpleDateFormat("yyyy-MM-dd-u")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_agenda)
        header_name.text = resources.getString(R.string.schedule_add_header)

        generatedView()

        restoreKidsFun()

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        spinner_kids.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                kidsId = mChildModel[position].id
            }
        }

        spinner_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                categoryId = categoryIdArray[position]
            }
        }

        startTime_value.setOnClickListener {
            startTime_value?.startAnimation(user_info.animasiButton)
            SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .displayDays(false)
                    .displayDaysOfMonth(true)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayAmPm(false)
                    .minutesStep(10)
                    .listener(object : SingleDateAndTimePickerDialog.Listener{
                        @SuppressLint("SimpleDateFormat")
                        override fun onDateSelected(datePick: Date?) {
                            Log.d("aim",datePick.toString())
                            val uiFormat = SimpleDateFormat("dd MMMM yyyy - kk:mm")
                            val serverFormat = SimpleDateFormat("yyyy-MM-dd kk:mm:ss")

                            start = serverFormat.format(datePick)
                            startTime_value.text = uiFormat.format(datePick)
                            startTime_value.error = null
                        }
                    }).display()
        }

        endTime_value.setOnClickListener {
            endTime_value?.startAnimation(user_info.animasiButton)
            SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .displayDays(false)
                    .displayDaysOfMonth(true)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayAmPm(false)
                    .minutesStep(10)
                    .listener(object : SingleDateAndTimePickerDialog.Listener{
                        @SuppressLint("SimpleDateFormat")
                        override fun onDateSelected(datePick: Date?) {
                            Log.d("aim",datePick.toString())
                            val uiFormat = SimpleDateFormat("dd MMMM yyyy - kk:mm")
                            val serverFormat = SimpleDateFormat("yyyy-MM-dd kk:mm:ss")

                            end = serverFormat.format(datePick)
                            endTime_value.text = uiFormat.format(datePick)
                            endTime_value.error = null
                        }
                    }).display()
        }

        switch_repeat.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                repeat_end_never.isChecked = true
                repeat_lay.visibility = View.VISIBLE
            } else {
                repeat_lay.visibility = View.GONE
            }
        }

        repeatType()

        weeklyOption()

        repeatEndOption()

        repeat_date_value.setOnClickListener {
            repeat_date_value.startAnimation(user_info.animasiButton)

            SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .displayDays(false)
                    .displayDaysOfMonth(true)
                    .displayMonth(true)
                    .displayYears(true)
                    .displayHours(false)
                    .displayMinutes(false)
                    .displayAmPm(false)
                    .listener(object : SingleDateAndTimePickerDialog.Listener{
                        @SuppressLint("SimpleDateFormat")
                        override fun onDateSelected(datePick: Date?) {
                            Log.d("aim",datePick.toString())
                            val uiFormat = SimpleDateFormat("dd MMMM yyyy")
                            val serverFormat = SimpleDateFormat("yyyy-MM-dd")

                            repeat_end_never.isChecked = false
                            repeat_end_date.isChecked = true

                            repeatEnd = serverFormat.format(datePick)
                            repeat_date_value.text = uiFormat.format(datePick)
                            repeat_date_value.error = null
                        }
                    }).display()
        }

        addAgenda?.setOnClickListener {
            addAgenda?.startAnimation(user_info.animasiButton)
            if (addAgenda.text == "Update") {
                if (emptyCheck()) { updateAgenda() }
            }
            else {
                if (emptyCheck()) { createAgenda() }
//                if (emptyCheck()) { testBody() }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun generatedView() {
        if (intent.hasExtra("id")) {
            scheduleId = intent.getStringExtra("id")
            val dateTimeStart = intent.getStringExtra("start").split(" ")
            val dateTimeEnd = intent.getStringExtra("end").split(" ")
            Log.d("aim","start $dateTimeStart, end : $dateTimeEnd")

            val title = intent.getStringExtra("title")
            val desc = intent.getStringExtra("desc")
            val location = intent.getStringExtra("location")

            header_name.text = "Update Agenda"
            addAgenda.text = "Update"
            title_value.setText(title)
            location_value.setText(location)

        }
        getScheduleCategory()
    }

    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {

//            val parent = ChildModel("", "Parent", "", "", "", 0.0,"")
//            mChildModel.add(parent)

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                Log.i("data" , childObject.toString())
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val foto = childObject.getString("photo")
                val birthday = childObject.getString("birthday")
                val gender = childObject.getString("gender")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val attendanceStatus = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,attendanceStatus)
                mChildModel.add(mItem)
            }

            spinner_kids.adapter = SpinnerChildAdapter(this, mChildModel)
            if (user_info.activeChildId != "See all" && user_info.activeChildId != "Add child" && user_info.activeChildId.isNotEmpty()) {
                mChildModel.forEach {
                    if (user_info.activeChildId == it.id) {
                        val index =  mChildModel.indexOf(it)
                        spinner_kids.setSelection(index)
                    }
                }
            } else {
                spinner_kids.setSelection(0)
            }
        }
    }

    private fun getScheduleCategory() {
        val builder = android.support.v7.app.AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()
        categoryIdArray.clear()
        categoryNameArray.clear()

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.scheduleSubject, Response.Listener { response ->
            try {
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    val dataObject = dataArray.getJSONObject(i)
                    val id = dataObject.getString("id")
                    val nama = dataObject.getString("nama")
                    categoryIdArray.add(id)
                    categoryNameArray.add(nama)
                }

                val arrayAdapter = ArrayAdapter(this, R.layout.simple_textview, categoryNameArray)
                arrayAdapter.setDropDownViewResource(R.layout.simple_textview)
                spinner_category.adapter = arrayAdapter

            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err getChild : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }

    private fun repeatType() {
        val spinnerData = arrayOf("Every day","Every week","Every month","Every year")
        val arrayAdapter = ArrayAdapter(this, R.layout.simple_textview, spinnerData)
        arrayAdapter.setDropDownViewResource(R.layout.simple_textview)
        spinner_repeat.adapter = arrayAdapter

        spinner_repeat.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (spinnerData[position]){
                    "Every day" -> {
                        weekly_lay.visibility = View.GONE
                        repeatType = "c"
                        customType = "d"
                    }
                    "Every week" -> {
                        weekly_lay.visibility = View.VISIBLE
                        repeatType = "c"
                        customType = "w"
                    }
                    "Every month" -> {
                        weekly_lay.visibility = View.GONE
                        repeatType = "c"
                        customType = "m"
                    }
                    "Every year" -> {
                        weekly_lay.visibility = View.GONE
                        repeatType = "c"
                        customType = "y"
                    }
                }
            }
        }
    }

    private fun weeklyOption() {
        senin.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                weeklyArray.add(1)
            } else {
                weeklyArray.remove(1)
            }
            Log.d("aim","weekly array $weeklyArray")
        }

        selasa.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                weeklyArray.add(2)
            } else {
                buttonView.isChecked = false
                weeklyArray.remove(2)
            }
            Log.d("aim","weekly array $weeklyArray")
        }

        rabu.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                weeklyArray.add(3)
            } else {
                buttonView.isChecked = false
                weeklyArray.remove(3)
            }
            Log.d("aim","weekly array $weeklyArray")
        }

        kamis.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                weeklyArray.add(4)
            } else {
                buttonView.isChecked = false
                weeklyArray.remove(4)
            }
            Log.d("aim","weekly array $weeklyArray")
        }

        jumat.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                weeklyArray.add(5)
            } else {
                buttonView.isChecked = false
                weeklyArray.remove(5)
            }
            Log.d("aim","weekly array $weeklyArray")
        }

        sabtu.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                weeklyArray.add(6)
            } else {
                buttonView.isChecked = false
                weeklyArray.remove(6)
            }
            Log.d("aim","weekly array $weeklyArray")
        }

        minggu.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                weeklyArray.add(0)
            } else {
                buttonView.isChecked = false
                weeklyArray.remove(0)
            }
            Log.d("aim","weekly array $weeklyArray")
        }
    }

    private fun repeatEndOption() {
        repeat_end_never.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                repeat_end_date.isChecked = false
                repeatEnd = ""
                repeat_date_value.hint = "Repeats end date"
            }
        }

        repeat_end_date.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                repeat_end_never.isChecked = false
                repeat_date_value.performClick()
            }
        }
    }

    private fun emptyCheck():Boolean {
        var cek = 0

        // ========================================================================================= General Check
        if(title_value.text.isEmpty()) {
            title_value.error = "Required"; cek++
        } else { title_value.error = null }

        if(location_value.text.isEmpty()) {
            location_value.error = "Required"; cek++
        } else { location_value.error = null }

        if (startTime_value.text.isEmpty()) {
            startTime_value.error = "Required"; cek++
        } else { startTime_value.error = null }

        if (endTime_value.text.isEmpty()) {
            endTime_value.error = "Required"; cek++
        } else { endTime_value.error = null }

        // ========================================================================================= Custom Repeat Check
        if (switch_repeat.isChecked) {
            if (customType == "w" && weeklyArray.isEmpty()) {
                Toast.makeText(this,"Select minimal 1 day in a week",Toast.LENGTH_LONG).show()
                cek++
            }
        }

        // ========================================================================================= Repeat End Check
        if (repeat_end_date.isChecked) {
            if (repeat_date_value.text.isEmpty()) {
                repeat_date_value.error = "Required"; cek++
            } else { repeat_date_value.error = null }
        }

        if (cek == 0){
            return true
        }
        return false
    }

    private fun createAgenda(){
        val builder = android.support.v7.app.AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val body = JSONObject()

        body.put("start_date",start)
        body.put("end_date",end)
        body.put("name",title_value.text.toString())
        body.put("location",location_value.text.toString())
        body.put("subject",categoryId)

        if (kidsId.isNotEmpty()) {
            body.put("child",kidsId)
        }

        if (switch_repeat.isChecked) {
            body.put("repeat_type",repeatType)

            body.put("custom_repeat_type",customType)
            when (customType) {
                "d" -> {
                    body.put("daily_repeat","1")
                }
                "w" -> {
                    body.put("weekly_repeat","1")
                    val ja = JSONArray()
                    weeklyArray.forEach {
                        ja.put(it)
                    }
                    body.put("weekly_repeat_day",ja)
                }
                "m" -> {
                    body.put("monthly_repeat","1")
                }
                "y" -> {
                    body.put("yearly_repeat","1")
                }
            }

            if (repeatEnd.isNotEmpty()) {
                body.put("repeat_duration_end_date",repeatEnd)
            }
        }

        Log.d("aim","add schedule $body")

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : JsonObjectRequest(Request.Method.POST, user_info.createAgenda,body, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim","respon schedule: $response")

            Toast.makeText(this,"Agenda Created", Toast.LENGTH_LONG).show()
            finish()

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun updateAgenda() {
        val builder = android.support.v7.app.AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val body = JSONObject()

        body.put("start_date",start)
        body.put("end_date",end)
        body.put("name",title_value.text.toString())
        body.put("location",location_value.text.toString())
        body.put("subject",categoryId)

        if (kidsId.isNotEmpty()) {
            body.put("child",kidsId)
        }

        if (switch_repeat.isChecked) {
            body.put("repeat_type",repeatType)

            body.put("custom_repeat_type",customType)
            when (customType) {
                "d" -> {
                    body.put("daily_repeat","1")
                }
                "w" -> {
                    body.put("weekly_repeat","1")
                    val ja = JSONArray()
                    weeklyArray.forEach {
                        ja.put(it)
                    }
                    body.put("weekly_repeat_day",ja)
                }
                "m" -> {
                    body.put("monthly_repeat","1")
                }
                "y" -> {
                    body.put("yearly_repeat","1")
                }
            }

            if (repeatEnd.isNotEmpty()) {
                body.put("repeat_duration_end_date",repeatEnd)
            }
        }

        Log.d("aim","add schedule $body")

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : JsonObjectRequest(Request.Method.POST, user_info.updateAgenda+scheduleId, body, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim","respon schedule: $response")

            Toast.makeText(this,"Agenda Updated", Toast.LENGTH_LONG).show()
            finish()

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

}
