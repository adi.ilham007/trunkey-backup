package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import innovacia.co.id.trunkey1.fragment.booking.MyBookingPagerAdapter
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_my_booking.*
import kotlinx.android.synthetic.main.sub_header.*

class MyBooking : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_booking)
        header_name.text = resources.getString(R.string.tx_purchase)

        back_link.setOnClickListener {
            onBackPressed()
        }

        viewpager_main.adapter = MyBookingPagerAdapter(supportFragmentManager)
        tabs_main.setupWithViewPager(viewpager_main)
    }
}
