package innovacia.co.id.trunkey1.model

data class ScheduleModel (
        val time    : String,
        val subject : String,
        val notif   : String
)