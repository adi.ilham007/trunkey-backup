package innovacia.co.id.trunkey1.model

data class ChildModel (
        val id : String = "",
        val name : String = "",
        val photo : String = "",
        val birthday : String = "",
        val gender : String = "",
        val saldo : Double = 0.0,
        val theme : String = "",
        val location: String?,
        val time: String?,
        val checkin_status: Int?
)