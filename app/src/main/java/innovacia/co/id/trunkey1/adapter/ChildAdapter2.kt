package innovacia.co.id.trunkey1.adapter

import android.app.Dialog
import android.content.Intent
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.AddChildManual
import innovacia.co.id.trunkey1.ChildProfile
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.Scan
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.list_carousel_large.view.*
import kotlinx.android.synthetic.main.list_carousel_small.view.*

class ChildAdapter2 : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var childData = mutableListOf<ChildModel>()

    companion object {
        var TYPE_OF_VIEW = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (TYPE_OF_VIEW) {
            0 -> ChildListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_carousel_large, parent, false))
            else -> ChildListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_carousel_small, parent, false))
        }
    }

    override fun getItemCount(): Int {
        // Invinite carousel size
        return  childData.size * 40
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as ChildListViewHolder
        holder.bindView(childData[position % childData.size])

        val id = childData[position % childData.size].id
        holder.itemView.setOnClickListener { view ->
            when (id) {
                "See all" -> {
                }
                "Add child" -> {
                    addChildDialog(view)
                }
                else -> {
                    val i = Intent(view.context, ChildProfile::class.java)
                    i.putExtra("id",id)
                    view.context.startActivity(i)
                }
            }
        }



//        holder.itemView.setOnClickListener { view ->
//            val popup = PopupMenu(view.context, viewHolder.itemView.option)
//            popup.inflate(R.menu.manage_child_menu)
//            popup.setOnMenuItemClickListener { item: MenuItem ->
//                when (item.itemId) {
//                    R.id.update ->{
//                        val i = Intent(view.context, EditProfileChild::class.java)
//                        i.putExtra("id", id)
//                        i.putExtra("nama", nama)
//                        i.putExtra("photo", photo)
//                        i.putExtra("gender", gender)
//                        i.putExtra("birthday", birthday)
//                        view.context.startActivity(i)
//                    }
//
//                    R.id.limit ->{
//                        val i = Intent(view.context, ChildSetLimit::class.java)
//                        i.putExtra("id", id)
//                        i.putExtra("nama", nama)
//                        i.putExtra("photo", photo)
//                        view.context.startActivity(i)
//                    }
//
//                    R.id.transfer ->{
//                        val i = Intent(view.context, Transfer::class.java)
//                        i.putExtra("id", id)
//                        i.putExtra("nama", nama)
//                        view.context.startActivity(i)
//                    }
//                }
//                true
//            }
//            //displaying the popup
//            popup.show()
//        }
    }

    private fun addChildDialog(view:View) {
        val dialogs = Dialog(view.context)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        textNotif.text = view.resources.getString(R.string.add_child)
        ok.text = "Yes"
        cancel.text = "No"

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val i = Intent(view.context, Scan::class.java)
            view.context.startActivity(i)
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            val i = Intent(view.context, AddChildManual::class.java)
            view.context.startActivity(i)
            dialogs.dismiss()
        }

        dialogs.show()
    }

    fun setList(listOfChild: List<ChildModel>) {
        this.childData = listOfChild.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(listOfTransaction: List<ChildModel>) {
        this.childData.addAll(listOfTransaction)
        notifyDataSetChanged()
    }

    class ChildListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()

        val orange = "#"+Integer.toHexString(ResourcesCompat.getColor(itemView.resources, R.color.colorPrimary, null)).replace("ff","").capitalize()
        val blue = "#"+Integer.toHexString(ResourcesCompat.getColor(itemView.resources, R.color.blue, null)).replace("ff","").capitalize()
        val gray = "#"+Integer.toHexString(ResourcesCompat.getColor(itemView.resources, R.color.gray, null)).replace("ff","").capitalize()

        fun bindView(mChildModel: ChildModel) {

            when (TYPE_OF_VIEW) {
                0 -> {
                    when (mChildModel.id) {
                        "See all"-> {
                            itemView.root_lay?.setBackgroundResource(R.color.blue)
                            itemView.desc_lay?.visibility = View.INVISIBLE
                            itemView.warning_add_child?.visibility = View.VISIBLE
                            itemView.warning_add_child?.text = "All Child"
                            itemView.child_photo?.setImageResource(R.drawable.child_all_icon)
                        }
                        "Add child" -> {
                            itemView.root_lay?.setBackgroundResource(R.color.gray)
                            itemView.desc_lay?.visibility = View.INVISIBLE
                            itemView.warning_add_child?.visibility = View.VISIBLE
                            itemView.warning_add_child?.text = "Add Child"
                            itemView.child_photo?.setImageResource(R.drawable.child_add_icon)
                        }
                        else -> {
                            itemView.warning_add_child?.visibility = View.GONE
                            itemView.desc_lay?.visibility = View.VISIBLE
                            itemView.child_name?.text = mChildModel.name
                            itemView.child_balance?.text = helper.bank(mChildModel.saldo)

                            try { Glide.with(itemView.context).load(mChildModel.photo)
                                        .apply(RequestOptions.skipMemoryCacheOf(true))
                                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                        .into(itemView.child_photo)
                            } catch (e:Exception){ Log.d("aim","glide err : $e") }

                            // Color theme setting
                            when (mChildModel.theme) {
                                blue -> { itemView.root_lay?.setBackgroundResource(R.color.blue) }
                                gray -> { itemView.root_lay?.setBackgroundResource(R.color.gray) }
                                else -> {itemView.root_lay?.setBackgroundResource(R.color.colorPrimary)}
                            }

                            if (mChildModel.checkin_status == 1){
                                itemView.child_time.text = mChildModel.time
                                itemView.child_status.visibility = View.VISIBLE
                                itemView.child_status.setBackgroundResource(R.drawable.ic_checkin)
                                itemView.child_vendor.text = mChildModel.location
                            }
                            else if(mChildModel.checkin_status == 0 && mChildModel.location == "0"){
                                itemView.child_time.text = "--:--"
                                itemView.child_status.visibility = View.INVISIBLE
                                itemView.child_vendor.text = "Data not available"
                            }
                            else{
                                itemView.child_time.text = mChildModel.time
                                itemView.child_status.visibility = View.VISIBLE
                                itemView.child_status.setBackgroundResource(R.drawable.ic_checkout)
                                itemView.child_vendor.text = mChildModel.location
                            }

                        }
                    }
                }
                1 -> {
                    when (mChildModel.id) {
                        "See all" -> {
                            itemView.child_name_small?.visibility = View.GONE
                            itemView.warning_add_child_small?.visibility = View.VISIBLE

                            itemView.root_lay_small?.setBackgroundResource(R.color.blue)
                            itemView.warning_add_child_small?.text = "All Child"
                            itemView.child_photo_small?.setImageResource(R.drawable.child_all_icon)
                        }
                        "Add child" -> {
                            itemView.child_name_small?.visibility = View.GONE
                            itemView.warning_add_child_small?.visibility = View.VISIBLE

                            itemView.root_lay_small?.setBackgroundResource(R.color.gray)
                            itemView.warning_add_child_small?.text = "Add Child"
                            itemView.child_photo_small?.setImageResource(R.drawable.child_add_icon)
                        }
                        else -> {
                            itemView.child_name_small?.visibility = View.VISIBLE
                            itemView.warning_add_child_small?.visibility = View.GONE

                            itemView.child_name_small?.text = mChildModel.name

                            try { Glide.with(itemView.context).load(mChildModel.photo)
                                        .apply(RequestOptions.skipMemoryCacheOf(true))
                                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                        .into(itemView.child_photo_small)
                            } catch (e:Exception){ Log.d("aim","glide err : $e") }

                            // Color theme setting
                            when (mChildModel.theme) {
                                blue -> { itemView.root_lay_small?.setBackgroundResource(R.color.blue) }
                                gray -> { itemView.root_lay_small?.setBackgroundResource(R.color.gray) }
                                else -> {itemView.root_lay_small?.setBackgroundResource(R.color.colorPrimary)}
                            }
                        }
                    }
                }

            }


        }
    }
}