package innovacia.co.id.trunkey1

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.adapter.TrunkeyCareHistoryAdapter
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.TrunkeyCareHistoryModel
import kotlinx.android.synthetic.main.activity_trunkey_care_history.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class TrunkeyCareHistory : AppCompatActivity() {

    val mVolleyHelper = VolleyHelper()

    private val historyData = mutableListOf<TrunkeyCareHistoryModel>()

    @SuppressLint("SimpleDateFormat")
    val sdf = SimpleDateFormat("yyyy-MM-dd-u")
    val today = sdf.format(Date()).split("-")
    var globalYear = today[0].toInt()
    var globalMonth = today[1].toInt()
    var globalDate = today[2].toInt()
    var globalDay = today[3].toInt()

    private var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trunkey_care_history)
        header_name.text = "Trunkey Care History"

        if (intent.hasExtra("id")){
            id = intent.getStringExtra("id")
            caretaker_name.text = intent.getStringExtra("name")
            val photoUrl = intent.getStringExtra("photo")
            Glide.with(this).load(photoUrl).into(photo)
            getHistory()
        }

        back_link.setOnClickListener {
            onBackPressed()
        }

        swipe_container.setOnRefreshListener {
            getHistory()
        }
    }

    private fun getHistory() {
        val errSection = "getHistory"
        swipe_container.isRefreshing = true

        val calendar = Calendar.getInstance()
        calendar.set(globalYear,globalMonth-1,globalDate)
        val startDate = calendar.getActualMinimum(Calendar.DAY_OF_MONTH)
        val endDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        val stringStart = "$globalYear-${String.format("%02d", globalMonth)}-${String.format("%02d", startDate)}"
        val stringEnd = "$globalYear-${String.format("%02d", globalMonth)}-${String.format("%02d", endDate)}"

        val body = JSONObject()
        body.put("start_date",stringStart)
        body.put("end_date",stringEnd)

        val request = object : JsonObjectRequest(Request.Method.POST, user_info.care_history+id,body, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val dataArray = response.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    val id = dataObj.getString("id")
                    val childName = dataObj.getString("child_name")
                    val childPhoto = dataObj.getString("child_photo_path")
                    val status = dataObj.getString("type")
                    var time = ""

                    if (status == "checkin" && dataObj.has("format_checkin")) {
                        val timeObj = dataObj.getJSONObject("format_checkin")
                        val jam = timeObj.getString("hour")
                        val menit = timeObj.getString("minute")
                        time = "$jam:$menit"
                    } else if (status == "checkout" && dataObj.has("format_checkout")) {
                        val timeObj = dataObj.getJSONObject("format_checkout")
                        val jam = timeObj.getString("hour")
                        val menit = timeObj.getString("minute")
                        time = "$jam:$menit"
                    }

                    val mItem = TrunkeyCareHistoryModel(id,childName,"",time,childPhoto,status)
                    historyData.add(mItem)
                }

                recycler_child.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val historyAdapter = TrunkeyCareHistoryAdapter()
                recycler_child.adapter = historyAdapter
                historyAdapter.setList(historyData)

            } catch (e:JSONException) {
                Log.d("aim", "$errSection $e")
            }

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }
}
