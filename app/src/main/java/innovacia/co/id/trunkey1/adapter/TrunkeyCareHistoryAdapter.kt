package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.DetailBlog
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.TrunkeyCareHistoryModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_trunkey_care_history.view.*

class TrunkeyCareHistoryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var historyData = mutableListOf<TrunkeyCareHistoryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_trunkey_care_history, parent, false))
    }
    override fun getItemCount(): Int = historyData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder
        holder.bindView(historyData[position])

        if (itemCount == 1) {
            holder.itemView.line_top.visibility = View.GONE
            holder.itemView.line_bottom.visibility = View.GONE
        } else if (position == 0) {
            holder.itemView.line_top.visibility = View.GONE
        } else if (position == itemCount -1) {
            holder.itemView.line_bottom.visibility = View.GONE
        }

        holder.itemView.setOnClickListener { view ->
            holder.itemView.startAnimation(user_info.animasiButton)

//            val idVideo = historyData[position].idVideo

//            val i = Intent(view.context, DetailBlog::class.java)
//            i.putExtra("idVideo", idVideo)
//            i.putExtra("slug", slug)
//            i.putExtra("category", category)
//            view.context.startActivity(i)
        }
    }
    fun setList(listOfVendor: List<TrunkeyCareHistoryModel>) {
        this.historyData = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<TrunkeyCareHistoryModel>) {
        this.historyData.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(model: TrunkeyCareHistoryModel) {
            itemView.name_value.text = model.nama
            itemView.time_value.text = model.time
            itemView.status_value.text = model.status

            Glide.with(itemView.context).load(model.photo).into(itemView.photo)
        }
    }

}