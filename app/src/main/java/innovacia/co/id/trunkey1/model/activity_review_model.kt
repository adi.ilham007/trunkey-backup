package innovacia.co.id.trunkey1.model

data class activity_review_model(
        var id_review       :String,
        var nama            :String,
        var keterangan      :String,
        var rating          :Int,
        var tanggal         :String,
        val jumlah_point    :Int
)