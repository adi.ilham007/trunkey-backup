package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_review.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class Review : AppCompatActivity() {

    private var linkReview = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)

        val id = intent.getStringExtra("id")

        name_header.text = intent.getStringExtra("tipe")
        nama.text = intent.getStringExtra("nama")
        val img = intent.getStringExtra("img")
        Glide.with(this).load(img).into(img_lay)

        when {
            name_header.text == "vendor" -> linkReview = user_info.reviewVendor
//            tipe.text == "course" ->
//            tipe.text == "event" ->
//            tipe.text == "product" ->
//            tipe.text == "blog" ->
        }

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
            finish()
        }

        postReview_btn.setOnClickListener {
            postReview_btn.startAnimation(user_info.animasiButton)
            if(Reviewtext.length() < 10){
                Reviewtext.setError("please enter 10 characters")
            }else{
                postReview(id)
            }

        }
    }

    fun postReview(idIntent:String){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, linkReview+idIntent, Response.Listener { response ->
            //========================================================================================= data from server
            Handler().postDelayed({ dialog.dismiss() }, 0)
            try {
                Log.d("aim","review Terkirim, $response")
//                onBackPressed()
//                val i = Intent(view.context, Course::class.java)
//                    i.putExtra("slug", idMix)
//                    view.context.startActivity(i)
                val id = intent.getStringExtra("id")
                val i = Intent(this, detail_vendor::class.java)
                i.putExtra("id",id)
                startActivity(i)
                finish()

            } catch (e: JSONException) {
                Log.e("aim", "post review e: $e")
//                Toast.makeText(this,"Data Tidak Tersedia",Toast.LENGTH_LONG).show()
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            Handler().postDelayed({dialog.dismiss()},0)
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map["rating"] = ratingBar.progress.toString()
                map["keterangan"] = Reviewtext.text.toString()
                Log.d("aim",map.toString())
                return map
            }
        }
        rq.add(sr)
    }
}
