package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.TiketModel
import android.support.v4.content.LocalBroadcastManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Window
import android.widget.*
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_ticket.view.*
import kotlinx.android.synthetic.main.list_detail_tiket.view.*
import kotlinx.android.synthetic.main.list_detail_tiket_pay.view.*

//private val qtyStateArray = SparseIntArray()
//private val memberStateArray = SparseIntArray()

class TiketAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var tiketList = mutableListOf<TiketModel>()
    var memberArray = ArrayList<String>()

    companion object {
        var TYPE_OF_VIEW = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (TYPE_OF_VIEW) {
            0 -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_ticket, parent, false))
            1 -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_detail_tiket, parent, false))
            else -> vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_detail_tiket_pay, parent, false))
        }
    }

    override fun getItemCount(): Int = tiketList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder

        holder.bindView(tiketList[position])
        holder.setIsRecyclable(false)

        when (TYPE_OF_VIEW) {
//            0 -> {
//                val id = tiketList[position].id
//                val harga1 = tiketList[position].harga1
//                val harga2 = tiketList[position].harga2
//                val cashBack = tiketList[position].cashBack
//                val maxCashBack = tiketList[position].maxCashBack
//                var qty = tiketList[position].jumlah
//
//                holder.itemView.constraintLayout2.setOnClickListener {view ->
//                    holder.itemView.constraintLayout2.startAnimation(user_info.animasiButton)
//
//                    val dialogs = Dialog(view.context)
//                    dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
//                    dialogs.setCancelable(true)
//                    dialogs.setContentView(R.layout.popup_detail_tiket)
//
//                    val namaTiket = dialogs.findViewById<TextView>(R.id.nama)
//                    val hargaTiket = dialogs.findViewById<TextView>(R.id.harga)
//                    val popupCashback = dialogs.findViewById<TextView>(R.id.cash_point)
//                    val popuptxtCashback = dialogs.findViewById<TextView>(R.id.txcash_point)
//                    val desc = dialogs.findViewById<TextView>(R.id.textView52)
//
//                    namaTiket.text = holder.itemView.nama.text
//                    hargaTiket.text = holder.itemView.harga2.text
//                    Log.d("aim","tiket adapter "+holder.itemView.cash_point.text)
//                    if(holder.itemView.cash_point.text != "IDR 0"){
//                        popupCashback.text = holder.itemView.cash_point.text
//                    }else{
//                        popupCashback.visibility = View.GONE
//                        popuptxtCashback.visibility = View.GONE
//                    }
//
//                    desc.text = holder.itemView.textView52.text
//                    dialogs.show()
//
//                }
//
//                holder.itemView.btn_plus.setOnClickListener { view ->
//                    holder.itemView.btn_plus.startAnimation(user_info.animasiButton)
//                    if (qty <= 4) {
//                        qty++
//                        holder.itemView.Amount.text = qty.toString()
//                        val total = qty * harga2
//                        val mCashBack = qty * cashBack
//
//                        val intent = Intent("qty")
//                        intent.putExtra("id", id)
//                        intent.putExtra("seat",qty)
//                        intent.putExtra("harga", total)
//
//                        if (mCashBack <= maxCashBack) {
//                            intent.putExtra("cashBack", mCashBack)
//                        } else {
//                            intent.putExtra("cashBack", maxCashBack)
//                        }
//                        Log.d("aim","+ $intent")
//                        LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
//                    }
//                }
//                holder.itemView.btn_minus.setOnClickListener { view ->
//                    holder.itemView.btn_minus.startAnimation(user_info.animasiButton)
//                    if (qty != 0) {
//                        qty--
//                        holder.itemView.Amount.text = qty.toString()
//                        val total = qty * harga2
//                        val mCashBack = qty * cashBack
//
//                        val intent = Intent("qty")
//                        intent.putExtra("id", id)
//                        intent.putExtra("seat",qty)
//                        intent.putExtra("harga", total)
//
//                        if (mCashBack <= maxCashBack) {
//                            intent.putExtra("cashBack", mCashBack)
//                        } else {
//                            intent.putExtra("cashBack", maxCashBack)
//                        }
//                        LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
//                        Log.d("aim","- $intent")
//                    }
//                }
//
////                viewHolder.itemView.Amount.text = qtyStateArray.get(position).toString()
//            }
            1 -> {
                val id = tiketList[position].id
                val jumlahBeli = tiketList[position].jumlah
                var allMember = ""

                Log.d("aim","jumlah beli : $jumlahBeli")

                holder.itemView.satu.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?){}
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        holder.itemView.add.setBackgroundResource(R.drawable.text_shape_blue)
                    }
                })
                holder.itemView.dua.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?){}
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        holder.itemView.add.setBackgroundResource(R.drawable.text_shape_blue)
                    }
                })
                holder.itemView.tiga.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?){}
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        holder.itemView.add.setBackgroundResource(R.drawable.text_shape_blue)
                    }
                })
                holder.itemView.empat.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?){}
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        holder.itemView.add.setBackgroundResource(R.drawable.text_shape_blue)
                    }
                })
                holder.itemView.lima.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?){}
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){}

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        holder.itemView.add.setBackgroundResource(R.drawable.text_shape_blue)
                    }
                })

                holder.itemView.add.setOnClickListener {
                    val satuSize = holder.itemView.satu.text.toString().trim().isEmpty()
                    val duaSize = holder.itemView.dua.text.toString().trim().isEmpty()
                    val tigaSize = holder.itemView.tiga.text.toString().trim().isEmpty()
                    val empatSize = holder.itemView.empat.text.toString().trim().isEmpty()
                    val limaSize = holder.itemView.lima.text.toString().trim().isEmpty()

//                    memberStateArray.put(position, 1)
                    Log.d("aim","position : $position")

                    var emptyCheck = false

                    when(jumlahBeli){
                        1 -> {
                            if (!satuSize) {
                                emptyCheck = true
                                allMember = "${holder.itemView.satu.text}"
                            } else {
                                Toast.makeText(holder.itemView.context,"Please enter participant name",Toast.LENGTH_LONG).show()
                            }
                        }
                        2 -> {
                            if (!satuSize && !duaSize) {
                                emptyCheck = true
                                allMember = "${holder.itemView.satu.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.dua.text}"
                            } else {
                                Toast.makeText(holder.itemView.context,"Please enter two participants name",Toast.LENGTH_LONG).show()
                            }
                        }
                        3 -> {
                            if (!satuSize && !duaSize && !tigaSize) {
                                emptyCheck = true
                                allMember = "${holder.itemView.satu.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.dua.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.tiga.text}"
                            } else {
                                Toast.makeText(holder.itemView.context,"Please enter three participants name",Toast.LENGTH_LONG).show()
                            }
                        }
                        4 -> {
                            if (!satuSize && !duaSize && !tigaSize && !empatSize) {
                                emptyCheck = true
                                allMember = "${holder.itemView.satu.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.dua.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.tiga.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.empat.text}"
                            } else {
                                Toast.makeText(holder.itemView.context,"Please enter four participants name",Toast.LENGTH_LONG).show()
                            }
                        }
                        5 -> {
                            if (!satuSize && !duaSize && !tigaSize && !empatSize && !limaSize) {
                                emptyCheck = true
                                allMember = "${holder.itemView.satu.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.dua.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.tiga.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.empat.text}|-<o>< ><o>-|" +
                                        "${holder.itemView.lima.text}"
                            } else {
                                Toast.makeText(holder.itemView.context,"Please enter five participants name",Toast.LENGTH_LONG).show()
                            }
                        }
                    }

                    Log.d("aim","empty eheck : $emptyCheck")
                    if (emptyCheck) {
                        if (memberArray.contains(id.toString())) {
                            val index = memberArray.indexOf(id.toString())
                            memberArray.removeAt(index)
                            memberArray.removeAt(index)
                            memberArray.add(index, id.toString())
                            memberArray.add(index + 1, allMember)
                        } else {
                            memberArray.add(id.toString())
                            memberArray.add(allMember)
                        }

                        val intent = Intent("member")
                        intent.putExtra("memberArray", memberArray)
                        LocalBroadcastManager.getInstance(holder.itemView.context).sendBroadcast(intent)
                        Log.d("aim", "all member : $memberArray")
                        Toast.makeText(holder.itemView.context,"Done",Toast.LENGTH_LONG).show()

                        holder.itemView.add.setBackgroundResource(R.drawable.text_shape_gray)
                    }

                }

            }
            2 -> {

            }
        }

    }

    fun reset() {
//        qtyStateArray.clear()
//        notifyDataSetChanged()
    }

    fun setList(listOfVendor: List<TiketModel>) {
        this.tiketList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<TiketModel>) {
        this.tiketList.addAll(listOfVendor)
        notifyDataSetChanged()
    }

    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()

        // Kids Data
        val kidsIdArray = ArrayList<String>()
        val kidsNameArray = ArrayList<String>()



        @SuppressLint("SetTextI18n")
        fun bindView(model: TiketModel) {
            when (TYPE_OF_VIEW) {
//                0 -> {
//
//                    // Handle Background
//                    if (model.tipe == "ticket") {
//                        itemView.constraintLayout2.setBackgroundResource(R.drawable.ic_bg_ticket)
//                    } else if (model.tipe == "class") {
//                        itemView.constraintLayout2.setBackgroundResource(R.drawable.ic_bg_courses)
//                    }
//                    itemView.nama.text = model.nama
//                    itemView.textView52.text = helper.textHtml(model.ket)
//
//                    if (model.harga1 > model.harga2) {
//                        itemView.harga1.text = helper.bank(model.harga1)
//                        itemView.harga1.paintFlags = itemView.harga1.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
//                    } else {
//                        itemView.harga1.visibility = View.GONE
//                    }
//
//                    itemView.harga2.text = helper.bank(model.harga2)
//                    if(model.cashBack != 0.0){
//                        itemView.cash_point.text = helper.bank(model.cashBack)
//                        itemView.maxCashBack_value.text = helper.bank(model.maxCashBack)
//                        Log.d("aim","tiket ${model.cashBack}")
//                        Log.d("aim","tiket idr ${helper.bank(model.cashBack)}")
//
//                    }else{
//                        itemView.cash_point.visibility = View.GONE
//                        itemView.txcash_point.visibility = View.GONE
//
//                        itemView.maxCashBack.visibility = View.GONE
//                        itemView.maxCashBack_value.visibility = View.GONE
//                    }
//
//                    itemView.Amount.text = model.jumlah.toString()
////                    itemView.tanggal_view.text = model.date
////                    itemView.time.text = model.time
//                }
                1 -> {
                    // Ticket Detail
                    itemView.title.text             = model.nama
                    itemView.keterangan.text        = model.ket

                    if (model.harga1 > model.harga2) {
                        itemView.harga_satu.text        = helper.bank(model.harga1)
                        itemView.harga_satu.paintFlags = itemView.harga_satu.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    } else {
                        itemView.harga_satu.visibility  = View.GONE
                    }
                    itemView.harga_dua.text         = helper.bank(model.harga2)
                    itemView.tx_nama_date.text      = model.date
                    //                itemView.time.text              = model.time
                    itemView.txLocation.text		= model.lokasi

                    when(model.jumlah){
                        1 -> {
                            itemView.dua.visibility = View.GONE
                            itemView.pencil2.visibility = View.GONE
                            itemView.tiga.visibility = View.GONE
                            itemView.pencil3.visibility = View.GONE
                            itemView.empat.visibility = View.GONE
                            itemView.pencil4.visibility = View.GONE
                            itemView.lima.visibility = View.GONE
                            itemView.pencil5.visibility = View.GONE
                        } 2 -> {
                            itemView.tiga.visibility = View.GONE
                            itemView.pencil3.visibility = View.GONE
                            itemView.empat.visibility = View.GONE
                            itemView.pencil4.visibility = View.GONE
                            itemView.lima.visibility = View.GONE
                            itemView.pencil5.visibility = View.GONE
                        } 3 -> {
                            itemView.empat.visibility = View.GONE
                            itemView.pencil4.visibility = View.GONE
                            itemView.lima.visibility = View.GONE
                            itemView.pencil5.visibility = View.GONE
                        } 4 -> {
                            itemView.lima.visibility = View.GONE
                            itemView.pencil5.visibility = View.GONE
                        }
                    }
                }
                2 -> {

                    getChild()

                    // Ticket Detail
                    itemView.id2.text               = "T#00${model.id}"
                    itemView.nama2.text             = model.nama
                    itemView.keterangan2.text       = helper.textHtml(model.ket)
                    if (model.harga1 > model.harga2) {
                        itemView.harga11.text        = helper.bank(model.harga1)
                        itemView.harga11.paintFlags  = itemView.harga11.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    } else {
                        itemView.harga11.visibility  = View.GONE
                    }
                    itemView.harga22.text         = helper.bank(model.harga2)

                    val listMember = model.member.split("|>:M:<|")
                    Log.d("aim","LIST : $listMember")

                    val emptyCheck = model.member.replace("|>:M:<|",", ")

                    if (emptyCheck != "") {
                        for (i in 0 until listMember.size) {
                            // Nama peserta dari id yang dipilih (Add child)
                            if (kidsIdArray.contains(listMember[i].trim())) {
                                when (i+1) {
                                    1 -> {
                                        val index = kidsIdArray.indexOf(listMember[i].trim())
                                        itemView.tvSatu.text = kidsNameArray[index]
                                    }
                                    2 -> {
                                        val index = kidsIdArray.indexOf(listMember[i].trim())
                                        itemView.tvDua.text = kidsNameArray[index]
                                    }
                                    3 -> {
                                        val index = kidsIdArray.indexOf(listMember[i].trim())
                                        itemView.tvTiga.text = kidsNameArray[index]
                                    }
                                    4 -> {
                                        val index = kidsIdArray.indexOf(listMember[i].trim())
                                        itemView.tvEmpat.text = kidsNameArray[index]
                                    }
                                    5 -> {
                                        val index = kidsIdArray.indexOf(listMember[i].trim())
                                        itemView.tvLima.text = kidsNameArray[index]
                                    }
                                }
                            }
                            // Nama peserta dari input manual (other kids)
                            else {
                                when (i+1) {
                                    1 -> {
                                        itemView.tvSatu.text = listMember[i].replace("_"," ").trim()
                                    }
                                    2 -> {
                                        itemView.tvDua.text = listMember[i].replace("_"," ").trim()
                                    }
                                    3 -> {
                                        itemView.tvTiga.text = listMember[i].replace("_"," ").trim()
                                    }
                                    4 -> {
                                        itemView.tvEmpat.text = listMember[i].replace("_"," ").trim()
                                    }
                                    5 -> {
                                        itemView.tvLima.text = listMember[i].replace("_"," ").trim()
                                    }
                                }
                            }
                        }

                        when (listMember.size) {
                            1 -> {
                                itemView.tvDua.visibility = View.GONE
                                itemView.tvTiga.visibility = View.GONE
                                itemView.tvEmpat.visibility = View.GONE
                                itemView.tvLima.visibility = View.GONE
                            }
                            2 -> {
                                itemView.tvTiga.visibility = View.GONE
                                itemView.tvEmpat.visibility = View.GONE
                                itemView.tvLima.visibility = View.GONE
                            }
                            3 -> {
                                itemView.tvEmpat.visibility = View.GONE
                                itemView.tvLima.visibility = View.GONE
                            }
                            4 -> {
                                itemView.tvLima.visibility = View.GONE
                            }

                        }
                    } else {
                        itemView.tvSatu.text = "Participant : N/A"
                        itemView.tvDua.visibility = View.GONE
                        itemView.tvTiga.visibility = View.GONE
                        itemView.tvEmpat.visibility = View.GONE
                        itemView.tvLima.visibility = View.GONE
                    }
                }
            }
        }

        fun getChild() {
            if (user_info.restoreKids != null) {
                val dataArray = user_info.restoreKids
                if (dataArray != null) {

                    kidsIdArray.clear()
                    kidsNameArray.clear()

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        kidsIdArray.add(childObject.getString("id"))
                        kidsNameArray.add(childObject.getString("nama"))
                    }
                }
            }
        }
    }
}