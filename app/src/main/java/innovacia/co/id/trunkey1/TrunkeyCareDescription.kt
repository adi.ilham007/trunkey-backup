package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_trunkey_care_description.*
import kotlinx.android.synthetic.main.sub_header.*
import kotlinx.android.synthetic.main.sub_header.view.*

class TrunkeyCareDescription : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trunkey_care_description)
        header_name.text = resources.getString(R.string.tx_care)

        back_link.setOnClickListener {
            val intent = Intent(this, HomeBaru::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        btn_skip.setOnClickListener {
            val intent = Intent(this, HomeBaru::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        btn_next.setOnClickListener {
            desc_lay.visibility = View.GONE
            step_lay.visibility = View.VISIBLE
        }

        btn_activate.setOnClickListener {
            val i = Intent(this,TrunkeyCareRegis1::class.java)
            startActivity(i)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, HomeBaru::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }
}
