package innovacia.co.id.trunkey1.fragment.home


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.*
import innovacia.co.id.trunkey1.model.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap
import android.support.v4.os.HandlerCompat.postDelayed
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AlertDialog
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.activity.*
import innovacia.co.id.trunkey1.adapter.*
import kotlinx.android.synthetic.main.activity_edit_profile_child.*
import kotlinx.android.synthetic.main.activity_select__purchase.*
import kotlinx.android.synthetic.main.list_mybooking.*


//note : respon getchild kadang Value <!DOCTYPE of type java.lang.String cannot be converted to JSONObject

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class HomeFragment : Fragment() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var lokasiGPS = Manifest.permission.ACCESS_FINE_LOCATION
    private var lokasiNET = Manifest.permission.ACCESS_COARSE_LOCATION

    lateinit var appContext: Context
    lateinit var viewContext: Context

    private var orange = ""
    private var blue = ""
    private var gray = ""

    private val helper = user_info()
    private val mVolleyHelper = VolleyHelper()

    private val mChildModel = mutableListOf<ChildModel>()
    private val vendorData = mutableListOf<vendor_model>()
    private val blogData = mutableListOf<blog_model>()
    private val mCustomDataA = mutableListOf<customModelA>()
    private val featureData = mutableListOf<FeatureModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        appContext = activity!!.applicationContext

        orange = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.colorPrimary, null)).replace("ff","").capitalize()
        blue = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.blue, null)).replace("ff","").capitalize()
        gray = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.gray, null)).replace("ff","").capitalize()

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewContext = view.context

        swipe_container?.setOnRefreshListener {
            getKids()
            getLocation()
            getNearMe()
        }

        cacheHandling()

        btnHandling()



        seeAllHanding()

//        getPromo()

        getLocation()
        getNearMe()

        getRead()

        getFeature()

        tester.setOnClickListener {
            val i = Intent(viewContext, login::class.java)
            startActivity(i)
        }
    }

    override fun onResume() {
        super.onResume()

        cacheHandling()

        // Handle carousel position (restore or not)
        ChildAdapter2.TYPE_OF_VIEW = 0
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }
    }


    private fun btnHandling() {

        btn_dashboard?.setOnClickListener {
            btn_dashboard?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                val i = Intent(viewContext, Dashboard::class.java)
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "dashboard")
                startActivity(i)
            }
        }

        btn_attendance?.setOnClickListener {
            btn_attendance?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                val i = Intent(viewContext, Attendance::class.java)
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "attendance")
                startActivity(i)
            }
        }

        btn_schedule?.setOnClickListener {
            btn_schedule?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                val i = Intent(viewContext, Schedule::class.java)
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "schedule")
                startActivity(i)
            }
        }

        btn_news?.setOnClickListener {
            btn_news?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                val i = Intent(viewContext, News::class.java)
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "news")
                startActivity(i)
            }
        }

        btn_billing?.setOnClickListener {
            btn_billing?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                val i = Intent(viewContext, Billing::class.java)
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "billing")
                startActivity(i)
            }
        }

        btn_wallet?.setOnClickListener {
            btn_wallet?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
//                val i = Intent(viewContext, Wallet2::class.java)
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "wallet_sementara")
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "wallet")
                startActivity(i)
            }
        }

        btn_place?.setOnClickListener {
            btn_place?.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                val i = Intent(viewContext,TrunkeyCare::class.java)
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "care")
                startActivity(i)
            }
        }

        btn_explore?.setOnClickListener {
            btn_explore?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, home::class.java)
            startActivity(i)
        }

        trunkey_care_banner.setOnClickListener {
            if (user_info.loginStatus) {
                val i = Intent(viewContext,TrunkeyCare::class.java)
                startActivity(i)
            } else {
                val i = Intent(viewContext, DescParentingTools::class.java)
                i.putExtra("title", "care")
                startActivity(i)
            }
        }

    }

    private fun seeAllHanding() {
        nearme_all.setOnClickListener {
            nearme_all.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, home_makanan::class.java)
            i.putExtra("category", "nearme")
            startActivity(i)
        }
        event_all.setOnClickListener {
            event_all.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, ListingNew::class.java)
            i.putExtra("category", user_info.event)
            startActivity(i)
        }
    }

    private fun cacheHandling() {
        if (user_info.restoreKids == null) getKids() else restoreKidsFun()
        if (user_info.restoreEvent == null) getEvent() else restoreEventFun()
        if (user_info.restorePromotion == null) getPromotion() else restorePromotionFun()
    }


    private fun getKids() {
        val errSection = "getKids :"
        swipe_container?.isRefreshing = true

        val rq = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                Log.d("aim", "$errSection $response")

                user_info.activeChildId = ""
                user_info.childCarouselState = 0

                mChildModel.clear()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restoreKids = dataArray
                if (dataArray.length() > 0) {
                    // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

                    user_info.activeChildId = dataArray.getJSONObject(0).getString("id")

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val gender = childObject.getString("gender")
                        val birthday = childObject.getString("birthday")
                        val foto = childObject.getString("photo")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val status = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                        mChildModel.add(mItem)

                        // Add child option
                        if (i+1 == dataArray.length()) {
                            val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                            mChildModel.add(addChild)
                        }
                    }
                } else {
                    for (i in 0 until 1) {
                        val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                        mChildModel.add(addChild)
                    }
                }

                val mLinearLayoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
                recycler_child?.layoutManager = mLinearLayoutManager

                // Triger carousel to center
                recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)

                val displayMetrics = viewContext.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 320) / 2
                val pxWidth = helper.dpToPx(viewContext, calculation.toInt())
                recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (recycler_child?.onFlingListener == null) {
                    val snapHelper = SnapHelperOneByOne()
                    snapHelper.attachToRecyclerView(recycler_child)
                }

                // Large Carousel
                ChildAdapter2.TYPE_OF_VIEW = 0
                val mChildAdapter = ChildAdapter2()
                recycler_child?.adapter = mChildAdapter
                mChildAdapter.setList(mChildModel)

                kidsCarouselListener(mLinearLayoutManager)
                getNotif(0)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        },Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }
    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {
            // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val gender = childObject.getString("gender")
                val birthday = childObject.getString("birthday")
                val foto = childObject.getString("photo")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val status = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                mChildModel.add(mItem)

                // Add child option
                if (i+1 == dataArray.length()) {
                    val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                    mChildModel.add(addChild)
                }
            }
        } else {
            for (i in 0 until 1) {
                val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                mChildModel.add(addChild)
            }
        }

        val mLinearLayoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
        recycler_child?.layoutManager = mLinearLayoutManager

        // Handle carousel position (restore or not)
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }

        val displayMetrics = viewContext.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 320) / 2
        val pxWidth = helper.dpToPx(viewContext, calculation.toInt())
        recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (recycler_child?.onFlingListener == null) {
            val snapHelper = SnapHelperOneByOne()
            snapHelper.attachToRecyclerView(recycler_child)
        }

        // Large Carousel
        ChildAdapter2.TYPE_OF_VIEW = 0
        val mChildAdapter = ChildAdapter2()
        recycler_child?.adapter = mChildAdapter
        mChildAdapter.setList(mChildModel)

        kidsCarouselListener(mLinearLayoutManager)
    }

    private fun kidsCarouselListener(mLinearLayoutManager: LinearLayoutManager) {
        recycler_child?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val snapHelper = SnapHelperOneByOne()
                if(newState == RecyclerView.TOUCH_SLOP_PAGING && newState != RecyclerView.NO_POSITION) {
                    Handler().postDelayed({
                        val centerView = snapHelper.findSnapView(mLinearLayoutManager)
                        if (centerView != null) {
                            val pos = mLinearLayoutManager.getPosition(centerView)
                            val getId = mChildModel[pos % mChildModel.size].id
                            user_info.activeChildId = getId
                            user_info.childCarouselState = pos
                            Log.d("aim", "active child id : $getId")

                            notif_dashboard.visibility = View.GONE
                            notif_attendance.visibility = View.GONE
                            notif_schedule.visibility = View.GONE
                            notif_news.visibility = View.GONE
                            notif_billing.visibility = View.GONE
                            notif_wallet.visibility = View.GONE

                            if (getId != "See all" && getId != "Add child") {
                                getNotif(pos)
                                getRecentAttendance(pos)
                            }
                        }
                    }, 500)
                }
            }
        })
    }

    private fun getLocation(){
        try {
            if (ContextCompat.checkSelfPermission(viewContext,lokasiGPS) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(viewContext,lokasiNET) == PackageManager.PERMISSION_GRANTED ) {

                fusedLocationClient = LocationServices.getFusedLocationProviderClient(viewContext)
                fusedLocationClient.lastLocation.addOnSuccessListener { location->

                    if (location != null) {
                        user_info.latitude = location.latitude
                        user_info.longitude = location.longitude
                        Log.d("aim", "Lokasi fused ${location.latitude}, ${location.longitude}")
                        Log.d("aim", "Lokasi var ${user_info.latitude}, ${user_info.longitude}")
                    }

                }
            }

        }
        catch (e:Exception) {
            Log.d("aim",e.toString())
        }
    }

    @SuppressLint("HardwareIds")
    private fun getNearMe() {
        val errSection = "getNearMe :"
        swipe_container?.isRefreshing = true

        val lat = user_info.latitude
        val lon = user_info.longitude

        val body = JSONObject()
        body.put("take","10")
        body.put("skip","0")
        body.put("nearme","1")
        body.put("type","vendor")
        body.put("latitude",lat)
        body.put("longitude",lon)
        Log.d("aim","nearme body request : $body, lat: $lat, lon: $lon")

        val sr = object : JsonObjectRequest(Request.Method.POST, user_info.listing, body, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("AIM", "$errSection $response")

                val jsonArray = response.getJSONArray("list")
                user_info.restoreNearMe = jsonArray
                for (i in 0 until jsonArray.length()) {
                    val jo = jsonArray.getJSONObject(i)

                    val jarak = if (jo.isNull("distance")) "" else jo.getString("distance")


                    val id = jo.getString("id_vendor")
                    val nama = jo.getString("nama")
                    val alamat = jo.getString("alamat_vendor")
                    val status = jo.getString("is_open")
                    val img = jo.getString("thumbnail_logo")
                    val category = jo.getString("type_id")

                    val harga1 = if (jo.isNull("price")) 0.0 else jo.getDouble("price")
                    val harga2 = if (jo.isNull("price")) 0.0 else jo.getDouble("price")

                    val tanggalMulai = jo.getString("tanggal_mulai")
                    val tanggalAkhir = jo.getString("tanggal_akhir")
                    val categoryName = jo.getString("vendor_type_nama")

                    val mItem = vendor_model(id, nama, jarak, 0, status, img, category,alamat, harga1,tanggalMulai,tanggalAkhir,"",categoryName,"","",harga2)
                    vendorData.add(mItem)
                }

                // ============ recycler_nearme ============
                recycler_nearme?.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
                val vendorAdapter = home_nearme_adapter()
                recycler_nearme?.adapter = vendorAdapter
                vendorAdapter.setList(vendorData)
                vendorData.clear()
            } catch (e: Exception) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {

            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
//                headers["X-DEVICE-ID"] = androidID
                headers["X-FCM-TOKEN"] = user_info.FCM
                return headers
            }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(sr)

    }

    private fun getPromo(){
        val errSection = "getPromo"
        swipe_container.isRefreshing = true
        val rq = object : JsonObjectRequest(Request.Method.GET, user_info.promo,null, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim","$errSection: $response")

                val dataArray = response.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    val id = dataObj.getString("id_vendor")
                    val type = dataObj.getString("type")
                    val nama = dataObj.getString("nama_vendor")
                    val harga1 = dataObj.getDouble("harga_normal")
                    val harga2 = dataObj.getDouble("price")
                    val img = dataObj.getString("thumbnail_logo")
                    val alamat = dataObj.getString("alamat_vendor")

                    val mItem = vendor_model(id, nama, "0", 0, "", img, "",alamat, harga1,"","","",type,"","",harga2)
                    vendorData.add(mItem)
                }

                // ============ recycler_nearme ============
                recycler_promo?.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
                val vendorAdapter = home_promotion_adapter()
                recycler_promo?.adapter = vendorAdapter
                vendorAdapter.setList(vendorData)
                vendorData.clear()

            } catch (e: JSONException) {
                Log.d("aim","$errSection: $e")
            }
        },Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }

    private fun getRead(){
        val errSection = "getRead"
        swipe_container.isRefreshing = true

        val body = JSONObject()
        body.put("category","1")

        val rq = object : JsonObjectRequest(Request.Method.POST,user_info.blog,body, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim","$errSection: $response")

                val dataObject = response.getJSONObject("data")
                val dataArray = dataObject.getJSONArray("data")
                user_info.cache_read = dataArray

                for (i in 0 until 2) {
                    val jo = dataArray.getJSONObject(i)
                    val slug = jo.getString("slug")
                    val nama = jo.getString("nama")
                    val desc = jo.getString("keterangan")
                    val img = jo.getString("image_path")
                    val dataCategory = jo.getJSONObject("category")
                    val category = dataCategory.getString("nama")

                    val descFix = helper.textHtml(desc)

                    val mItem = blog_model("",slug, nama, descFix,img, category)
                    blogData.add(mItem)
                }

                // ============ recycler_read ============
                recycler_read.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.VERTICAL, false)
                val adapter = BlogAdapter()
                recycler_read.adapter = adapter
                adapter.setList(blogData)
                blogData.clear()

            } catch (e: JSONException) {
                Log.d("aim","$errSection: $e")
            }
        },Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }

    private fun getEvent() {
        val errSection = "getEvent"
        swipe_container?.isRefreshing = true

        val sr = object : StringRequest(Request.Method.POST, user_info.onGoingEvent, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val jsonArray = jsonObject.getJSONArray("data")
                user_info.restoreEvent = jsonArray
                for (i in 0 until jsonArray.length()) {
                    val jo = jsonArray.getJSONObject(i)

                    val tipe = jo.getString("type")
                    var mixID = ""

                    when (tipe) {
                        "vendor" -> {
                            mixID = jo.getString("id_vendor")
                        }
                        "course" -> {
                            mixID = jo.getString("slug")
                        }
                        "event" -> {
                            mixID = jo.getString("slug")
                        }
                        "product" -> {
                            mixID = jo.getString("slug")
                        }
                    }

                    val nama = jo.getString("nama")
                    val alamat = jo.getString("alamat_vendor")
                    val img = jo.getString("thumbnail_logo")
                    val category = jo.getString("type_id")

                    val tagsArray = jo.getJSONArray("tags")
                    var tagsName = ""

                    if (tagsArray.length() != 0) {
                        for (y in 0 until tagsArray.length()) {
                            val tagsObject = tagsArray.getJSONObject(y)
                            val tags = tagsObject.optJSONObject("tags")
                            try {
                                tagsName = tags.getString("nama")
                            } catch (e: Exception) {
                                Log.e("aim", "Err array : $e")
                            }
                        }
                    }

                    val tanggalMulai = jo.getString("tanggal_mulai")
                    val tanggalAkhir = jo.getString("tanggal_akhir")
                    val categoryName = jo.getString("vendor_type_nama")

                    val mItem = vendor_model(mixID, nama, "0", 0, "", img, category,alamat, 0.0,tanggalMulai,tanggalAkhir,tipe,categoryName,tagsName,"",0.0)
                    vendorData.add(mItem)
                }

                // safe call views component
                horizontal_cycle?.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
                horizontal_cycle?.layoutManager?.scrollToPosition(Int.MAX_VALUE / 2)
                val displayMetrics = viewContext.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 200) / 2
                val pxWidth = helper.dpToPx(viewContext, calculation.toInt())
                horizontal_cycle?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (horizontal_cycle?.onFlingListener == null) {
                    val snapHelper = LinearSnapHelper()
                    snapHelper.attachToRecyclerView(horizontal_cycle)
                }

                if (jsonArray.length() != 0) {
                    val vendorAdapter = home_event_adapter()
                    horizontal_cycle?.adapter = vendorAdapter
                    vendorAdapter.setList(vendorData)
                    vendorData.clear()
                }

            } catch (e: Exception) {
                Log.e("aim", "event e: $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map["type"] = "event"
                if (user_info.latitude != 0.0 && user_info.longitude != 0.0) {
                    map["latitude"] = user_info.latitude.toString()
                    map["longitude"] = user_info.longitude.toString()
                }
                return map
            }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(sr)
    }
    private fun restoreEventFun(){
        val jsonArray = user_info.restoreEvent
        for (i in 0 until jsonArray!!.length()) {
            val jo = jsonArray.getJSONObject(i)

            val tipe = jo.getString("type")
            var mixID = ""

            when (tipe) {
                "vendor" -> {
                    mixID = jo.getString("id_vendor")
                }
                "course" -> {
                    mixID = jo.getString("slug")
                }
                "event" -> {
                    mixID = jo.getString("slug")
                }
                "product" -> {
                    mixID = jo.getString("slug")
                }
            }

            val nama = jo.getString("nama")
            val alamat = jo.getString("alamat_vendor")
            val img = jo.getString("thumbnail_logo")
            val category = jo.getString("type_id")

            val tagsArray = jo.getJSONArray("tags")
            var tagsName = ""

            if (tagsArray.length() != 0) {
                for (y in 0 until tagsArray.length()) {
                    val tagsObject = tagsArray.getJSONObject(y)
                    val tags = tagsObject.optJSONObject("tags")
                    try {
                        tagsName = tags.getString("nama")
                    } catch (e: Exception) {
                        Log.e("aim", "Err array : $e")
                    }
                }
            }

            val tanggalMulai = jo.getString("tanggal_mulai")
            val tanggalAkhir = jo.getString("tanggal_akhir")
            val categoryName = jo.getString("vendor_type_nama")

            val mItem = vendor_model(mixID, nama, "0", 0, "", img, category,alamat, 0.0,tanggalMulai,tanggalAkhir,tipe,categoryName,tagsName,"",0.0)
            vendorData.add(mItem)
        }

        // safe call views component
        horizontal_cycle?.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
        horizontal_cycle?.layoutManager?.scrollToPosition(Int.MAX_VALUE / 2)
        val displayMetrics = viewContext.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 200) / 2
        val pxWidth = helper.dpToPx(viewContext, calculation.toInt())
        horizontal_cycle?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (horizontal_cycle?.onFlingListener == null) {
            val snapHelper = LinearSnapHelper()
            snapHelper.attachToRecyclerView(horizontal_cycle)
        }

        if (jsonArray.length() != 0) {
            val vendorAdapter = home_event_adapter()
            horizontal_cycle?.adapter = vendorAdapter
            vendorAdapter.setList(vendorData)
            vendorData.clear()
        }
    }

    private fun getPromotion() {
        val errSection = "getPromotion :"
        swipe_container?.isRefreshing = true
        val rq = object : StringRequest(Request.Method.POST, user_info.availableData, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restorePromotion = dataArray
                for (i in 0 until dataArray.length()) {
                    val jo = dataArray.getJSONObject(i)

                    val type = jo.getString("type")
                    val slug = when (type) {
                        "course" -> {
                            jo.getString("id_course")
                        }
                        "event" -> {
                            jo.getString("id_event")
                        }
                        "product" -> {
                            jo.getString("id_product")
                        }
                        else -> {
                            jo.getString("id_vendor")
                        }
                    }

                    val id = jo.getString("id_vendor")
                    val nama = jo.getString("nama")
                    val harga1 = jo.getDouble("harga_normal")
                    val harga2 = jo.getDouble("price")
                    val foto = jo.getString("thumbnail_logo")

                    val mItem = customModelA(id,slug,nama,harga1,harga2,foto,type)
                    mCustomDataA.add(mItem)
                }

                recycler_customA?.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
                val adapter = home_customA_adapter()
                recycler_customA?.adapter = adapter
                adapter.setList(mCustomDataA)
                mCustomDataA.clear()
            } catch (e: Exception) {
                Log.e("aim", "$errSection $e")
            }
        },Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }
    private fun restorePromotionFun(){
        val dataArray = user_info.restorePromotion
        for (i in 0 until dataArray!!.length()) {
            val jo = dataArray.getJSONObject(i)

            val type = jo.getString("type")
            val slug = when (type) {
                "course" -> {
                    jo.getString("id_course")
                }
                "event" -> {
                    jo.getString("id_event")
                }
                "product" -> {
                    jo.getString("id_product")
                }
                else -> {
                    jo.getString("id_vendor")
                }
            }

            val id = jo.getString("id_vendor")
            val nama = jo.getString("nama")
            val harga1 = jo.getDouble("harga_normal")
            val harga2 = jo.getDouble("price")
            val foto = jo.getString("thumbnail_logo")

            val mItem = customModelA(id,slug,nama,harga1,harga2,foto,type)
            mCustomDataA.add(mItem)
        }

        recycler_customA?.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.HORIZONTAL, false)
        val adapter = home_customA_adapter()
        recycler_customA?.adapter = adapter
        adapter.setList(mCustomDataA)
        mCustomDataA.clear()
    }

    private fun getFeature() {
        val errSection = "getFeature"
        swipe_container.isRefreshing = true
        val rq = object : JsonObjectRequest(Request.Method.GET,user_info.feature,null, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim","$errSection: $response")
                val dataArray = response.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    val id = dataObj.getString("id")
                    val nama = dataObj.getString("nama")
                    val desc = dataObj.getString("keterangan")
                    val link = dataObj.getString("link_to_article")
                    val photo = dataObj.getString("image_path")

                    val descFix = helper.textHtml(desc)

                    val mItem = FeatureModel(id,nama,descFix,link,photo)
                    featureData.add(mItem)
                }

                recycler_feature.layoutManager = LinearLayoutManager(viewContext, OrientationHelper.VERTICAL, false)
                val adapter = home_feature_adapter()
                recycler_feature.adapter = adapter
                adapter.setList(featureData)
                featureData.clear()

            } catch (e: JSONException) {
                Log.d("aim","$errSection: $e")
            }
        },Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }

    private fun getNotif(position:Int) {
        val errSection = "getNotif"
        swipe_container?.isRefreshing = true

        val rq = object : JsonObjectRequest(Request.Method.GET,user_info.detailChild+user_info.activeChildId,null,Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                val data = response.getJSONObject("data")

                val saldo = data.getDouble("available_saldo")
                val holder = recycler_child?.findViewHolderForAdapterPosition(position)
                val mView = holder?.itemView?.findViewById<TextView>(R.id.child_balance)
                mView?.text = helper.bank(saldo)

                // Notif News
                if (data.has("unread_notifications")) {
                    val total = data.optInt("unread_notifications",0)
                    if (total > 0) {
                        notif_news.visibility = View.VISIBLE
                        notif_news.text = total.toString()
                    }
                }

                // Notif Billing
                if (data.has("unread_trans_billing")) {
                    val total = data.optInt("unread_trans_billing",0)
                    if (total > 0) {
                        notif_billing.visibility = View.VISIBLE
                        notif_billing.text = total.toString()
                    }
                }

                // Notif Wallet
                if (data.has("unread_orders")) {
                    val total = data.optInt("unread_orders",0)
                    if (total > 0) {
                        notif_wallet.visibility = View.VISIBLE
                        notif_wallet.text = total.toString()
                    }
                }
            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        },Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
//            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }

    private fun getRecentAttendance(position:Int) {
        val errSection = "getAttendance :"
        swipe_container?.isRefreshing = true

        val sr = object : StringRequest(Request.Method.POST, user_info.childStatus+user_info.activeChildId, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val holder = recycler_child?.findViewHolderForAdapterPosition(position)
                val vendorView = holder?.itemView?.findViewById<TextView>(R.id.child_vendor)
                val iconView = holder?.itemView?.findViewById<ImageView>(R.id.child_status)
                val timeView = holder?.itemView?.findViewById<TextView>(R.id.child_time)

                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data")
                val location = data.getString("location")
                val time = data.getString("time")
                val checkin = data.getString("is_check_in")
                if (checkin == "1"){
                    timeView?.text = time
                    iconView?.visibility = View.VISIBLE
                    iconView?.setBackgroundResource(R.drawable.ic_checkin)
                    vendorView?.text = location
                }
                else if(checkin == "0" && location == "0"){
                    timeView?.text = "--:--"
                    iconView?.visibility = View.INVISIBLE
                    vendorView?.text = "Data not available"
                }
                else{
                    timeView?.text = time
                    iconView?.visibility = View.VISIBLE
                    iconView?.setBackgroundResource(R.drawable.ic_checkout)
                    vendorView?.text = location
                }

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        },Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
//            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(sr)
    }
}