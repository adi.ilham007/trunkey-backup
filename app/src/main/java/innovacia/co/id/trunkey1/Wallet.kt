package innovacia.co.id.trunkey1


import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.WalletAdaper
import innovacia.co.id.trunkey1.model.WalletModel
import kotlinx.android.synthetic.main.activity_wallet.*
import org.json.JSONObject
import kotlinx.android.synthetic.main.activity_wallet.forum_footer_profile
import kotlinx.android.synthetic.main.activity_wallet.home_footer_profile
import kotlinx.android.synthetic.main.activity_wallet.profile_footer_prof
import kotlinx.android.synthetic.main.activity_wallet.profile_text_prof
import kotlinx.android.synthetic.main.activity_wallet.search_footer_profile
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import innovacia.co.id.trunkey1.database.Cart
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_wallet.back_link
import org.json.JSONException

class Wallet : AppCompatActivity() {

    private val walletData = mutableListOf<WalletModel>()
    private val helper = user_info()

    private var loadMore = false    // First load
    private var lastData = false    // Pagination
    private var page = 1 //page now
    private var max_page = 1 //maxpage

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)

        if (!user_info.walletStatus) {
            id_trunkey.visibility = View.GONE
            text_idr.visibility = View.GONE
            topup.isEnabled = false
            topup_icon.setTextColor(Color.parseColor("#ADABAB"))
            topup_text.setTextColor(Color.parseColor("#ADABAB"))
            create_wallet.visibility = View.VISIBLE
        }

        if (!user_info.pinStatus) {
            pinDialog()
        }

        footerHandling()

        back_link.setOnClickListener{
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        create_wallet.setOnClickListener {
            create_wallet.startAnimation(user_info.animasiButton)
            if (!user_info.pinStatus) {
                pinDialog()
            }
            if (user_info.pinStatus && !user_info.walletStatus) {
                walletDialog()
            }
        }

        topup.setOnClickListener {
            topup.startAnimation(user_info.animasiButton)
            val i = Intent(this, TopUp::class.java)
            startActivity(i)
        }

        cart.setOnClickListener {
            cart.startAnimation(user_info.animasiButton)
            val i = Intent(this, Cart::class.java)
            startActivity(i)
        }

        getProfile(page)
        rvListener()
    }

    private fun rvListener() {
        scrollWallet.viewTreeObserver.addOnScrollChangedListener {
            val view = scrollWallet.getChildAt(scrollWallet.childCount - 1) as View

            val diff = view.bottom - (scrollWallet.height + scrollWallet
                    .scrollY)
            Log.d("aim","${view.bottom} "+(scrollWallet.height + scrollWallet
                    .scrollY))
            if (diff == 0) {

                if(page <= max_page){
                    page++
//                    progress_bar_wallet.visibility = View.VISIBLE
                    loadMore = true
                    getProfile(page)
//                    Log.d("aim","take: 10, skip: ")

                }

            }
        }
    }

    fun getProfile(page:Int) {
        if(page==1) {
            shimmerLayout.startShimmerAnimation()
        }
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.profile_transaction+"?page="+page, Response.Listener { response ->
            try {
                shimmerLayout.stopShimmerAnimation()
                text_idr.text = helper.bank(user_info.wallet)
                IDR_cash_point.text = helper.bank(user_info.cashBack)
                val jsonObject = JSONObject(response)
                val dataobject = jsonObject.getJSONObject("data")
                max_page = dataobject.getInt("last_page")
                val jsonArray= dataobject.getJSONArray("data")
                Log.d("aim",jsonArray.length().toString())
                for (i in 0 until jsonArray.length()) {  // ======================================================== custom take
                    val jo = jsonArray.getJSONObject(i)
                    val id = jo.getString("id")
                    val type = jo.getString("type")
                    val debet = jo.getString("debet")
                    val kredit = jo.getString("kredit")

                    val tanggal = jo.getString("created")

                    val transaksi = jo.getJSONObject("transaction")


                    val nama = transaksi.getString("nama")
                    val status = transaksi.getString("status")
                    val jumlah  = transaksi.getString("jumlah")
                    var cashBackIDR = "IDR 0"


                    val helper = user_info()
//                    val jumlahIDR = helper.bank(jumlah)
                    if(transaksi.getString("cashback")!="null"){
                        val cashback = transaksi.getDouble("cashback")
                        cashBackIDR = helper.bank(cashback)
                    }


                    var status_fix: String
                    if (status == "1") {
                        status_fix = "Success"
                    } else {
                        status_fix = "Failed"
                    }

                    val mItem = WalletModel(id,nama,tanggal,status,jumlah,type,cashBackIDR,debet,kredit)
                    walletData.add(mItem)
                    Log.d("aim", "$mItem, index: $i,$type")
                    Log.d("aim", "wallet profile, index: $i,$type")

                }

                if (recycler_transaction != null && !loadMore) {
                    try {
                        recycler_transaction.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                        val walletAdapter = WalletAdaper()
                        recycler_transaction.adapter = walletAdapter
                        walletAdapter.setList(walletData)
//                        progress_bar_wallet.visibility = View.GONE
                    }catch (e: JSONException) {
                        Log.e("aim", "Err Swipe Pager if : $e")
                    }
                }

                else if (recycler_transaction != null && loadMore) {
                    try {
                        val recyclerViewState = recycler_transaction.layoutManager!!.onSaveInstanceState()
                        val walletAdapter = WalletAdaper()
                        recycler_transaction.adapter = walletAdapter
                        walletAdapter.setList(walletData)
                        recycler_transaction.layoutManager!!.onRestoreInstanceState(recyclerViewState)
//                        progress_bar_wallet.visibility = View.GONE
                    }catch (e: JSONException) {
                        Log.e("aim", "Err Swipe Pager else : $e")
                    }

                }
//
//                // ============ recycler_nearme ============
//                recycler_transaction.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
//                val walletAdapter = WalletAdaper()
//                recycler_transaction.adapter = walletAdapter
//                walletAdapter.setList(walletData)
//                walletData.clear()


                getWalletId()



            }catch (e: JSONException) {
                Log.e("aim", "Wallet e: $e")
            }
        }, Response.ErrorListener { response ->
            shimmerLayout.stopShimmerAnimation()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String,String> {
                val headers = HashMap<String,String>()

                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

//            override fun getParams(): MutableMap<String,String> {
//                val map = HashMap<String, String>()
//                return map
//            }


        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }

    fun footerHandling(){
        // ========================================================================================= Footer onClick
        try {
            profile_footer_prof.setTextColor(resources.getColor(R.color.blue))
            profile_text_prof.setTextColor(resources.getColor(R.color.blue))
        }catch (e: JSONException) {
            Log.e("aim", "Err : $e")
        }

        // ========================================================================================= Footer
        home_footer_profile.setOnClickListener {
            home_footer_profile.startAnimation(user_info.animasiButton)
            val i = Intent(this, home::class.java)
            startActivity(i)
        }

        search_footer_profile.setOnClickListener {
            search_footer_profile.startAnimation(user_info.animasiButton)
            val i = Intent(this, Scan::class.java)
            startActivity(i)
        }

        forum_footer_profile.setOnClickListener{
            forum_footer_profile.startAnimation(user_info.animasiButton)
            val i = Intent(this, Forum::class.java)
            startActivity(i)
        }

//        profile_footer_prof.setOnClickListener {
//            profile_text_prof.startAnimation(user_info.animasiButton)
//
//            if (!user_info.loginStatus) {
//                val i = Intent(this, popup_register::class.java)
//                i.putExtra("back", "profile")
//                startActivity(i)
//            } else {
//                user_info.statePosition = "profile"
//                val i = Intent(this, Profile::class.java)
//                startActivity(i)
//            }
//        }
    }

    fun getWalletId(){
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.xfers_account, Response.Listener { response ->
            try {
                val jsonObject = JSONObject(response)
                val dataobject = jsonObject.getJSONObject("data")
                val userObject= dataobject.getJSONObject("user")
                val id_user = userObject.getString("trunkey_wallet_account_name")
                id_trunkey.text = id_user

            }
            catch (e: JSONException) {
                Log.e("aim", "Wallet e: $e")
            }
        }, Response.ErrorListener { response ->
            shimmerLayout.stopShimmerAnimation()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()

                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }

    private fun pinDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_pin)

        val pinCreate = dialogs.findViewById<EditText>(R.id.newPin)
        val confirmPinCreate = dialogs.findViewById<EditText>(R.id.confirmPin)

        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val pinValue = pinCreate.text.toString()
            val confirmValue = confirmPinCreate.text.toString()
            if (pinValue == confirmValue) {
                createPin(pinValue, confirmValue)
                dialogs.dismiss()
            } else {
                Toast.makeText(this, "Pin mismatch", Toast.LENGTH_LONG).show()
            }
        }

        dialogs.show()
    }
    fun createPin(newPin:String,confirmPin:String) {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.set_pin, Response.Listener { response ->
            try {
                Log.d("aim", response)
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    Toast.makeText(this, "Create pin Success", Toast.LENGTH_LONG).show()
                    user_info.pinStatus = true
                }else {
                    Toast.makeText(this, "Create pin fail", Toast.LENGTH_LONG).show()
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            pinDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = java.util.HashMap<String, String>()
                map["pin"] = newPin
                map["pin_confirmation"] = confirmPin
                Log.d("aim","$map")
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }

    private fun walletDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_register)

        val previewPhone = dialogs.findViewById<TextView>(R.id.previewPhone)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        previewPhone.text = user_info.phone

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            sendOtp()
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            val i = Intent(this,Editprofile::class.java)
            startActivity(i)
        }

        dialogs.show()
    }
    private fun sendOtp() {
        shimmerLayout.startShimmerAnimation()
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.send_otp, Response.Listener { response ->
            try {
                shimmerLayout.stopShimmerAnimation()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    otpDialog()
                }
            } catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            shimmerLayout.stopShimmerAnimation()
            walletDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["phone"] = user_info.phone
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
        Log.d("aim",sr.toString())
    }

    private fun otpDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_otp)

        val pinOtp = dialogs.findViewById<EditText>(R.id.pinOtp)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            prosesOtp(pinOtp.text.toString())
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
            walletDialog()
        }

        dialogs.show()
    }
    private fun prosesOtp(otpCode:String) {
        shimmerLayout.startShimmerAnimation()
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.proses_otp, Response.Listener { response ->
            try {
                shimmerLayout.stopShimmerAnimation()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    Toast.makeText(this, "Trunkey wallet created", Toast.LENGTH_LONG).show()
                    user_info.walletStatus = true
                    getProfile(page)
                }else {
                    Toast.makeText(this, "Error verification code", Toast.LENGTH_LONG).show()
                    user_info.walletStatus = false
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            shimmerLayout.stopShimmerAnimation()
            otpDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["phone"] = user_info.phone
                map["otp_code"] = otpCode
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }
}


