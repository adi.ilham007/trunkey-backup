package innovacia.co.id.trunkey1.model

data class MyBookingModel (
        var id : String,
        var vendor_name : String,
        var nama_tiket : String,
        var keterangan : String,
        var biaya : String,
        var tanggal_booking : String,
        var tanggal_mulai : String,
        var tanggal_akhir : String,
        var nama_participant : String,
        var code_booking : String,
        var tempat : String,
        var type : String
)