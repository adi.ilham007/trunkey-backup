package innovacia.co.id.trunkey1.model

data class TrunkeyCareModel(
        val id:String,
        val nama:String,
        val type:String,
        val photo:String,
        val status:Int,
        val child: MutableList<TrunkeyCareChildModel>
)