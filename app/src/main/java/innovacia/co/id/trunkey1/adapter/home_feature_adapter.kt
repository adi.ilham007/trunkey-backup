package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.DetailBlog
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.FeatureModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_feature.view.*
import android.support.v4.content.ContextCompat.startActivity
import android.net.Uri


class home_feature_adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var featureData = mutableListOf<FeatureModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_feature, parent, false))
    }
    override fun getItemCount(): Int = featureData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder
        holder.bindView(featureData[position])

        holder.itemView.setOnClickListener { view ->
            holder.itemView.startAnimation(user_info.animasiButton)

            val link = featureData[position].link

            val uri = Uri.parse(link)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            view.context.startActivity(intent)

        }
    }
    fun setList(listOfVendor: List<FeatureModel>) {
        this.featureData = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<FeatureModel>) {
        this.featureData.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(dataFeature: FeatureModel) {
            itemView.name_value.text = dataFeature.nama
            itemView.desc.text = dataFeature.desc

            Glide.with(itemView.context).load(dataFeature.photo).into(itemView.file_logo)
        }
    }

}