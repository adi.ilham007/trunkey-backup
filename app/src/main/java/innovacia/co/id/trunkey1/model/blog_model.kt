package innovacia.co.id.trunkey1.model

data class blog_model(
        var idVideo     :String,
        var slug        :String,
        var nama        :String,
        var keterangan  :String,
        var img         :String,
        var type        :String
)