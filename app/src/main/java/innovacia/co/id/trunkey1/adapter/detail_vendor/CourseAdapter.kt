package innovacia.co.id.trunkey1.adapter.detail_vendor

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.Course
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.vendor_adapter
import innovacia.co.id.trunkey1.model.detail_model
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_detail.view.*

class CourseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var vendorList = mutableListOf<detail_model>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_detail, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as vendorListViewHolder

        viewHolder.bindView(vendorList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val slug = vendorList[position].slug
//                Click listener
            val i = Intent(view.context, Course::class.java)
            i.putExtra("slug", slug)
            view.context.startActivity(i)

        }

    }

    fun update(modelList:MutableList<detail_model>){
        vendorList = modelList
        val vendorAdapter = vendor_adapter()
        vendorAdapter.notifyDataSetChanged()
    }

    fun setList(listOfVendor: List<detail_model>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<detail_model>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val helper = user_info()

        fun bindView(vendorModel: detail_model) {
            itemView.id_vendor.text = vendorModel.slug
            itemView.nama.text      = vendorModel.nama
            itemView.harga.text     = helper.bank(vendorModel.harga)
//            Glide.with(itemView.context).load(vendorModel.file_photo).into(itemView.file_photo)

            Glide
                .with(itemView.context)
                .load(vendorModel.file_photo)
                .into(itemView.file_photo)
        }
    }
}