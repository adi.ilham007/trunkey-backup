package innovacia.co.id.trunkey1.model

data class NewsModel(
        val id:String,
        val childId:String,
        val newsDate:String,
        val dueDate:String,
        val vendorName:String,
        val vendorAddrs:String,
        val newsTitle:String,
        val newsDesc:String,
        val attachment: MutableList<String>
)