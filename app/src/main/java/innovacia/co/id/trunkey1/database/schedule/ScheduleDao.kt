package innovacia.co.id.trunkey1.database.schedule

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface ScheduleDao {
    @Query("SELECT * FROM schedule_table")
    fun getAll(): LiveData<List<ScheduleEntity>>

    @Query("SELECT * FROM schedule_table WHERE id =:id")
    fun getByIdTrans(id: Int): List<ScheduleEntity>

    @Query("SELECT * FROM schedule_table WHERE date =:date")
    fun getBydate(date: String): LiveData<List<ScheduleEntity>>

    @Query("SELECT * FROM schedule_table WHERE date LIKE :yearAndMonth")
    fun getScheduleInMonth(yearAndMonth: String): List<ScheduleEntity>
//
//    @Query("SELECT * FROM schedule_table WHERE merchantId =:id")
//    fun getByIdMerchant(id: Int): List<ScheduleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(schedule: ScheduleEntity)

    @Delete
    fun delete(schedule: ScheduleEntity)

    @Update
    fun update(schedule: ScheduleEntity)
}
