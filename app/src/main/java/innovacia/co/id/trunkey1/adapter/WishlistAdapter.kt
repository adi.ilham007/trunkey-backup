package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.graphics.Paint
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.WishlistModel
import kotlinx.android.synthetic.main.list_wishlist.view.*



class WishlistAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    private var mCustomAModel = mutableListOf<WishlistModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_wishlist, parent, false))
    }
    override fun getItemCount(): Int = mCustomAModel.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder
        holder.bindView(mCustomAModel[position])

        val id = mCustomAModel[position].id
        val slug = mCustomAModel[position].slug
        val type = mCustomAModel[position].type

        holder.itemView.btn_like.setOnClickListener {
            val intent = Intent("data")
            intent.putExtra("id",id)
            intent.putExtra("type",type)
            Log.d("aim","id : $id, type : $type")
            LocalBroadcastManager.getInstance(holder.itemView.context).sendBroadcast(intent)
        }

        holder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            when (type) {
                "vendor" -> {
                    val i = Intent(view.context, detail_vendor::class.java)
                    i.putExtra("id", id)
                    view.context.startActivity(i)
                }
                "event" -> {
                    val i = Intent(view.context, Event::class.java)
                    i.putExtra("slug", slug)
                    view.context.startActivity(i)
                }
                "course" -> {
                    val i = Intent(view.context, Course::class.java)
                    i.putExtra("slug", slug)
                    view.context.startActivity(i)
                }
            }
        }
    }

    fun setList(listOfVendor: List<WishlistModel>) {
        this.mCustomAModel = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(listOfVendor: List<WishlistModel>) {
        this.mCustomAModel.addAll(listOfVendor)
        notifyDataSetChanged()
    }

    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(mCustomModel: WishlistModel) {
            val helper = user_info()

            itemView.name_value.text      = mCustomModel.nama
            itemView.category_value.text  = mCustomModel.type

            itemView.harga2.text         = helper.bank(mCustomModel.harga)

            Glide.with(itemView.context)
                    .load(mCustomModel.foto)
                    .into(itemView.file_logo)
        }
    }

}