package innovacia.co.id.trunkey1.model.filter

data class AgeModel(
        var id          :String,
        var name        :String,
        var desc        :String,
        var isSelected  :Boolean
)