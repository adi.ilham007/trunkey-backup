package innovacia.co.id.trunkey1.activity

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.adapter.AttendanceAdapter
import innovacia.co.id.trunkey1.adapter.ChildAdapter2
import innovacia.co.id.trunkey1.helper.SnapHelperOneByOne
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.AttendanceModel
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.activity_attendance.*
import kotlinx.android.synthetic.main.sub_header_rv.*
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class Attendance : AppCompatActivity() {

    private val helper = user_info()
    private val mChildModel = mutableListOf<ChildModel>()
    private val mAttendanceModel = mutableListOf<AttendanceModel>()

    @SuppressLint("SimpleDateFormat")
    private val uiDateFormat = SimpleDateFormat("dd MMMM yyyy")

    @SuppressLint("SimpleDateFormat")
    private val serverDateFormat = SimpleDateFormat("yyyy-MM-dd")

    private var dateChose = serverDateFormat.format(Date())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendance)
        header_name?.text = resources.getString(R.string.attendance_header)

        swipe_container?.setOnRefreshListener {
            mAttendanceModel.clear()
            getHistoryAttendance()
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        date_picker.setOnClickListener {
            date_picker.startAnimation(user_info.animasiButton)

            val c = Calendar.getInstance()

            val yearC = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener {
                view, year, monthOfYear, dayOfMonth ->

                c.set(Calendar.YEAR, year)
                c.set(Calendar.MONTH, monthOfYear)
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                dateChose = serverDateFormat.format(c.time)
                date_picker.text = uiDateFormat.format(c.time)
                getHistoryAttendance()

            }, yearC, month, day)
            dpd.show()
        }

        cacheHandling()

        date_picker.text = uiDateFormat.format(Date())
        getHistoryAttendance()

    }

    private fun cacheHandling() { if (user_info.restoreKids == null) getKids() else restoreKidsFun() }

    private fun getKids() {
        val errSection = "getKids :"
        swipe_container?.isRefreshing = true

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.GET, user_info.getChild, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                user_info.activeChildId = ""
                user_info.childCarouselState = 0

                mChildModel.clear()
                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")
                user_info.restoreKids = dataArray
                if (dataArray.length() > 0) {
                    // Default see all
//                    if (dataArray.length() > 1) {
//                        val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                        mChildModel.add(seeAll)
//                    }

                    for (i in 0 until dataArray.length()) {
                        val childObject = dataArray.getJSONObject(i)
                        val id = childObject.getString("id")
                        val nama = childObject.getString("nama")
                        val gender = childObject.getString("gender")
                        val birthday = childObject.getString("birthday")
                        val foto = childObject.getString("photo")
                        val saldo = childObject.getDouble("available_saldo")
                        val colorTheme = childObject.getString("color_theme")

                        val attendance = childObject.getJSONObject("last_attendance")
                        val location = attendance.getString("location")
                        val time = attendance.getString("time")
                        val status = attendance.getInt("is_check_in")

                        val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                        mChildModel.add(mItem)

                        // Add child option
                        if (i+1 == dataArray.length()) {
                            val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                            mChildModel.add(addChild)
                        }
                    }
                } else {
                    for (i in 0 until 1) {
                        val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                        mChildModel.add(addChild)
                    }
                }

                val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                recycler_child?.layoutManager = mLinearLayoutManager

                // Triger carousel to center
                recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)

                val displayMetrics = this.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 320) / 2
                val pxWidth = helper.dpToPx(this, calculation.toInt())
                recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (recycler_child?.onFlingListener == null) {
                    val snapHelper = SnapHelperOneByOne()
                    snapHelper.attachToRecyclerView(recycler_child)
                }

                // Large Carousel
                ChildAdapter2.TYPE_OF_VIEW = 1
                val mChildAdapter = ChildAdapter2()
                recycler_child?.adapter = mChildAdapter
                mChildAdapter.setList(mChildModel)

                kidsCarouselListener(mLinearLayoutManager)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }
    private fun restoreKidsFun() {
        mChildModel.clear()
        val dataArray = user_info.restoreKids!!
        if (dataArray.length() > 0) {
            // Default see all
//            if (dataArray.length() > 1) {
//                val seeAll = ChildModel("See all", "", "", "", "", 0.0,"")
//                mChildModel.add(seeAll)
//            }

            for (i in 0 until dataArray.length()) {
                val childObject = dataArray.getJSONObject(i)
                val id = childObject.getString("id")
                val nama = childObject.getString("nama")
                val gender = childObject.getString("gender")
                val birthday = childObject.getString("birthday")
                val foto = childObject.getString("photo")
                val saldo = childObject.getDouble("available_saldo")
                val colorTheme = childObject.getString("color_theme")

                val attendance = childObject.getJSONObject("last_attendance")
                val location = attendance.getString("location")
                val time = attendance.getString("time")
                val status = attendance.getInt("is_check_in")

                val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,status)
                mChildModel.add(mItem)

                // Add child option
                if (i+1 == dataArray.length()) {
                    val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                    mChildModel.add(addChild)
                }
            }
        } else {
            for (i in 0 until 1) {
                val addChild = ChildModel("Add child", "", "","","",0.0,"","","",0)
                mChildModel.add(addChild)
            }
        }

        val mLinearLayoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
        recycler_child?.layoutManager = mLinearLayoutManager

        // Handle carousel position (restore or not)
        if (user_info.childCarouselState != 0){
            recycler_child?.layoutManager?.scrollToPosition(user_info.childCarouselState)
        } else {
            recycler_child?.layoutManager?.scrollToPosition(mChildModel.size * 20)
        }

        val displayMetrics = this.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val calculation = (dpWidth - 320) / 2
        val pxWidth = helper.dpToPx(this, calculation.toInt())
        recycler_child?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

        if (recycler_child?.onFlingListener == null) {
            val snapHelper = SnapHelperOneByOne()
            snapHelper.attachToRecyclerView(recycler_child)
        }

        // Large Carousel
        ChildAdapter2.TYPE_OF_VIEW = 1
        val mChildAdapter = ChildAdapter2()
        recycler_child?.adapter = mChildAdapter
        mChildAdapter.setList(mChildModel)

        kidsCarouselListener(mLinearLayoutManager)
    }
    private fun kidsCarouselListener(mLinearLayoutManager: LinearLayoutManager) {
        recycler_child?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val snapHelper = SnapHelperOneByOne()
                if(newState == RecyclerView.TOUCH_SLOP_PAGING && newState != RecyclerView.NO_POSITION) {
                    Handler().postDelayed({
                        val centerView = snapHelper.findSnapView(mLinearLayoutManager)
                        if (centerView != null) {
                            val pos = mLinearLayoutManager.getPosition(centerView)
                            val getId = mChildModel[pos % mChildModel.size].id
                            user_info.activeChildId = getId
                            user_info.childCarouselState = pos
                            Log.d("aim", "active child id : $getId")

                            mAttendanceModel.clear()

//                            getRecentAttendance()
                            getHistoryAttendance()
                        }

                    }, 500)
                }
            }
        })
    }

    private fun getHistoryAttendance() {
        val errSection = "getHistoryAttendance :"
        swipe_container?.isRefreshing = true

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.historyAttendace, Response.Listener { response ->
            try{
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val dataArray = jsonObject.getJSONArray("data")

                if (dataArray.length() > 0) {
                    for(i in 0 until dataArray.length()) {
                        val dataObject = dataArray.getJSONObject(i)
                        val kidName = dataObject.getString("child_name").capitalize()
                        val vendorName = dataObject.getString("vendor_name").capitalize()
                        val checkIn = dataObject.getString("is_check_in")
                        val checkOut = dataObject.getString("is_check_out")
                        val timeIN = dataObject.getString("check_in")
                        val timeOut = dataObject.getString("check_out")
                        val method = dataObject.getString("method").capitalize()

                        val mItem = AttendanceModel(kidName,vendorName,checkIn,checkOut,timeIN,timeOut,method)
                        mAttendanceModel.add(mItem)
                    }
                } else {
                    Toast.makeText(this,"No data available",Toast.LENGTH_LONG).show()
                }

                recycler_attendance?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val mAttendanceAdapter = AttendanceAdapter()
                recycler_attendance?.adapter = mAttendanceAdapter
                mAttendanceAdapter.setList(mAttendanceModel)

            }catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["start_date"] = dateChose
                map["end_date"] = dateChose
                map["child"] = user_info.activeChildId
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
