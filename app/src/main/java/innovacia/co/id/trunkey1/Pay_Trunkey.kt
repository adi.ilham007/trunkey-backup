package innovacia.co.id.trunkey1

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.chaos.view.PinView
import innovacia.co.id.trunkey1.adapter.TrunkeyCareSubscribeAdapter
import innovacia.co.id.trunkey1.database.cart.CartRespository
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_pay__trunkey.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class Pay_Trunkey : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val helper = user_info()
    val mVolleyHelper = VolleyHelper()

    val space = "|>:M:<|"
    lateinit var cartRespository    : CartRespository

    private var payFor = ""
    // transaction
    lateinit var idArray: ArrayList<Int>
    lateinit var seatArray: ArrayList<Int>
    lateinit var hargaArray: ArrayList<*>
    var memberArray = ArrayList<String>()

    var idBilling = ""
    var typeArray = ArrayList<String>()

    var saldo = user_info.wallet
    var cashBack = user_info.cashBack
    var isiWallet = saldo + cashBack

    var slug = ""
    var totalHarga = 0.0
    var totalCashBack = 0.0

    var inputWallet = 0.0
    var inputCashBack = 0.0

    var txPin = ""
    var txConfirmPin = ""

    var statusBayar = false
    private val method = arrayOf("Wallet","Cash Back","Split Payment")
    var methodString = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay__trunkey)
        header_name.text = "Payment"

        cartRespository = CartRespository(application)

        (pin as PinView).setAnimationEnable(true)
        spinner!!.onItemSelectedListener = this
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, method)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.adapter = aa

        generatedView()

        Log.d("aim","pin status ${user_info.pinStatus}")
        if (!user_info.pinStatus) {
            createPinDialog()
        }

        // Pay With Cash Back
        if (user_info.walletStatus) {
            IDR.text = helper.bank(saldo).replace("IDR ", "")
        } else {
            IDR.text = "Inactive"
            IDR.setTextColor(Color.parseColor("#FF0000"))
            spinner.visibility = View.GONE
            splitWallet_lay.visibility = View.GONE
            methodString = "cashback"
        }
        cashback.text = helper.bank(cashBack).replace("IDR ","")

        // Saldo Tidak Cukup
        if (isiWallet < totalHarga) {
            pay_lay.visibility = View.GONE

            txnotif.visibility = View.VISIBLE
            option_lay.visibility = View.VISIBLE
            statusBayar = false
        } else {
            statusBayar = true
        }

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

//        valueWallet.setCurrency("IDR")
        valueWallet.setDelimiter(false)
        valueWallet.setSpacing(true)
        valueWallet.setDecimals(false)
        valueWallet.setSeparator(".")

        valueWallet.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?){
                val wallet = valueWallet.cleanDoubleValue
                val hitungCashBack = totalHarga - wallet
                if (hitungCashBack >= 0) {
                    inputWallet = wallet
                    inputCashBack = hitungCashBack
                    valueCashBack.setText(helper.bank(hitungCashBack).replace("IDR ",""))
                } else {
                    inputWallet = totalHarga
                    inputCashBack = 0.0
                    valueCashBack.setText(helper.bank(0.0).replace("IDR ",""))
                    splitPaymentWarning()
                }
                Log.d("aim","wallet : $inputWallet, cash back : $inputCashBack")
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        pay.setOnClickListener {
            pay.startAnimation(user_info.animasiButton)
            if (user_info.pinStatus) {
                if (statusBayar) {
                    if (payFor == "transaction") { bayar() }
                    else if (payFor == "billing") { bayarBilling() }
                    else if (payFor == "caretaker") { bayarCareTaker() }
                }
            } else {
                createPinDialog()
            }
        }

        topup.setOnClickListener {
            topup.startAnimation(user_info.animasiButton)
            val i = Intent(this, TopUp::class.java)
            startActivity(i)
        }

    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        when {
            method[position] == "Wallet" -> {
                methodString = "wallet"
                splitWallet_lay.visibility = View.GONE
            }
            method[position] == "Cash Back" -> {
                methodString = "cashback"
                splitWallet_lay.visibility = View.GONE
            }
            method[position] == "Split Payment" -> {
                methodString = "split_payment"
                splitWallet_lay.visibility = View.VISIBLE
            }
        }

    }
    override fun onNothingSelected(arg0: AdapterView<*>) {}

    private fun generatedView() {
        when {
            intent.hasExtra("bayar") -> {
                payFor = "transaction"
                typeArray = intent.getStringArrayListExtra("whatBuy")
                idArray = intent.getIntegerArrayListExtra("idArray")
                seatArray = intent.getIntegerArrayListExtra("seatArray")
                hargaArray = intent.getSerializableExtra("hargaArray") as ArrayList<*>
                memberArray = intent.getStringArrayListExtra("memberArray")
                totalHarga = intent.getDoubleExtra("totalHarga",0.0)
                totalCashBack = intent.getDoubleExtra("totalCashBack",0.0)
                bill.text = helper.bank(totalHarga).replace("IDR ","")
                Log.d("aim","intent id: $idArray, seat: $seatArray, member: $memberArray, harga: $hargaArray")
            }
            intent.hasExtra("bayarBilling") -> {
                payFor = "billing"
                idBilling = intent.getStringExtra("idBilling")
                totalHarga = intent.getDoubleExtra("hargaBilling",0.0)
                bill.text = helper.bank(totalHarga).replace("IDR ","")
                Log.d("aim","$payFor,$idBilling,$totalHarga")
            }
            intent.hasExtra("caretaker") -> {
                payFor = "caretaker"
                idBilling = intent.getStringExtra("id")
                totalHarga = intent.getDoubleExtra("price",0.0)
                bill.text = helper.bank(totalHarga).replace("IDR ","")
            }
        }
    }

    fun splitPaymentWarning() {
        Toast.makeText(this,"Your payment bigger than bill amount",Toast.LENGTH_LONG).show()
    }

    private fun bayar(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        var participantArray = JSONArray()
        val dataArray = JSONArray()
        var dataObject = JSONObject()

        Log.d("aim","memberarray : $memberArray")

        for (i in 0 until idArray.size) {

            val id = idArray[i]
            val amount = seatArray[i]
            val whatBuy = typeArray[i]

            Log.d("aim","member boiler plate")
            val memberProduct = memberArray[i].split(space)

            for (x in 0 until memberProduct.size) {
                participantArray.put(memberProduct[x].trim())
            }

            dataObject.put("type",whatBuy)
            dataObject.put("product",id)
            dataObject.put("amount",participantArray.length())
            dataObject.put("participant",participantArray) // Order dr mas wisnu

            dataArray.put(i,dataObject)

            // Reset
            dataObject = JSONObject()
            participantArray = JSONArray()
        }

        val body = JSONObject()
        body.put("method",methodString)
        body.put("data", dataArray)
        if (methodString == "split_payment") {
            body.put("cashback_nominal",inputCashBack)
            body.put("wallet_nominal", inputWallet)
        }
        body.put("pin", pin.text.toString())

        Log.d("aim","body : $body")


        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : JsonObjectRequest(Request.Method.POST, user_info.transaksi,body, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim","respon transaksi : $response")

            val jsonObj: JSONObject = response
            val status = jsonObj.getInt("status")

            if (status == 1) {
//                deleteCart()
                doneDialog()
            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            pin.setText("")
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }
        }
        Log.i("AIM", body.toString())
        sr.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    private fun bayarBilling(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.POST,user_info.payBilling+idBilling, Response.Listener { response ->
            try {
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                val status = jsonObject.getInt("status")

                if (status == 1) {
//                    deleteCart()
                    doneDialog()
                    Log.i("AIM", response.toString())
                }

            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["method"] = methodString
                map["cashback_nominal"] = "$inputCashBack"
                map["wallet_nominal"] = "$inputWallet"
                map["pin"] = pin.text.toString()
                Log.d("aim","bayar billing : $map")
                return map
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }

    private fun bayarCareTaker() {
        val errSection = "caretakerPay"

        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val body = JSONObject()
        body.put("subscription_type",idBilling)
        body.put("method",methodString)
        body.put("cashback_nominal",inputCashBack)
        body.put("wallet_nominal",inputWallet)
        body.put("pin",pin.text.toString())

        val request = object : JsonObjectRequest(Request.Method.POST, user_info.care_pay,body, Response.Listener { response ->
            try {
                dialog.dismiss()
                Log.d("aim", "$errSection $response")

                val status = response.getInt("status")
                if (status == 1) {
                    doneDialog()
                }

            } catch (e: JSONException) {
                Log.d("aim", "$errSection $e")
            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }

    fun deleteCart() {
        for (i in 0 until idArray.size) {
            val thread = object : Thread() {
                override fun run() {
                    val list = cartRespository.getByIdProduct(idArray[i])
                    if (list.isNotEmpty()) {
                        cartRespository.deleteEntry(list[0])
                    }
                }
            }
            thread.start()
        }
    }

    private fun doneDialog() {
        // Dialog full screen
        val dialogs = Dialog(this,android.R.style.Theme_Light_NoTitleBar_Fullscreen)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_transaction_success)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        textNotif.text = resources.getText(R.string.transaction_done)

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val intent = Intent(this, HomeBaru::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun createPinDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_pin)

        val pin = dialogs.findViewById<EditText>(R.id.newPin)
        val confirmPin = dialogs.findViewById<EditText>(R.id.confirmPin)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        pin.setText(txPin)
        confirmPin.setText(txConfirmPin)

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            txPin = pin.text.toString()
            txConfirmPin = confirmPin.text.toString()

            if (txPin == txConfirmPin) {
                createPin(pin.text.toString(), confirmPin.text.toString())
                dialogs.dismiss()
            } else {
                pin.setText("")
                confirmPin.setText("")
                Toast.makeText(this,"Pin mismatch",Toast.LENGTH_LONG).show()
            }
        }

        dialogs.show()
    }

    fun createPin(newPin:String,confirmPin:String) {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.set_pin, Response.Listener { response ->
            try {
                Log.d("aim", response)
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    Toast.makeText(this, "Create pin Success", Toast.LENGTH_LONG).show()
                    user_info.walletStatus = true
                    user_info.pinStatus = true
                }else {
                    Toast.makeText(this, "Create pin fail", Toast.LENGTH_LONG).show()
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            // Resend pin
            createPinDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["pin"] = newPin
                map["pin_confirmation"] = confirmPin
                Log.d("aim","$map")
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
