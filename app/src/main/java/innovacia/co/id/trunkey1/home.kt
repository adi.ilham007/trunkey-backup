package innovacia.co.id.trunkey1

import android.Manifest
import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.OrientationHelper
import android.util.DisplayMetrics
import android.util.EventLogTags
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.crashlytics.android.Crashlytics
import com.google.android.gms.location.LocationServices
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsOptions
import innovacia.co.id.trunkey1.adapter.*
import innovacia.co.id.trunkey1.helper.Firebase
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.*
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONObject
import java.util.*

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import org.json.JSONException

class home : AppCompatActivity(),LocationListener {

    private val helper = user_info()
    private val mVolleyHelper = VolleyHelper()
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    // Permissions
    private var lokasiGps = Manifest.permission.ACCESS_FINE_LOCATION
    private var lokasiNet = Manifest.permission.ACCESS_COARSE_LOCATION

//    private val test = QuickPermissionsOptions(
//            handleRationale = true,
//            permanentlyDeniedMessage = "Okay, we appreciate your decision",
//            permanentDeniedMethod = { req -> getNearMe() },
//            permissionsDeniedMethod = { req -> getNearMe() }
//    )

    // Model
    private val vendorData = mutableListOf<vendor_model>()
    private val recomendData = mutableListOf<recomended_model>()

    override fun onLocationChanged(location: Location?) {
        this.getLocation()
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProviderEnabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProviderDisabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        header_name?.text = resources.getString(R.string.explore_header)

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        search.setOnEditorActionListener { v, actionId, event ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)

            val i = Intent(this, home_makanan::class.java)
            i.putExtra("search", "vendor")
            i.putExtra("text", search.text.toString())
            startActivity(i)
            true
        }

        categoryHandling()

        getLocation()

//        getEvent()

        getNearest()

        getRecomended()
    }

    fun categoryHandling(){
        seni.setOnClickListener {
            seni.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.enrichment)
            startActivity(i)
        }

        hiburan.setOnClickListener {
            hiburan.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.entertainment)
            startActivity(i)
        }

        akademik.setOnClickListener {
            akademik.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.academic)
            startActivity(i)
        }

        store.setOnClickListener {
            store.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.shop)
            startActivity(i)
        }

        health.setOnClickListener {
            health.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.healthcare)
            startActivity(i)
        }

        makanan.setOnClickListener {
            makanan.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.kidsmeal)
            startActivity(i)
        }

        event.setOnClickListener {
            event.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.event)
            startActivity(i)
        }

        parent_corner.setOnClickListener {
            parent_corner.startAnimation(user_info.animasiButton)
            val i = Intent(this, ListingNew::class.java)
            i.putExtra("category", user_info.parentcorner)
            startActivity(i)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() = runWithPermissions(lokasiGps, lokasiNet) {
        try {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation.addOnSuccessListener { location->

                if (location != null) {
                    user_info.latitude = location.latitude
                    user_info.longitude = location.longitude
                    Log.d("aim", "Lokasi ${location.latitude}, ${location.longitude}")
                }
            }
        }
        catch (e:Exception) { Log.d("aim",e.toString()) }
    }

    private fun getNearest(){
        val errSection = "getNearest"
        shimmerEvent?.startShimmerAnimation()

        val url = user_info.nearest
                .replace("<lat>",user_info.latitude.toString())
                .replace("<lon>",user_info.longitude.toString())

        val rq = object : JsonObjectRequest(Request.Method.GET, url,null, Response.Listener { response ->
            try {
                shimmerEvent?.stopShimmerAnimation()
                Log.d("aim","$errSection: $response")

                val dataArray = response.getJSONArray("data")
                for (i in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(i)
                    val courseArray = dataObj.getJSONArray("data")
                    for (x in 0 until courseArray.length()) {
                        val courseObj = courseArray.getJSONObject(x)

                        val id = courseObj.getString("id_vendor")
                        val type = courseObj.getString("type")
                        val nama = courseObj.getString("nama_vendor")
                        val harga1 = courseObj.getDouble("harga_normal")
                        val harga2 = courseObj.getDouble("price")
                        val img = courseObj.getString("thumbnail_logo")
                        val alamat = courseObj.getString("alamat_vendor")

                        val mItem = vendor_model(id, nama, "0", 0, "", img, "",alamat, harga1,"","","",type,"","",harga2)
                        vendorData.add(mItem)
                    }
                }

                // ============ recycler_nearme ============
                recycler_nearme?.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                val vendorAdapter = home_promotion_adapter()
                recycler_nearme?.adapter = vendorAdapter
                vendorAdapter.setList(vendorData)
                vendorData.clear()

            } catch (e: JSONException) {
                Log.d("aim","$errSection: $e")
            }
        },Response.ErrorListener { response ->
            shimmerEvent?.stopShimmerAnimation()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }

    private fun getEvent() {
        val errSection = "getEvent"
        shimmerEvent?.startShimmerAnimation()
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.onGoingEvent, Response.Listener { response ->
            try {
                shimmerEvent?.stopShimmerAnimation()
                Log.d("aim", "$errSection $response")

                val jsonObject = JSONObject(response)
                val jsonArray = jsonObject.getJSONArray("data")
                user_info.restoreEvent = jsonArray
                for (i in 0 until jsonArray.length()) {
                    val jo = jsonArray.getJSONObject(i)

                    val tipe = jo.getString("type")
                    var mixID = ""

                    when (tipe) {
                        "vendor" -> {
                            mixID = jo.getString("id_vendor")
                        }
                        "course" -> {
                            mixID = jo.getString("slug")
                        }
                        "event" -> {
                            mixID = jo.getString("slug")
                        }
                        "product" -> {
                            mixID = jo.getString("slug")
                        }
                    }

                    val nama = jo.getString("nama")
                    val alamat = jo.getString("alamat_vendor")
                    val img = jo.getString("thumbnail_logo")
                    val category = jo.getString("type_id")

                    val tagsArray = jo.getJSONArray("tags")
                    var tagsName = ""

                    if (tagsArray.length() != 0) {
                        for (y in 0 until tagsArray.length()) {
                            val tagsObject = tagsArray.getJSONObject(y)
                            val tags = tagsObject.optJSONObject("tags")
                            try {
                                tagsName = tags.getString("nama")
                            } catch (e: Exception) {
                                Log.e("aim", "Err array : $e")
                            }
                        }
                    }

                    val tanggalMulai = jo.getString("tanggal_mulai")
                    val tanggalAkhir = jo.getString("tanggal_akhir")
                    val categoryName = jo.getString("vendor_type_nama")

                    val mItem = vendor_model(mixID, nama, "0", 0, "", img, category,alamat, 0.0,tanggalMulai,tanggalAkhir,tipe,categoryName,tagsName,"",0.0)
                    vendorData.add(mItem)
                }

                // safe call views component
                horizontal_cycle?.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                horizontal_cycle?.layoutManager?.scrollToPosition(Int.MAX_VALUE / 2)
                val displayMetrics = this.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                val dpWidth = displayMetrics.widthPixels / displayMetrics.density
                val calculation = (dpWidth - 200) / 2
                val pxWidth = helper.dpToPx(this, calculation.toInt())
                horizontal_cycle?.setPaddingRelative(pxWidth, 0, pxWidth, 0)

                if (horizontal_cycle?.onFlingListener == null) {
                    val snapHelper = LinearSnapHelper()
                    snapHelper.attachToRecyclerView(horizontal_cycle)
                }

                if (jsonArray.length() != 0) {
                    val vendorAdapter = home_event_adapter()
                    horizontal_cycle?.adapter = vendorAdapter
                    vendorAdapter.setList(vendorData)
                    vendorData.clear()
                }

            } catch (e: Exception) {
                Log.e("aim", "event e: $e")
            }
        }, Response.ErrorListener { response ->
            shimmerEvent?.stopShimmerAnimation()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String>()
                map["type"] = "event"
                if (user_info.latitude != 0.0 && user_info.longitude != 0.0) {
                    map["latitude"] = user_info.latitude.toString()
                    map["longitude"] = user_info.longitude.toString()
                }
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    fun getRecomended() {
        if ( user_info.cache_recomended  == null) {
            shimmerRecomended.startShimmerAnimation()
            val rq: RequestQueue = Volley.newRequestQueue(this)
            val sr = object : StringRequest(Request.Method.POST, user_info.recomended, Listener { response ->
                try {
                    shimmerRecomended.stopShimmerAnimation()
                    Log.i("AIM", "Recomended : $response")
                    val jsonObject = JSONObject(response)
                    val dataArray = jsonObject.getJSONArray("data")
                    user_info.cache_recomended = dataArray

                    for (i in 0 until dataArray.length()) {
                        val jo = dataArray.getJSONObject(i)
                        val idVendor = jo.getString("id_vendor")
                        val namaVendor = jo.getString("nama_vendor")
                        var alamatVendor = jo.getString("alamat_vendor")
                        alamatVendor = helper.textHtml(alamatVendor)
                        val img = jo.getString("thumbnail_logo")
                        val slug = jo.getString("slug")
                        val type = jo.getString("type")

                        val mItem = recomended_model(idVendor,namaVendor,alamatVendor,img,slug,type)
                        recomendData.add(mItem)
                    }

                    // ============ recycler_read ============
                    recycler_Recomended.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                    val adapter = home_recomended_adapter()
                    recycler_Recomended.adapter = adapter
                    adapter.setList(recomendData)
                } catch (e: Exception) {
                    Log.e("aim", "recomended e: $e")
                }
            }, Response.ErrorListener { response ->
                val networkResponse = response.networkResponse
                if (networkResponse == null){
                    getRecomended()
                    Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
                }
                else {
                    val code = networkResponse.statusCode
                    if (code == 401) {
                    }
                }
            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = java.util.HashMap<String, String>()
                    headers["Accept"] = "application/json"
//                    headers["Content-Type"] = "application/json"
                    headers["Authorization"] = "Bearer ${user_info.token}"
                    return headers
                }
            }
            sr.retryPolicy = DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )
            rq.add(sr)
        }
        else {
            Log.d("aim","cache must read")
            val dataArray = user_info.cache_recomended
            for (i in 0 until dataArray!!.length()) {
                val jo = dataArray.getJSONObject(i)
                val idVendor = jo.getString("id_vendor")
                val namaVendor = jo.getString("nama_vendor")
                var alamatVendor = jo.getString("alamat_vendor")
                alamatVendor = helper.textHtml(alamatVendor)
                val img = jo.getString("thumbnail_logo")
                val slug = jo.getString("slug")
                val type = jo.getString("type")

                val mItem = recomended_model(idVendor,namaVendor,alamatVendor,img,slug,type)
                recomendData.add(mItem)
            }
            // ============ recycler_read ============
            recycler_Recomended.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
            val adapter = home_recomended_adapter()
            recycler_Recomended.adapter = adapter
            adapter.setList(recomendData)
        }
    }
}


