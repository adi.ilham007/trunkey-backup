package innovacia.co.id.trunkey1

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.provider.SyncStateContract.Helpers.update
import android.view.ViewGroup


class ListingPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val tabNames: ArrayList<String> = ArrayList()
    private val fragments: ArrayList<Fragment> = ArrayList()

    fun add(fragment: Fragment, title: String) {
        tabNames.add(title)
        fragments.add(fragment)
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

//    override fun getItemPosition(`object`: Any): Int {
//        val f = `object` as FragmentListing
//        f.update()
//        return super.getItemPosition(`object`)
//    }

    override fun getPageTitle(position: Int): CharSequence {
        return tabNames[position]
    }
}