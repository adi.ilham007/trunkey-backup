package innovacia.co.id.trunkey1

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.filter.AgeAdapter
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import innovacia.co.id.trunkey1.model.filter.AgeModel
import kotlinx.android.synthetic.main.activity_filter_new.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener



class FilterNew : AppCompatActivity() {

    // Hard code area
    val surabayaID = "3578"
    val jakartaID = "3173"
    val baliID = "5171"
    val makassarID = "7371"

    val mVolleyHelper = VolleyHelper()

    private val ageData = mutableListOf<AgeModel>()
    private var childData = mutableListOf<ChildModel>()

    private var childID = ""
    private var area = ""
    private var min = 0
    private var max = 0
    private var rating = "[]"
    private var show = "all"

    var surabayaState = 0
    var jakartaState = 0
    var baliState = 0
    var makassarState = 0

    var ratingAllState = 0
    var ratingLimaState = 0
    var ratingEmpatState = 0

    var showAllState = 0
    var showCourseState = 0
    var showEventState = 0
    var showProductState = 0
    var showPartnerState = 0

    var forID = 0
    var ageID = 0
    var locationID = 0
    var priceMin = 0
    var priceMax = 0
//    var rating = 0
    var showID = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_new)
        header_name.text = resources.getString(R.string.filter_header)

        restoreKidsFun()

        back_link.setOnClickListener {
            val i = Intent()
            i.putExtra("resetFilter","resetFilter")
            setResult(Activity.RESULT_CANCELED,i)
            finish()
        }

        if (ageData.isNotEmpty()) {
            val mAgeAdapter = AgeAdapter()
            recycler_age.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
            recycler_age.adapter = mAgeAdapter
        } else {
            getCategory()
        }

        Log.d("aim","child data : $childData")
        if (childData.isNotEmpty()) {
            child_lay.visibility = View.VISIBLE

            val childArray = ArrayList<String>()
            childData.forEach {
                childArray.add(it.name)
            }

            val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, childArray)
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_kids!!.adapter = aa

            spinner_kids.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) { }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    childID = childData[position].id
                }
            }

        } else {
            child_lay.visibility = View.GONE
        }

        filterRating()
        filterLocation()
        filterGrade()

        rangeSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            min_value.setText(minValue.toString())
            max_value.setText(maxValue.toString())

            min = minValue.toInt()
            max = maxValue.toInt()

            Log.d("aim","min : $min, max : $$max")
        }

        btn_apply.setOnClickListener {
            val intent = Intent()
            intent.putExtra("filter","filter")
            intent.putExtra("child",childID)
            intent.putExtra("ageSelected",ageSelectedItem())
            intent.putExtra("area",area)
            intent.putExtra("min",min)
            intent.putExtra("max",max)
            intent.putExtra("rating",rating)
            intent.putExtra("show",show)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onBackPressed() {
        val i = Intent()
        i.putExtra("resetSort","resetSort")
        setResult(Activity.RESULT_CANCELED,i)
        finish()
    }

    override fun onStop() {
        super.onStop()
        val queue = VolleySingleton.getInstance(this.applicationContext).requestQueue
        queue.cancelAll {
            true // -> always yes
        }
    }

    private fun restoreKidsFun() {
        childData.clear()
        val dataArray = user_info.restoreKids!!
        for (i in 0 until dataArray.length()) {
            val childObject = dataArray.getJSONObject(i)
            val id = childObject.getString("id")
            val nama = childObject.getString("nama")
            val gender = childObject.getString("gender")
            val birthday = childObject.getString("birthday")
            val foto = childObject.getString("photo")
            val saldo = childObject.getDouble("available_saldo")
            val colorTheme = childObject.getString("color_theme")

            val attendance = childObject.getJSONObject("last_attendance")
            val location = attendance.getString("location")
            val time = attendance.getString("time")
            val attendanceStatus = attendance.getInt("is_check_in")

            val mItem = ChildModel(id,nama,foto,birthday,gender,saldo,colorTheme,location,time,attendanceStatus)
            childData.add(mItem)
        }
    }

    private fun filterGrade() {
        show_all.setOnClickListener {
            show = "all"
            showAllState++
            if (showAllState % 2 == 0) {
                show_all.setBackgroundResource(R.drawable.textview_border_yes)
                show_course.setBackgroundResource(R.drawable.textview_border_no)
                show_event.setBackgroundResource(R.drawable.textview_border_no)
                show_product.setBackgroundResource(R.drawable.textview_border_no)
                show_partner.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                show_all.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        show_course.setOnClickListener {
            show = "course"
            showCourseState++
            if (showCourseState % 2 == 0) {
                show_course.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                show_all.setBackgroundResource(R.drawable.textview_border_no)
                show_course.setBackgroundResource(R.drawable.textview_border_yes)
                show_event.setBackgroundResource(R.drawable.textview_border_no)
                show_product.setBackgroundResource(R.drawable.textview_border_no)
                show_partner.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        show_event.setOnClickListener {
            show = "event"
            showEventState++
            if (showEventState % 2 == 0) {
                show_event.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                show_all.setBackgroundResource(R.drawable.textview_border_no)
                show_course.setBackgroundResource(R.drawable.textview_border_no)
                show_event.setBackgroundResource(R.drawable.textview_border_yes)
                show_product.setBackgroundResource(R.drawable.textview_border_no)
                show_partner.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        show_product.setOnClickListener {
            show = "product"
            showProductState++
            if (showProductState % 2 == 0) {
                show_product.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                show_all.setBackgroundResource(R.drawable.textview_border_no)
                show_course.setBackgroundResource(R.drawable.textview_border_no)
                show_event.setBackgroundResource(R.drawable.textview_border_no)
                show_product.setBackgroundResource(R.drawable.textview_border_yes)
                show_partner.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        show_partner.setOnClickListener {
            show = "vendor"
            showPartnerState++
            if (showPartnerState % 2 == 0) {
                show_partner.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                show_all.setBackgroundResource(R.drawable.textview_border_no)
                show_course.setBackgroundResource(R.drawable.textview_border_no)
                show_event.setBackgroundResource(R.drawable.textview_border_no)
                show_product.setBackgroundResource(R.drawable.textview_border_no)
                show_partner.setBackgroundResource(R.drawable.textview_border_yes)
            }
        }

    }

    private fun filterLocation() {
        area_surabaya.setOnClickListener {
            area = surabayaID
            surabayaState++
            if (surabayaState % 2 == 0) {
                area_surabaya.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                area_surabaya.setBackgroundResource(R.drawable.textview_border_yes)
                area_jakarta.setBackgroundResource(R.drawable.textview_border_no)
                area_bali.setBackgroundResource(R.drawable.textview_border_no)
                area_makassar.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        area_jakarta.setOnClickListener {
            area = jakartaID
            jakartaState++
            if (jakartaState % 2 == 0) {
                area_jakarta.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                area_surabaya.setBackgroundResource(R.drawable.textview_border_no)
                area_jakarta.setBackgroundResource(R.drawable.textview_border_yes)
                area_bali.setBackgroundResource(R.drawable.textview_border_no)
                area_makassar.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        area_bali.setOnClickListener {
            area = baliID
            baliState++
            if (baliState % 2 == 0) {
                area_bali.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                area_surabaya.setBackgroundResource(R.drawable.textview_border_no)
                area_jakarta.setBackgroundResource(R.drawable.textview_border_no)
                area_bali.setBackgroundResource(R.drawable.textview_border_yes)
                area_makassar.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        area_makassar.setOnClickListener {
            area = makassarID
            makassarState++
            if (makassarState % 2 == 0) {
                area_makassar.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                area_surabaya.setBackgroundResource(R.drawable.textview_border_no)
                area_jakarta.setBackgroundResource(R.drawable.textview_border_no)
                area_bali.setBackgroundResource(R.drawable.textview_border_no)
                area_makassar.setBackgroundResource(R.drawable.textview_border_yes)
            }

        }
    }

    private fun filterRating() {
        rating_all.setOnClickListener {
            rating = "[0,1,2,3,4,5]"
            ratingAllState++
            if (ratingAllState % 2 == 0) {
                rating_all.setBackgroundResource(R.drawable.textview_border_yes)
                rating_lima.setBackgroundResource(R.drawable.textview_border_no)
                rating_empat.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                rating_all.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        rating_lima.setOnClickListener {
            rating = "[5]"
            ratingLimaState++
            if (ratingLimaState % 2 == 0) {
                rating_lima.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                rating_all.setBackgroundResource(R.drawable.textview_border_no)
                rating_lima.setBackgroundResource(R.drawable.textview_border_yes)
                rating_empat.setBackgroundResource(R.drawable.textview_border_no)
            }
        }

        rating_empat.setOnClickListener {
            rating = "[4]"
            ratingEmpatState++
            if (ratingEmpatState % 2 == 0) {
                rating_empat.setBackgroundResource(R.drawable.textview_border_no)
            } else {
                rating_all.setBackgroundResource(R.drawable.textview_border_no)
                rating_lima.setBackgroundResource(R.drawable.textview_border_no)
                rating_empat.setBackgroundResource(R.drawable.textview_border_yes)
            }
        }
    }

    private fun getCategory() {
        val errSection = "getCategory"
        val url = "https://trunkey.id/v2/main/getcategories" // user_info_getCategory
        Log.d("aim","child data : $childData")
        val request = object : JsonObjectRequest(Request.Method.POST,url,null,Response.Listener { response ->

            try {
                Log.d("aim","age data : $response")
                val data = response.getJSONObject("data")

                val grade = data.getJSONArray("grade")
                var mItem = AgeModel("all","All","",true)
                ageData.add(mItem)
                for (i in 0 until grade.length()) {
                    val gradeObj = grade.getJSONObject(i)
                    val gradeId = gradeObj.getString("id")
                    val gradeName = gradeObj.getString("nama")
                    val gradeDesc = gradeObj.getString("keterangan")

                    mItem = AgeModel(gradeId,gradeName,gradeDesc,false)
                    ageData.add(mItem)
                }
                val mAgeAdapter = AgeAdapter()
                recycler_age.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                recycler_age.adapter = mAgeAdapter
                mAgeAdapter.setList(ageData)

            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }

        },Response.ErrorListener { response ->
//            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }

    // get selected data from model
    fun ageSelectedItem():String {
        val filterResult = ageData.filter { it.isSelected }
        filterResult.sortedBy { it.isSelected }
        val selectedItem = ArrayList<String>()
        filterResult.forEach {
            selectedItem.add(it.id)
        }

        if (selectedItem.contains("all")) {
            selectedItem.clear()
        }
        return selectedItem.toString()
    }
}
