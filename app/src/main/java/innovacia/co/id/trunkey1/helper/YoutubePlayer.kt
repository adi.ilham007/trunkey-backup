package innovacia.co.id.trunkey1.helper

import android.os.Bundle
import android.util.Log
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import innovacia.co.id.trunkey1.R
import kotlinx.android.synthetic.main.activity_youtube_player.*


class YoutubePlayer : YouTubeBaseActivity() {

    private var videoID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE
        setContentView(R.layout.activity_youtube_player)

        videoID = intent.getStringExtra("idVideo")
        initializeYoutubePlayer()
    }

    private fun initializeYoutubePlayer() {
        val youtubeAPI: String = getString(R.string.api_youtube)
        youtube_player_view!!.initialize(youtubeAPI, object : YouTubePlayer.OnInitializedListener {

            override fun onInitializationSuccess(provider: YouTubePlayer.Provider, youTubePlayer: YouTubePlayer,
                                                 wasRestored: Boolean) {
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT)
                    youTubePlayer.loadVideo(videoID)
                    youTubePlayer.setFullscreen(true)
                    youTubePlayer.play()
            }

            override fun onInitializationFailure(arg0: YouTubePlayer.Provider, arg1: YouTubeInitializationResult) {
                //print or show error if initialization failed
                Log.e("aim", "Youtube Player View initialization failed")
            }
        })
    }
}
