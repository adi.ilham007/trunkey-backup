package innovacia.co.id.trunkey1.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.ListBankModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_bank.view.*
import android.content.Intent
import android.util.Log
import innovacia.co.id.trunkey1.TopUp_Instruction


class ListBankAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var bankModel = mutableListOf<ListBankModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return bankModelViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_bank, parent, false))
    }
    override fun getItemCount(): Int = bankModel.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as bankModelViewHolder

        viewHolder.bindView(bankModel[position])
        user_info.animasiButton.duration = 500



        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val kode = bankModel[position].kodeVA
            val nama_bank = bankModel[position].namabank
            val topUp_Instruction = bankModel[position].topup_tutorial

            val i = Intent(view.context, TopUp_Instruction::class.java)
            i.putExtra("kodeVA", kode)
            i.putExtra("nama", nama_bank )
            i.putExtra("instruction", topUp_Instruction)
            Log.d("aim","instruksi : $kode,$nama_bank, $topUp_Instruction")
            view.context.startActivity(i)

        }
    }
    fun setList(listOfVendor: List<ListBankModel>) {
        this.bankModel = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<ListBankModel>) {
        this.bankModel.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class bankModelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: ListBankModel) {
            itemView.nama_bank.text    = vendorModel.namabank
            itemView.virtual.text       = vendorModel.kodeVA

            Glide.with(itemView.context).load(vendorModel.imgbank).into(itemView.image_bank)

//            Glide
//                    .with(itemView.context)
//                    .load(vendorModel.file_logo)
//                    .apply(RequestOptions().override(100, 100))
//                    .into(itemView.file_logo)
//
//            if (vendorModel.id_vendor == "more"){
//                itemView.file_logo.setBackgroundResource(R.mipmap.load_more)
//                itemView.icon.visibility = View.GONE
//            }
        }
    }

}