package innovacia.co.id.trunkey1

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.support.v7.app.AlertDialog
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_forum_post.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException

class ForumPost : AppCompatActivity() {

    private val imageRequest = 1
    var imageName = ""
    var image = ""
    var text = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forum_post)

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        addImage.setOnClickListener {
            addImage.startAnimation(user_info.animasiButton)
            val intent = Intent()
                    .setType("image/*")
                    .setAction(Intent.ACTION_GET_CONTENT)

            startActivityForResult(Intent.createChooser(intent, "Select a Photo"), imageRequest)
        }

        post.setOnClickListener {
            post.startAnimation(user_info.animasiButton)
            if (contentText.length() > 10) {
                postingForum()
            } else {
                Toast.makeText(this,"At least 10 characters",Toast.LENGTH_LONG).show()
            }
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == imageRequest && resultCode == RESULT_OK && data != null && data.data != null) {
            val filePath = data.data
            imageName = getFileName(filePath)
            val fileType = imageName.split(".")
            val lastIndex = fileType.lastIndex
            val extension = fileType[lastIndex].toLowerCase()
            Log.d("aim","nama file : $imageName, extension : $extension")

            if (extension == "jpg" || extension == "jpeg") {
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
//                Bitmap lastBitmap = null;
//                lastBitmap = bitmap;

                    //encoding image to string
                    image = getStringImage(bitmap)
                    contentImage.visibility = View.VISIBLE
                    contentImage.setImageBitmap(bitmap)
                    text = contentText.text.toString()
                    Log.d("aim", "body post : $text")

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(this,"Please choose picture with .jpg or .jpeg extension",Toast.LENGTH_LONG).show()
            }
        }
    }

    fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    fun getStringImage(bmp: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }

    private fun postingForum() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.forum_post, Response.Listener { response ->
            Log.d("aim","success : $response")
            dialog.dismiss()
            Toast.makeText(this, "Posting success", Toast.LENGTH_LONG).show()

            val i = Intent(this, Forum::class.java)
            startActivity(i)


        }, Response.ErrorListener { response ->
            dialog.dismiss()
            Log.d("aim", "Err code : $response")
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this, R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String,String>()
//                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                val image64 = "data:image/png;base64,$image"
                map["name"] = ""
                map["description"] = contentText.text.toString()
                if (image != "") {
                    map["link_foto"] = image64
                }
                Log.d("aim","forum post : $map")
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }
}
