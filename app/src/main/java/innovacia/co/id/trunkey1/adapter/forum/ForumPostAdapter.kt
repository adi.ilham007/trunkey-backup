package innovacia.co.id.trunkey1.adapter.forum

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.ForumModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_forum_post.view.*

class ForumPostAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var forumList = mutableListOf<ForumModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return forumListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_forum_post, parent, false))
    }
    override fun getItemCount(): Int = forumList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as forumListViewHolder

        viewHolder.bindView(forumList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

//            var id_vendor = forumList[position].id_vendor
//            var category = forumList[position].category
//
//            if (id_vendor == "more") {
//                val i = Intent(view.context, home_makanan::class.java)
//                i.putExtra("category", category)
//                view.context.startActivity(i)
//            }
//            else if (id_vendor != "more"){
//                val i = Intent(view.context, detail_vendor::class.java)
//                i.putExtra("id", id_vendor)
//                i.putExtra("category", category)
//                view.context.startActivity(i)
//            }

        }
    }

    fun setList(listOfVendor: List<ForumModel>) {
        this.forumList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<ForumModel>) {
        this.forumList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class forumListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(model: ForumModel) {

//            when {
//                model.is_open == "Close" -> itemView.is_open.setBackgroundResource(R.drawable.text_shape_blue)
//                model.is_open == "Open" -> itemView.is_open.setBackgroundResource(R.drawable.text_shape_orange)
//                model.is_open == "Unknown" -> itemView.is_open.visibility = View.GONE
//            }
//
//            when {
//                model.rating == 1 -> itemView.rating.setBackgroundResource(R.drawable.bintang2)
//                model.rating == 2 -> itemView.rating.setBackgroundResource(R.drawable.bintang2)
//                model.rating == 3 -> itemView.rating.setBackgroundResource(R.drawable.bintang3)
//                model.rating == 4 -> itemView.rating.setBackgroundResource(R.drawable.bintang4)
//                model.rating == 5 -> itemView.rating.setBackgroundResource(R.drawable.bintang5)
//            }

//            var id:String,
//            var userName:String,
//            var userPhoto:String,
//            var desc:String,
//            var comment:String,
//            var like:String,
//            var photo:String

            itemView.nama.text      = model.userName
            itemView.contentText.text  = model.contentDesc
            itemView.comment.text      = model.comment
            itemView.like.text      = model.like
            Glide.with(itemView.context).load(model.userPhoto).into(itemView.photo)
            Glide.with(itemView.context).load(model.contentPhoto).into(itemView.contentImage)

        }
    }
}