package innovacia.co.id.trunkey1.model

data class ChildTransHistory_model(
    val faktur          :String,
    val merchant        :String,
    val orderDate       :String,
    val product         :String,
    val harga           :String,
    val total           :String
)