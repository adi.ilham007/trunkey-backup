package innovacia.co.id.trunkey1.database.schedule

import android.app.Application
import android.arch.lifecycle.LiveData

class ScheduleRespository constructor(application: Application){

    lateinit var dao         : ScheduleDao
    lateinit var data        : LiveData<List<ScheduleEntity>>

    init {
        val database = ScheduleDatabase.getInstance(application)
        if (database != null) {
            dao = database.dao()
            data = dao.getAll()
        }
    }

    fun getAll() : LiveData<List<ScheduleEntity>> {
        return data
    }

    fun getBydate(date: String) : LiveData<List<ScheduleEntity>> {
        return dao.getBydate(date)
    }

    fun getScheduleInMonth(yearAndMonth: String) : List<ScheduleEntity> {
        return dao.getScheduleInMonth(yearAndMonth)
    }

//    fun getByIdMerchant(id: Int) : List<ScheduleEntity> {
//        return dao.getByIdMerchant(id)
//    }

    fun add(entity: ScheduleEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.insert(entity)
            }
        }
        thread.start()
    }

    fun update(entity: ScheduleEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.update(entity)
            }
        }
        thread.start()
    }

    fun deleteEntry(entity: ScheduleEntity){
        val thread = object : Thread() {
            override fun run() {
                dao.delete(entity)
            }
        }
        thread.start()
    }

}
