package innovacia.co.id.trunkey1

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_add_child_manual.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.DecimalFormat
import java.util.*

class AddChildManual : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val gender = arrayOf("Boy", "Girl")
    private var genderString = ""
    private var childPin = ""
    private val PICK_IMAGE_REQUEST = 1111
    var imageName = ""
    var imageData = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_child_manual)
        header_name?.text = resources.getString(R.string.add_child_header)

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

//        image_lay.setOnClickListener {
//            image_lay.startAnimation(user_info.animasiButton)
//            val intent = Intent()
//                    .setType("image/*")
//                    .setAction(Intent.ACTION_GET_CONTENT)
//
//            startActivityForResult(Intent.createChooser(intent, "Select a Photo"), PICK_IMAGE_REQUEST)
//        }

        AddChildButton.setOnClickListener {
            addChild()
            //Toast.makeText(this,"Data anak berhasil ditambahkan",Toast.LENGTH_SHORT).show()
        }

        child_gender_spinner!!.onItemSelectedListener = this
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, gender)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        child_gender_spinner!!.adapter = aa

    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        Log.d("aim","Selected : "+gender[position])
        when {
            gender[position] == "Boy" -> {
                genderString = "male"
            }
            gender[position] == "Girl" -> {
                genderString = "female"
            }
        }

    }
    override fun onNothingSelected(arg0: AdapterView<*>) {

    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
//            val filePath = data.data
//            imageName = getFileName(filePath)
//            val fileType = imageName.split(".")
//            val lastIndex = fileType.lastIndex
//            val extension = fileType[lastIndex].toLowerCase()
//            Log.d("aim","nama file : $imageName, extension : $extension")
//
//            if (extension == "jpg" || extension == "jpeg") {
//                try {
//                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
//                    //Bitmap lastBitmap = null;
//                    //lastBitmap = bitmap;
//
//                    //encoding image to string
//                    imageData = getStringImage(bitmap)
////                    sendImage(imageData)
//
//                } catch (e:IOException) {
//                    e.printStackTrace()
//                }
//            } else {
//                Toast.makeText(this,"Please choose picture with .jpg or .jpeg extension",Toast.LENGTH_LONG).show()
//            }
//        }
//    }
//
//    fun getFileName(uri: Uri): String {
//        var result: String? = null
//        if (uri.scheme == "content") {
//            val cursor = contentResolver.query(uri, null, null, null, null)
//            try {
//                if (cursor != null && cursor.moveToFirst()) {
//                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
//                }
//            } finally {
//                cursor!!.close()
//            }
//        }
//        if (result == null) {
//            result = uri.path
//            val cut = result!!.lastIndexOf('/')
//            if (cut != -1) {
//                result = result.substring(cut + 1)
//            }
//        }
//        return result
//    }
//
//    fun getStringImage(bmp: Bitmap): String {
//        val baos = ByteArrayOutputStream()
//        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
//        val imageBytes = baos.toByteArray()
//        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
//    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun DatePicker(view: View) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener {
            view, year, monthOfYear, dayOfMonth ->

            val f = DecimalFormat("00")
            val tanggal = (f.format(dayOfMonth))
            val bulan = (f.format(monthOfYear+1))

            val allDate = "$year-$bulan-$tanggal"
            child_birthday_edittext.setText(allDate)
        }, year, month, day)
        dpd.show()
    }

    fun addChild(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.addChild, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim",response)
            val jsonObject = JSONObject(response)

            Toast.makeText(this,"Done",Toast.LENGTH_LONG).show()
            val i = Intent(this, HomeBaru::class.java)
            startActivity(i)
//            confirmDialog(id,nama)

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["nama"] = child_name_edittext.text.toString()
                map["birthday"] = child_birthday_edittext.text.toString()
                map["gender"] = genderString
                Log.d("aim",map.toString())
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }


}
