package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.list_home1.view.*
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.detail_vendor
import innovacia.co.id.trunkey1.home_makanan
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.vendor_model


class home_promotion_adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<vendor_model>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_home1, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as VendorListViewHolder

        holder.bindView(vendorList[position])

        holder.itemView.setOnClickListener { view ->
            holder.itemView.startAnimation(user_info.animasiButton)

            val id = vendorList[position].id_vendor
            val category = vendorList[position].category

            val i = Intent(view.context, detail_vendor::class.java)
            i.putExtra("id", id)
            i.putExtra("category", category)
            view.context.startActivity(i)

        }
    }
    fun setList(listOfVendor: List<vendor_model>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<vendor_model>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class VendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: vendor_model) {
            val helper = user_info()

            itemView.name_value.text = vendorModel.nama
            itemView.category_value.text = vendorModel.category_name

//            if (vendorModel.harga_normal != 0.0) {
//                itemView.harga1.visibility = View.VISIBLE
//                itemView.harga1.text = helper.bank(vendorModel.harga_normal)
//            }
//
//            itemView.harga2.text = helper.bank(vendorModel.harga_discount)

            Glide.with(itemView.context)
                    .load(vendorModel.file_logo)
                    .into(itemView.file_logo)
        }
    }
}