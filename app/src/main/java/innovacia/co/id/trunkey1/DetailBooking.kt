package innovacia.co.id.trunkey1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_detail_history.*
import org.json.JSONException
import org.json.JSONObject

class DetailBooking : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_booking)

        val idIntent:String = intent.getStringExtra("id")
        getDetail(idIntent)

        back_link_history.setOnClickListener{
            onBackPressed()
        }
    }

    fun getDetail(idIntent : String) {
        shimmerHistory.startShimmerAnimation()
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.profile_transaction+"/"+idIntent, Response.Listener { response ->
            try {
                shimmerHistory.stopShimmerAnimation()

                var vendorName = ""
                var transaksi = ""
                var jumlah = ""
                var cashBack = ""
                var tipe_pembelian = ""
                var placename = ""
                var transReff = ""
                var tanggal = ""
                var separator = ""
                var debet = "0"
//                var kredit = ""


                val jsonObject = JSONObject(response)
                val dataObject = jsonObject.optJSONObject("data")
                transReff = dataObject.getString("trans_ref")
                Log.d("aim","trans reff"+transReff)

                if(dataObject.getString("debet")!="0"){
                    debet = dataObject.getString("debet")
//                val kredit = jsonObject.getString("kredit")

                }
//                Log.d("aim","debet"+dataObject.getString("debet"))


                val transaksiObject = dataObject.optJSONObject("transactions")
                transaksi = transaksiObject.getString("nama")
                Log.d("aim","transaksi"+transaksi)

                jumlah = transaksiObject.getString("jumlah")
                Log.d("aim","jumlah"+jumlah)
                var status_text = transaksiObject.getString("status_text")


                if(transaksiObject.optJSONObject("detail")!=null){
                    val detailObject = transaksiObject.optJSONObject("detail")
                    cashBack = detailObject.getString("total_cashback")
                    Log.d("aim","cashback "+cashBack)
                    tanggal = detailObject.getString("tanggal")
                    Log.d("aim","tanggal "+tanggal)

                    val vendorObject = detailObject .optJSONObject("vendor")
                    vendorName = vendorObject.getString("nama")
                    Log.d("aim","Vendor name"+vendorName)

                    val imageVendor = vendorObject.getString("photo_path")

                    Glide.with(this).load(imageVendor).into(vendorImage) //Image


                    val detailArray = detailObject.getJSONArray("detail")
                    for(i in 0 until detailArray.length() ){
                        val detailObject = detailArray.optJSONObject(i)
                        tipe_pembelian = detailObject.getString("type_product")
                        Log.d("aim","tipe_pembelian"+tipe_pembelian)
                        if(detailObject.optJSONObject("detail_product")!=null){
                            val detail_product_Object = detailObject.optJSONObject("detail_product")
                            if(detail_product_Object.optJSONObject("event")!=null){
                                val event_Object = detail_product_Object.optJSONObject("event")
                                placename = event_Object.getString("place_nama")
                                Log.d("aim","placename"+placename)


                            }

                        }

                    }

                }







//
//                // ============ recycler_nearme ============
//                recycler_transaction.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
//                val walletAdapter = WalletAdaper()
//                recycler_transaction.adapter = walletAdapter
//                walletAdapter.setList(walletData)
//                walletData.clear()


                vendor_name_view.text = vendorName
                transaksi_view.text = transaksi
                if(debet != "0"){
                    separator = "+"
                }else{
                    separator = "-"
                }
                jumlah_view.text = separator+" "+jumlah

                if(cashBack == "null" ||cashBack == "0"){
                    cashBack_view.visibility = View.GONE
                }else{
                    cashBack_view.text = "CashBack "+cashBack
                }

                if(tipe_pembelian == ""){
                    tipe_pembelian_view.visibility =  View.GONE
                    barier_pembelian.visibility =  View.GONE
                    lbl_tipePembelian.visibility = View.GONE
                }else{
                    tipe_pembelian_view.text = tipe_pembelian
                }

                if(placename == ""){
                    location_view.visibility =  View.GONE
                    barier_location.visibility =  View.GONE
                    lbl_location.visibility = View.GONE
                }else{
                    location_view.text = placename
                }

                if(status_text == ""){
                    status_view.visibility =  View.GONE
                    barier_status.visibility =  View.GONE
                    lbl_status.visibility = View.GONE
                }else{
                    status_view.text = status_text
                }


                if(transReff == ""){
                    reff_view.visibility =  View.GONE
                    barier_transaksiReff.visibility =  View.GONE
                    lbl_transaksiReff.visibility = View.GONE
                }else{
                    reff_view.text = transReff
                }

                if(tanggal == ""){
                    tanggal_view.visibility =  View.GONE
                    barier_tanggal.visibility =  View.GONE
                    lbl_tanggal.visibility = View.GONE
                }else{
                    tanggal_view.text = tanggal
                }

//                status_view.text = status_text
//                reff_view.text = transReff
//                tanggal_view.text = tanggal






            }catch (e: Exception) {
                Log.e("aim", "Detail History e: $e")
            }
        }, Response.ErrorListener { response ->
            shimmerHistory.stopShimmerAnimation()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String,String> {
                val headers = HashMap<String,String>()

                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

//            override fun getParams(): MutableMap<String,String> {
//                val map = HashMap<String, String>()
//                return map
//            }


        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }
}
