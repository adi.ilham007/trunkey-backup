package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ScheduleModel
import kotlinx.android.synthetic.main.list_schedule.view.*
import android.R.attr.data
import android.text.method.TextKeyListener.clear



class ScheduleAdapter  : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var scheduleData = mutableListOf<ScheduleModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ScheduleDataViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_schedule, parent, false))
    }
    override fun getItemCount(): Int = scheduleData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as ScheduleDataViewHolder
        viewHolder.bindView(scheduleData[position])

//        viewHolder.itemView.left_btn.setOnClickListener { view ->
//            viewHolder.itemView.left_btn.startAnimation(user_info.animasiButton)
//            val goto = position-1
//            if (goto >= 0) {
//                val intent = Intent("recycler")
//                intent.putExtra("position", goto)
//                LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
//            }
//        }

//        viewHolder.itemView.right_btn.setOnClickListener { view ->
//            viewHolder.itemView.right_btn.startAnimation(user_info.animasiButton)
//            val goto = position+1
//            if (goto <= scheduleData.size) {
//                val intent = Intent("recycler")
//                intent.putExtra("position", position + 1)
//                LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
//            }
//        }

    }
    fun setList(listOfReview: List<ScheduleModel>) {
        this.scheduleData = listOfReview.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfReview: List<ScheduleModel>) {
        this.scheduleData.addAll(listOfReview)
        notifyDataSetChanged()
    }
    fun reset() {
        scheduleData.removeAll(scheduleData)
        notifyDataSetChanged()
    }

    class ScheduleDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(mScheduleModel: ScheduleModel) {
            itemView.tx_nama.text = mScheduleModel.subject
            itemView.tx_waktu.text = mScheduleModel.time

            itemView.logo_huruf?.text = mScheduleModel.subject[0].toString().capitalize()
        }
    }
}
