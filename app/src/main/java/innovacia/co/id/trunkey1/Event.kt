package innovacia.co.id.trunkey1

import android.Manifest
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.text.Html
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.adapter.AdapterGallery
import innovacia.co.id.trunkey1.adapter.TiketAdapter2
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.GalleryModel
import innovacia.co.id.trunkey1.model.TiketModel
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_event.swipe_container
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.util.HashMap

@Suppress("DEPRECATION")
class Event : AppCompatActivity() {

    val helper = user_info()

    private var writeStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private val phone_permission = Manifest.permission.CALL_PHONE

    private val listGallery = mutableListOf<GalleryModel>()
    private val tiketModel = mutableListOf<TiketModel>()

    var idArray = ArrayList<Int>()
    var seatArray = ArrayList<Int>()
    var hargaArray = ArrayList<Double>()
    val cashBackArray = ArrayList<Double>()

    private var phone = ""
    private var file = ""
    private var idVendor = 0
    private var mNamaVendor = ""
    private var mPlace = ""
    private var latitude = 0.0
    private var longitude = 0.0
    private var slug = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)
        header_name?.text = "Event Page"

        generateView()

        swipe_container.setOnRefreshListener {
            getEvent()
        }

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        vendor_photo.setOnClickListener {
            vendor_photo.startAnimation(user_info.animasiButton)
            val i = Intent(this, detail_vendor::class.java)
            i.putExtra("id", idVendor.toString())
            startActivity(i)
        }

//        download.setOnClickListener {
//            download.startAnimation(user_info.animasiButton)
//            Glide.with(this)
//                    .asBitmap()
//                    .load(file)
//                    .into(object : SimpleTarget<Bitmap>(){
//                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
//                            poster_download.setImageBitmap(resource)
//                            saveImage(resource)
//                        }
//                    })
//        }

        direction.setOnClickListener {
            direction.startAnimation(user_info.animasiButton)
            if (latitude != 0.0 && longitude != 0.0) {
                val i = Intent(this, MapsActivity::class.java)
                i.putExtra("nama", "test")
                i.putExtra("lat", latitude)
                i.putExtra("lon", longitude)
                startActivity(i)
            } else {
                Toast.makeText(this, "Location not available", Toast.LENGTH_LONG).show()
            }
        }

        share.setOnClickListener {
            val message = "Hii.., We have key for successful parenting https://play.google.com/store/apps/details?id=innovacia.co.id.com.innovacia.trunkey1"

            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT,message)
            intent.type = "text/plain"
            try {
                startActivity(Intent.createChooser(intent, "Share to :"))
            }catch (e: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "There are no share clients installed.", Toast.LENGTH_LONG).show()
            }
        }

        btn_wishlist.setOnClickListener {

        }


        btn_buy.setOnClickListener {
            btn_buy.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                    val i = Intent(this, list_tiket_detail::class.java)
                    i.putExtra("qty", "qty")
                    i.putExtra("slug", slug)
                    i.putExtra("idArray", idArray)
                    i.putExtra("seatArray", seatArray)
                    i.putExtra("hargaArray", hargaArray)
                    i.putExtra("totalHarga", hargaArray.sum())
                    i.putExtra("totalCashBack", cashBackArray.sum())
                    Log.d("aim", "kirim harga: $hargaArray")
                    Log.d("aim", "kirim id: $idArray")
                    startActivity(i)
            } else {
                if (!user_info.loginStatus) {
                    val i = Intent(this, login::class.java)
                    i.putExtra("back", "event")
                    startActivity(i)
                } else if (!user_info.walletStatus) {
                    activatedWalletDialog()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        TiketAdapter2.TYPE_OF_VIEW = 0
    }

    override fun onDestroy() {
        super.onDestroy()
        TiketAdapter2.TYPE_OF_VIEW = 0
    }

    // tambahan
//    override fun onResume() {
//        super.onResume()
//        TiketAdapter.TYPE_OF_VIEW = 0
//
//        val mtiketAdapter = TiketAdapter()
//        mtiketAdapter.reset()
//    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadCastReceiver, IntentFilter("qty"))
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                "qty" -> {
                    val idTiket = intent.getIntExtra("id",0)
                    val seatTiket = intent.getIntExtra("seat",0)
                    val hargaTiket = intent.getDoubleExtra("harga",0.0)
                    val cashBack = intent.getDoubleExtra("cashBack",0.0)

//                    recycler_tiket.findViewHolderForAdapterPosition(index)!!.itemView.Amount.text.toString()

                    if (idArray.contains(idTiket)){
                        val a = idArray.indexOf(idTiket)
                        if (seatTiket != 0) {
                            idArray[a] = idTiket
                            seatArray[a] = seatTiket
                            hargaArray[a] = hargaTiket
                            cashBackArray[a] = cashBack
                        } else {
                            idArray.removeAt(a)
                            seatArray.removeAt(a)
                            hargaArray.removeAt(a)
                            cashBackArray.removeAt(a)
                        }
                    } else {
                        idArray.add(idTiket)
                        seatArray.add(seatTiket)
                        hargaArray.add(hargaTiket)
                        cashBackArray.add(cashBack)
                    }

                    Log.d("aim","all id: $idArray")
                    Log.d("aim","all seat: $seatArray")
                    Log.d("aim","all harga: $hargaArray")
                    Log.d("aim","all cashBack: $cashBackArray")

//                    harga1.text = helper.bank(hargaArray.sum())
//                    cashback_value.text = helper.bank(cashBackArray.sum())


//                    Log.d("aim","index: $index, id: $id, harga: $harga, tiket: $indexSum")

                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(broadCastReceiver)
    }

    fun phone_call() = runWithPermissions(phone_permission) {
        Log.d("aimo","ini Event")
        try {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:+$phone")
            startActivity(intent)
        }catch (e: Exception) {
//            Log.e("aim ", "phone e: $e")
            Toast.makeText(this,"Data tidak tersedia",Toast.LENGTH_LONG).show()
        }


        //call lama
//        try {
//            val intent = Intent(Intent.ACTION_CALL)
//            intent.data = Uri.parse("tel:$phone")
//            startActivity(intent)
//        }catch (e: Exception) {
//            Log.e("aim", "phone e: $e")
////            Toast.makeText(this,"Data tidak tersedia",Toast.LENGTH_LONG).show()
//        }
    }

    private fun generateView() {
        if (intent.hasExtra("data") && intent.getStringExtra("data") != null) {
            // From notification / FCM
            slug = intent.getStringExtra("data")
            getEvent()
        } else if (intent.hasExtra("slug") && intent.getStringExtra("slug") != null) {
            // From home
            slug = intent.getStringExtra("slug")
            val category = intent.getStringExtra("category")
            Log.d("aim","slug : $slug")
            Log.d("aim","category : $category")
            getEvent()
        }
        Log.d("aim",slug)
    }

    private fun getEvent(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        Log.d("aim","url : ${user_info.event_api}$slug")

            val rq: RequestQueue = Volley.newRequestQueue(this)
            val sr = object : StringRequest(Request.Method.POST, user_info.event_api+slug,Response.Listener { response ->
                //========================================================================================= data from server
                dialog.dismiss()
                swipe_container.isRefreshing = false

                try {
                    val jsonObject = JSONObject(response)
                    val data = jsonObject.getJSONObject("data")

//                    val nullJson1: JSONObject? = jsonObject.optJSONObject("1")
//                    val nullJson2: JSONObject? = nullJson1?.optJSONObject("2")
//                    val nullString = nullJson2?.optString("3","iki kosong goblok")
//                    Log.d("aim","object1: $nullJson1, object2: $nullJson2, turunan: $nullString")

                    // Data Event
                    val id = data.getInt("id")
                    val banner = data.getString("wide_logo")
                    file = data.getString("thumbnail_logo")

                    Glide.with(this).load(banner)
                            .apply(RequestOptions.skipMemoryCacheOf(true)) //
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(image)

                    Glide.with(this).load(file)
                            .apply(RequestOptions.skipMemoryCacheOf(true)) //
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(poster_download)

                    event_name.text = data.getString("nama")

                    if(data.getString("place_nama")!="null"||data.getString("place_nama")!=""||data.getString("place_nama")!=null){
                        mPlace = data.getString("place_nama")
                        event_location.text = mPlace
                    }

                    latitude = data.getDouble("place_latitude")
                    longitude = data.getDouble("place_longitude")
                    val objVendor = data.getJSONObject("vendor")
                    val objType = objVendor.getJSONObject("type")
                    val typeName = objType.getString("nama")

                    val wishlist = data.optInt("is_wishlist")
                    Log.d("aim","wish list: $wishlist")
                    if (wishlist == 1) {
                        btn_wishlist.setBackgroundResource(R.drawable.ic_like_true)
                    } else {
                        btn_wishlist.setBackgroundResource(R.drawable.ic_like_false)
                    }

                    val timeStart = data.getString("tanggal_mulai").split(" ").toTypedArray()
                    val timeEnd = data.getString("tanggal_akhir").split(" ").toTypedArray()
                    val minuteHourStart = timeStart[1].split(":").toTypedArray()
                    val minuteHourEnd = timeEnd[1].split(":").toTypedArray()
                    event_time?.text = "${minuteHourStart[0]}:${minuteHourStart[1]} - ${minuteHourEnd[0]}:${minuteHourEnd[1]}"
                    event_category.text = typeName

                    // Only Date
                    val dateStart = timeStart[0].split("-").toTypedArray()
                    val dateEnd = timeEnd[0].split("-").toTypedArray()

                    if(dateStart[2] == dateEnd[2]){
                        tanggal_view.text = dateStart[2]
                    }else{
                        tanggal_view.text = dateStart[2]+ " - " + dateEnd[2]
                    }


                    // Month
                    var bulanAwalString = ""
                    var bulanAkhirString = ""
                    val bulanAwalDigit = dateStart[1]
                    val bulanAkhirDigit = dateEnd[1]
                    when (bulanAwalDigit) {
                        "01" -> bulanAwalString = "Jan"
                        "02" -> bulanAwalString = "Feb"
                        "03" -> bulanAwalString = "March"
                        "04" -> bulanAwalString = "Apr"
                        "05" -> bulanAwalString = "May"
                        "06" -> bulanAwalString = "Jun"
                        "07" -> bulanAwalString = "Jul"
                        "08" -> bulanAwalString = "Aug"
                        "09" -> bulanAwalString = "Sep"
                        "10" -> bulanAwalString = "Oct"
                        "11" -> bulanAwalString = "Nov"
                        "12" -> bulanAwalString = "Des"
                    }
                    when (bulanAkhirDigit) {
                        "01" -> bulanAkhirString = "Jan"
                        "02" -> bulanAkhirString = "Feb"
                        "03" -> bulanAkhirString = "March"
                        "04" -> bulanAkhirString = "Apr"
                        "05" -> bulanAkhirString = "May"
                        "06" -> bulanAkhirString = "Jun"
                        "07" -> bulanAkhirString = "Jul"
                        "08" -> bulanAkhirString = "Aug"
                        "09" -> bulanAkhirString = "Sep"
                        "10" -> bulanAkhirString = "Oct"
                        "11" -> bulanAkhirString = "Nov"
                        "12" -> bulanAkhirString = "Des"
                    }
                    if(bulanAwalString != bulanAkhirString){
                        bulan.text = "$bulanAwalString - $bulanAkhirString"
                    }else{
                        bulan.text = bulanAwalString
    //                    bulan.text = bulanAwalString
                    }
    //                bulan.text = bulanAwalString

                    val aboutHtml = data.getString("keterangan")
//                    about?.text = helper.textHtml_desc(aboutHtml)
//                    about.loadDataWithBaseURL("",aboutHtml,"text/html","UTF-8","")
//                    val webSettings = about.getSettings()
//                    webSettings.setDefaultFontSize(helper.dpToPx(this,7))
//                    webSettings.fixedFontFamily = "@font/avenir_roman"
                    about.text = Html.fromHtml(aboutHtml)

                    if (!data.isNull("photos")) {
                        val galleryArray = data.getJSONArray("photos")
                        if (galleryArray.length() > 0) {
                            for (i in 0 until galleryArray.length()) {
                                val jo = galleryArray.getJSONObject(i)
                                val photoId = jo.getString("id")
                                val img = jo.getString("photo_path")

                                val mItem = GalleryModel(photoId,img)
                                listGallery.add(mItem)
                            }
                            // ============ recycler_course ============
                            recycler_gallery.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL,false)
                            val galleryAdapter = AdapterGallery()
                            recycler_gallery.adapter = galleryAdapter
                            galleryAdapter.setList(listGallery)
                        } else {
                            gallery_lay?.visibility = View.GONE
                        }
                    }

//                    harga.text = "IDR 0"

    //                rating.text = data.getString("")

                    // Wallet Status
                    val walletObject = data.optJSONObject("is_wallet")
                    val walletStatus = walletObject.getInt("status")
                    if (walletStatus != 0) {
                        user_info.walletStatus = true
                        user_info.wallet = walletObject.getDouble("balance")
                        Log.d("aim","wallet ${user_info.walletStatus}, ${user_info.wallet}")
                    } else {
                        user_info.walletStatus = false
                        user_info.wallet = 0.0
                        Log.d("aim","wallet ${user_info.walletStatus}, ${user_info.wallet}")
                    }

                    // Data Vendor
                    val vendor = data.getJSONObject("vendor")
                    val logoVendor = vendor.getString("photo_path")

                    Glide.with(this).load(logoVendor)
                            .apply(RequestOptions.skipMemoryCacheOf(true)) //
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(vendor_photo)

                    idVendor = data.getInt("id_eca_vendors")
                    mNamaVendor = vendor.getString("nama")
                    vendor_name.text = mNamaVendor
                    vendor_addrs.text = helper.textHtml(vendor.getString("alamat"))

                    if (!data.isNull("tickets")) {
                        val tiketArray = data.getJSONArray("tickets")
                        if (tiketArray.length() != 0) {
                            for (i in 0 until tiketArray.length()) {
                                val jo = tiketArray.getJSONObject(i)
                                val id = jo.getInt("id")
                                val nama = jo.getString("nama")
                                val ket = jo.getString("keterangan")


                                val harga = jo.getDouble("harga_diskon")
                                val kapasitas = jo.getInt("capacity")
                                val partisipan = jo.getInt("participant")
                                val seat = kapasitas - partisipan

                                val maxCashBack = jo.getDouble("max_cashback")
                                val cashBack = jo.getDouble("nominal_cashback")
                                Log.d("aim","cash back : $cashBack, max : $maxCashBack")

    //                            var ket = jo.getString("keterangan")
    //                            ket = helper.textHtml(ket)

                                val lokasi = data.getString("place_nama")
                                val dateTime = data.getString("tanggal_mulai").split(" ")

                                val mItem = TiketModel(id, nama, ket, 0.0,harga,cashBack,maxCashBack,"",0,seat, dateTime[0],dateTime[1],lokasi,mNamaVendor,"ticket")
                                tiketModel.add(mItem)
                            }
                            // =========================================== Layout Setting
                            TiketAdapter2.TYPE_OF_VIEW = 0 // Ticket Plus Minus.

                            // ============ recycler_tiket ============
                            recycler_tiket.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                            val vendorAdapter = TiketAdapter2()
                            recycler_tiket.adapter = vendorAdapter
                            vendorAdapter.setList(tiketModel)
                            tiketModel.clear()
                        } else {
                            txTiket.visibility = View.GONE
                            txAmount.visibility = View.GONE
                            recycler_tiket.visibility = View.GONE
                            btn_buy.visibility = View.GONE
                            Toast.makeText(this,"No Ticket Available",Toast.LENGTH_LONG).show()
//                            footer.visibility = View.GONE
                        }
                    }
                }
                catch (e: Exception) {
                    Log.e("aim", "Event err: $e")
                }

            }, Response.ErrorListener { response ->
                //========================================================================================= error handling
                swipe_container.isRefreshing = false
                Handler().postDelayed({ dialog.dismiss() }, 0)
                val networkResponse = response.networkResponse
                if (networkResponse == null){
                    Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
                }
                else {
                    try {
                        val err = String(networkResponse.data)
                        val jsonObj = JSONObject(err)
                        val errCode   = jsonObj.getString("code")
                        val errMessage   = jsonObj.getString("message")

                        Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                        Log.d("aim", "Err code : $errCode, $err")
                    } catch (e: JSONException) {
                        Log.e("aim", "Err : $e")
                    }
                }
            }) {
                override fun getHeaders(): MutableMap<String,String> {
                    val headers = HashMap<String,String>()
                    headers["Accept"] = "application/json"
                    headers["Content-Type"] = "application/json"
                    return headers
                }
            }
            rq.add(sr)
    }

    private fun activatedWalletDialog() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        textNotif.text = resources.getText(R.string.activated_wallet)

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val i = Intent(this, Profile::class.java)
            i.putExtra("from", "event")
            i.putExtra("lastActivity", slug)
            startActivity(i)
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun saveImage(image: Bitmap)= runWithPermissions(writeStorage) {
        val savedImagePath: String
        val imageFileName = "Poster_$mNamaVendor.jpg"
        val storageDir = File((Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)), "/Trunkey")
        var success = true
        if (!storageDir.exists())
        {
            success = storageDir.mkdirs()
        }
        if (success)
        {
            val imageFile = File(storageDir, imageFileName)
            savedImagePath = imageFile.absolutePath
            try
            {
                val fOut = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            }
            catch (e:Exception) {
                e.printStackTrace()
            }
            // Add the image to the system gallery
            galleryAddPic(savedImagePath)
            Toast.makeText(this, "Poster saved", Toast.LENGTH_LONG).show()
        }
    }
    private fun galleryAddPic(imagePath:String) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f = File(imagePath)
        val contentUri = Uri.fromFile(f)
        mediaScanIntent.data = contentUri
        sendBroadcast(mediaScanIntent)
    }
}
