package innovacia.co.id.trunkey1.helper

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.*
import java.text.SimpleDateFormat
import java.util.*
import android.widget.AdapterView
import android.view.View
import innovacia.co.id.trunkey1.R
import kotlinx.android.synthetic.main.calendar_layout.view.*



class CalendarCustomView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    var cal = Calendar.getInstance()
    @SuppressLint("SimpleDateFormat")
    val sdf = SimpleDateFormat("dd-MM-yyyy")

    private var eventHandler: CalendarClickHandler? = null

    // calendar components
    private lateinit var header: LinearLayout
    private lateinit var btnToday: Button
    private lateinit var btnPrev: ImageView
    private lateinit var btnNext: ImageView
    private lateinit var txtDateDay: TextView
    private lateinit var txtDisplayDate: TextView
    private lateinit var txtDateYear: TextView
    private lateinit var gridView: GridView

    init {
        initControl(context, attrs)
    }

    private fun assignUiElements() {
        // layout is inflated, assign local variables to components
        header = findViewById(R.id.calendar_header)
        btnPrev = findViewById(R.id.calendar_prev_button)
        btnNext = findViewById(R.id.calendar_next_button)
        txtDateDay = findViewById(R.id.date_display_day)
        txtDateYear = findViewById(R.id.date_display_year)
        txtDisplayDate = findViewById(R.id.date_display_date)
        btnToday = findViewById(R.id.date_display_today)
        gridView = findViewById(R.id.calendar_grid)
    }

    /**
     * Load control xml layout
     */
    private fun initControl(context: Context, attrs: AttributeSet) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(innovacia.co.id.trunkey1.R.layout.calendar_layout, this)

        assignUiElements()
        assignClickHandlers()

//        setUpCalendarAdapter()

//        setPreviousButtonClickEvent()
//        setNextButtonClickEvent()
//        setGridCellClickEvents()

    }

    fun assignClickHandlers() {

        btnToday.setOnClickListener {
            cal = Calendar.getInstance()
        }

        btnPrev.setOnClickListener {
            eventHandler?.prevMonth()
        }

        btnNext.setOnClickListener {
            eventHandler?.nextMonth()
        }


        // long-pressing a day
        gridView.onItemClickListener = object : AdapterView.OnItemClickListener {

            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                eventHandler?.onDayPress(parent?.getItemAtPosition(position) as Date)
            }
        }
    }

//    fun setPreviousButtonClickEvent() {
//        btnPrev.setOnClickListener {
//            cal.add(Calendar.MONTH, -1)
//            setUpCalendarAdapter()
//        }
//    }
//
//    fun setNextButtonClickEvent() {
//        btnNext.setOnClickListener {
//            cal.add(Calendar.MONTH, 1)
//            setUpCalendarAdapter()
//        }
//    }
//
//    private fun setGridCellClickEvents() {
//        gridView.onItemClickListener = AdapterView.OnItemClickListener {
//            parent, view, position, id ->
//            Toast.makeText(context, "Clicked $position", Toast.LENGTH_LONG).show()
//        }
//    }

    fun setEmptyCalendar(state:String?,allEvent:ArrayList<String>) {
        if (state != null) {
            val pickDate = state.split("-")
            val year = pickDate[0].toInt()
            val month = pickDate[1].toInt()
            val date = pickDate[2].toInt()
            cal.set(year,month-1,date-1)
        }

        val dayValueInCells = ArrayList<Date>()
        val mCal = cal.clone() as Calendar
        mCal.set(Calendar.DATE, 1) // tgl 1
        val firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK)
        // minngu, senin, selasa, rabu, kamis, jumat, sabtu
        var ruwet = 0
        when(firstDayOfTheMonth){
            1 -> { ruwet = 7-1 }
            2 -> { ruwet = 1-1 }
            3 -> { ruwet = 2-1 }
            4 -> { ruwet = 3-1 }
            5 -> { ruwet = 4-1 }
            6 -> { ruwet = 5-1 }
            7 -> { ruwet = 6-1 }
        }

        mCal.add(Calendar.DATE, - ruwet)

        // senin sampai jumat 35 kolom
        if (ruwet <= 4) {
            while (dayValueInCells.size < 35) {
                dayValueInCells.add(mCal.time)
                mCal.add(Calendar.DAY_OF_MONTH, 1)
            }
        } else if (ruwet > 4) {
            // sabtu sampai minggu 42 kolom
            while (dayValueInCells.size < 42) {
                dayValueInCells.add(mCal.time)
                mCal.add(Calendar.DAY_OF_MONTH, 1)
            }
        }

        Log.d("aim", "firstDayOfTheMonth $firstDayOfTheMonth")
        Log.d("aim", "Number of date " + dayValueInCells.size)
        date_display_day.text = sdf.format(Date())
        gridView.adapter = CalendarAdapter(context, dayValueInCells, cal, allEvent)
    }

    fun setUpCalendarAdapter(state:String?,allEvent:ArrayList<String>) {
//        when(state) {
//            "prev" -> cal.add(Calendar.MONTH, -1)
//            "next" -> cal.add(Calendar.MONTH, 1)
//        }

        if (state != null) {
            val pickDate = state.split("-")
            val year = pickDate[0].toInt()
            val month = pickDate[1].toInt()
            val date = pickDate[2].toInt()
            cal.set(year,month-1,date-1)
        }

        val dayValueInCells = ArrayList<Date>()
        val mCal = cal.clone() as Calendar
        mCal.set(Calendar.DATE, 1) // tgl 1
        val firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK)
        // minngu, senin, selasa, rabu, kamis, jumat, sabtu
        var ruwet = 0
        when(firstDayOfTheMonth){
            1 -> { ruwet = 7-1 }
            2 -> { ruwet = 1-1 }
            3 -> { ruwet = 2-1 }
            4 -> { ruwet = 3-1 }
            5 -> { ruwet = 4-1 }
            6 -> { ruwet = 5-1 }
            7 -> { ruwet = 6-1 }
        }

        mCal.add(Calendar.DATE, - ruwet)

        // senin sampai jumat 35 kolom
        if (ruwet <= 4) {
            while (dayValueInCells.size < 35) {
                dayValueInCells.add(mCal.time)
                mCal.add(Calendar.DAY_OF_MONTH, 1)
            }
        } else if (ruwet > 4) {
            // sabtu sampai minggu 42 kolom
            while (dayValueInCells.size < 42) {
                dayValueInCells.add(mCal.time)
                mCal.add(Calendar.DAY_OF_MONTH, 1)
            }
        }

        Log.d("aim", "firstDayOfTheMonth $firstDayOfTheMonth")
        Log.d("aim", "Number of date " + dayValueInCells.size)
        date_display_day.text = sdf.format(Date())
        gridView.adapter = CalendarAdapter(context, dayValueInCells, cal, allEvent)
    }

    fun setEventHandler(eventHandler: CalendarClickHandler) {
        try {
            this.eventHandler = eventHandler
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface CalendarClickHandler {
        fun onDayPress(date: Date)
        fun nextMonth()
        fun prevMonth()
    }
}