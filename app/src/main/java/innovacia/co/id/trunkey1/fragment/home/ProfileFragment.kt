package innovacia.co.id.trunkey1.fragment.home


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.activity.Dashboard
import innovacia.co.id.trunkey1.adapter.ChildAdapter
import innovacia.co.id.trunkey1.database.Cart
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject

class ProfileFragment : Fragment() {

    lateinit var appContext: Context
    lateinit var viewContext: Context

    private val helper = user_info()
    private val mVolleyHelper = VolleyHelper()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        appContext = activity!!.applicationContext

//        val a = activity?.applicationContext
//        val b = viewContext.context!!

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        header_name?.text = resources.getString(R.string.profile_header)

        viewContext = view.context

        back_link?.visibility = View.GONE
        divider_header.visibility = View.GONE
        version_text?.text = "Version " + BuildConfig.VERSION_NAME

        swipe_container?.setOnRefreshListener {
            getProfile()
        }

        buttonHandling()
    }

    override fun onResume() {
        super.onResume()

        getProfile()

    }

    private fun buttonHandling(){

        profile_lay?.setOnClickListener {
            profile_lay?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, Profile::class.java)
            startActivity(i)
        }

        wallet_lay?.setOnClickListener {
            wallet_lay?.startAnimation(user_info.animasiButton)

            if (!user_info.pinStatus && !user_info.walletStatus) {
                walletDialog()
            }else if (!user_info.pinStatus && user_info.walletStatus) {
                pinDialog()
            } else {
                // skip Wallet.kt goto TopUp.kt
                val i = Intent(viewContext, TopUp::class.java)
                startActivity(i)
            }

        }

        notif_lay?.setOnClickListener {
            notif_lay?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, Notification::class.java)
            startActivity(i)
        }

        purchase_lay?.setOnClickListener {
            purchase_lay?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, MyBooking::class.java)
            startActivity(i)
        }

        wish_lay?.setOnClickListener {
            wish_lay?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, Whislist::class.java)
            startActivity(i)
        }

        care_lay?.setOnClickListener {
            care_lay?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext,TrunkeyCare::class.java)
            startActivity(i)
        }

        manage_lay?.setOnClickListener {
            manage_lay?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, ManageChild::class.java)
            startActivity(i)
        }

        refferal_lay?.setOnClickListener {
            refferal_lay?.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext, Referal::class.java)
            startActivity(i)
        }

        logout_lay?.setOnClickListener {
            logout_lay?.startAnimation(user_info.animasiButton)
            logOut()
        }
    }

    private fun getProfile(){
        val errSection = "getProfile"
        swipe_container.isRefreshing = true
        val rq = object : JsonObjectRequest(Request.Method.POST, user_info.profile, null, Response.Listener { response ->
            try {
                Log.i("AIM", response.toString())
                swipe_container.isRefreshing = false

                val status = response.getInt("status")
                if (status == 1) {
                    val jo = response.getJSONObject("data")
                    // User
                    val user = jo.getJSONObject("user")
                    val needPin = user.getInt("need_set_pin")
                    val photo = user.getString("photo")
                    Log.d("aim","need pin $needPin")
                    if (needPin == 1) { user_info.pinStatus = true }
                    user_info.gender = user.getString("gender")
                    user_info.userName = user.getString("username")
                    user_info.name = user.getString("nama")
                    user_info.alamat = user.getString("alamat")
                    user_info.email = user.getString("email")
                    user_info.phone = user.getString("no_telp")
                    user_info.photo = photo

                    parent_name.text = user.getString("nama")
                    profile_cash_value.text = helper.bank(user_info.cashBack)
                    user_info.referal_code = user.getString("referal_code")
                    Glide.with(viewContext).load(photo)
                            .apply(RequestOptions.skipMemoryCacheOf(true))
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                            .into(parent_photo)

                    // Balance
                    val balance = jo.getJSONObject("balance")
                    if (balance.getInt("status") == 1) {
                        user_info.walletStatus = true
                        val saldo = balance.getDouble("balance")
                        val point = user.getDouble("points")
                        user_info.wallet = saldo
                        user_info.cashBack = point
                        val saldoIDR = helper.bank(saldo)
                        val saldoCashBack = helper.bank(point)
                        profile_wallet_value.text = saldoIDR
                        profile_cash_value.text = saldoCashBack
                    } else {
                        user_info.walletStatus = false
                        profile_wallet_value.text = "Inactive"
                        val point = user.getDouble("points")
                        user_info.cashBack = point
                        val saldoCashBack = helper.bank(point)
                        profile_cash_value.text = saldoCashBack
                        profile_wallet_value.setTextColor(Color.parseColor("#FF0000"))
                    }
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        },Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(rq)
    }

    private fun logOut(){
        val errSection = "logout"
        swipe_container.isRefreshing = true
        val sr = object : JsonObjectRequest(Request.Method.POST, user_info.logout, null, Response.Listener { response ->
            swipe_container.isRefreshing = false
            user_info.loginStatus = false

            val reader2 = viewContext.getSharedPreferences("my_preferences", Context.MODE_PRIVATE)
            val editor = reader2.edit()
            editor.putString("access_token","")
            editor.putString("refresh_token","")
            editor.apply()

            val helper = user_info()
            helper.reset()

            val intent = Intent(viewContext, HomeBaru::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            activity?.finish()

        },Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(viewContext,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(sr)
    }

    private fun pinDialog() {
        val dialogs = Dialog(viewContext)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_pin)

        val pinCreate = dialogs.findViewById<EditText>(R.id.newPin)
        val confirmPinCreate = dialogs.findViewById<EditText>(R.id.confirmPin)

        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            val pinValue = pinCreate.text.toString()
            val confirmValue = confirmPinCreate.text.toString()
            if (pinValue == confirmValue) {
                createPin(pinValue, confirmValue)
                dialogs.dismiss()
            } else {
                Toast.makeText(viewContext, "Pin mismatch", Toast.LENGTH_LONG).show()
            }
        }

        dialogs.show()
    }
    fun createPin(newPin:String,confirmPin:String) {
        val builder = AlertDialog.Builder(viewContext)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(viewContext)
        val sr = object : StringRequest(Request.Method.POST, user_info.set_pin, Response.Listener { response ->
            try {
                Log.d("aim", response)
                dialog.dismiss()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    Toast.makeText(viewContext, "Create pin Success", Toast.LENGTH_LONG).show()
                    user_info.pinStatus = true
                }else {
                    Toast.makeText(viewContext, "Create pin fail", Toast.LENGTH_LONG).show()
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            pinDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(viewContext,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(viewContext, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = java.util.HashMap<String, String>()
                map["pin"] = newPin
                map["pin_confirmation"] = confirmPin
                Log.d("aim","$map")
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }

    private fun walletDialog() {
        val dialogs = Dialog(viewContext)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_register)

        val previewPhone = dialogs.findViewById<TextView>(R.id.previewPhone)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        previewPhone.text = user_info.phone

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            sendOtp()
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            val i = Intent(viewContext,Editprofile::class.java)
            startActivity(i)
        }

        dialogs.show()
    }
    private fun sendOtp() {
        shimmerLayout.startShimmerAnimation()
        val rq: RequestQueue = Volley.newRequestQueue(viewContext)
        val sr = object : StringRequest(Request.Method.POST, user_info.send_otp, Response.Listener { response ->
            try {
                shimmerLayout.stopShimmerAnimation()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    otpDialog()
                }
            } catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            shimmerLayout.stopShimmerAnimation()
            walletDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(viewContext,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(viewContext, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["phone"] = user_info.phone
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
        Log.d("aim",sr.toString())
    }

    private fun otpDialog() {
        val dialogs = Dialog(viewContext)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(false)
        dialogs.setContentView(R.layout.popup_otp)

        val pinOtp = dialogs.findViewById<EditText>(R.id.pinOtp)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
        val close = dialogs.findViewById<TextView>(R.id.close)

        close.setOnClickListener {
            close.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
        }

        ok.setOnClickListener {
            ok.startAnimation(user_info.animasiButton)
            prosesOtp(pinOtp.text.toString())
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            cancel.startAnimation(user_info.animasiButton)
            dialogs.dismiss()
            walletDialog()
        }

        dialogs.show()
    }
    private fun prosesOtp(otpCode:String) {
        shimmerLayout.startShimmerAnimation()
        val rq: RequestQueue = Volley.newRequestQueue(viewContext)
        val sr = object : StringRequest(Request.Method.POST, user_info.proses_otp, Response.Listener { response ->
            try {
                shimmerLayout.stopShimmerAnimation()
                val jsonObject = JSONObject(response)
                if (jsonObject.getInt("status") == 1) {
                    Toast.makeText(viewContext, "Trunkey wallet created", Toast.LENGTH_LONG).show()
                    user_info.walletStatus = true
                    getProfile()
                }else {
                    Toast.makeText(viewContext, "Error verification code", Toast.LENGTH_LONG).show()
                    user_info.walletStatus = false
                }
            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            shimmerLayout.stopShimmerAnimation()
            otpDialog()

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(viewContext,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(viewContext, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["phone"] = user_info.phone
                map["otp_code"] = otpCode
                return map
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        rq.add(sr)
    }


}
