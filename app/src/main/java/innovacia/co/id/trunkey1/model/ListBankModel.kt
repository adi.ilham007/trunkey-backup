package innovacia.co.id.trunkey1.model

data class ListBankModel (
    var imgbank         : String = "",
    val namabank        : String = "",
    val kodeVA          : String = "",
    val kodebank        : String = "",
    val topup_tutorial  : String = ""


)