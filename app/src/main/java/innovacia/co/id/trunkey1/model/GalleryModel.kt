package innovacia.co.id.trunkey1.model

data class GalleryModel (
    var id      :String,
    var img     :String
)