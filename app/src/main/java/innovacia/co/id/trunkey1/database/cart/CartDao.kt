package innovacia.co.id.trunkey1.database.cart

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface CartDao {

    @Query("SELECT * FROM cart_table")
    fun getAll(): LiveData<List<CartEntity>>

    @Query("SELECT * FROM cart_table WHERE id =:id")
    fun getByIdTrans(id: Int): List<CartEntity>

    @Query("SELECT * FROM cart_table WHERE itemId =:id")
    fun getByIdProduct(id: Int): List<CartEntity>

    @Query("SELECT * FROM cart_table WHERE merchantId =:id")
    fun getByIdMerchant(id: Int): List<CartEntity>



//    //========================================================================= Transaksi
//    @Query("SELECT id_item FROM cart_table WHERE id_trans =:id")
//    fun getId(id: Int): List<CartEntity>
//
//    @Query("SELECT kuantitas FROM cart_table WHERE id_trans =:id")
//    fun getJumlah(id: Int): List<CartEntity>



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cart: CartEntity)

    @Delete
    fun delete(cart: CartEntity)

    @Update
    fun update(cart: CartEntity)
}
