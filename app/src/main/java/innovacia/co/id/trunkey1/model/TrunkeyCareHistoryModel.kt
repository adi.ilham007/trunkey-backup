package innovacia.co.id.trunkey1.model

data class TrunkeyCareHistoryModel(
        val id:String,
        val nama:String,
        val theme:String,
        val time:String,
        val photo:String,
        val status:String
)