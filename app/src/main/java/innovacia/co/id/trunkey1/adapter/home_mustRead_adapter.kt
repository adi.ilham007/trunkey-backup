package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.DetailBlog
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.blog_model
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_home_blog.view.*

class home_mustRead_adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var blogList = mutableListOf<blog_model>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_home_blog, parent, false))
    }
    override fun getItemCount(): Int = blogList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as vendorListViewHolder

        viewHolder.bindView(blogList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val slug = blogList[position].slug
            val category = blogList[position].type

            val i = Intent(view.context, DetailBlog::class.java)
            i.putExtra("slug", slug)
            i.putExtra("category", category)
            view.context.startActivity(i)

        }
    }
    fun setList(listOfVendor: List<blog_model>) {
        this.blogList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<blog_model>) {
        this.blogList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: blog_model) {
            itemView.nama.text       = vendorModel.nama
            itemView.logo_video.visibility = View.GONE

            Glide
                    .with(itemView.context)
                    .load(vendorModel.img)
                    .into(itemView.img)
//
//            if (vendorModel.id_vendor == "more"){
//                itemView.file_logo.setBackgroundResource(R.mipmap.load_more)
//                itemView.icon.visibility = View.GONE
//            }
        }
    }

}