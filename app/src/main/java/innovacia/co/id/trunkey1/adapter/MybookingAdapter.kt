package innovacia.co.id.trunkey1.adapter

import android.app.Dialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.MyBookingModel
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_mybooking.view.*
import kotlinx.android.synthetic.main.list_vendor2.view.*
import java.lang.Exception

class MybookingAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private var bookingList= mutableListOf<MyBookingModel>()
//    private val bookingData = mutableListOf<MyBookingModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return reviewListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_mybooking, parent, false))
    }
    override fun getItemCount(): Int = bookingList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as reviewListViewHolder
        viewHolder.bindView(bookingList[position])
//
        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)
//
//            val id = bookingList[position].id
//            val i = Intent(view.context, DetailHistory::class.java)
//            i.putExtra("id",id)
//            view.context.startActivity(i)
            val dialogs = Dialog(view.context)
            dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialogs.setCancelable(false)
            dialogs.setContentView(R.layout.popup_qrcode)

            val imgQrcode = dialogs.findViewById<ImageView>(R.id.img_qrcode)
            val close = dialogs.findViewById<TextView>(R.id.close)

            var text=bookingList[position].code_booking // Whatever you need to encode in the QR code
            val multiFormatWriter = MultiFormatWriter()
            try {
                val bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,1000,1000)
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                imgQrcode.setImageBitmap(bitmap)
            } catch ( e :Exception) {
                e.printStackTrace()
            }

            close.setOnClickListener {
                close.startAnimation(user_info.animasiButton)
                dialogs.dismiss()
            }

            dialogs.show()


        }

    }

//    private fun otpDialog() {
//        val dialogs = Dialog(view.context.)
//        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialogs.setCancelable(false)
//        dialogs.setContentView(R.layout.popup_otp)
//
//        val pinOtp = dialogs.findViewById<EditText>(R.id.pinOtp)
//        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
//        val ok = dialogs.findViewById<TextView>(R.id.btnOk)
//        val close = dialogs.findViewById<TextView>(R.id.close)
//
//        close.setOnClickListener {
//            close.startAnimation(user_info.animasiButton)
//            dialogs.dismiss()
//        }
//
//        ok.setOnClickListener {
//            ok.startAnimation(user_info.animasiButton)
////            prosesOtp(otp.text.toString())
//            prosesOtp(pinOtp.text.toString())
//            dialogs.dismiss()
//        }
//
//        cancel.setOnClickListener {
//            cancel.startAnimation(user_info.animasiButton)
//            dialogs.dismiss()
//            walletDialog()
//        }
//
//        dialogs.show()
//    }

    fun setList(listOfBooking: List<MyBookingModel>) {
        this.bookingList= listOfBooking.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfBooking: List<MyBookingModel>) {
        this.bookingList.addAll(listOfBooking)
        notifyDataSetChanged()
    }

    class reviewListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(bookingModel: MyBookingModel) {

            itemView.vendor.text = bookingModel.vendor_name
            itemView.nama_tiket.text =bookingModel.nama_tiket
//            itemView.bookingCode
            itemView.keterangan.text = bookingModel.keterangan
            val tanggalMulai = bookingModel.tanggal_mulai
            val tanggalAkhir = bookingModel.tanggal_akhir



//                tanggalMulai.subSequence(8,10)

            if(tanggalAkhir !="null"  && tanggalMulai != "null"){
                val bulanAwal = tanggalMulai.subSequence(5,7)
                val bulanAkhir = tanggalAkhir.subSequence(5,7)
                var textBulanAwal = ""
                var textBulanAkhir = ""

                when (bulanAwal) {
                    "01" -> textBulanAwal= "Jan"
                    "02" -> textBulanAwal= "Feb"
                    "03" -> textBulanAwal= "March"
                    "04" -> textBulanAwal= "Apr"
                    "05" -> textBulanAwal= "May"
                    "06" -> textBulanAwal= "Jun"
                    "07" -> textBulanAwal= "Jul"
                    "08" -> textBulanAwal= "Aug"
                    "09" -> textBulanAwal= "Sep"
                    "10" -> textBulanAwal= "Oct"
                    "11" -> textBulanAwal= "Nov"
                    "12" -> textBulanAwal= "Des"
                }
                when (bulanAkhir) {
                    "01" -> textBulanAkhir= "Jan"
                    "02" -> textBulanAkhir= "Feb"
                    "03" -> textBulanAkhir= "March"
                    "04" -> textBulanAkhir= "Apr"
                    "05" -> textBulanAkhir= "May"
                    "06" -> textBulanAkhir= "Jun"
                    "07" -> textBulanAkhir= "Jul"
                    "08" -> textBulanAkhir= "Aug"
                    "09" -> textBulanAkhir= "Sep"
                    "10" -> textBulanAkhir= "Oct"
                    "11" -> textBulanAkhir= "Nov"
                    "12" -> textBulanAkhir= "Des"
                }

                var tahun = tanggalMulai.subSequence(0,4)

                if(textBulanAwal != textBulanAkhir){
                    itemView.tanggal_acara.text = tanggalMulai.subSequence(8,10).toString()+ " "+ textBulanAwal +" - " +tanggalAkhir.subSequence(8,10).toString() + " "+textBulanAkhir +" "+ tahun
                }else{

                    itemView.tanggal_acara.text= tanggalMulai.subSequence(8,10).toString()+ " - " +tanggalAkhir.subSequence(8,10).toString() + " "+textBulanAwal +" "+ tahun

                }

            }else{
                itemView.tanggal_view.text = "null"
            }



//            itemView.tanggal_booking.text = bookingModel.tanggal_booking
            itemView.nama_participant.text = "Partcipant : "+bookingModel.nama_participant
            itemView.txTotal.text = bookingModel.biaya
            itemView.place.text = "Place : "+bookingModel.tempat

            val text = bookingModel.code_booking // Whatever you need to encode in the QR code
            val multiFormatWriter = MultiFormatWriter()
            try {
                val bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 1000, 1000)
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                itemView.bookingCode.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }
}