package innovacia.co.id.trunkey1.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.content.FileProvider
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.BuildConfig
import innovacia.co.id.trunkey1.DownloadPreview
import innovacia.co.id.trunkey1.HomeBaru
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.InputStreamVolleyRequest
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_news_detail.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream

class NewsDetail : AppCompatActivity() {

    private val readStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private val writeStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private val mVolleyHelper = VolleyHelper()

    private var fcmStatus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)
        header_name.text = resources.getString(R.string.news_detail)
        divider_header.visibility = View.GONE

        back_link.setOnClickListener {
            if (fcmStatus) {
                val intent = Intent(this, HomeBaru::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            } else {
                onBackPressed()
            }
        }

        generateView()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (fcmStatus) {
            val intent = Intent(this, HomeBaru::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }

    private fun generateView() {
        if (intent.hasExtra("data")) {
            fcmStatus = true
            // From notification / FCM

            val reader = this.getSharedPreferences("my_preferences", Context.MODE_PRIVATE)
            val token = reader.getString("access_token", "")
            val refresh = reader.getString("refresh_token","")

            if (!token.isNullOrEmpty() && !refresh.isNullOrEmpty()) {
                user_info.loginStatus = true
                user_info.token = token
                user_info.refresh = refresh

                val fcmData = JSONObject(intent.getStringExtra("data"))
                val newsId = fcmData.getString("id")
                val childId = fcmData.getString("id_children")
                getDetail(newsId,childId)
            }
        } else if (intent.hasExtra("newsId")) {
            val newsId = intent.getStringExtra("newsId")
            val childId = intent.getStringExtra("childId")
            getDetail(newsId,childId)
        }
    }

    private fun getDetail(newsId:String,childId:String) {
        val url = user_info.newsDetail
        val test = url.replace("<news>",newsId).replace("<child>",childId)

        val errSection = "detail news"
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq = object : JsonObjectRequest(Request.Method.POST,test,null,Response.Listener { response ->
            dialog.dismiss()
            try {
                val data = response.getJSONObject("data")
                val title = data.getString("nama")
                title_value.text = title
                content_value.text = data.getString("keterangan")

                if (data.has("files")) {
                    val file = data.getJSONArray("files")
                    val fileArray = arrayOfNulls<String>(file.length())

                    for (i in 0 until file.length()) {
                        fileArray[i] = file[i].toString()
                    }

                    if (file.length() == 0) {
                        tx_attachment.visibility = View.GONE
                    }

                    list_attachment.adapter = ArrayAdapter(this, R.layout.simple_one_textview, fileArray)
                    list_attachment.setOnItemClickListener { parent, view, position, id ->
                        view.startAnimation(user_info.animasiButton)

                        if (fileArray[position]!!.contains(".jpg") ||
                                fileArray[position]!!.contains(".jpeg") ||
                                fileArray[position]!!.contains(".png")) {

                            val i = Intent(view.context,DownloadPreview::class.java)
                            i.putExtra("title",title)
                            i.putExtra("imageUrl",fileArray[position])
                            startActivity(i)

                        } else {
                            downloadFile(fileArray[position]!!)
                        }
                    }
                }

            } catch (e:JSONException) {

            }

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String,String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }

    private fun downloadFile (fileUrl:String) = runWithPermissions(writeStorage) {

        Log.d("aim","File : $fileUrl")

        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val nullMap = java.util.HashMap<String, String>()
        val request = InputStreamVolleyRequest(Request.Method.GET, fileUrl,object:Response.Listener<ByteArray> {
            override fun onResponse(response:ByteArray) {
                dialog.dismiss()
                try
                {
                    val storageDir = File(Environment.getExternalStorageDirectory(), "Trunkey/File")

                    if (!storageDir.exists()) {
                        storageDir.mkdirs()
                    }

                    try {
                        val link = fileUrl.split("/")
                        val fileName = link[link.lastIndex]

                        val imageFile = File(storageDir,fileName)
                        val outputStream = FileOutputStream(imageFile)
                        outputStream.write(response)
                        outputStream.close()

                        openFileDialog(this@NewsDetail,fileName)
                    }
                    catch (e:Exception) {
                        e.printStackTrace()
                    }

                }
                catch (e:Exception) {
                    Toast.makeText(this@NewsDetail,"Download Fail",Toast.LENGTH_LONG).show()
                    Log.d("aim", "UNABLE TO DOWNLOAD FILE")
                    e.printStackTrace()
                }
            }
        }, object:Response.ErrorListener {
            override fun onErrorResponse(error: VolleyError) {
                dialog.dismiss()

                Toast.makeText(this@NewsDetail,"Download Fail",Toast.LENGTH_LONG).show()
                error.printStackTrace()
            }
        }, nullMap)
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }

    @SuppressLint("SetTextI18n")
    private fun openFileDialog(mContext: Context,fileName:String) {
        val dialogs = Dialog(mContext)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        textNotif.text = "File saved in your internal memory"
        ok.text = "Open File"
        cancel.text = "Close"

        ok.setOnClickListener {
            openDownloadFile(fileName)
//            open(fileName)
            dialogs.dismiss()
        }

        cancel.setOnClickListener {
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun openDownloadFile (fileName:String) = runWithPermissions(readStorage) {
        val storageDir = File(Environment.getExternalStorageDirectory(), "Trunkey/File/$fileName")
        val uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider",storageDir)

        val intent = Intent(Intent.ACTION_VIEW)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        if (storageDir.toString().contains(".doc") || storageDir.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if(storageDir.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if(storageDir.toString().contains(".ppt") || storageDir.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint")
        } else if(storageDir.toString().contains(".xls") || storageDir.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel")
        } else if(storageDir.toString().contains(".zip") || storageDir.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav")
        } else if(storageDir.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf")
        } else if(storageDir.toString().contains(".wav") || storageDir.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav")
        } else if(storageDir.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif")
        } else if(storageDir.toString().contains(".jpg") || storageDir.toString().contains(".jpeg") || storageDir.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/*")
        } else if(storageDir.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else if(storageDir.toString().contains(".3gp") || storageDir.toString().contains(".mpg") || storageDir.toString().contains(".mpeg") || storageDir.toString().contains(".mpe") || storageDir.toString().contains(".mp4") || storageDir.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*")
        } else {
            //any other file (manual user choice)
            intent.setDataAndType(uri, "*/*")
        }

        try {
            if (storageDir.exists())
                startActivity(Intent.createChooser(intent, "Open"))
            else
                Toast.makeText(this, "File is corrupted", Toast.LENGTH_LONG).show();

        } catch (e: Exception) {
            val mFileName = fileName.split(".")
            val fileType = mFileName[mFileName.lastIndex]
            Log.d("aim","error open file $e")
            Toast.makeText(this,"Your device not supported to open .$fileType file",Toast.LENGTH_LONG).show()
        }
    }
}
