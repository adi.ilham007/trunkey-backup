package innovacia.co.id.trunkey1

import android.Manifest
import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.crashlytics.android.Crashlytics
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.fragment.home.HomeFragment
import innovacia.co.id.trunkey1.fragment.home.ProfileFragment
import innovacia.co.id.trunkey1.fragment.home.ResourceFragment
import innovacia.co.id.trunkey1.fragment.home.ScanFragment
import innovacia.co.id.trunkey1.helper.*
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_home_baru.*
import org.json.JSONObject

class HomeBaru : AppCompatActivity(), LocationListener {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val fm = supportFragmentManager
    private val fragment1:Fragment = HomeFragment()
    private val fragment2:Fragment = ResourceFragment()
    private val fragment3:Fragment = ProfileFragment()

    // Permissions
    private var lokasiGps = Manifest.permission.ACCESS_FINE_LOCATION
    private var lokasiNet = Manifest.permission.ACCESS_COARSE_LOCATION
    // Update Play Store
    private val MY_REQUEST_CODE = 2121
    // Exit App
    private var doubleBackToExitPressedOnce = false

    override fun onLocationChanged(location: Location?) {
        this.getLocation()
    }
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        this.getLocation()
    }
    override fun onProviderEnabled(provider: String?) {
        this.getLocation()
    }
    override fun onProviderDisabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                fm.beginTransaction().show(fragment1)
                        .hide(fragment2)
                        .hide(fragment3)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
//            R.id.navigation_scan -> {
//                val mScanFragment = fragments[item.itemId] ?: ScanFragment()
//                fragments[item.itemId] = mScanFragment
//                saveCurrentState()
//                stateHelper.restoreState(mScanFragment, item.itemId)
//                addFragment(mScanFragment)
//                return@OnNavigationItemSelectedListener true
//            }
            R.id.navigation_resource -> {
                fm.beginTransaction().show(fragment2)
                        .hide(fragment1)
                        .hide(fragment3)
                        .commit()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                if (user_info.loginStatus) {
                    fm.beginTransaction().show(fragment3)
                            .hide(fragment1)
                            .hide(fragment2)
                            .commit()

                    return@OnNavigationItemSelectedListener true
                } else {
                    val i = Intent(this, login::class.java)
                    i.putExtra("back", "")
                    startActivity(i)
                }

            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_baru)

        addFragment()

        systemHandling()

        updateSetPrev()

        appIntro()

        languageSetting()

        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {

            val reader = this.getSharedPreferences("update_preferences", Context.MODE_PRIVATE)
            val editor = reader.edit()
            editor.putBoolean("every_open", true)
            editor.apply()
            editor.commit()

            finishAffinity()
            System.exit(0)
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun onStop() {
        super.onStop()
        val reader = this.getSharedPreferences("update_preferences", Context.MODE_PRIVATE)
        val editor = reader.edit()
        editor.putBoolean("every_open", true)
        editor.apply()
        editor.commit()
        Log.d("aim","onStop")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                installUpdate()
            }
        }
    }

    @SuppressLint("HardwareIds")
    private fun systemHandling() {
        // Firebase Token and Device ID
        val firebase = Firebase()
        firebase.getFirebaseToken()
        val androidID = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
        Log.d("aim", "Android ID : $androidID")

        // Firebase Crashlytic
        Fabric.with(this, Crashlytics())
        val fabric = Fabric.Builder(this).kits(Crashlytics()).debuggable(true).build()
        Fabric.with(fabric)

        // Handling phone manufacture
        val intent = Intent()
        val manufacturer = android.os.Build.MANUFACTURER
        when (manufacturer) {
            "xiaomi" -> intent.component = ComponentName("com.miui.securitycenter",
                    "com.miui.permcenter.autostart.AutoStartManagementActivity")
            "oppo" -> intent.component = ComponentName("com.coloros.safecenter",
                    "com.coloros.safecenter.permission.startup.StartupAppListActivity")
            "vivo" -> intent.component = ComponentName("com.vivo.permissionmanager",
                    "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")
        }
        val arrayList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        if (arrayList.size > 0) { startActivity(intent) }
    }

    private fun updateSetPrev() {
        val reader = this.getSharedPreferences("update_preferences", Context.MODE_PRIVATE)
        val first = reader.getBoolean("every_open", true)
        Log.d("aim","setprev : $first")
        if (first) {
            val editor = reader.edit()
            editor.putBoolean("every_open", false)
            editor.apply()
            editor.commit()

            checkUpdate()
        }
    }

    private fun checkUpdate() {
        val appUpdateManager = AppUpdateManagerFactory.create(this)

        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE )) {

                appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                        AppUpdateType.FLEXIBLE,
                        // The current activity making the update request.
                        this,
                        // Include a request code to later monitor this update request.
                        MY_REQUEST_CODE)

                installUpdate()
            }
        }
    }

    private fun installUpdate() {
        // Install Update
        val appUpdateManager = AppUpdateManagerFactory.create(this)
        val listener = InstallStateUpdatedListener {
            it.installStatus() // represents the update state
            it.installErrorCode() // represents the error state
            Log.d("aim","update : ${it.installStatus()}")

            if (it.installStatus() == InstallStatus.DOWNLOADED) {

                Snackbar.make(
                        findViewById(R.id.home_root),
                        "New update downloaded.",
                        Snackbar.LENGTH_INDEFINITE
                ).apply {
                    setAction("RESTART") { appUpdateManager.completeUpdate() }
//                    setActionTextColor(resources.getColor(R.color.textWhite))
                    show()
                }

            }
        }
        appUpdateManager.registerListener(listener)
    }

    private fun appIntro() {
        //AppIntro
        val reader = this.getSharedPreferences("my_preferences", Context.MODE_PRIVATE)
        val first = reader.getBoolean("is_first", true)
        if (first) {
            val editor = reader.edit()
            editor.putBoolean("is_first", false)
            editor.apply()
            val i =  Intent(this,TrunkeyCareIntro::class.java)
            startActivity(i)
        } else {
            getLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() = runWithPermissions(lokasiGps, lokasiNet) {
        try {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation.addOnSuccessListener { location->

                if (location != null) {
                    user_info.latitude = location.latitude
                    user_info.longitude = location.longitude
                }
            }
        }
        catch (e:Exception) { Log.d("aim",e.toString()) }
    }

    private fun languageSetting() {
        val mLocaleHelper = LocaleHelper()
        val translateResult = mLocaleHelper.getResourcesLanguage(this)

        nav_view.menu.getItem(0).title = translateResult.getString(R.string.nav_home)
        nav_view.menu.getItem(1).title = translateResult.getString(R.string.nav_resource)
        nav_view.menu.getItem(2).title = translateResult.getString(R.string.nav_profile)
//        nav_view.menu.getItem(3).title = translateResult.getString(R.string.nav_profile)
    }

    @SuppressLint("PrivateResource")
    private fun addFragment() {

        if (fm.fragments.isEmpty()) {

//            fm.fragments.clear()

            fm.beginTransaction().add(R.id.fragment_layout, fragment3, fragment3.javaClass.simpleName)
                    .hide(fragment3).commit()

            fm.beginTransaction().add(R.id.fragment_layout, fragment2, fragment2.javaClass.simpleName)
                    .hide(fragment2).commit()

            fm.beginTransaction().add(R.id.fragment_layout, fragment1, fragment1.javaClass.simpleName)
                    .commit()

        }
    }
}
