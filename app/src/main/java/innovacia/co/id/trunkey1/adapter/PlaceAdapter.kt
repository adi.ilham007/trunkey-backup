package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.graphics.Color
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.ChildSetLimit
import innovacia.co.id.trunkey1.MapsActivity
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.PlaceModel
import kotlinx.android.synthetic.main.list_place.view.*


class PlaceAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var vendorList = mutableListOf<PlaceModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_place, parent, false))
    }
    override fun getItemCount(): Int = vendorList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as ListViewHolder

        holder.bindView(vendorList[position])

        holder.itemView.share?.setOnClickListener { view ->
            holder.itemView.share?.startAnimation(user_info.animasiButton)
            val intent = Intent("data")
            intent.putExtra("state","share")
            LocalBroadcastManager.getInstance(holder.itemView.context).sendBroadcast(intent)
        }

        holder.itemView.direction?.setOnClickListener { view ->
            holder.itemView.direction?.startAnimation(user_info.animasiButton)
            val intent = Intent("data")
            intent.putExtra("state","location")
            intent.putExtra("lat", vendorList[position].lattitude)
            intent.putExtra("lon", vendorList[position].longitude)
            LocalBroadcastManager.getInstance(holder.itemView.context).sendBroadcast(intent)
        }

        holder.itemView.call?.setOnClickListener {
            holder.itemView.call?.startAnimation(user_info.animasiButton)
            val intent = Intent("data")
            intent.putExtra("state","telepon")
            intent.putExtra("telepon", vendorList[position].telepon)
            LocalBroadcastManager.getInstance(holder.itemView.context).sendBroadcast(intent)
        }
    }
    fun setList(listOfVendor: List<PlaceModel>) {
        this.vendorList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<PlaceModel>) {
        this.vendorList.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(mPlaceModel: PlaceModel) {
            itemView.tx_nama?.text = mPlaceModel.nama
            itemView.tx_alamat?.text = mPlaceModel.alamat
            itemView.logo_huruf?.text = mPlaceModel.nama[0].toString().capitalize()

            if (mPlaceModel.theme != "null") {
                itemView.logo_huruf?.setBackgroundColor(Color.parseColor(mPlaceModel.theme))
            }
        }
    }
}