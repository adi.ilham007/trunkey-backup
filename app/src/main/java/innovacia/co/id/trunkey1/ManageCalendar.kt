package innovacia.co.id.trunkey1

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.BillingAdapter
import innovacia.co.id.trunkey1.adapter.CalendarAdapter
import innovacia.co.id.trunkey1.adapter.ScheduleAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.CalendarModel
import innovacia.co.id.trunkey1.model.ScheduleModel
import kotlinx.android.synthetic.main.activity_manage_calendar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class ManageCalendar : AppCompatActivity() {

    private val mCalendarModel = mutableListOf<CalendarModel>()
    private var loadMore = false    // Pagination
    private var page = 1
    private var max_page = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_calendar)
        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        swipe_container.setOnRefreshListener {
            page = 1
            mCalendarModel.clear()
            scheduleByUser()
        }

        addAgenda.setOnClickListener {
            addAgenda.startAnimation(user_info.animasiButton)
            val i = Intent(this, CreateAgenda::class.java)
            startActivity(i)
        }

        rvListener()
        scheduleByUser()


    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadCastReceiver, IntentFilter("delete"))
    }
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                "delete" -> {
                    val id = intent.getIntExtra("id",0)
                    deleteAgenda(id)
                }
            }
        }
    }
    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(broadCastReceiver)
    }

//    override fun onResume() {
//        super.onResume()
//        page = 1
//        scheduleByUser()
//    }

    override fun onRestart() {
        super.onRestart()
        page = 1
        mCalendarModel.clear()
        scheduleByUser()
    }

    private fun rvListener() {
        recycler_calendar.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                if (isLastPosition && page < max_page) {
//                    progress_bar_wallet.visibility = View.VISIBLE
                    loadMore = true

                    page++
                    scheduleByUser()
                    Log.d("aim", "page: $page")

                }
            }
        })
    }

    private fun scheduleByUser() {
        swipe_container.isRefreshing = true
        val rq1: RequestQueue = Volley.newRequestQueue(this)
        val sr1 = object : StringRequest(Request.Method.POST, user_info.scheduleByUser+"?page=$page",Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim", "Status Anak : $response")
                val jsonObject = JSONObject(response)
                val rootData = jsonObject.getJSONObject("data")
                max_page = rootData.getInt("last_page")

                val dataArray = rootData.getJSONArray("data")
                for(i in 0 until dataArray.length()) {
                    val dataObject = dataArray.getJSONObject(i)

                    val startDateTime = dataObject.getString("tanggal_start").split(" ")
                    val startDateFormater = startDateTime[0].split("-")
                    val startDate = "${startDateFormater[2]}-${startDateFormater[1]}-${startDateFormater[0]}"
                    val startTime = startDateTime[1]

                    val endDateTime = dataObject.getString("tanggal_end").split(" ")
                    val endDateFormater = endDateTime[0].split("-")
                    val endDate = "${endDateFormater[2]}-${endDateFormater[1]}-${endDateFormater[0]}"
                    val endTime = endDateTime[1]

                    val id = dataObject.getString("id")
                    val nama = dataObject.getString("nama")
                    val location = dataObject.getString("location")

                    val start = "$startDate $startTime"
                    val end = "$endDate $endTime"

                    val mItem = CalendarModel(id, start,end,nama,"",location,"0","0","")
                    mCalendarModel.add(mItem)
                }

                val mCalendarAdapter = CalendarAdapter()
                if (recycler_calendar != null && !loadMore) {
                    recycler_calendar.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    recycler_calendar.adapter = mCalendarAdapter
                    mCalendarModel.reverse()
                    mCalendarAdapter.setList(mCalendarModel)
                } else if (recycler_calendar != null && loadMore) {
                    val recyclerViewState = recycler_calendar.layoutManager!!.onSaveInstanceState()
                    recycler_calendar.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    recycler_calendar.adapter = mCalendarAdapter
                    mCalendarModel.reverse()
                    mCalendarAdapter.addData(mCalendarModel)
                    recycler_calendar.layoutManager!!.onRestoreInstanceState(recyclerViewState)
                }

            }catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }
        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container.isRefreshing = false
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        sr1.retryPolicy = DefaultRetryPolicy(0,-1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq1.add(sr1)
    }

    private fun deleteAgenda(id:Int) {
        val builder = android.support.v7.app.AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.deleteAgenda+id, Response.Listener { response ->
            dialog.dismiss()
            Log.d("aim","respon schedule: $response")
            val jsonObject = JSONObject(response)
            val status = jsonObject.getInt("status")
            val message = jsonObject.getString("message")
            if(status == 1) {
                user_info.kalender = ""
                mCalendarModel.clear()
                scheduleByUser()
                Toast.makeText(this,message, Toast.LENGTH_LONG).show()
            }
        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }
        }
        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }
}
