package innovacia.co.id.trunkey1.model

data class customModelA(
    val id          :String,
    val slug        :String,
    val nama        :String,
    val harga1      :Double,
    val harga2      :Double,
    val foto        :String,
    val type        :String
)