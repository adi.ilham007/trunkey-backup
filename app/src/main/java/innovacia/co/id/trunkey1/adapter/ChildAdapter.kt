package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.list_child.view.*

class ChildAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var profile_child_list = mutableListOf<ChildModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return childListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_child, parent, false))
    }

    override fun getItemCount(): Int = profile_child_list.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as childListViewHolder
        viewHolder.bindView(profile_child_list[position])

        val id = profile_child_list[position].id.toString()
        val photo = profile_child_list[position].photo
        val nama = profile_child_list[position].name
        val gender = profile_child_list[position].gender
        val birthday = profile_child_list[position].birthday


        viewHolder.itemView.option.setOnClickListener { view ->
            val popup = PopupMenu(view.context, viewHolder.itemView.option)
            popup.inflate(R.menu.manage_child_menu)
            popup.setOnMenuItemClickListener { item: MenuItem ->
                when (item.itemId) {
                    R.id.update ->{
                        val i = Intent(view.context, ChildProfile::class.java)
                        i.putExtra("id", id)
                        view.context.startActivity(i)
                    }

                    R.id.limit ->{
                        val i = Intent(view.context, ChildSetLimit::class.java)
                        i.putExtra("id", id)
                        i.putExtra("nama", nama)
                        i.putExtra("photo", photo)
                        view.context.startActivity(i)
                    }

                    R.id.transfer ->{
                        val i = Intent(view.context, Transfer::class.java)
                        i.putExtra("id", id)
                        i.putExtra("nama", nama)
                        view.context.startActivity(i)
                    }
                }
                true
            }
            //displaying the popup
            popup.show()
        }
    }

    fun setList(listOfChild: List<ChildModel>) {
        this.profile_child_list = listOfChild.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(listOfTransaction: List<ChildModel>) {
        this.profile_child_list.addAll(listOfTransaction)
        notifyDataSetChanged()
    }


    class childListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val helper = user_info()

        fun bindView(ChildModel: ChildModel) {
            itemView.nama.text = ChildModel.name
            itemView.saldo.text = helper.bank(ChildModel.saldo)

            Glide.with(itemView.context).load(ChildModel.photo)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(itemView.childphoto)
        }
    }
}