package innovacia.co.id.trunkey1

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.WalletModel
import kotlinx.android.synthetic.main.activity_topup.*
import kotlinx.android.synthetic.main.activity_topup.profile_cash_value
import kotlinx.android.synthetic.main.activity_topup.profile_wallet_value
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException

class TopUp : AppCompatActivity(){

    val helper = user_info()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topup)
        header_name?.text = resources.getString(R.string.topup_header)

        val saldoIDR = helper.bank(user_info.wallet)
        val saldoCashBack = helper.bank(user_info.cashBack)
        profile_wallet_value.text = saldoIDR
        profile_cash_value.text = saldoCashBack

        back_link?.setOnClickListener{
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        text_bank?.setOnClickListener {
            text_bank?.startAnimation(user_info.animasiButton)
            val i = Intent(this, DetailTopup::class.java)
            startActivity(i)
        }
    }
}