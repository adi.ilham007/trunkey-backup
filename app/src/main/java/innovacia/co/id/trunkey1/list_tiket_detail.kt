package innovacia.co.id.trunkey1

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.TiketAdapter2
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.TiketModel
import kotlinx.android.synthetic.main.activity_list_tiket_detail.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class list_tiket_detail : AppCompatActivity() {

    // Transaction Data
    private val productId = ArrayList<Int>()
    private val productPrice = ArrayList<Double>()
    private val productCashBack = ArrayList<Double>()
    private val productMaxCashBack = ArrayList<Double>()
    private val productMember = ArrayList<String>()
    private val productMemberCount = ArrayList<Int>()

    lateinit var idArray: ArrayList<Int>
    lateinit var seatArray: ArrayList<Int>
    lateinit var hargaArray: ArrayList<*>
    var memberArray = ArrayList<String>()

    val helper = user_info()

    val space = "|>:M:<|"
    var whatBuy = ""
    var slug = ""
    var totalHarga = 0.0
    var totalCashBack = 0.0

    private val tiketModel = mutableListOf<TiketModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_tiket_detail)

        if (intent.hasExtra("slug")) {
            header_name.text = resources.getString(R.string.trans_header_ticket)
            whatBuy = "ticket"
            slug = intent.getStringExtra("slug")

            Log.d("aim","SLUG : $slug")
            getTiket()
        }
        else  if (intent.hasExtra("class")) {
            header_name.text = resources.getString(R.string.trans_header_course)
            whatBuy = "class"
            slug = intent.getStringExtra("slug")
            idArray = intent.getIntegerArrayListExtra("idArray")
            seatArray = intent.getIntegerArrayListExtra("seatArray")
            hargaArray = intent.getSerializableExtra("hargaArray") as ArrayList<*>
            totalHarga = intent.getDoubleExtra("totalHarga",0.0)
            totalCashBack = intent.getDoubleExtra("totalCashBack",0.0)
//            harga.text = helper.bank(totalHarga)
//            cashback.text = helper.bank(totalCashBack)

            Log.d("aim","intent id: $idArray, seat: $seatArray, harga: $hargaArray")
            getCourse()
        }

        else  if (intent.hasExtra("product")) {
            header_name.text = resources.getString(R.string.trans_header_Product)
        }

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        btn_buy.setOnClickListener {
            btn_buy.startAnimation(user_info.animasiButton)

            if (productId.size > 0) {
                val i = Intent(this, DetailPurchase::class.java)
                i.putExtra("buyer", "buyer")
                i.putExtra("whatBuy",whatBuy)
                i.putExtra("slug", slug)
                i.putExtra("idArray", productId)
                i.putExtra("seatArray", productMemberCount)
                i.putExtra("hargaArray", productPrice)
                i.putExtra("memberArray", productMember)
                i.putExtra("totalHarga", productPrice.sum())
                i.putExtra("totalCashBack", productCashBack.sum())
                startActivity(i)
            } else {
                Toast.makeText(this,"Please enter ${seatArray.sum()} participant name(s)",Toast.LENGTH_LONG).show()
            }

            // memberArray include id
//            if (idArray.size == memberArray.size / 2) {
//                val i = Intent(this, DetailPurchase::class.java)
//                i.putExtra("buyer", "buyer")
//                i.putExtra("whatBuy",whatBuy)
//                i.putExtra("slug", slug)
//                i.putExtra("idArray", idArray)
//                i.putExtra("seatArray", seatArray)
//                i.putExtra("hargaArray", hargaArray)
//                i.putExtra("memberArray", memberArray)
//
//                i.putExtra("totalHarga", totalHarga)
//                i.putExtra("totalCashBack", totalCashBack)
//                startActivity(i)
//            } else {
//                Toast.makeText(this,"Please enter ${seatArray.sum()} participant name(s)",Toast.LENGTH_LONG).show()
//            }
        }
    }

    // tambahan
    override fun onResume() {
        super.onResume()
        TiketAdapter2.TYPE_OF_VIEW = 1
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadCastReceiver, IntentFilter("data"))
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                "data" -> {
                    when {
                        intent.getStringExtra("state") == "+" -> {
                            productId.add(intent.getIntExtra("productId", 0))
                            productPrice.add(intent.getDoubleExtra("productPrice", 0.0))
                            productCashBack.add(intent.getDoubleExtra("productCashBack", 0.0))
                            productMaxCashBack.add(intent.getDoubleExtra("productMaxCashBack", 0.0))
                        }
                        intent.getStringExtra("state") == "+member" -> {
                            val seat = intent.getStringExtra("memberArray").split(space)
                            productMember.add(intent.getStringExtra("memberArray"))
                            productMemberCount.add(seat.size)
                            Log.d("aim","member : $seat")
                        }

                        intent.getStringExtra("state") == "-" -> {
                            productId.remove(intent.getIntExtra("productId", 0))
                            productPrice.remove(intent.getDoubleExtra("productPrice", 0.0))
                            productCashBack.remove(intent.getDoubleExtra("productCashBack", 0.0))
                            productMaxCashBack.remove(intent.getDoubleExtra("productMaxCashBack", 0.0))
                        }
                        intent.getStringExtra("state") == "-member" -> {
                            // Remove member by cancel button
                            val seat = intent.getStringExtra("memberArray").split(space)
                            productMember.remove(intent.getStringExtra("memberArray"))
                            productMemberCount.remove(seat.size)
                            Log.d("aim","member : $seat")
                        }

                        intent.getStringExtra("state") == "edit" -> {
                            val idTiket = intent.getIntExtra("productId",0)
                            val seat = intent.getStringExtra("memberArray").split(space)

                            if (productId.contains(idTiket)) {
                                val index = productId.indexOf(idTiket)
                                productMember[index] = intent.getStringExtra("memberArray")
                                productMemberCount[index] = seat.size
                            }
                        }
                    }

                    Log.d("aim", "productId: $productId")
                    Log.d("aim", "productPrice: $productPrice")
                    Log.d("aim", "productCashBack: $productCashBack")
                    Log.d("aim", "productMaxCashBack: $productMaxCashBack")
                    Log.d("aim", "productMember: $productMember")
                    Log.d("aim","productCount: $productMemberCount")

                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(broadCastReceiver)
    }

    fun getTiket() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog, null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        Log.d("aim","url : ${user_info.event_api}$slug")

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.event_api+slug,Response.Listener { response ->

            dialog.dismiss()

            try {
                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data")
                val namaVendor = data.getString("nama")

                if (!data.isNull("tickets")) {
                    val tiketArray = data.getJSONArray("tickets")
                    if (tiketArray.length() != 0) {
                        for (i in 0 until tiketArray.length()) {
                            val jo = tiketArray.getJSONObject(i)
                            val id = jo.getInt("id")
                            val nama = jo.getString("nama")
                            val ket = jo.getString("keterangan")


                            val harga = jo.getDouble("harga_diskon")
                            val kapasitas = jo.getInt("capacity")
                            val partisipan = jo.getInt("participant")
                            val seat = kapasitas - partisipan

                            val maxCashBack = jo.getDouble("max_cashback")
                            val cashBack = jo.getDouble("nominal_cashback")
                            Log.d("aim","cash back : $cashBack, max : $maxCashBack")

                            val lokasi = data.getString("place_nama")
                            val dateTime = data.getString("tanggal_mulai").split(" ")

                            val mItem = TiketModel(id, nama, ket, 0.0,harga,cashBack,maxCashBack,"",0,seat, dateTime[0],dateTime[1],lokasi,namaVendor,"ticket")
                            tiketModel.add(mItem)
                        }
                        // =========================================== Layout Setting
                        TiketAdapter2.TYPE_OF_VIEW = 1 // Ticket Plus Minus

                        // ============ recycler_tiket ============
                        recycler_tiket.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                        val vendorAdapter = TiketAdapter2()
                        recycler_tiket.adapter = vendorAdapter
                        vendorAdapter.setList(tiketModel)
                        tiketModel.clear()
                    } else {
                        recycler_tiket.visibility = View.GONE
                        Toast.makeText(this, "No Ticket Available", Toast.LENGTH_LONG).show()
                    }
                }
            } catch (e: JSONException) {
                Log.e("aim", "Err : $e")
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String,String> {
                val headers = HashMap<String,String>()
                headers["Accept"] = "application/json"
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }
        }
        rq.add(sr)
    }

    fun getCourse(){
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.detail_course+ slug, Response.Listener { response ->
            //========================================================================================= data from server
            Log.d("aim",response)
            dialog.dismiss()
//            try {
            val jsonObject = JSONObject(response)
            val data = jsonObject.getJSONObject("data")

            // Data Vendor
            val vendor = data.getJSONObject("vendor")
            val logoVendor = vendor.getString("photo_path")
            val namaVendor = vendor.getString("nama")

            if (!data.isNull("classes")) {
                val tiketArray = data.getJSONArray("classes")
                if (tiketArray.length() != 0) {
                    for (i in 0 until tiketArray.length()) {
                        val jo = tiketArray.getJSONObject(i)
                        val id = jo.getInt("id")
                        val nama = jo.getString("batch")
//                        val ket = jo.getString("keterangan")
                        val keterangan = helper.textHtml(jo.getString("keterangan"))

                        val harga = jo.getDouble("harga_diskon")
                        val kapasitas = jo.getInt("capacity")
                        val partisipan = jo.getInt("participant")
                        val seat = kapasitas - partisipan

                        var mCashBack = 0.0
                        val maxCashBack = jo.getDouble("max_cashback")
                        val cashBack = jo.getDouble("nominal_cashback")
                        Log.d("aim","cash back : $cashBack, max : $maxCashBack")
                        if (cashBack < maxCashBack) {
                            mCashBack = cashBack
                        } else if (cashBack >= maxCashBack) {
                            mCashBack = maxCashBack
                        }

                        //                            var ket = jo.getString("keterangan")
                        //                            ket = helper.textHtml(ket)

                        val lokasi = "Under Construction"
                        val dateTime = data.getString("tanggal_mulai")

                        if (idArray.contains(id)) {
                            val index = idArray.indexOf(id)
                            val seatBuy = seatArray[index]
                            val hargaDouble = hargaArray[index]
                            if (seatArray[index] != 0) {
                                val mItem = TiketModel(id, nama, keterangan, 0.0, hargaDouble as Double,0.0,0.0,"", seatBuy, seat, dateTime, "", lokasi, namaVendor)
                                tiketModel.add(mItem)
                            }
                        }
                    }
                    // =========================================== Layout Setting
                    TiketAdapter2.TYPE_OF_VIEW = 1 // Ticket Plus Minus

                    // ============ recycler_tiket ============
                    recycler_tiket.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    val vendorAdapter = TiketAdapter2()
                    recycler_tiket.adapter = vendorAdapter
                    vendorAdapter.setList(tiketModel)
                    tiketModel.clear()
                } else {
                    recycler_tiket.visibility = View.GONE
                    Toast.makeText(this, "No Course Available", Toast.LENGTH_LONG).show()
                }
            }
//            } catch (e: JSONException) {
//                Log.e("aim", "Event err: $e")
//            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {        }
        rq.add(sr)
    }
}
