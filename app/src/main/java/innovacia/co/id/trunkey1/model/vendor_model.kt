package innovacia.co.id.trunkey1.model

data class vendor_model(
    var id_vendor   :String,
    var nama        :String,
    var distance    :String,
    var rating      :Int,
    var is_open     :String,
    var file_logo   :String,
    var category    :String,
    var alamat      :String,
    var harga_normal :Double,
    var tanggal_mulai :String,
    var tanggal_akhir : String,
    var tipe :String,
    var category_name :String,
    var tags : String,
    var regency : String,
    val harga_discount : Double
)