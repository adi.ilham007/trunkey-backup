package innovacia.co.id.trunkey1.model

data class WishlistModel(
    val id          :String,
    val slug        :String,
    val nama        :String,
    val harga       :Double,
    val foto        :String,
    val type        :String
)