package innovacia.co.id.trunkey1.model

data class RaportModel(
    val id_vendor   :String,
    val id_class    :String,
    val id_child    :String,
    val vendor_name :String,
    val class_name  :String,
    val child_name  :String,
    val raport: MutableList<String>
)