package innovacia.co.id.trunkey1.model

data class ForumModel (
        var id          :String,
        var id_user     :String,
        var userName    :String,
        var userPhoto   :String,
        var contentDesc :String,
        var contentPhoto:String,
        var comment     :String,
        var like        :String,
        var like_status :Int

)
