package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.customModelA
import innovacia.co.id.trunkey1.model.recomended_model
import kotlinx.android.synthetic.main.list_home1.view.*
import android.graphics.Paint.STRIKE_THRU_TEXT_FLAG



class home_customA_adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    private var mCustomAModel = mutableListOf<customModelA>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_home1, parent, false))
    }
    override fun getItemCount(): Int = mCustomAModel.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as vendorListViewHolder

        holder.bindView(mCustomAModel[position])

        holder.itemView.setOnClickListener { view ->
            holder.itemView.startAnimation(user_info.animasiButton)

            val id = mCustomAModel[position].id
            val slug = mCustomAModel[position].slug
            val type = mCustomAModel[position].type


            when (type) {
                "vendor" -> {
                    val i = Intent(view.context, detail_vendor::class.java)
                    i.putExtra("id", id)
                    view.context.startActivity(i)
                }
                "course" -> {
                    val i = Intent(view.context, Course::class.java)
                    i.putExtra("slug", slug)
                    view.context.startActivity(i)
                }
                "event" -> {
                    val i = Intent(view.context, Event::class.java)
                    i.putExtra("slug", slug)
                    view.context.startActivity(i)
                }

            }
        }
    }

    fun setList(listOfVendor: List<customModelA>) {
        this.mCustomAModel = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(listOfVendor: List<customModelA>) {
        this.mCustomAModel.addAll(listOfVendor)
        notifyDataSetChanged()
    }

    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(mCustomModel: customModelA) {
            val helper = user_info()

            itemView.name_value.text      = mCustomModel.nama
            itemView.category_value.text  = mCustomModel.type

//            if (mCustomModel.harga1 > mCustomModel.harga2) {
//                itemView.harga1.text        = helper.bank(mCustomModel.harga1)
//                itemView.harga1.paintFlags = itemView.harga1.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
//            } else {
//                itemView.harga1.visibility  = View.GONE
//            }
//            itemView.harga2.text         = helper.bank(mCustomModel.harga2)

            Glide.with(itemView.context)
                    .load(mCustomModel.foto)
                    .into(itemView.file_logo)
        }
    }

}