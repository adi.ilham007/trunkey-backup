package innovacia.co.id.trunkey1.model

data class AttendanceModel(
    val childName: String,
    val vendorName: String,
    val isCheckIN: String,
    val isCheckOUT: String,
    val timeIN: String,
    val timeOUT: String,
    val method: String
)