package innovacia.co.id.trunkey1

import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_change_parent_pin.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class ChangeParentPin : AppCompatActivity() {

    val mVolleyHelper = VolleyHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_parent_pin)
        header_name.text = resources.getString(R.string.change_parent_pin)

        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_pin.setOnClickListener {
            btn_pin.startAnimation(user_info.animasiButton)
            if (pinCheck()) {
                updatePin()
            }
        }

    }

    private fun pinCheck(): Boolean{
        var cek = 0

        if(pin.length() < 4){
            pin.error = "At least Four characters"
            cek++
        }
        if(confirm_pin.length() < 4){
            confirm_pin.error = "At least Four characters"
            cek++
        }
        if (pin.text.toString() != confirm_pin.text.toString()){
            Log.d("aim","confirm : $confirm_pin")
            Log.d("aim","password : $pin")
            pin.error = "Password mismatch"
            confirm_pin.error = "Password mismatch"
            cek++
        }
        if (cek == 0){
            return true
        }
        return false
    }

    private fun updatePin() {
        val errSection = "update pin"
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val body = JSONObject()
        body.put("pin",pinNow.text.toString())
        body.put("new_pin",pin.text.toString())
        body.put("new_pin_confirmation",confirm_pin.text.toString())

        val rq = object : JsonObjectRequest(Request.Method.POST,user_info.update_pin,body,Response.Listener { response ->
            dialog.dismiss()
            finish()
        },Response.ErrorListener { response ->
            dialog.dismiss()
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }
}
