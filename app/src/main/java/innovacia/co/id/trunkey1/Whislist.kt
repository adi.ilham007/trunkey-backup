package innovacia.co.id.trunkey1

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import innovacia.co.id.trunkey1.adapter.WishlistAdapter
import innovacia.co.id.trunkey1.adapter.home_customA_adapter
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.WishlistModel
import innovacia.co.id.trunkey1.model.customModelA
import kotlinx.android.synthetic.main.activity_whislist.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONObject

class Whislist : AppCompatActivity() {

    private val mVolleyHelper = VolleyHelper()
    private val helper = user_info()

    private val mCustomDataA = mutableListOf<WishlistModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_whislist)
        header_name.text = resources.getString(R.string.tx_wish)

        back_link.setOnClickListener {
            onBackPressed()
        }

        swipe_container.setOnRefreshListener {
            getWishlist()
        }

        getWishlist()
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadCastReceiver, IntentFilter("data"))
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                "data" -> {
                    val id = intent.getStringExtra("id")
                    val type = intent.getStringExtra("type")
                    syncWishList(id,type)
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(broadCastReceiver)
    }

    private fun getWishlist() {
        val errSection = "getWishlist"
        swipe_container.isRefreshing = true
        val request = object : JsonObjectRequest(Request.Method.POST,user_info.wishList,null, Response.Listener { response ->

            try {
                swipe_container.isRefreshing = false
                val dataObject = response.getJSONObject("data")
                val dataArray = dataObject.getJSONArray("data")

                for(i in 0 until dataArray.length()) {
                    val dataMain = dataArray.getJSONObject(i)
                    val id = dataMain.getString("id_type")
                    val type = dataMain.getString("type")

                    if (!dataMain.isNull("data")) {
                        val data = dataMain.getJSONObject("data")
                        var harga = 0.0

                        when(type) {
                            "course" ->  harga = data.getDouble("harga_diskon")
                            "event" ->  harga = data.getDouble("harga_diskon")
                            "product" ->  harga = data.getDouble("harga_diskon")
                        }

                        val slug = data.getString("slug")
                        val nama = data.getString("nama")
                        val foto = data.getString("photo_path")

                        val mItem = WishlistModel(id, slug, nama, harga, foto, type)
                        mCustomDataA.add(mItem)
                    }
                }

                recycler_wishlist?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                val adapter = WishlistAdapter()
                recycler_wishlist?.adapter = adapter
                adapter.setList(mCustomDataA)

            } catch (e: Exception) {
                Log.e("aim", "$errSection : $e")
            }

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }

    private fun syncWishList(id:String,type:String) {
        val errSection = "syncWishlist"
        swipe_container.isRefreshing = true

        val body = JSONObject()
        body.put("type",type)
        body.put("id",id)

        val request = object : JsonObjectRequest(Request.Method.POST,user_info.syncWishList,body, Response.Listener { response ->

            mCustomDataA.clear()
            getWishlist()

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }
}
