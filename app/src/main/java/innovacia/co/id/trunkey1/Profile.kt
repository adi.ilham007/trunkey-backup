package innovacia.co.id.trunkey1

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.mikephil.charting.charts.PieChart
import innovacia.co.id.trunkey1.adapter.ChildAdapter
import innovacia.co.id.trunkey1.database.Cart
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.ChildModel
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject

class Profile : AppCompatActivity() {

    val helper = user_info()

    // Recycler View
    private val childData = mutableListOf<ChildModel>()

    // Create Pin
    var need_add_pin = ""

    // Restore activity before create wallet
    private var lastActivity = false
    private var from = ""
    private var slugOrId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        header_name.text = resources.getString(R.string.profile_header)

        if (!user_info.pinStatus) changePin.visibility = View.GONE
        else changePin.visibility = View.VISIBLE

        // Restore activity setelah beli
        if (intent.hasExtra("lastActivity")) {
            lastActivity = true
            from = intent.getStringExtra("from")
            slugOrId = intent.getStringExtra("lastActivity")
            Log.d("aim", "lastActivity : $slugOrId")
        }

        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_edit.setOnClickListener {
            val i = Intent(this,Editprofile::class.java)
            startActivity(i)
        }

        changePass.setOnClickListener {
            val i = Intent(this,ChangePassword::class.java)
            startActivity(i)
        }

        changePin.setOnClickListener {
            val i = Intent(this,ChangeParentPin::class.java)
            startActivity(i)
        }

        profileLocal()

    }

    private fun profileLocal() {
        parent_name.text = user_info.name
        name_value.setText(user_info.name)
        email_value.setText(user_info.email)
        phone_value.setText(user_info.phone)
        gender_value.setText(user_info.gender)

        Glide.with(this).load(user_info.photo)
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .into(parent_photo)
    }
}
