package innovacia.co.id.trunkey1.adapter.filter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.detail_vendor
import innovacia.co.id.trunkey1.home_makanan
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.filter.AgeModel
import kotlinx.android.synthetic.main.list_filter.view.*
import kotlinx.android.synthetic.main.list_vendor.view.*


class AgeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var ageData = mutableListOf<AgeModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AgeDataViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_filter, parent, false))
    }
    override fun getItemCount(): Int = ageData.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as AgeDataViewHolder
        holder.bindView(ageData[position])

        holder.itemView.item.setOnClickListener { view ->
            holder.itemView.startAnimation(user_info.animasiButton)
            if (ageData[position].isSelected) {
                ageData[position].isSelected = false
                view.setBackgroundResource(R.drawable.textview_border_no)
                notifyItemChanged(position)
            }
            else {
                ageData[position].isSelected = true
                view.setBackgroundResource(R.drawable.textview_border_yes)
                notifyItemChanged(position)
            }
        }
    }
    fun setList(listOfVendor: List<AgeModel>) {
        this.ageData = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfVendor: List<AgeModel>) {
        this.ageData.addAll(listOfVendor)
        notifyDataSetChanged()
    }


    class AgeDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(vendorModel: AgeModel) {
            itemView.item.text      = vendorModel.name

            if (vendorModel.isSelected) {
                itemView.item.setBackgroundResource(R.drawable.textview_border_yes)
            } else {
                itemView.item.setBackgroundResource(R.drawable.textview_border_no)
            }

        }
    }
}