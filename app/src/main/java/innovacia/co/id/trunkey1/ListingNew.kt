package innovacia.co.id.trunkey1

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.CardView
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.helper.user_info
//import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.model.vendor_model
import kotlinx.android.synthetic.main.activity_listing_new.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ListingNew : AppCompatActivity(){

    val listAll: kotlin.collections.MutableList<String> = java.util.ArrayList()
    val helper = user_info()
    var tagData = ""

    private val lokasi = arrayOf("Your Location")
    private var lokasiString = ""
    var ads: IntArray = intArrayOf(R.drawable.ads_a, R.drawable.ads_b, R.drawable.ads_c, R.drawable.ads_d)
    val pageAdapter = ListingPagerAdapter(supportFragmentManager)
    private val vendorData = mutableListOf<vendor_model>()

    // Permissions
    private var lokasi_gps = Manifest.permission.ACCESS_FINE_LOCATION
    private var lokasi_net = Manifest.permission.ACCESS_COARSE_LOCATION

    var latitude = user_info.latitude
    var longitude = user_info.longitude
    var idCategory = ""
//    var tagParent = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listing_new)
        header_name?.text = resources.getString(R.string.explore_header)

        generate_view()

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        search.setOnEditorActionListener { v, actionId, event ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)

            val i = Intent(this, home_makanan::class.java)
            i.putExtra("search", "vendor_category")
            i.putExtra("text", search.text.toString())
            i.putExtra("category_id",idCategory)
            i.putExtra("category_name",header_name.text.toString())
//            i.putExtra("category",tagParent)
            startActivity(i)
            true
        }
    }

    fun generate_view() {
        when {
            intent.getStringExtra("category") == user_info.academic -> {
                idCategory = user_info.academic
                loadTag(idCategory)
                header_name?.text = "Academic"
            }
            intent.getStringExtra("category") == user_info.enrichment -> {
                idCategory = user_info.enrichment
                loadTag(idCategory)
                header_name?.text = "Enrichment"
            }
            intent.getStringExtra("category") == user_info.entertainment -> {
                idCategory = user_info.entertainment
                loadTag(idCategory)
                header_name?.text = "Activity"
            }
            intent.getStringExtra("category") == user_info.kidsmeal -> {
                idCategory = user_info.kidsmeal
                loadTag(idCategory)
                header_name?.text = "Kid's Meal"
            }
            intent.getStringExtra("category") == user_info.healthcare -> {
                idCategory = user_info.healthcare
                loadTag(idCategory)
                header_name?.text = "Health Care"
            }
            intent.getStringExtra("category") == user_info.shop -> {
                idCategory = user_info.shop
                loadTag(idCategory)
                header_name?.text = "Store"
            }
            intent.getStringExtra("category") == user_info.event -> {
                idCategory = user_info.event
                loadTag(idCategory)
                header_name?.text = "Event"
            }
            intent.getStringExtra("category") == user_info.parentcorner -> {
                idCategory = user_info.parentcorner
                loadTag(idCategory)
                header_name?.text = "Other"
            }
        }
    }

    fun loadTag(idCategory:String) {    // All Category
        Log.d("aim", "idCategory = $idCategory")
        val rqTag: RequestQueue = Volley.newRequestQueue(this)
        val srTag = object : StringRequest(Request.Method.POST,
                user_info.get_tag + idCategory, Response.Listener { response ->
            try {
//                Log.d("aim", response.toString())
                val fromServer = JSONObject(response)
                val data = fromServer.getJSONObject("data")
                tagData = data.toString()

                var arrayUniversal: JSONArray? = null
                if (data.optJSONArray("subjects") != null) {
                    arrayUniversal = data.getJSONArray("subjects")
                } else if (data.optJSONArray("tags") != null) {
                    arrayUniversal = data.getJSONArray("tags")
                }

                // ALL
                pageAdapter.add(FragmentListing.newInstance(0,idCategory.toInt(),0,11), "All")

                if (arrayUniversal != null) {
                    for (i in 0 until arrayUniversal.length()) {
                        val jo = arrayUniversal.getJSONObject(i)
                        val idTag = jo.getInt("id")
                        val nama = jo.getString("nama")
                        val parent = jo.getString("id_m_eca_vendors_type")

                        pageAdapter.add(FragmentListing.newInstance(i + 1,idCategory.toInt(),idTag,11), nama)
                    }

                    // ============ tab_tag ============
                    viewpager_main.adapter = pageAdapter
                    tabs_main.setupWithViewPager(viewpager_main)
                }
            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }
        }, Response.ErrorListener { response ->
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {}
        rqTag.add(srTag)
    }

    fun blogCategory() {    // Parent Corner
        val rqTag: RequestQueue = Volley.newRequestQueue(this)
        val srTag = object : StringRequest(Request.Method.POST,
                user_info.blog_category, Response.Listener { response ->
            try {
//                Log.d("aim", response.toString())
                val fromServer = JSONObject(response)
                val dataArray = fromServer.getJSONArray("data")

                // ALL
                pageAdapter.add(FragmentListing.newInstance(0,0,0,22), "All")

                if (dataArray != null) {
                    for (i in 0 until dataArray.length()) {
                        val jo = dataArray.getJSONObject(i)
                        val idTag = jo.getInt("id")
                        val nama = jo.getString("nama")

                        pageAdapter.add(FragmentListing.newInstance(i + 1,0,idTag,22), nama)
                    }

                    // ============ tab_tag ============
                    viewpager_main.adapter = pageAdapter
                    tabs_main.setupWithViewPager(viewpager_main)
                    parentPage()
                }

            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }
        }, Response.ErrorListener { response ->
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {}
        rqTag.add(srTag)
    }

    fun parentPage(){
        Log.d("aim","TAB")
        val tagParent = intent.getStringExtra("parentTag")
        Log.d("aim","tag : $tagParent")
        if (tagParent != null) {

            when (tagParent) {
                "1" -> viewpager_main.setCurrentItem(1,true) // Must read
                "3" -> viewpager_main.setCurrentItem(2,true) // Tips
                "4" -> viewpager_main.setCurrentItem(3,true) // Stories
                "7" -> viewpager_main.setCurrentItem(4,true) // Must Watch
            }
        }
    }
}
