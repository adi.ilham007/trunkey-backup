package innovacia.co.id.trunkey1

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_top_up__instruction.*
import kotlinx.android.synthetic.main.sub_header.*

class  TopUp_Instruction : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_up__instruction)
        header_name?.text = resources.getString(R.string.topup_instruction_header)

        back_link.setOnClickListener{
            onBackPressed()
        }

        copy_lay.setOnClickListener {
            copy_lay.startAnimation(user_info.animasiButton)
            val clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val myClip = ClipData.newPlainText("label", virtual.text)
            clipboard.primaryClip = myClip
            Toast.makeText(this, "Virtual code copied", Toast.LENGTH_SHORT).show()
        }


        val kodeVA :String = intent.getStringExtra("kodeVA")
        val nama:String = intent.getStringExtra("nama")
        val instruksi:String = intent.getStringExtra("instruction")

        Log.d("aim","instruksi : $kodeVA,$nama, $instruksi")

        virtual.text = kodeVA
        jenis_bank.text = nama

        val webHtml =
                "<html><head> <style>iframe{width:100%}</style> </head>" +
                        "<body>$instruksi</body></html>"
        intruksi.loadData(webHtml, "text/html", "UTF-8")

    }

}
