package innovacia.co.id.trunkey1.database.cart

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "cart_table")
class CartEntity (
        @PrimaryKey(autoGenerate = true)
        var id                  : Int,
        var merchantId          : Int,
        var merchantSlug        : String,
        var merchantNama        : String,
        var merchantAlamat      : String,
        var merchantLogo        : String,
        var tanggal             : String,
        var itemId              : Int,
        var itemNama            : String,
        var itemMember          : String,
        var itemDesc            : String,
        var itemType            : String,
        var itemPrice           : Double,
        var itemCashBack        : Double,
        var itemDiscount        : Int,
        var itemQty             : Int,
        var itemSubTotal        : Double,
        var isSelected          : Boolean
)
