package innovacia.co.id.trunkey1

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.adapter.AdapterGallery
import innovacia.co.id.trunkey1.adapter.detail_vendor.EventAdapter
//import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import innovacia.co.id.trunkey1.adapter.detail_vendor.ReviewAdapter
import innovacia.co.id.trunkey1.adapter.detail_vendor.CourseAdapter
import innovacia.co.id.trunkey1.adapter.detail_vendor.ProductAdapter
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.GalleryModel
import innovacia.co.id.trunkey1.model.ReviewModel
import innovacia.co.id.trunkey1.model.detail_model
import innovacia.co.id.trunkey1.model.event_model
import kotlinx.android.synthetic.main.activity_detail_vendor.*
import kotlinx.android.synthetic.main.activity_detail_vendor.direction
import kotlinx.android.synthetic.main.activity_detail_vendor.*
import kotlinx.android.synthetic.main.activity_detail_vendor.logo_lay
import kotlinx.android.synthetic.main.activity_detail_vendor.nama_vendor
import kotlinx.android.synthetic.main.activity_detail_vendor.recycler_gallery
import kotlinx.android.synthetic.main.activity_detail_vendor.share
import kotlinx.android.synthetic.main.activity_detail_vendor.swipe_container
import kotlinx.android.synthetic.main.activity_detail_vendor.textView32
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import kotlin.Exception

class detail_vendor : AppCompatActivity() {

    private var phone_permission = Manifest.permission.CALL_PHONE
    private val listGallery = mutableListOf<GalleryModel>()
    private val detailService = mutableListOf<detail_model>()
    private val listEvent = mutableListOf<event_model>()
    private val reviewVendor = mutableListOf<ReviewModel>()

    val helper = user_info()
    var idIntent = ""

    var idVendor = ""
    var namaVendor = ""
    var alamatVendor = ""
    var imgVendor = ""

    var latitude = 0.0
    var longitude = 0.0
    var phone = ""
    var email = ""

//    val bintangLima = textView114
//    val bintangEmpat= textView30
//    val bintangTiga = textView32
//    val bintangDua  = textView44
//    val bintangSatu = textView46
//
//    val barLima = progressBar
//    val barEmpat= progressBar2
//    val barTiga = progressBar3
//    val barDua  = progressBar4
//    val barSatu = progressBar5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_vendor)
        header_name?.text = resources.getString(R.string.vendor_header)

        // ========================================================================================= Footer onClick
//        handlingFooter()

        generate_view()

        about_lay.visibility = View.GONE
        gallery_lay.visibility = View.GONE
        course_lay.visibility = View.GONE
        product_lay.visibility = View.GONE
        event_lay.visibility = View.GONE
        offer_lay.visibility = View.GONE

        review_lay.visibility = View.GONE
        detail_review.visibility = View.GONE
        separator_review.visibility = View.GONE

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)

            onBackPressed()
        }

        telepon.setOnClickListener {
            phoneCall()
        }

        swipe_container.setOnRefreshListener {
            detailVendor(idIntent)
        }

        direction.setOnClickListener {
            if (latitude != 0.0 && longitude != 0.0) {
                val i = Intent(this, MapsActivity::class.java)
                i.putExtra("nama", "test")
                i.putExtra("lat", latitude)
                i.putExtra("lon", longitude)
                startActivity(i)
            } else {
                Toast.makeText(this, "Location not available", Toast.LENGTH_LONG).show()
            }
        }

        share.setOnClickListener {
            val message = "Hii.., We have key for successful parenting https://play.google.com/store/apps/details?id=innovacia.co.id.com.innovacia.trunkey1"

            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT,message)
            intent.type = "text/plain"
            try {
                startActivity(Intent.createChooser(intent, "Share to :"))
            }catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "There are no share clients installed.", Toast.LENGTH_SHORT).show()
            }
        }

//        chat.setOnClickListener {
//            val i = Intent(Intent.ACTION_SEND)
//            i.type = "message/rfc822"
//            i.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
//            i.putExtra(Intent.EXTRA_SUBJECT, "Trunkey ECA")
////            i.putExtra(Intent.EXTRA_TEXT, "Hii.. Mom, please open https://eca.trunkey.id/")
//            try {
//                startActivity(Intent.createChooser(i, "Send mail..."))
//            } catch (ex: android.content.ActivityNotFoundException) {
//                Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
//            }
//
//        }

        // ========================================================================================= Review
        review.setOnClickListener {
            review.startAnimation(user_info.animasiButton)
            if (user_info.loginStatus) {
                val i = Intent(this, Review::class.java)
                i.putExtra("id", idVendor)
                i.putExtra("nama", namaVendor)
                i.putExtra("img", imgVendor)
                i.putExtra("tipe", "vendor")
                startActivity(i)
            } else {
                val i = Intent(this, login::class.java)
                i.putExtra("back", "detail_vendor")
                startActivity(i)
            }
        }
    }

    fun generate_view(){
        if (intent.hasExtra("data") && intent.getStringExtra("data") != null) {
            // From notification / FCM
            idIntent = intent.getStringExtra("data")
            detailVendor(idIntent)
        }
        else if (intent.hasExtra("id") && intent.getStringExtra("id") != null){
            idIntent = intent.getStringExtra("id")
            detailVendor(idIntent)
            Log.d("aim",idIntent)
        }
    }

    fun phoneCall() = runWithPermissions(phone_permission) {
        try {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:+$phone")
            startActivity(intent)
        }catch (e: Exception) {
            Log.e("aim ", "phone e: $e")
//            Toast.makeText(this,"Data tidak tersedia",Toast.LENGTH_LONG).show()
        }


        //call lama
//        try {
//            val intent = Intent(Intent.ACTION_CALL)
//            intent.data = Uri.parse("tel:$phone")
//            startActivity(intent)
//        }catch (e: Exception) {
//            Log.e("aim ", "phone e: $e")
////            Toast.makeText(this,"Data tidak tersedia",Toast.LENGTH_LONG).show()
//        }
    }

    fun detailVendor(idIntent:String){
        val errSection = "detailVendor :"
        swipe_container?.isRefreshing = true

        separator_about.visibility = View.GONE
        separator_gallery.visibility = View.GONE
        separator_course.visibility = View.GONE
        separator_product.visibility = View.GONE
        separator_event.visibility = View.GONE
        separator_offer.visibility = View.GONE
        separator_review.visibility = View.GONE

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.detail_vendor+ idIntent, Listener { response ->
            //========================================================================================= data from server
            swipe_container?.isRefreshing = false
            Log.d("aim","$errSection $response")

            try {
                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data")
                val id = data.getInt("id")
                val img = data.getString("thumbnail_logo")
                val banner = data.getString("wide_logo")
                val nama = data.getString("nama")
                val rating = data.getString("rating")
                val alamat = data.getString("alamat")
                kota.text = helper.textHtml(alamat)

                // Global Variable
                idVendor = id.toString()
                namaVendor = nama
                alamatVendor = helper.textHtml(alamat)
                imgVendor = img
                latitude = data.getDouble("latitude")
                longitude = data.getDouble("longitude")

                Glide.with(this).load(banner).into(banner_lay) // Banner
                Glide.with(this).load(img).into(logo_lay) // Logo
//                txRating.text = rating // Rating

                nama_vendor.text = nama

                email = data.getString("email")
                if (email.isEmpty()) {
                    mail.visibility = View.GONE
                }

                if (latitude != 0.0 && longitude != 0.0) {
                    direction.visibility = View.GONE
                }

                val tlpArray = data.getJSONArray("phones")
                if (tlpArray.length() != 0) {
                    val pivot = tlpArray.getJSONObject(0).getJSONObject("pivot")
                    phone = pivot.getString("telp").replace("[^\\\\d.-]", "")
                    Log.d("aim", "telepon : $phone")
                } else {
                    telepon.visibility = View.GONE
                }



                val aboutHtml = data.getString("keterangan")
                if (aboutHtml.isNotEmpty()) {
//                    about.text = helper.textHtml(aboutHtml)
                    about.text = Html.fromHtml(aboutHtml)
                    about_lay.visibility = View.VISIBLE
                    separator_about.visibility = View.VISIBLE
                }

                if (!data.isNull("photos")) {
                    val galleryArray = data.getJSONArray("photos")
                    user_info.gallery = galleryArray
                    if (galleryArray.length() != 0) {
                        for (i in 0 until galleryArray.length()) {
                            val jo = galleryArray.getJSONObject(i)
                            val photoId = jo.getString("id")
                            val photoValue = jo.getString("photo_path")

                            val mItem = GalleryModel(photoId,photoValue)
                            listGallery.add(mItem)
                        }
                        gallery_lay.visibility = View.VISIBLE
                        separator_gallery.visibility = View.VISIBLE
                        // ============ recycler_course ============
                        recycler_gallery?.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL,false)
                        val galleryAdapter = AdapterGallery()
                        recycler_gallery?.adapter = galleryAdapter
                        galleryAdapter.setList(listGallery)
                    }

                }

                if (!data.isNull("course")) {
                    val courseArray = data.getJSONArray("course")
                    if (courseArray.length() != 0) {
                        for (i in 0 until courseArray.length()) {
                            val jo = courseArray.getJSONObject(i)
                            val slug = jo.getString("slug")


                            val courseName = jo.getString("nama")
                            val coursePrice = jo.getDouble("total")
                            val courseImage = jo.getString("thumbnail_logo")

                            val mItem = detail_model(slug, courseName, coursePrice, courseImage)
                            detailService.add(mItem)
                        }
                        course_lay?.visibility = View.VISIBLE
                        separator_course?.visibility = View.VISIBLE
                        // ============ recycler_course ============
                        recycler_course?.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                        val vendorAdapter = CourseAdapter()
                        recycler_course?.adapter = vendorAdapter
                        vendorAdapter.setList(detailService)
                        detailService.clear()
                    }
                }

                if (!data.isNull("products")) {
                    val productArray = data.getJSONArray("products")
                    if (productArray.length() != 0) {
                        for (i in 0 until productArray.length()) {
                            val jo = productArray.getJSONObject(i)
                            val slug = jo.getString("slug")

                            val productName = jo.getString("nama")
                            val productPrice = jo.getDouble("harga")
                            val productImage =jo.getString("file")

                            val mItem = detail_model(slug, productName, productPrice, productImage)
                            detailService.add(mItem)
                        }
                        product_lay.visibility = View.VISIBLE
                        separator_product.visibility = View.VISIBLE
                        // ============ recycler_products ============
                        recycler_product.layoutManager = GridLayoutManager(this,2)
                        val vendorAdapter = ProductAdapter()
                        recycler_product.adapter = vendorAdapter
                        vendorAdapter.setList(detailService)
                        detailService.clear()
                    }
                }

                if (!data.isNull("event")) {
                    val eventArray = data.getJSONArray("event")
                    if (eventArray.length() != 0) {
                        for (i in 0 until eventArray.length()) {
                            val jo = eventArray.getJSONObject(i)
                            val slug = jo.getString("slug")
                            val eventName = jo.getString("nama")
                            var desc = jo.getString("keterangan")
                            desc = helper.textHtml(desc)

                            val dateApi = jo.getString("tanggal_mulai")
                            val dateAll = dateApi.split(" ")
                            val date =  dateAll[0]
                            val time = dateAll[1]

                            val lokasi = jo.getString("place_nama")

                            val kapasitas = jo.getInt("capacity")
                            val partisipan = jo.getInt("participant")
                            val kuota = (kapasitas - partisipan).toString()

                            val eventImage = jo.getString("default_logo")

                            val mItem = event_model(slug, eventName, desc, date,time,lokasi,"$kuota Seat available",eventImage)
                            listEvent.add(mItem)
                        }
                        event_lay?.visibility = View.VISIBLE
                        separator_event?.visibility = View.VISIBLE
                        // ============ recycler_products ============
                        recycler_event?.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
                        val eventAdapter = EventAdapter()
                        recycler_event?.adapter = eventAdapter
                        eventAdapter.setList(listEvent)
                        listEvent.clear()
                    }
                }

                val totalRating = data.getDouble("total_rating")
                val df = DecimalFormat("#.##")
                rating2?.text = df.format(totalRating)
                rating_img2?.rating = totalRating.toFloat()

//                getReview(id)

            }catch (e: JSONException) {
                Log.d("aim","$errSection $e")
            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse

            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }
        }
        rq.add(sr)
        sr.retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)
    }

    fun getReview(idVendor:Int){
        val errSection = ""
        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.listReview_vendor+idVendor, Response.Listener { response ->
            //========================================================================================= data from server
            swipe_container?.isRefreshing = false
            Log.d("aim","$errSection $response")
//            try {
                val respon = JSONObject(response)
                val data = respon.getJSONObject("data")
                val detailReview = data.getJSONObject("detail_review")

                // Rating Angka
                val limaObject = detailReview.getJSONObject("5")
                textView114.text = limaObject.getInt("jumlah").toString()
                val empatObject = detailReview.getJSONObject("4")
                textView30.text = empatObject.getInt("jumlah").toString()
                val tigaObject = detailReview.getJSONObject("3")
                textView32.text = tigaObject.getInt("jumlah").toString()
                val duaObject = detailReview.getJSONObject("2")
                textView44.text = duaObject.getInt("jumlah").toString()
                val satuObject = detailReview.getJSONObject("1")
                textView46.text = satuObject.getInt("jumlah").toString()

                val review = data.getJSONObject("reviews")
                val reviewArray = review.getJSONArray("data")

                if (reviewArray.length() != 0) {

                    val totalReview = reviewArray.length()
                    textView23.text = totalReview.toString().plus(" Reviews")

                    progressBar.max = totalReview
                    progressBar.progress = limaObject.getInt("jumlah")
                    progressBar2.max = totalReview
                    progressBar2.progress = empatObject.getInt("jumlah")
                    progressBar3.max = totalReview
                    progressBar3.progress = tigaObject.getInt("jumlah")
                    progressBar4.max = totalReview
                    progressBar4.progress = duaObject.getInt("jumlah")
                    progressBar5.max = totalReview
                    progressBar5.progress = satuObject.getInt("jumlah")

                    for (i in 0 until reviewArray.length()) {
                        if (i <= 5) {
                            val jo = reviewArray.getJSONObject(i)
                            val id = jo.getString("id")

                            val member = jo.getJSONObject("member")
                            val nama = member.getString("nama")
                            val img = member.getString("photo")

                            val review = jo.getString("keterangan")
                            val rating = jo.getString("rating")
                            val tanggal = jo.getString("created")

                            val mItem = ReviewModel(id, nama, review, rating, tanggal, img)
                            reviewVendor.add(mItem)
                        }
                    }
                    review_lay.visibility = View.VISIBLE
                    detail_review.visibility = View.VISIBLE
                    separator_review.visibility = View.VISIBLE
                    // ============ recycler_review ============
                    recycler_review.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
                    val reviewAdapter = ReviewAdapter()
                    recycler_review.adapter = reviewAdapter
                    reviewAdapter.setList(reviewVendor)
                    reviewVendor.clear()
                }

//            } catch (e: JSONException) {
//                Toast.makeText(this, "Data Tidak Tersedia", Toast.LENGTH_LONG).show()
//            }

        }, Response.ErrorListener { response ->
            //========================================================================================= error handling
            swipe_container?.isRefreshing = false
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail,Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Accept"] = "application/json"
                headers["Authorization"] = "Bearer " + user_info.token
                return headers
            }
        }
        rq.add(sr)
    }
}

