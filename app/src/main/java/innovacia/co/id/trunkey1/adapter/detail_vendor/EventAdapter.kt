package innovacia.co.id.trunkey1.adapter.detail_vendor

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.Event
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.event_model
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_event.view.*

class EventAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var eventList = mutableListOf<event_model>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return eventListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_event, parent, false))
    }
    override fun getItemCount(): Int = eventList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as eventListViewHolder

        viewHolder.bindView(eventList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            //Click listener
            val slug = eventList[position].slug
            val i = Intent(view.context, Event::class.java)
            i.putExtra("slug", slug)
            view.context.startActivity(i)

        }

    }

    fun setList(listOfEvent: List<event_model>) {
        this.eventList = listOfEvent.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfEvent: List<event_model>) {
        this.eventList.addAll(listOfEvent)
        notifyDataSetChanged()
    }


    class eventListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(eventModel: event_model) {
            itemView.nama.text          = eventModel.nama
            itemView.keterangan.text    = eventModel.keterangan
            itemView.date.text          = eventModel.date
            itemView.time.text          = eventModel.time
            itemView.lokasi.text        = eventModel.lokasi
            itemView.kuota.text         = eventModel.kuota

            Glide.with(itemView.context).load(eventModel.img).into(itemView.image)
        }
    }
}