package innovacia.co.id.trunkey1


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.BlogAdapter
import innovacia.co.id.trunkey1.adapter.vendor_adapter
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.model.blog_model
import innovacia.co.id.trunkey1.model.vendor_model
import kotlinx.android.synthetic.main.fragment_fragment_listing.*
import org.json.JSONObject
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.sub_filter_sort.*
import kotlinx.android.synthetic.main.sub_filter_sort.view.*
import org.json.JSONException
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class FragmentListing : Fragment() {

    val mVolleyHelper = VolleyHelper()

    lateinit var viewContext: Context
    lateinit var appContext: Context

    private val vendorData = mutableListOf<vendor_model>()
    private val blogData = mutableListOf<blog_model>()
    private val helper = user_info()

    private var loadMore = false    // First load
    private var lastData = false    // Pagination
    private var skip = 0            // Pagination

    // Filter
    private var filterStatus = false
    private var childID = ""
    private var grade = ""
    private var area = ""
    private var min = 0
    private var max = 0
    private var rating = ""
    private var show = "vendor" // default type (vendor,course,event,product)

    // Sort
    private var sortStatus = false
    private var price = ""

    var mType = 0

    var category = 0    // Vendor
    var tag  = 0        // Blog

    var page = 1
    var lastPage = 0

    companion object {
        val PAGE_NUM = "PAGE_NUM"
        val CATEGORY = "CATEGORY"
        val TAG = "TAG"
        val TYPE = "TYPE"
        fun newInstance(page: Int,category:Int,tag:Int,type:Int): FragmentListing {
            val fragment = FragmentListing()
            val args = Bundle()
//            args.putInt(PAGE_NUM, page)
            args.putInt(CATEGORY,category)
            args.putInt(TAG,tag)
            args.putInt(TYPE,type)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appContext = activity!!.applicationContext

//        val a = activity?.applicationContext
//        val b = this.context!!
        return inflater.inflate(R.layout.fragment_fragment_listing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadMore = false
        viewContext = view.context
        if (activity != null) {
            category = arguments!!.getInt(CATEGORY)
            tag = arguments!!.getInt(TAG)
            mType = arguments!!.getInt(TYPE)
            skip = 0    // Reset when move tag
            lastPage = 0

            Log.d("aim","categori listing : $category")

            vendorData.clear()
            blogData.clear()

            if (category != 0) {
                Log.d("aim", "Tag = $tag, Category $category")
                loadVendor(tag, category)
            } else {
                Log.d("aim", "Tag = $tag, Category Parent's Corner")
                filter_lay.visibility = View.GONE
                loadBlog(tag)
            }
        }

        swipe_container.setOnRefreshListener {
            if (category != 0) {
                Log.d("aim", "Tag = $tag, Category $category")
                skip = 0
                vendorData.clear()
                loadVendor(tag, category)
            } else {
                Log.d("aim", "Tag = $tag, Category Parent's Corner")
                page = 1
                lastPage = 0
                loadMore = false    // First load
                lastData = false    // Pagination
                blogData.clear()
                loadBlog(tag)
            }
        }

        rvListener()

        btn_filter.setOnClickListener {
            val intent = Intent(activity,FilterNew::class.java)
            if (filterStatus) {
                intent.putExtra("grade",grade)
                intent.putExtra("city",area)
                intent.putExtra("price_min","$min.0")
                intent.putExtra("price_max","$max.0")
                intent.putExtra("rating",rating)
                intent.putExtra("type",show)
            }
            startActivityForResult(intent,2121)
        }

        btn_sort.setOnClickListener {
            val intent = Intent(activity,Sort::class.java)
            if (sortStatus) {
                intent.putExtra("price",price)
            }
            startActivityForResult(intent,2121)
        }

    }

//    override fun onResume() {
//
//        Log.d("aim", "Tag = $tag, Category $category")
//        loadVendor(tag, category)
//
//        super.onResume()
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && data != null) {
            when {
                data.hasExtra("filter") -> {
                    filterStatus = true
                    ic_filter.setBackgroundResource(R.drawable.ic_filter_orange)
                    childID = data.getStringExtra("child")
                    grade = data.getStringExtra("ageSelected")
                    area = data.getStringExtra("area")
                    min = data.getIntExtra("min",0)
                    max = data.getIntExtra("max",0)
                    rating = data.getStringExtra("rating")
                    show = data.getStringExtra("show")

                    Log.d("aim", "Tag = $tag, Category $category")
                    skip = 0
                    vendorData.clear()
                    loadVendor(tag, category)
                }
                data.hasExtra("sort") -> {
                    sortStatus = true
                    ic_sort.setBackgroundResource(R.drawable.ic_sort_orange)
                    price = data.getStringExtra("price")

                    Log.d("aim", "Tag = $tag, Category $category")
                    skip = 0
                    vendorData.clear()
                    loadVendor(tag, category)
                }
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED && data != null) {
            when {
                data.hasExtra("resetFilter") -> {
                    filterStatus = false
                    ic_filter.setBackgroundResource(R.drawable.ic_filter)
                    grade = ""
                    area = ""
                    min = 0
                    max = 0
                    rating = ""
                    show = ""

                    Log.d("aim", "Tag = $tag, Category $category")
                    skip = 0
                    vendorData.clear()
                    loadVendor(tag, category)
                }

                data.hasExtra("resetSort") -> {
                    sortStatus = false
                    ic_sort.setBackgroundResource(R.drawable.ic_sort)

                    Log.d("aim", "Tag = $tag, Category $category")
                    skip = 0
                    vendorData.clear()
                    loadVendor(tag, category)
                }
            }
        }
    }

    fun update() {
        vendorData.clear()
        blogData.clear()

        if (category != 8) {
            loadVendor(tag, category)
        } else {
            loadBlog(tag)
        }
    }

    private fun rvListener() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition

                Log.d("aim","type : $TYPE")

                if (isLastPosition && !lastData && mType == 11) {     // Bukan Blog
                    progress_bar.visibility = View.VISIBLE
                    loadMore = true
                    skip += 10
                    loadVendor(tag, category)
                    Log.d("aim","take: 10, skip: $skip")
                }else if(isLastPosition && !lastData && page < lastPage && mType == 22) {     // Blog
                    page ++
                    progress_bar.visibility = View.VISIBLE
                    loadMore = true
                    loadBlog(tag)
                }
            }
        })
    }

    fun loadVendor(tag:Int,category:Int) {
        val errSection = "loadVendor :"
        swipe_container?.isRefreshing = true

//        val url = if (childID.isEmpty()) user_info.listing else user_info.listingAuth
//        Log.d("aim","url listing : $url")

        val rq: RequestQueue = Volley.newRequestQueue(activity)
        val sr = object : StringRequest(Request.Method.POST, user_info.listing, Response.Listener { response ->
            Log.d("aim", "$errSection $response")
            Log.d("aimo","hello world")
            try {
                swipe_container?.isRefreshing = false

                val jsonObject = JSONObject(response)
                val jsonArray = jsonObject.getJSONArray("list")

                if (jsonArray.length() == 0 && skip == 0){          // Data tidak tersedia
                    root_lay?.visibility = View.VISIBLE
                    root_lay?.setBackgroundResource(R.drawable.ic_oops)
                } else if (jsonArray.length() == 0 && skip != 0){     // Last data
                    root_lay?.setBackgroundResource(R.color.textWhite)
                    lastData = true
                } else {
                    root_lay?.setBackgroundResource(R.color.textWhite)

                    for (i in 0 until jsonArray.length()) {
                        val jo = jsonArray.getJSONObject(i)

                        var jarak: String
                        if (jo.isNull("distance")) {
                            jarak = "Unknown"
                        } else jarak = jo["distance"].toString() + " Km"

                        val tipe = jo.getString("type")
                        var mixID = ""

                        when (tipe) {
                            "vendor" -> {
                                mixID = jo.getString("id_vendor")
                            }
                            "course" -> {
                                mixID = jo.getString("slug")
                            }
                            "event" -> {
                                mixID = jo.getString("slug")
                            }
                            "product" -> {
                                mixID = jo.getString("slug")
                            }
                        }

                        val nama = jo.getString("nama")
                        val img = jo.getString("thumbnail_logo")
                        val categoriID = jo.getString("vendor_type")

                        var alamat = jo.getString("alamat_vendor")
                        alamat = helper.textHtml(alamat).trim()

                        val tagsArray = jo.optJSONArray("tags")
                        var tagsName = ""

                        if (tagsArray.length() != 0) {
                            for (y in 0 until tagsArray.length()) {
                                val tagsObject = tagsArray.getJSONObject(y)
                                val tags = tagsObject.optJSONObject("tags")
                                try {
                                    tagsName = tags.getString("nama")
                                } catch (e: Exception) {
                                    Log.e("aim", "Err array : $e")
                                }
                            }
                        }

                        val regency = jo.optJSONObject("regency")
                        var regencyName = ""
                        if(regency != null){
                            try {
                                regencyName = regency.getString("name")
                            }catch (e: Exception) {
                                Log.e("aim", "Err array : $e")
                            }
                        }

                        val tanggal_mulai = jo.getString("tanggal_mulai")
                        val tanggal_akhir = jo.getString("tanggal_akhir")
                        val category_name = jo.getString("vendor_type_nama")

                        var harga_normal = 0.0
                        if(jo.optString("harga_normal") != "null") {
                            harga_normal = jo.getDouble("harga_normal")
                        }
                        val mItem = vendor_model(mixID, nama, jarak, 0, "", img, categoriID,alamat, harga_normal,tanggal_mulai,tanggal_akhir,tipe,category_name,tagsName,regencyName,0.0)
                        vendorData.add(mItem)
                    }
                }

                // ============ recycler_category ============
                val vendorAdapter = vendor_adapter()
                when (category) {
                    6 -> { vendor_adapter.TYPE_OF_VIEW = 1 }
                    else -> { vendor_adapter.TYPE_OF_VIEW = 0 }
                }

                if (!loadMore) {
                    recyclerView?.layoutManager = LinearLayoutManager(activity, OrientationHelper.VERTICAL, false)
                    recyclerView?.adapter = vendorAdapter
                    vendorAdapter.setList(vendorData)
                    progress_bar?.visibility = View.GONE
                    swipe_container?.isRefreshing = false
                } else {
                    val recyclerViewState = recyclerView.layoutManager?.onSaveInstanceState()
                    recyclerView?.adapter = vendorAdapter
                    vendorAdapter.addData(vendorData)
                    recyclerView?.layoutManager!!.onRestoreInstanceState(recyclerViewState)
                    progress_bar?.visibility = View.GONE
                    Log.d("aimo","hello world")
                }
            } catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }
        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            progress_bar?.visibility = View.GONE

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(activity,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(activity, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            @SuppressLint("SimpleDateFormat")
            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["take"] = "10"
                map["skip"] = "$skip"

//                map["children"] = "[$childID]"
                map["category"] = "[$category]"

                if (category == 3) {    // Subject, khusus untuk academic
                    if (tag != 0) {     // All tag = 0
                        map["subject"] = "[$tag]"
                    }
                } else {
                    if (tag != 0) {
                        map["tags"] = "[$tag]"
                    }
                }

                if (category == 6) {
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    val currentDate = sdf.format(Date())
                    Log.d("aim","date: $currentDate")
                    map["date_start"] = currentDate
                }

                // Location
                if (user_info.latitude != 0.0 && user_info.longitude != 0.0) {
                    map["nearme"] = "1"
                    map["latitude"] = user_info.latitude.toString()
                    map["longitude"] = user_info.longitude.toString()
                }

                // Filter
                if (filterStatus) {
                    map["grade"] = grade
                    map["city"] = area
                    map["price_min"] = "$min.0"
                    map["price_max"] = "$max.0"
//                    map["ratings"] = rating
                    map["type"] = show
                }

                // Sort
                if (sortStatus) {
                    map["sort_by"] = "price"
                    map["sort_method"] = price
                }

                Log.d("aim", "$errSection -> $map")
                return map
            }

//            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
//        sr.retryPolicy = DefaultRetryPolicy(0,-1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        rq.add(sr)

    }

    fun loadBlog(tag:Int) {
        val errSection = "loadBlog :"
        swipe_container?.isRefreshing = true

        val sr = object : StringRequest(Request.Method.POST, user_info.blog+"?page="+page, Response.Listener { response ->
            try {
                swipe_container?.isRefreshing = false
                Log.d("aim", "$errSection $response")

                user_info.tagSearch = ""

                val jsonObject = JSONObject(response)
                val dataObject = jsonObject.getJSONObject("data")
                val dataArray = dataObject.getJSONArray("data")

                lastPage = dataObject.getInt("last_page")

                if (dataArray.length() == 0 && skip == 0){          // Data tidak tersedia
                    root_lay?.visibility = View.VISIBLE
                    root_lay?.setBackgroundResource(R.drawable.ic_oops)
                } else if (dataArray.length() == 0 && skip != 0){     // Last data
                    root_lay?.setBackgroundResource(R.color.textWhite)
                    lastData = true
                } else {
                    root_lay?.setBackgroundResource(R.color.textWhite)

                    if (tag != 7) {     // Blog Artikel
                        for (i in 0 until dataArray.length()) {
                            val jo = dataArray.getJSONObject(i)
                            val slug = jo.getString("slug")
                            val nama = jo.getString("nama")
                            val img = jo.getString("image_path")

                            var category = ""
                            if (!jo.isNull("category")) {
                                val dataCategory = jo.getJSONObject("category")
                                category = dataCategory.getString("nama")
                            }

                            val aboutHtml = jo.getString("keterangan")
                            val aboutText = helper.textHtml(aboutHtml)

                            val mItem = blog_model("",slug, nama, aboutText, img, category)
                            blogData.add(mItem)
                        }
                    } else {        // Blog Video
                        for (i in 0 until dataArray.length()) {
                            val jo = dataArray.getJSONObject(i)
                            val slug = jo.getString("slug")
                            val nama = jo.getString("nama")
                            val keterangan = jo.getString("keterangan")
                            val img = jo.getString("image_path")
                            val idVideoHtml = jo.getString("keterangan")

                            val idVideo = helper.textHtml(idVideoHtml)

                            var category = ""
                            if (!jo.isNull("category")) {
                                val dataCategory = jo.getJSONObject("category")
                                category = dataCategory.getString("nama")
                            }

                            val mItem = blog_model(idVideo, slug, nama, "", idVideo, category)
                            blogData.add(mItem)
                        }
                    }
                }

                // ============ recycler_parent ============
                val adapter = BlogAdapter()

                if (!loadMore) {
                    recyclerView?.layoutManager = LinearLayoutManager(activity, OrientationHelper.VERTICAL, false)
                    recyclerView?.adapter = adapter
                    adapter.setList(blogData)
                    progress_bar?.visibility = View.GONE
                }
                else {
                    val recyclerViewState = recyclerView.layoutManager?.onSaveInstanceState()
                    recyclerView?.adapter = adapter
                    adapter.addData(blogData)
                    recyclerView?.layoutManager!!.onRestoreInstanceState(recyclerViewState)
                    progress_bar?.visibility = View.GONE
                }
            } catch (e: JSONException) {
                Log.e("aim", "$errSection $e")
            }

        }, Response.ErrorListener { response ->
            swipe_container?.isRefreshing = false
            progress_bar?.visibility = View.GONE

            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(activity,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(activity, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "$errSection $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "$errSection $e")
                }
            }
        }) {
            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                if (tag != 7) {
                    map["category"] = "$tag"
                } else {
                    map["category"] = "$tag"
                    map["type"] = "VIDEOS"
                }
                return map
            }
        }
        VolleySingleton.getInstance(appContext).addToRequestQueue(sr)
    }

    fun deleteCache(context: Context) {
        try {
            val dir = context.cacheDir
            deleteDir(dir)
        } catch (e: Exception) {
        }

    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
            return dir.delete()
        } else return if (dir != null && dir.isFile)
            dir.delete()
        else {
            false
        }
    }
}
