package innovacia.co.id.trunkey1

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_trunkey_care_info.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject

class TrunkeyCareInfo : AppCompatActivity() {

    val mVolleyHelper = VolleyHelper()
    var id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trunkey_care_info)
        header_name.text = "Trunkey Care Info"

        generatedView()

        swipe_container.setOnRefreshListener {
            getProfile()
        }

        back_link.setOnClickListener {
            onBackPressed()
        }

        // View profile
        btn_delete.setOnClickListener {
            btn_delete.startAnimation(user_info.animasiButton)
            deleteConfirm()
        }

        // Accept Request
        request_accept.setOnClickListener {
            request_accept.startAnimation(user_info.animasiButton)
            addCareTaker()
        }
        request_cancel.setOnClickListener {
            request_cancel.startAnimation(user_info.animasiButton)
            cancelConfirm()
        }
    }

    private fun generatedView() {
        when {
            // From notification - request
            intent.hasExtra("data") -> {
                val data = intent.getStringExtra("data")
                try {
                    btn_delete.visibility = View.GONE
                    request_accept.visibility = View.VISIBLE
                    request_cancel.visibility = View.VISIBLE

                    val dataObj = JSONObject(data)
                    val pivotObj = dataObj.getJSONObject("pivot")
                    id = pivotObj.getString("id_eca_vendors")
                } catch (e:JSONException) { }
            }
            // View profile / delete
            intent.hasExtra("id") -> {
                btn_delete.visibility = View.VISIBLE
                request_accept.visibility = View.GONE
                request_cancel.visibility = View.GONE

                id = intent.getStringExtra("id")
                getProfile()
            }
            // Request
            intent.hasExtra("request_id") -> {
                btn_delete.visibility = View.GONE
                request_accept.visibility = View.VISIBLE
                request_cancel.visibility = View.VISIBLE

                id = intent.getStringExtra("request_id")
                getProfile()
            }
        }
    }

    private fun getProfile() {
        val errSection = "getDriverInfo"
        swipe_container.isRefreshing = true
        val request = object : JsonObjectRequest(Request.Method.GET,user_info.care_info+id,null, Response.Listener { response ->
            try {
                swipe_container.isRefreshing = false
                Log.d("aim","age data : $response")
                val data = response.getJSONObject("data")
                name_value.text = data.getString("nama")
                ktp_value.text = data.getString("identification_number")
                type_value.text = data.getString("type")

                val contactObj = data.getJSONObject("contact")
                telepon_value.text = contactObj.getString("telp")

                val ktpPhoto = data.getString("identification_image_path")
                val driverPhoto = data.getString("thumbnail_logo")
                Glide.with(this).load(driverPhoto).into(photo)
                Glide.with(this).load(ktpPhoto).into(photo_ktp)

            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }


    private fun deleteConfirm() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        val warning = resources.getString(R.string.care_delete)
        val warningFix = warning.replace("-name-",name_value.text.toString())

        textNotif.text = warningFix
        ok.text = "Yes"
        cancel.text = "No"

        ok.setOnClickListener {
            deleteCareTaker()
        }

        cancel.setOnClickListener {
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun deleteCareTaker() {
        val errSection = "deleteCareTaker"
        swipe_container.isRefreshing = true
        val request = object : JsonObjectRequest(Request.Method.POST,user_info.care_delete+id,null, Response.Listener { response ->

            swipe_container.isRefreshing = false
            finish()

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }


    private fun cancelConfirm() {
        val dialogs = Dialog(this)
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs.setCancelable(true)
        dialogs.setContentView(R.layout.popup_empty)

        val textNotif = dialogs.findViewById<TextView>(R.id.textNotif)
        val cancel = dialogs.findViewById<TextView>(R.id.btnCancel)
        val ok = dialogs.findViewById<TextView>(R.id.btnOk)

        val warning = resources.getString(R.string.care_cancel)
        val warningFix = warning.replace("-name-",name_value.text.toString())

        textNotif.text = warningFix
        ok.text = "Yes"
        cancel.text = "No"

        ok.setOnClickListener {
            cancelRequest()
        }

        cancel.setOnClickListener {
            dialogs.dismiss()
        }

        dialogs.show()
    }

    private fun cancelRequest() {
        val errSection = "addCareTaker"
        swipe_container.isRefreshing = true
        val request = object : JsonObjectRequest(Request.Method.POST,user_info.care_cancel_req+id,null, Response.Listener { response ->

            swipe_container.isRefreshing = false
            finish()

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }


    private fun addCareTaker() {
        val errSection = "addCareTaker"
        swipe_container.isRefreshing = true
        val request = object : JsonObjectRequest(Request.Method.POST,user_info.care_add+id,null, Response.Listener { response ->

            swipe_container.isRefreshing = false
            finish()

        }, Response.ErrorListener { response ->
            swipe_container.isRefreshing = false
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }


}
