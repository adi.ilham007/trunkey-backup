package innovacia.co.id.trunkey1.model

data class CalendarModel(
        var id                  : String,
        var date                : String,
        var startTime           : String,
        var endTime             : String,
        var subject             : String,
        var location            : String,
        var userId              : String,
        var childId             : String,
        var keterangan          : String
)