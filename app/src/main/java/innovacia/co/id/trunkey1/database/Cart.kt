package innovacia.co.id.trunkey1.database

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.Select_Purchase
import innovacia.co.id.trunkey1.database.adapter.CartAdapter
import innovacia.co.id.trunkey1.database.cart.CartEntity
import innovacia.co.id.trunkey1.database.cart.CartRespository
import innovacia.co.id.trunkey1.database.model.CartModelDB
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_cart.*

class Cart : AppCompatActivity() {

    var idArray =  java.util.ArrayList<Int>()
    var seatArray = java.util.ArrayList<Int>()
    var hargaArray = java.util.ArrayList<Double>()
    var cashBackArray = java.util.ArrayList<Double>()
    var memberArray = java.util.ArrayList<String>()
    var typeArray = java.util.ArrayList<String>()

    lateinit var mCartModelDB: CartModelDB
    private lateinit var cartRespository: CartRespository
    var data : List<CartEntity> = ArrayList()
    private val helper = user_info()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        mCartModelDB = ViewModelProviders.of(this).get(CartModelDB(application)::class.java)
        cartRespository = CartRespository(application)

        generate_view("rv")

        back_link.setOnClickListener {
            back_link.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        checkBox1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                reset()
                (recycler_cart.adapter as CartAdapter).selectAll()
            } else {
                reset()
                (recycler_cart.adapter as CartAdapter).unSelectAll()
            }
        }

        pay.setOnClickListener {
            pay.startAnimation(user_info.animasiButton)
            if (idArray.size != 0) {
                val i = Intent(this, Select_Purchase::class.java)
                i.putExtra("bayar", "bayar")
                i.putExtra("whatBuy",typeArray)
                i.putExtra("idArray", idArray)
                i.putExtra("seatArray", seatArray)
                i.putExtra("hargaArray", hargaArray)
                i.putExtra("memberArray",memberArray)
                i.putExtra("totalHarga",hargaArray.sum())
                i.putExtra("totalCashBack",cashBackArray.sum())
                startActivity(i)
            } else {
                Toast.makeText(this,"Choose at least 1 item in shoping cart",Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        (recycler_cart.adapter as CartAdapter).reset()
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadCastReceiver, IntentFilter("cart"))
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                "cart" -> {
                    val method = intent.getStringExtra("method")

                    if (method == "+" || method == "-") {
                        val whatBuy = intent.getStringExtra("whatBuy")
                        val idDB = intent.getIntExtra("idDB", 0)
                        val id = intent.getIntExtra("id", 0)
                        val seat = intent.getIntExtra("seat", 0)
                        val cashBack = intent.getDoubleExtra("cashBack", 0.0)
                        val hargaItem = intent.getDoubleExtra("harga", 0.0)
                        val member = intent.getStringExtra("member")
                        var isSelected = false

                        Log.d("aim", "cash back : $cashBack")

                        if (method == "+") {
                            if (!idArray.contains(id)) {
                                idArray.add(id)
                                seatArray.add(seat)
                                hargaArray.add(hargaItem)
                                cashBackArray.add(cashBack)
                                memberArray.add(id.toString())
                                memberArray.add(member)
                                typeArray.add(whatBuy)
                            }
                        } else {
                            idArray.remove(id)
                            seatArray.remove(seat)
                            hargaArray.remove(hargaItem)
                            cashBackArray.remove(cashBack)
                            memberArray.remove(id.toString())
                            memberArray.remove(member)
                            typeArray.remove(whatBuy)
                        }
                    }
                    else if (method == "enable" || method == "disable") {
                        if (method == "enable") {
                            generate_view(method)
                        } else {
                            generate_view(method)
                        }
                    }

                    val totalCart =  (recycler_cart.adapter as CartAdapter).itemCount

                    if (idArray.size == totalCart) {
                        checkBox1.isChecked = true
                    } else if (idArray.size == 0) {
                        checkBox1.isChecked = false
                    }
                    harga.text = helper.bank(hargaArray.sum())
                    cashback_value.text = helper.bank(cashBackArray.sum())
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(broadCastReceiver)
    }

    private fun generate_view(typeView:String){
        when (typeView) {
            "rv" -> {
                recycler_cart.setHasFixedSize(true)
                recycler_cart.layoutManager = LinearLayoutManager(this)
                val adapter = CartAdapter(this, data, CartRespository(application))
                recycler_cart.adapter = adapter

                mCartModelDB.getAll().observe(this, Observer { datas ->
                    if (datas != null) {
                        adapter.setDatas(datas.asReversed())
                        (recycler_cart.adapter as CartAdapter).notifyDataSetChanged()

                    }
                })
            }
            "enable" -> mCartModelDB.getAll().observe(this, Observer { datas ->
                if (datas != null) {
                    Log.d("aim", "interupsi enable")
                    reset()
                    for (i in 0 until datas.size) {
                        val mCartEntity = datas[i]
                        typeArray.add(mCartEntity.itemType)
                        idArray.add(mCartEntity.itemId)
                        seatArray.add(mCartEntity.itemQty)
                        hargaArray.add(mCartEntity.itemPrice)
                        memberArray.add(mCartEntity.itemId.toString())
                        memberArray.add(mCartEntity.itemMember)
                        cashBackArray.add(mCartEntity.itemCashBack)
                    }
                    Log.d("aim","reset : $hargaArray, $cashBackArray,$memberArray")
                }
            })
            "disable" -> {
                Log.d("aim", "interupsi disable")
                reset()
            }
        }
    }

    private fun reset() {
        idArray.clear()
        seatArray.clear()
        hargaArray.clear()
        cashBackArray.clear()
        memberArray.clear()
        memberArray.clear()
        typeArray.clear()
    }
}
