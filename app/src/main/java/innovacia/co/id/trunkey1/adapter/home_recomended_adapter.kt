package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.recomended_model
import kotlinx.android.synthetic.main.list_home2.view.*

class home_recomended_adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    private var recomendedList = mutableListOf<recomended_model>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return vendorListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_home2, parent, false))
    }
    override fun getItemCount(): Int = recomendedList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as vendorListViewHolder

        viewHolder.bindView(recomendedList[position])

        viewHolder.itemView.setOnClickListener { view ->
            viewHolder.itemView.startAnimation(user_info.animasiButton)

            val slug = recomendedList[position].slug
            val Type = recomendedList[position].type
            val id = recomendedList[position].id_vendor

            if (Type == "event") {
                val i = Intent(view.context, Event::class.java)
                i.putExtra("slug", slug)
                view.context.startActivity(i)
            } else {
                val i = Intent(view.context, detail_vendor::class.java)
                i.putExtra("id", id)
                view.context.startActivity(i)
            }
        }
    }

    fun setList(listOfVendor: List<recomended_model>) {
        this.recomendedList = listOfVendor.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(listOfVendor: List<recomended_model>) {
        this.recomendedList.addAll(listOfVendor)
        notifyDataSetChanged()
    }

    class vendorListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(recomendedModel: recomended_model) {
            itemView.id_vendor.text    = recomendedModel.id_vendor
            itemView.nama.text       = recomendedModel.nama_vendor
            if(recomendedModel.alamat_vendor != ""){
                itemView.alamat.text    = recomendedModel.alamat_vendor
            } else {
                itemView.alamat.visibility = View.INVISIBLE
            }

            Glide.with(itemView.context)
                    .load(recomendedModel.img)
                    .into(itemView.file_logo)
        }
    }

}