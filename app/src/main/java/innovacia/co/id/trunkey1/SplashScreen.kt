package innovacia.co.id.trunkey1

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import innovacia.co.id.trunkey1.adapter.filter.AgeAdapter
import innovacia.co.id.trunkey1.helper.VolleyHelper
import innovacia.co.id.trunkey1.helper.VolleySingleton
import innovacia.co.id.trunkey1.helper.user_info
import innovacia.co.id.trunkey1.model.filter.AgeModel
import kotlinx.android.synthetic.main.activity_splash_screen.*
import org.json.JSONException
import org.json.JSONObject



class SplashScreen : AppCompatActivity() {

    private val mVolleyHelper = VolleyHelper()

    private val ageData = mutableListOf<AgeModel>()

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val myFadeInAnimation = AnimationUtils.loadAnimation(this, R.layout.anim_splash_screen)
        tagline.startAnimation(myFadeInAnimation)
        logo.startAnimation(myFadeInAnimation)

        gerFilterAge()

        val reader = this.getSharedPreferences("my_preferences", Context.MODE_PRIVATE)
        val token = reader.getString("access_token", "")
        val refresh = reader.getString("refresh_token","")

        if (!token.isNullOrEmpty() && !refresh.isNullOrEmpty()) {
            user_info.loginStatus = true
            user_info.token = token
            user_info.refresh = refresh

//            refreshToken(refresh)
        }

        // Normal Flow
        Handler().postDelayed({
            startActivity(Intent(this,HomeBaru::class.java))
            finish()
        }, 3000)
    }

    private fun refreshToken(refresh:String){
        val errSection = "refreshToken"

        val body = JSONObject()
        body.put("refresh_token",refresh)

        val url = user_info.refresh_token
        val rq = object : JsonObjectRequest(Request.Method.POST, url,body, Response.Listener { response ->

            try {
                Log.d("aim","$errSection : $response")
                user_info.loginStatus = true
                user_info.token = response.getString("access_token")
                user_info.refresh = response.getString("refresh_token")

                val reader2 = this.getSharedPreferences("my_preferences", Context.MODE_PRIVATE)
                val editor = reader2.edit()
                editor.putString("access_token",response.getString("access_token"))
                editor.putString("refresh_token",response.getString("refresh_token"))
                editor.apply()

            } catch (e: Exception) {
                Log.d("aim","refresh token fail")
            }

        },Response.ErrorListener { response ->
            mVolleyHelper.errHandling(this,response,errSection)
        }) { }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(rq)
    }

    // ============================================================================================= Get filter variable
    private fun gerFilterAge() {
        val errSection = "getCategory"
        val url = "https://trunkey.id/v2/main/getcategories" // user_info_getCategory
        val request = object : JsonObjectRequest(Request.Method.POST,url,null,Response.Listener { response ->

            try {
                Log.d("aim","$errSection : $response")
                val data = response.getJSONObject("data")

                val grade = data.getJSONArray("grade")
                var mItem = AgeModel("all","All","",true)
                ageData.add(mItem)
                for (i in 0 until grade.length()) {
                    val gradeObj = grade.getJSONObject(i)
                    val gradeId = gradeObj.getString("id")
                    val gradeName = gradeObj.getString("nama")
                    val gradeDesc = gradeObj.getString("keterangan")

                    mItem = AgeModel(gradeId,gradeName,gradeDesc,false)
                    ageData.add(mItem)
                }
                val mAgeAdapter = AgeAdapter()
//                recycler_age.layoutManager = LinearLayoutManager(this, OrientationHelper.HORIZONTAL, false)
//                recycler_age.adapter = mAgeAdapter
                mAgeAdapter.setList(ageData)

            } catch (e: Exception) {
                Log.e("aim", "Err tag : $e")
            }

        },Response.ErrorListener { response ->
            mVolleyHelper.errHandling(this,response,errSection)
        }) {
            override fun getHeaders(): MutableMap<String, String> { return mVolleyHelper.header() }
        }
        VolleySingleton.getInstance(this.applicationContext).addToRequestQueue(request)
    }
}



