package innovacia.co.id.trunkey1.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import innovacia.co.id.trunkey1.R
import innovacia.co.id.trunkey1.model.ChildTransHistory_model
import kotlinx.android.synthetic.main.list_child_trans_history.view.*

class ChildTransHistoriAdapter  : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var transHistory = mutableListOf<ChildTransHistory_model>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return transHistoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_child_trans_history, parent, false))
    }
    override fun getItemCount(): Int = transHistory.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as transHistoryViewHolder
        viewHolder.bindView(transHistory[position])

//        viewHolder.itemView.left_btn.setOnClickListener { view ->
//            viewHolder.itemView.left_btn.startAnimation(user_info.animasiButton)
//            val goto = position-1
//            if (goto >= 0) {
//                val intent = Intent("recycler")
//                intent.putExtra("position", goto)
//                LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
//            }
//        }

//        viewHolder.itemView.right_btn.setOnClickListener { view ->
//            viewHolder.itemView.right_btn.startAnimation(user_info.animasiButton)
//            val goto = position+1
//            if (goto <= transHistory.size) {
//                val intent = Intent("recycler")
//                intent.putExtra("position", position + 1)
//                LocalBroadcastManager.getInstance(view.context).sendBroadcast(intent)
//            }
//        }

    }
    fun setList(listOfReview: List<ChildTransHistory_model>) {
        this.transHistory = listOfReview.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfReview: List<ChildTransHistory_model>) {
        this.transHistory.addAll(listOfReview)
        notifyDataSetChanged()
    }

    class transHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(mChildTransHistory_model: ChildTransHistory_model) {
            itemView.tx_nama.text = mChildTransHistory_model.merchant
            itemView.tx_tanggal.text = mChildTransHistory_model.orderDate
            itemView.tx_value.text = mChildTransHistory_model.total

            itemView.logo_huruf?.text = mChildTransHistory_model.merchant[0].toString().capitalize()
        }
    }
}
