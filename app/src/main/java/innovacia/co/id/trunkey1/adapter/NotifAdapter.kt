package innovacia.co.id.trunkey1.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import innovacia.co.id.trunkey1.model.NotifModel
import kotlinx.android.synthetic.main.list_gallery.view.*
import android.util.Log
import innovacia.co.id.trunkey1.*
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.list_notif.view.*


class NotifAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mNotifModel = mutableListOf<NotifModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NotifViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_notif, parent, false))
    }
    override fun getItemCount(): Int = mNotifModel.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = viewHolder as NotifViewHolder

        viewHolder.bindView(mNotifModel[position])
        var buttonState = 0

        viewHolder.itemView.button.setOnClickListener { view ->
            viewHolder.itemView.button.startAnimation(user_info.animasiButton)
            buttonState++
            if (buttonState %2 == 0){
                viewHolder.itemView.desc.visibility = View.GONE
                viewHolder.itemView.button.text = "\uf078"
            } else {
                viewHolder.itemView.desc.visibility = View.VISIBLE
                viewHolder.itemView.button.text = "\uf077"
            }
        }
    }

    fun setList(listOfGallery: List<NotifModel>) {
        this.mNotifModel = listOfGallery.toMutableList()
        notifyDataSetChanged()
    }
    fun addData(listOfGallery: List<NotifModel>) {
        this.mNotifModel.addAll(listOfGallery)
        notifyDataSetChanged()
    }
    fun reset() {
        mNotifModel.removeAll(mNotifModel)
        notifyDataSetChanged()
    }

    class NotifViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(eventModel: NotifModel) {
            itemView.title.text = eventModel.title
            itemView.desc.text = eventModel.desc
        }
    }
}