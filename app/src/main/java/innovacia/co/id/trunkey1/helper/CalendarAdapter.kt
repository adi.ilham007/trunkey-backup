package innovacia.co.id.trunkey1.helper

import android.annotation.SuppressLint
import android.widget.TextView
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.graphics.Typeface
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import innovacia.co.id.trunkey1.R
import java.text.SimpleDateFormat
import java.util.*




class CalendarAdapter(context: Context,
                      private val monthlyDates: List<Date>,
                      private val currentDate: Calendar,
                      private val allEvents: ArrayList<String>
) : ArrayAdapter<Date>(context, R.layout.calendar_layout_cell) {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    @SuppressLint("SimpleDateFormat")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var mView = convertView
        // Statis calendar by sistem to populate layout cell
        val systemCalendar = Calendar.getInstance()
        val date = getItem(position)
        systemCalendar.time = date
        val systemDate = systemCalendar.get(Calendar.DATE)
        val systemMonth = systemCalendar.get(Calendar.MONTH)
        val systemYear = systemCalendar.get(Calendar.YEAR)

        // Dinamic calendar by user
        val userCalendar = currentDate
        val userDate = userCalendar.get(Calendar.DATE)
        val userMonth = userCalendar.get(Calendar.MONTH)
        val userYear = userCalendar.get(Calendar.YEAR)

        // today
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val now = sdf.format(Date()).split("-")
        val nowDate = now[0].toInt()
        val nowMonth = now[1].toInt()
        val nowYear = now[2].toInt()

        if (mView == null) {
            mView = mInflater.inflate(R.layout.calendar_layout_cell, parent, false)

        }

        // Inflate cell layout
        (mView as LinearLayout).setBackgroundResource(R.drawable.calendar_border)

        // Reset cell view
        val txTanggal = mView.findViewById<TextView>(R.id.calendar_date_id)
        val eventIndicator = mView.findViewById<TextView>(innovacia.co.id.trunkey1.R.id.event_id)
        eventIndicator.visibility = View.GONE

        if (systemMonth+1 == nowMonth) {
            // if this day is outside current month, grey it out
            txTanggal.setTextColor(Color.parseColor("#707070"))
        }

        if (systemDate == nowDate && systemMonth+1 == nowMonth && systemYear == nowYear) {
            // if it is today, set it to blue/bold
            mView.setBackgroundResource(R.drawable.calendar_today_border)
            txTanggal.setTextColor(Color.parseColor("#FFFFFF"))
        }

        // set text
        txTanggal.text = systemCalendar.get(Calendar.DATE).toString()

        //Add events to the calendar
//        Log.d("aim","all events calendar adapter: $allEvents")
        for (i in 0 until allEvents.size) {
            val event = allEvents[i].split("-")
            val eventDate = event[0].toInt()
            val eventMonth = event[1].toInt()
            val eventYear = event[2].toInt()

//            Log.d("aim","draw event, date: $eventDate, month: $eventMonth, year: $eventYear")
//            Log.d("aim","draw event, date: $systemDate, month: $systemMonth, year: $systemYear")
//            Log.d("aim","-----------------------------------------------------------------")

            if (systemDate == eventDate && systemMonth == eventMonth-1 && systemYear == eventYear) {
                eventIndicator.visibility = View.VISIBLE
//                Log.d("aim","draw event, date: $eventDate, month: $eventMonth, year: $eventYear")
            }
        }

        return mView
    }

    override fun getCount(): Int {
        return monthlyDates.size
    }

    override fun getItem(position: Int): Date? {
        return monthlyDates[position]
    }

//    override fun getPosition(item: Any?): Int {
//        return monthlyDates.indexOf(item)
//    }
}