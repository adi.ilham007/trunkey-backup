package innovacia.co.id.trunkey1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import innovacia.co.id.trunkey1.fragment.TrunkeyCareIntroFragmnet
import kotlinx.android.synthetic.main.activity_trunkey_care_intro.*

class TrunkeyCareIntro : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trunkey_care_intro)

        val pageAdapter = TrunkeyCareIntroPagerAdapter(supportFragmentManager)
        pageAdapter.add(TrunkeyCareIntroFragmnet.newInstance(1), "1")
        pageAdapter.add(TrunkeyCareIntroFragmnet.newInstance(2), "2")
        pageAdapter.add(TrunkeyCareIntroFragmnet.newInstance(3), "3")
        pageAdapter.add(TrunkeyCareIntroFragmnet.newInstance(4), "4")
        pageAdapter.add(TrunkeyCareIntroFragmnet.newInstance(5), "5")

        viewpager_main.adapter = pageAdapter

        btn_next.setOnClickListener {
            val page = viewpager_main.currentItem + 2
            if (page <= 5) {
                viewpager_main.setCurrentItem(page,true)
            } else {
                val i = Intent(this,HomeBaru::class.java)
                startActivity(i)
                finish()
            }
        }

        btn_skip.setOnClickListener {
            val i = Intent(this,HomeBaru::class.java)
            startActivity(i)
            finish()
        }
    }
}
