package innovacia.co.id.trunkey1

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.support.v4.content.res.ResourcesCompat
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import innovacia.co.id.trunkey1.helper.user_info
import kotlinx.android.synthetic.main.activity_edit_profile_child.*
import kotlinx.android.synthetic.main.sub_header.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class EditProfileChild : AppCompatActivity() {

    private var orange = ""
    private var blue = ""
    private var gray = ""

    private val PICK_IMAGE_REQUEST = 1111
    var imageData = ""
    var genderString = ""
    var dateString = ""
    var themeString = ""

    @SuppressLint("SimpleDateFormat")
    val sdf = SimpleDateFormat("yyyy-MM-dd-u")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile_child)
        header_name?.text = resources.getString(R.string.child_header)

        orange = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.colorPrimary, null)).replace("ff","").capitalize()
        blue = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.blue, null)).replace("ff","").capitalize()
        gray = "#"+Integer.toHexString(ResourcesCompat.getColor(resources, R.color.gray, null)).replace("ff","").capitalize()

        generateView()

        back_link?.setOnClickListener {
            back_link?.startAnimation(user_info.animasiButton)
            onBackPressed()
        }

        themeChoose()

        genderChoose()

        editPhoto?.setOnClickListener {
            editPhoto?.startAnimation(user_info.animasiButton)
            val intent = Intent()
                    .setType("image/*")
                    .setAction(Intent.ACTION_GET_CONTENT)

            startActivityForResult(Intent.createChooser(intent, "Select a Photo"), PICK_IMAGE_REQUEST)
        }


        birthday_value?.setOnClickListener {
            birthday_value?.startAnimation(user_info.animasiButton)
            SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .displayDays(false)
                    .displayDaysOfMonth(true)
                    .displayMonth(true)
                    .displayYears(true)
                    .listener(object : SingleDateAndTimePickerDialog.Listener{
                        @SuppressLint("SimpleDateFormat")
                        override fun onDateSelected(datePick: Date?) {
                            Log.d("aim",datePick.toString())
                            val uiFormat = SimpleDateFormat("dd MMMM yyyy")
                            val serverFormat = SimpleDateFormat("yyyy-MM-dd")

                            dateString = serverFormat.format(datePick)
                            birthday_value.text = uiFormat.format(datePick)

                        }
                    }).display()
        }

        // Profile Parent Layout Visibility
//        btn_save?.setOnClickListener {
//            btn_save?.startAnimation(user_info.animasiButton)
//            if (profileCheck()) {
//                name_lay.error = ""
////                birthday_lay.error = ""
//                updateProfile()
//            }
//        }

        btn_save?.setOnClickListener {
            btn_save?.startAnimation(user_info.animasiButton)

            updateProfile()

        }

    }

    private fun genderChoose() {
        boy?.setOnClickListener {
            boy?.setBackgroundResource(R.drawable.textview_border_yes)
            girl?.setBackgroundResource(R.drawable.textview_border_no)
            genderString = "m"
        }

        girl?.setOnClickListener {
            boy?.setBackgroundResource(R.drawable.textview_border_no)
            girl?.setBackgroundResource(R.drawable.textview_border_yes)
            genderString = "f"
        }
    }

    private fun themeChoose() {
        blue_lay?.setOnClickListener {
            blue_lay?.setBackgroundResource(R.drawable.textview_border_yes)
            gray_lay?.setBackgroundResource(R.drawable.textview_border_no)
            orange_lay?.setBackgroundResource(R.drawable.textview_border_no)
            themeString = blue
        }

        gray_lay?.setOnClickListener {
            blue_lay?.setBackgroundResource(R.drawable.textview_border_no)
            gray_lay?.setBackgroundResource(R.drawable.textview_border_yes)
            orange_lay?.setBackgroundResource(R.drawable.textview_border_no)
            themeString = gray
        }

        orange_lay?.setOnClickListener {
            blue_lay?.setBackgroundResource(R.drawable.textview_border_no)
            gray_lay?.setBackgroundResource(R.drawable.textview_border_no)
            orange_lay?.setBackgroundResource(R.drawable.textview_border_yes)
            themeString = orange
        }
    }

    private fun generateView() {
        name_value?.setText(user_info.activeChildName)
        birthday_value?.text = user_info.activeChildBirthday

        name_value.setText(intent.getStringExtra("name"))
        birthday_value.text = intent.getStringExtra("birth")

        val gender = intent.getStringExtra("gender")
        genderString = if (gender == "male") {
            boy.setBackgroundResource(R.drawable.textview_border_yes)
            "m"
        } else {
            girl.setBackgroundResource(R.drawable.textview_border_yes)
            "f"
        }

        val theme = intent.getStringExtra("theme")
        Log.d("aim","terima : $theme == #${blue.toUpperCase()}")

        themeString = when (theme) {
            blue -> {
                blue_lay.setBackgroundResource(R.drawable.textview_border_yes)
                blue
            }
            gray -> {
                gray_lay.setBackgroundResource(R.drawable.textview_border_yes)
                gray
            }
            else -> {
                orange_lay.setBackgroundResource(R.drawable.textview_border_yes)
                orange
            }
        }


        val foto = intent.getStringExtra("photo")
        Glide.with(this).load(foto)
                .apply(RequestOptions.skipMemoryCacheOf(true)) //
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .into(photo)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            val filePath = data.data

            if (filePath != null) {
                val imageName = getFileName(filePath)
                val fileType = imageName.split(".")
                val lastIndex = fileType.lastIndex
                val extension = fileType[lastIndex].toLowerCase()
                Log.d("aim", "nama file : $imageName, extension : $extension")

                if (extension == "jpg" || extension == "jpeg") {
                    try {
                        val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                        //Bitmap lastBitmap = null;
                        //lastBitmap = bitmap;
                        //encoding image to string
                        imageData = getStringImage(bitmap)
                        photo.setImageBitmap(bitmap)

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                } else {
                    Toast.makeText(this, "Please choose picture with .jpg or .jpeg extension", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    fun getStringImage(bmp: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        return Base64.encodeToString(imageBytes, Base64.DEFAULT)
    }

    private fun updateProfile() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val rq: RequestQueue = Volley.newRequestQueue(this)
        val sr = object : StringRequest(Request.Method.POST, user_info.updateChild+user_info.activeChildId, Response.Listener { response ->
            dialog.dismiss()
            try {
                Log.d("aim","update child profile : $response")

                // Reset Child Data
                user_info.restoreKids = null

                finish()

            }catch (e: JSONException){
                Log.d("aim","err json : $e")
            }
            onBackPressed()

        }, Response.ErrorListener { response ->
            dialog.dismiss()
            val networkResponse = response.networkResponse
            if (networkResponse == null){
                Toast.makeText(this,R.string.connFail, Toast.LENGTH_LONG).show()
            }
            else {
                try {
                    val err = String(networkResponse.data)
                    val jsonObj = JSONObject(err)
                    val errCode   = jsonObj.getString("code")
                    val errMessage   = jsonObj.getString("message")

                    Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show()
                    Log.d("aim", "Err code : $errCode, $err")
                } catch (e: JSONException) {
                    Log.e("aim", "Err : $e")
                }
            }
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String,String>()
                headers["Accept"] = "application/json"
//                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Bearer ${user_info.token}"
                return headers
            }

            override fun getParams(): MutableMap<String,String> {
                val map = HashMap<String, String>()
                map["name"] = name_value.text.toString()
                map["birthday"] = dateString
                map["gender"] = genderString
                map["color_theme"] = themeString
                if (imageData != "") {
                    val image64 = "data:image/png;base64,$imageData"
                    map["photo"] = image64
                }

                Log.d("aim",map.toString())
                return map
            }
        }
        rq.add(sr)
    }
}
